<?php namespace Tests\Repositories;

use App\Models\MessageContact;
use App\Repositories\MessageContactRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MessageContactRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MessageContactRepository
     */
    protected $messageContactRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->messageContactRepo = \App::make(MessageContactRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_message_contact()
    {
        $messageContact = MessageContact::factory()->make()->toArray();

        $createdMessageContact = $this->messageContactRepo->create($messageContact);

        $createdMessageContact = $createdMessageContact->toArray();
        $this->assertArrayHasKey('id', $createdMessageContact);
        $this->assertNotNull($createdMessageContact['id'], 'Created MessageContact must have id specified');
        $this->assertNotNull(MessageContact::find($createdMessageContact['id']), 'MessageContact with given id must be in DB');
        $this->assertModelData($messageContact, $createdMessageContact);
    }

    /**
     * @test read
     */
    public function test_read_message_contact()
    {
        $messageContact = MessageContact::factory()->create();

        $dbMessageContact = $this->messageContactRepo->find($messageContact->id);

        $dbMessageContact = $dbMessageContact->toArray();
        $this->assertModelData($messageContact->toArray(), $dbMessageContact);
    }

    /**
     * @test update
     */
    public function test_update_message_contact()
    {
        $messageContact = MessageContact::factory()->create();
        $fakeMessageContact = MessageContact::factory()->make()->toArray();

        $updatedMessageContact = $this->messageContactRepo->update($fakeMessageContact, $messageContact->id);

        $this->assertModelData($fakeMessageContact, $updatedMessageContact->toArray());
        $dbMessageContact = $this->messageContactRepo->find($messageContact->id);
        $this->assertModelData($fakeMessageContact, $dbMessageContact->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_message_contact()
    {
        $messageContact = MessageContact::factory()->create();

        $resp = $this->messageContactRepo->delete($messageContact->id);

        $this->assertTrue($resp);
        $this->assertNull(MessageContact::find($messageContact->id), 'MessageContact should not exist in DB');
    }
}
