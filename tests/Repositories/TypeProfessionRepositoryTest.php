<?php namespace Tests\Repositories;

use App\Models\TypeProfession;
use App\Repositories\TypeProfessionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeProfessionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeProfessionRepository
     */
    protected $typeProfessionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeProfessionRepo = \App::make(TypeProfessionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_profession()
    {
        $typeProfession = TypeProfession::factory()->make()->toArray();

        $createdTypeProfession = $this->typeProfessionRepo->create($typeProfession);

        $createdTypeProfession = $createdTypeProfession->toArray();
        $this->assertArrayHasKey('id', $createdTypeProfession);
        $this->assertNotNull($createdTypeProfession['id'], 'Created TypeProfession must have id specified');
        $this->assertNotNull(TypeProfession::find($createdTypeProfession['id']), 'TypeProfession with given id must be in DB');
        $this->assertModelData($typeProfession, $createdTypeProfession);
    }

    /**
     * @test read
     */
    public function test_read_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();

        $dbTypeProfession = $this->typeProfessionRepo->find($typeProfession->id);

        $dbTypeProfession = $dbTypeProfession->toArray();
        $this->assertModelData($typeProfession->toArray(), $dbTypeProfession);
    }

    /**
     * @test update
     */
    public function test_update_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();
        $fakeTypeProfession = TypeProfession::factory()->make()->toArray();

        $updatedTypeProfession = $this->typeProfessionRepo->update($fakeTypeProfession, $typeProfession->id);

        $this->assertModelData($fakeTypeProfession, $updatedTypeProfession->toArray());
        $dbTypeProfession = $this->typeProfessionRepo->find($typeProfession->id);
        $this->assertModelData($fakeTypeProfession, $dbTypeProfession->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();

        $resp = $this->typeProfessionRepo->delete($typeProfession->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeProfession::find($typeProfession->id), 'TypeProfession should not exist in DB');
    }
}
