<?php namespace Tests\Repositories;

use App\Models\EvaluationMensuel;
use App\Repositories\EvaluationMensuelRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EvaluationMensuelRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EvaluationMensuelRepository
     */
    protected $evaluationMensuelRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->evaluationMensuelRepo = \App::make(EvaluationMensuelRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->make()->toArray();

        $createdEvaluationMensuel = $this->evaluationMensuelRepo->create($evaluationMensuel);

        $createdEvaluationMensuel = $createdEvaluationMensuel->toArray();
        $this->assertArrayHasKey('id', $createdEvaluationMensuel);
        $this->assertNotNull($createdEvaluationMensuel['id'], 'Created EvaluationMensuel must have id specified');
        $this->assertNotNull(EvaluationMensuel::find($createdEvaluationMensuel['id']), 'EvaluationMensuel with given id must be in DB');
        $this->assertModelData($evaluationMensuel, $createdEvaluationMensuel);
    }

    /**
     * @test read
     */
    public function test_read_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();

        $dbEvaluationMensuel = $this->evaluationMensuelRepo->find($evaluationMensuel->id);

        $dbEvaluationMensuel = $dbEvaluationMensuel->toArray();
        $this->assertModelData($evaluationMensuel->toArray(), $dbEvaluationMensuel);
    }

    /**
     * @test update
     */
    public function test_update_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();
        $fakeEvaluationMensuel = EvaluationMensuel::factory()->make()->toArray();

        $updatedEvaluationMensuel = $this->evaluationMensuelRepo->update($fakeEvaluationMensuel, $evaluationMensuel->id);

        $this->assertModelData($fakeEvaluationMensuel, $updatedEvaluationMensuel->toArray());
        $dbEvaluationMensuel = $this->evaluationMensuelRepo->find($evaluationMensuel->id);
        $this->assertModelData($fakeEvaluationMensuel, $dbEvaluationMensuel->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();

        $resp = $this->evaluationMensuelRepo->delete($evaluationMensuel->id);

        $this->assertTrue($resp);
        $this->assertNull(EvaluationMensuel::find($evaluationMensuel->id), 'EvaluationMensuel should not exist in DB');
    }
}
