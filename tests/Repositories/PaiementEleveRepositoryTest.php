<?php namespace Tests\Repositories;

use App\Models\PaiementEleve;
use App\Repositories\PaiementEleveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PaiementEleveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaiementEleveRepository
     */
    protected $paiementEleveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paiementEleveRepo = \App::make(PaiementEleveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->make()->toArray();

        $createdPaiementEleve = $this->paiementEleveRepo->create($paiementEleve);

        $createdPaiementEleve = $createdPaiementEleve->toArray();
        $this->assertArrayHasKey('id', $createdPaiementEleve);
        $this->assertNotNull($createdPaiementEleve['id'], 'Created PaiementEleve must have id specified');
        $this->assertNotNull(PaiementEleve::find($createdPaiementEleve['id']), 'PaiementEleve with given id must be in DB');
        $this->assertModelData($paiementEleve, $createdPaiementEleve);
    }

    /**
     * @test read
     */
    public function test_read_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();

        $dbPaiementEleve = $this->paiementEleveRepo->find($paiementEleve->id);

        $dbPaiementEleve = $dbPaiementEleve->toArray();
        $this->assertModelData($paiementEleve->toArray(), $dbPaiementEleve);
    }

    /**
     * @test update
     */
    public function test_update_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();
        $fakePaiementEleve = PaiementEleve::factory()->make()->toArray();

        $updatedPaiementEleve = $this->paiementEleveRepo->update($fakePaiementEleve, $paiementEleve->id);

        $this->assertModelData($fakePaiementEleve, $updatedPaiementEleve->toArray());
        $dbPaiementEleve = $this->paiementEleveRepo->find($paiementEleve->id);
        $this->assertModelData($fakePaiementEleve, $dbPaiementEleve->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();

        $resp = $this->paiementEleveRepo->delete($paiementEleve->id);

        $this->assertTrue($resp);
        $this->assertNull(PaiementEleve::find($paiementEleve->id), 'PaiementEleve should not exist in DB');
    }
}
