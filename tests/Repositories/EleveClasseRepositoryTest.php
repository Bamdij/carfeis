<?php namespace Tests\Repositories;

use App\Models\EleveClasse;
use App\Repositories\EleveClasseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EleveClasseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EleveClasseRepository
     */
    protected $eleveClasseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->eleveClasseRepo = \App::make(EleveClasseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->make()->toArray();

        $createdEleveClasse = $this->eleveClasseRepo->create($eleveClasse);

        $createdEleveClasse = $createdEleveClasse->toArray();
        $this->assertArrayHasKey('id', $createdEleveClasse);
        $this->assertNotNull($createdEleveClasse['id'], 'Created EleveClasse must have id specified');
        $this->assertNotNull(EleveClasse::find($createdEleveClasse['id']), 'EleveClasse with given id must be in DB');
        $this->assertModelData($eleveClasse, $createdEleveClasse);
    }

    /**
     * @test read
     */
    public function test_read_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();

        $dbEleveClasse = $this->eleveClasseRepo->find($eleveClasse->id);

        $dbEleveClasse = $dbEleveClasse->toArray();
        $this->assertModelData($eleveClasse->toArray(), $dbEleveClasse);
    }

    /**
     * @test update
     */
    public function test_update_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();
        $fakeEleveClasse = EleveClasse::factory()->make()->toArray();

        $updatedEleveClasse = $this->eleveClasseRepo->update($fakeEleveClasse, $eleveClasse->id);

        $this->assertModelData($fakeEleveClasse, $updatedEleveClasse->toArray());
        $dbEleveClasse = $this->eleveClasseRepo->find($eleveClasse->id);
        $this->assertModelData($fakeEleveClasse, $dbEleveClasse->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();

        $resp = $this->eleveClasseRepo->delete($eleveClasse->id);

        $this->assertTrue($resp);
        $this->assertNull(EleveClasse::find($eleveClasse->id), 'EleveClasse should not exist in DB');
    }
}
