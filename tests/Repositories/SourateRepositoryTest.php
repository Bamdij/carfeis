<?php namespace Tests\Repositories;

use App\Models\Sourate;
use App\Repositories\SourateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SourateRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SourateRepository
     */
    protected $sourateRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sourateRepo = \App::make(SourateRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sourate()
    {
        $sourate = Sourate::factory()->make()->toArray();

        $createdSourate = $this->sourateRepo->create($sourate);

        $createdSourate = $createdSourate->toArray();
        $this->assertArrayHasKey('id', $createdSourate);
        $this->assertNotNull($createdSourate['id'], 'Created Sourate must have id specified');
        $this->assertNotNull(Sourate::find($createdSourate['id']), 'Sourate with given id must be in DB');
        $this->assertModelData($sourate, $createdSourate);
    }

    /**
     * @test read
     */
    public function test_read_sourate()
    {
        $sourate = Sourate::factory()->create();

        $dbSourate = $this->sourateRepo->find($sourate->id);

        $dbSourate = $dbSourate->toArray();
        $this->assertModelData($sourate->toArray(), $dbSourate);
    }

    /**
     * @test update
     */
    public function test_update_sourate()
    {
        $sourate = Sourate::factory()->create();
        $fakeSourate = Sourate::factory()->make()->toArray();

        $updatedSourate = $this->sourateRepo->update($fakeSourate, $sourate->id);

        $this->assertModelData($fakeSourate, $updatedSourate->toArray());
        $dbSourate = $this->sourateRepo->find($sourate->id);
        $this->assertModelData($fakeSourate, $dbSourate->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sourate()
    {
        $sourate = Sourate::factory()->create();

        $resp = $this->sourateRepo->delete($sourate->id);

        $this->assertTrue($resp);
        $this->assertNull(Sourate::find($sourate->id), 'Sourate should not exist in DB');
    }
}
