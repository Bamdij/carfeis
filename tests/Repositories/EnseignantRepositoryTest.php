<?php namespace Tests\Repositories;

use App\Models\Enseignant;
use App\Repositories\EnseignantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EnseignantRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EnseignantRepository
     */
    protected $enseignantRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->enseignantRepo = \App::make(EnseignantRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_enseignant()
    {
        $enseignant = Enseignant::factory()->make()->toArray();

        $createdEnseignant = $this->enseignantRepo->create($enseignant);

        $createdEnseignant = $createdEnseignant->toArray();
        $this->assertArrayHasKey('id', $createdEnseignant);
        $this->assertNotNull($createdEnseignant['id'], 'Created Enseignant must have id specified');
        $this->assertNotNull(Enseignant::find($createdEnseignant['id']), 'Enseignant with given id must be in DB');
        $this->assertModelData($enseignant, $createdEnseignant);
    }

    /**
     * @test read
     */
    public function test_read_enseignant()
    {
        $enseignant = Enseignant::factory()->create();

        $dbEnseignant = $this->enseignantRepo->find($enseignant->id);

        $dbEnseignant = $dbEnseignant->toArray();
        $this->assertModelData($enseignant->toArray(), $dbEnseignant);
    }

    /**
     * @test update
     */
    public function test_update_enseignant()
    {
        $enseignant = Enseignant::factory()->create();
        $fakeEnseignant = Enseignant::factory()->make()->toArray();

        $updatedEnseignant = $this->enseignantRepo->update($fakeEnseignant, $enseignant->id);

        $this->assertModelData($fakeEnseignant, $updatedEnseignant->toArray());
        $dbEnseignant = $this->enseignantRepo->find($enseignant->id);
        $this->assertModelData($fakeEnseignant, $dbEnseignant->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_enseignant()
    {
        $enseignant = Enseignant::factory()->create();

        $resp = $this->enseignantRepo->delete($enseignant->id);

        $this->assertTrue($resp);
        $this->assertNull(Enseignant::find($enseignant->id), 'Enseignant should not exist in DB');
    }
}
