<?php namespace Tests\Repositories;

use App\Models\Niveau;
use App\Repositories\NiveauRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NiveauRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NiveauRepository
     */
    protected $niveauRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->niveauRepo = \App::make(NiveauRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_niveau()
    {
        $niveau = Niveau::factory()->make()->toArray();

        $createdNiveau = $this->niveauRepo->create($niveau);

        $createdNiveau = $createdNiveau->toArray();
        $this->assertArrayHasKey('id', $createdNiveau);
        $this->assertNotNull($createdNiveau['id'], 'Created Niveau must have id specified');
        $this->assertNotNull(Niveau::find($createdNiveau['id']), 'Niveau with given id must be in DB');
        $this->assertModelData($niveau, $createdNiveau);
    }

    /**
     * @test read
     */
    public function test_read_niveau()
    {
        $niveau = Niveau::factory()->create();

        $dbNiveau = $this->niveauRepo->find($niveau->id);

        $dbNiveau = $dbNiveau->toArray();
        $this->assertModelData($niveau->toArray(), $dbNiveau);
    }

    /**
     * @test update
     */
    public function test_update_niveau()
    {
        $niveau = Niveau::factory()->create();
        $fakeNiveau = Niveau::factory()->make()->toArray();

        $updatedNiveau = $this->niveauRepo->update($fakeNiveau, $niveau->id);

        $this->assertModelData($fakeNiveau, $updatedNiveau->toArray());
        $dbNiveau = $this->niveauRepo->find($niveau->id);
        $this->assertModelData($fakeNiveau, $dbNiveau->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_niveau()
    {
        $niveau = Niveau::factory()->create();

        $resp = $this->niveauRepo->delete($niveau->id);

        $this->assertTrue($resp);
        $this->assertNull(Niveau::find($niveau->id), 'Niveau should not exist in DB');
    }
}
