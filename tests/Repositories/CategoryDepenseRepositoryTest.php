<?php namespace Tests\Repositories;

use App\Models\CategoryDepense;
use App\Repositories\CategoryDepenseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CategoryDepenseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoryDepenseRepository
     */
    protected $categoryDepenseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->categoryDepenseRepo = \App::make(CategoryDepenseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->make()->toArray();

        $createdCategoryDepense = $this->categoryDepenseRepo->create($categoryDepense);

        $createdCategoryDepense = $createdCategoryDepense->toArray();
        $this->assertArrayHasKey('id', $createdCategoryDepense);
        $this->assertNotNull($createdCategoryDepense['id'], 'Created CategoryDepense must have id specified');
        $this->assertNotNull(CategoryDepense::find($createdCategoryDepense['id']), 'CategoryDepense with given id must be in DB');
        $this->assertModelData($categoryDepense, $createdCategoryDepense);
    }

    /**
     * @test read
     */
    public function test_read_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();

        $dbCategoryDepense = $this->categoryDepenseRepo->find($categoryDepense->id);

        $dbCategoryDepense = $dbCategoryDepense->toArray();
        $this->assertModelData($categoryDepense->toArray(), $dbCategoryDepense);
    }

    /**
     * @test update
     */
    public function test_update_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();
        $fakeCategoryDepense = CategoryDepense::factory()->make()->toArray();

        $updatedCategoryDepense = $this->categoryDepenseRepo->update($fakeCategoryDepense, $categoryDepense->id);

        $this->assertModelData($fakeCategoryDepense, $updatedCategoryDepense->toArray());
        $dbCategoryDepense = $this->categoryDepenseRepo->find($categoryDepense->id);
        $this->assertModelData($fakeCategoryDepense, $dbCategoryDepense->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();

        $resp = $this->categoryDepenseRepo->delete($categoryDepense->id);

        $this->assertTrue($resp);
        $this->assertNull(CategoryDepense::find($categoryDepense->id), 'CategoryDepense should not exist in DB');
    }
}
