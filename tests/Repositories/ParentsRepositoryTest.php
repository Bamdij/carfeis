<?php namespace Tests\Repositories;

use App\Models\Parents;
use App\Repositories\ParentsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ParentsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ParentsRepository
     */
    protected $parentsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->parentsRepo = \App::make(ParentsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_parents()
    {
        $parents = Parents::factory()->make()->toArray();

        $createdParents = $this->parentsRepo->create($parents);

        $createdParents = $createdParents->toArray();
        $this->assertArrayHasKey('id', $createdParents);
        $this->assertNotNull($createdParents['id'], 'Created Parents must have id specified');
        $this->assertNotNull(Parents::find($createdParents['id']), 'Parents with given id must be in DB');
        $this->assertModelData($parents, $createdParents);
    }

    /**
     * @test read
     */
    public function test_read_parents()
    {
        $parents = Parents::factory()->create();

        $dbParents = $this->parentsRepo->find($parents->id);

        $dbParents = $dbParents->toArray();
        $this->assertModelData($parents->toArray(), $dbParents);
    }

    /**
     * @test update
     */
    public function test_update_parents()
    {
        $parents = Parents::factory()->create();
        $fakeParents = Parents::factory()->make()->toArray();

        $updatedParents = $this->parentsRepo->update($fakeParents, $parents->id);

        $this->assertModelData($fakeParents, $updatedParents->toArray());
        $dbParents = $this->parentsRepo->find($parents->id);
        $this->assertModelData($fakeParents, $dbParents->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_parents()
    {
        $parents = Parents::factory()->create();

        $resp = $this->parentsRepo->delete($parents->id);

        $this->assertTrue($resp);
        $this->assertNull(Parents::find($parents->id), 'Parents should not exist in DB');
    }
}
