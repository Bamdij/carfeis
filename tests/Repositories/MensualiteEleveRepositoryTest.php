<?php namespace Tests\Repositories;

use App\Models\MensualiteEleve;
use App\Repositories\MensualiteEleveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MensualiteEleveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MensualiteEleveRepository
     */
    protected $mensualiteEleveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->mensualiteEleveRepo = \App::make(MensualiteEleveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->make()->toArray();

        $createdMensualiteEleve = $this->mensualiteEleveRepo->create($mensualiteEleve);

        $createdMensualiteEleve = $createdMensualiteEleve->toArray();
        $this->assertArrayHasKey('id', $createdMensualiteEleve);
        $this->assertNotNull($createdMensualiteEleve['id'], 'Created MensualiteEleve must have id specified');
        $this->assertNotNull(MensualiteEleve::find($createdMensualiteEleve['id']), 'MensualiteEleve with given id must be in DB');
        $this->assertModelData($mensualiteEleve, $createdMensualiteEleve);
    }

    /**
     * @test read
     */
    public function test_read_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();

        $dbMensualiteEleve = $this->mensualiteEleveRepo->find($mensualiteEleve->id);

        $dbMensualiteEleve = $dbMensualiteEleve->toArray();
        $this->assertModelData($mensualiteEleve->toArray(), $dbMensualiteEleve);
    }

    /**
     * @test update
     */
    public function test_update_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();
        $fakeMensualiteEleve = MensualiteEleve::factory()->make()->toArray();

        $updatedMensualiteEleve = $this->mensualiteEleveRepo->update($fakeMensualiteEleve, $mensualiteEleve->id);

        $this->assertModelData($fakeMensualiteEleve, $updatedMensualiteEleve->toArray());
        $dbMensualiteEleve = $this->mensualiteEleveRepo->find($mensualiteEleve->id);
        $this->assertModelData($fakeMensualiteEleve, $dbMensualiteEleve->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();

        $resp = $this->mensualiteEleveRepo->delete($mensualiteEleve->id);

        $this->assertTrue($resp);
        $this->assertNull(MensualiteEleve::find($mensualiteEleve->id), 'MensualiteEleve should not exist in DB');
    }
}
