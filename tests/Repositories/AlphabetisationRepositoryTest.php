<?php namespace Tests\Repositories;

use App\Models\Alphabetisation;
use App\Repositories\AlphabetisationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AlphabetisationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AlphabetisationRepository
     */
    protected $alphabetisationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->alphabetisationRepo = \App::make(AlphabetisationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->make()->toArray();

        $createdAlphabetisation = $this->alphabetisationRepo->create($alphabetisation);

        $createdAlphabetisation = $createdAlphabetisation->toArray();
        $this->assertArrayHasKey('id', $createdAlphabetisation);
        $this->assertNotNull($createdAlphabetisation['id'], 'Created Alphabetisation must have id specified');
        $this->assertNotNull(Alphabetisation::find($createdAlphabetisation['id']), 'Alphabetisation with given id must be in DB');
        $this->assertModelData($alphabetisation, $createdAlphabetisation);
    }

    /**
     * @test read
     */
    public function test_read_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();

        $dbAlphabetisation = $this->alphabetisationRepo->find($alphabetisation->id);

        $dbAlphabetisation = $dbAlphabetisation->toArray();
        $this->assertModelData($alphabetisation->toArray(), $dbAlphabetisation);
    }

    /**
     * @test update
     */
    public function test_update_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();
        $fakeAlphabetisation = Alphabetisation::factory()->make()->toArray();

        $updatedAlphabetisation = $this->alphabetisationRepo->update($fakeAlphabetisation, $alphabetisation->id);

        $this->assertModelData($fakeAlphabetisation, $updatedAlphabetisation->toArray());
        $dbAlphabetisation = $this->alphabetisationRepo->find($alphabetisation->id);
        $this->assertModelData($fakeAlphabetisation, $dbAlphabetisation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();

        $resp = $this->alphabetisationRepo->delete($alphabetisation->id);

        $this->assertTrue($resp);
        $this->assertNull(Alphabetisation::find($alphabetisation->id), 'Alphabetisation should not exist in DB');
    }
}
