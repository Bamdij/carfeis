<?php namespace Tests\Repositories;

use App\Models\Eleve;
use App\Repositories\EleveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EleveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EleveRepository
     */
    protected $eleveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->eleveRepo = \App::make(EleveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_eleve()
    {
        $eleve = Eleve::factory()->make()->toArray();

        $createdEleve = $this->eleveRepo->create($eleve);

        $createdEleve = $createdEleve->toArray();
        $this->assertArrayHasKey('id', $createdEleve);
        $this->assertNotNull($createdEleve['id'], 'Created Eleve must have id specified');
        $this->assertNotNull(Eleve::find($createdEleve['id']), 'Eleve with given id must be in DB');
        $this->assertModelData($eleve, $createdEleve);
    }

    /**
     * @test read
     */
    public function test_read_eleve()
    {
        $eleve = Eleve::factory()->create();

        $dbEleve = $this->eleveRepo->find($eleve->id);

        $dbEleve = $dbEleve->toArray();
        $this->assertModelData($eleve->toArray(), $dbEleve);
    }

    /**
     * @test update
     */
    public function test_update_eleve()
    {
        $eleve = Eleve::factory()->create();
        $fakeEleve = Eleve::factory()->make()->toArray();

        $updatedEleve = $this->eleveRepo->update($fakeEleve, $eleve->id);

        $this->assertModelData($fakeEleve, $updatedEleve->toArray());
        $dbEleve = $this->eleveRepo->find($eleve->id);
        $this->assertModelData($fakeEleve, $dbEleve->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_eleve()
    {
        $eleve = Eleve::factory()->create();

        $resp = $this->eleveRepo->delete($eleve->id);

        $this->assertTrue($resp);
        $this->assertNull(Eleve::find($eleve->id), 'Eleve should not exist in DB');
    }
}
