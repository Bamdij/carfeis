<?php namespace Tests\Repositories;

use App\Models\EnseignantClasse;
use App\Repositories\EnseignantClasseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EnseignantClasseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EnseignantClasseRepository
     */
    protected $enseignantClasseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->enseignantClasseRepo = \App::make(EnseignantClasseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->make()->toArray();

        $createdEnseignantClasse = $this->enseignantClasseRepo->create($enseignantClasse);

        $createdEnseignantClasse = $createdEnseignantClasse->toArray();
        $this->assertArrayHasKey('id', $createdEnseignantClasse);
        $this->assertNotNull($createdEnseignantClasse['id'], 'Created EnseignantClasse must have id specified');
        $this->assertNotNull(EnseignantClasse::find($createdEnseignantClasse['id']), 'EnseignantClasse with given id must be in DB');
        $this->assertModelData($enseignantClasse, $createdEnseignantClasse);
    }

    /**
     * @test read
     */
    public function test_read_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();

        $dbEnseignantClasse = $this->enseignantClasseRepo->find($enseignantClasse->id);

        $dbEnseignantClasse = $dbEnseignantClasse->toArray();
        $this->assertModelData($enseignantClasse->toArray(), $dbEnseignantClasse);
    }

    /**
     * @test update
     */
    public function test_update_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();
        $fakeEnseignantClasse = EnseignantClasse::factory()->make()->toArray();

        $updatedEnseignantClasse = $this->enseignantClasseRepo->update($fakeEnseignantClasse, $enseignantClasse->id);

        $this->assertModelData($fakeEnseignantClasse, $updatedEnseignantClasse->toArray());
        $dbEnseignantClasse = $this->enseignantClasseRepo->find($enseignantClasse->id);
        $this->assertModelData($fakeEnseignantClasse, $dbEnseignantClasse->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();

        $resp = $this->enseignantClasseRepo->delete($enseignantClasse->id);

        $this->assertTrue($resp);
        $this->assertNull(EnseignantClasse::find($enseignantClasse->id), 'EnseignantClasse should not exist in DB');
    }
}
