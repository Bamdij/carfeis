<?php namespace Tests\Repositories;

use App\Models\InfoParent;
use App\Repositories\InfoParentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InfoParentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InfoParentRepository
     */
    protected $infoParentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->infoParentRepo = \App::make(InfoParentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_info_parent()
    {
        $infoParent = InfoParent::factory()->make()->toArray();

        $createdInfoParent = $this->infoParentRepo->create($infoParent);

        $createdInfoParent = $createdInfoParent->toArray();
        $this->assertArrayHasKey('id', $createdInfoParent);
        $this->assertNotNull($createdInfoParent['id'], 'Created InfoParent must have id specified');
        $this->assertNotNull(InfoParent::find($createdInfoParent['id']), 'InfoParent with given id must be in DB');
        $this->assertModelData($infoParent, $createdInfoParent);
    }

    /**
     * @test read
     */
    public function test_read_info_parent()
    {
        $infoParent = InfoParent::factory()->create();

        $dbInfoParent = $this->infoParentRepo->find($infoParent->id);

        $dbInfoParent = $dbInfoParent->toArray();
        $this->assertModelData($infoParent->toArray(), $dbInfoParent);
    }

    /**
     * @test update
     */
    public function test_update_info_parent()
    {
        $infoParent = InfoParent::factory()->create();
        $fakeInfoParent = InfoParent::factory()->make()->toArray();

        $updatedInfoParent = $this->infoParentRepo->update($fakeInfoParent, $infoParent->id);

        $this->assertModelData($fakeInfoParent, $updatedInfoParent->toArray());
        $dbInfoParent = $this->infoParentRepo->find($infoParent->id);
        $this->assertModelData($fakeInfoParent, $dbInfoParent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_info_parent()
    {
        $infoParent = InfoParent::factory()->create();

        $resp = $this->infoParentRepo->delete($infoParent->id);

        $this->assertTrue($resp);
        $this->assertNull(InfoParent::find($infoParent->id), 'InfoParent should not exist in DB');
    }
}
