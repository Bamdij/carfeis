<?php namespace Tests\Repositories;

use App\Models\EvaluationSemestre;
use App\Repositories\EvaluationSemestreRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EvaluationSemestreRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EvaluationSemestreRepository
     */
    protected $evaluationSemestreRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->evaluationSemestreRepo = \App::make(EvaluationSemestreRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->make()->toArray();

        $createdEvaluationSemestre = $this->evaluationSemestreRepo->create($evaluationSemestre);

        $createdEvaluationSemestre = $createdEvaluationSemestre->toArray();
        $this->assertArrayHasKey('id', $createdEvaluationSemestre);
        $this->assertNotNull($createdEvaluationSemestre['id'], 'Created EvaluationSemestre must have id specified');
        $this->assertNotNull(EvaluationSemestre::find($createdEvaluationSemestre['id']), 'EvaluationSemestre with given id must be in DB');
        $this->assertModelData($evaluationSemestre, $createdEvaluationSemestre);
    }

    /**
     * @test read
     */
    public function test_read_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();

        $dbEvaluationSemestre = $this->evaluationSemestreRepo->find($evaluationSemestre->id);

        $dbEvaluationSemestre = $dbEvaluationSemestre->toArray();
        $this->assertModelData($evaluationSemestre->toArray(), $dbEvaluationSemestre);
    }

    /**
     * @test update
     */
    public function test_update_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();
        $fakeEvaluationSemestre = EvaluationSemestre::factory()->make()->toArray();

        $updatedEvaluationSemestre = $this->evaluationSemestreRepo->update($fakeEvaluationSemestre, $evaluationSemestre->id);

        $this->assertModelData($fakeEvaluationSemestre, $updatedEvaluationSemestre->toArray());
        $dbEvaluationSemestre = $this->evaluationSemestreRepo->find($evaluationSemestre->id);
        $this->assertModelData($fakeEvaluationSemestre, $dbEvaluationSemestre->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();

        $resp = $this->evaluationSemestreRepo->delete($evaluationSemestre->id);

        $this->assertTrue($resp);
        $this->assertNull(EvaluationSemestre::find($evaluationSemestre->id), 'EvaluationSemestre should not exist in DB');
    }
}
