<?php namespace Tests\Repositories;

use App\Models\FraisInscription;
use App\Repositories\FraisInscriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FraisInscriptionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FraisInscriptionRepository
     */
    protected $fraisInscriptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fraisInscriptionRepo = \App::make(FraisInscriptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->make()->toArray();

        $createdFraisInscription = $this->fraisInscriptionRepo->create($fraisInscription);

        $createdFraisInscription = $createdFraisInscription->toArray();
        $this->assertArrayHasKey('id', $createdFraisInscription);
        $this->assertNotNull($createdFraisInscription['id'], 'Created FraisInscription must have id specified');
        $this->assertNotNull(FraisInscription::find($createdFraisInscription['id']), 'FraisInscription with given id must be in DB');
        $this->assertModelData($fraisInscription, $createdFraisInscription);
    }

    /**
     * @test read
     */
    public function test_read_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();

        $dbFraisInscription = $this->fraisInscriptionRepo->find($fraisInscription->id);

        $dbFraisInscription = $dbFraisInscription->toArray();
        $this->assertModelData($fraisInscription->toArray(), $dbFraisInscription);
    }

    /**
     * @test update
     */
    public function test_update_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();
        $fakeFraisInscription = FraisInscription::factory()->make()->toArray();

        $updatedFraisInscription = $this->fraisInscriptionRepo->update($fakeFraisInscription, $fraisInscription->id);

        $this->assertModelData($fakeFraisInscription, $updatedFraisInscription->toArray());
        $dbFraisInscription = $this->fraisInscriptionRepo->find($fraisInscription->id);
        $this->assertModelData($fakeFraisInscription, $dbFraisInscription->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();

        $resp = $this->fraisInscriptionRepo->delete($fraisInscription->id);

        $this->assertTrue($resp);
        $this->assertNull(FraisInscription::find($fraisInscription->id), 'FraisInscription should not exist in DB');
    }
}
