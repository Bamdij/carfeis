<?php namespace Tests\Repositories;

use App\Models\InscriptionEleve;
use App\Repositories\InscriptionEleveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InscriptionEleveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InscriptionEleveRepository
     */
    protected $inscriptionEleveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->inscriptionEleveRepo = \App::make(InscriptionEleveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->make()->toArray();

        $createdInscriptionEleve = $this->inscriptionEleveRepo->create($inscriptionEleve);

        $createdInscriptionEleve = $createdInscriptionEleve->toArray();
        $this->assertArrayHasKey('id', $createdInscriptionEleve);
        $this->assertNotNull($createdInscriptionEleve['id'], 'Created InscriptionEleve must have id specified');
        $this->assertNotNull(InscriptionEleve::find($createdInscriptionEleve['id']), 'InscriptionEleve with given id must be in DB');
        $this->assertModelData($inscriptionEleve, $createdInscriptionEleve);
    }

    /**
     * @test read
     */
    public function test_read_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();

        $dbInscriptionEleve = $this->inscriptionEleveRepo->find($inscriptionEleve->id);

        $dbInscriptionEleve = $dbInscriptionEleve->toArray();
        $this->assertModelData($inscriptionEleve->toArray(), $dbInscriptionEleve);
    }

    /**
     * @test update
     */
    public function test_update_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();
        $fakeInscriptionEleve = InscriptionEleve::factory()->make()->toArray();

        $updatedInscriptionEleve = $this->inscriptionEleveRepo->update($fakeInscriptionEleve, $inscriptionEleve->id);

        $this->assertModelData($fakeInscriptionEleve, $updatedInscriptionEleve->toArray());
        $dbInscriptionEleve = $this->inscriptionEleveRepo->find($inscriptionEleve->id);
        $this->assertModelData($fakeInscriptionEleve, $dbInscriptionEleve->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();

        $resp = $this->inscriptionEleveRepo->delete($inscriptionEleve->id);

        $this->assertTrue($resp);
        $this->assertNull(InscriptionEleve::find($inscriptionEleve->id), 'InscriptionEleve should not exist in DB');
    }
}
