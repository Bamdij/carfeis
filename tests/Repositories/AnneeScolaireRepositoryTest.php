<?php namespace Tests\Repositories;

use App\Models\AnneeScolaire;
use App\Repositories\AnneeScolaireRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AnneeScolaireRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AnneeScolaireRepository
     */
    protected $anneeScolaireRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->anneeScolaireRepo = \App::make(AnneeScolaireRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->make()->toArray();

        $createdAnneeScolaire = $this->anneeScolaireRepo->create($anneeScolaire);

        $createdAnneeScolaire = $createdAnneeScolaire->toArray();
        $this->assertArrayHasKey('id', $createdAnneeScolaire);
        $this->assertNotNull($createdAnneeScolaire['id'], 'Created AnneeScolaire must have id specified');
        $this->assertNotNull(AnneeScolaire::find($createdAnneeScolaire['id']), 'AnneeScolaire with given id must be in DB');
        $this->assertModelData($anneeScolaire, $createdAnneeScolaire);
    }

    /**
     * @test read
     */
    public function test_read_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();

        $dbAnneeScolaire = $this->anneeScolaireRepo->find($anneeScolaire->id);

        $dbAnneeScolaire = $dbAnneeScolaire->toArray();
        $this->assertModelData($anneeScolaire->toArray(), $dbAnneeScolaire);
    }

    /**
     * @test update
     */
    public function test_update_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();
        $fakeAnneeScolaire = AnneeScolaire::factory()->make()->toArray();

        $updatedAnneeScolaire = $this->anneeScolaireRepo->update($fakeAnneeScolaire, $anneeScolaire->id);

        $this->assertModelData($fakeAnneeScolaire, $updatedAnneeScolaire->toArray());
        $dbAnneeScolaire = $this->anneeScolaireRepo->find($anneeScolaire->id);
        $this->assertModelData($fakeAnneeScolaire, $dbAnneeScolaire->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();

        $resp = $this->anneeScolaireRepo->delete($anneeScolaire->id);

        $this->assertTrue($resp);
        $this->assertNull(AnneeScolaire::find($anneeScolaire->id), 'AnneeScolaire should not exist in DB');
    }
}
