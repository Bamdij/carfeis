<?php namespace Tests\Repositories;

use App\Models\SousNiveau;
use App\Repositories\SousNiveauRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SousNiveauRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SousNiveauRepository
     */
    protected $sousNiveauRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sousNiveauRepo = \App::make(SousNiveauRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->make()->toArray();

        $createdSousNiveau = $this->sousNiveauRepo->create($sousNiveau);

        $createdSousNiveau = $createdSousNiveau->toArray();
        $this->assertArrayHasKey('id', $createdSousNiveau);
        $this->assertNotNull($createdSousNiveau['id'], 'Created SousNiveau must have id specified');
        $this->assertNotNull(SousNiveau::find($createdSousNiveau['id']), 'SousNiveau with given id must be in DB');
        $this->assertModelData($sousNiveau, $createdSousNiveau);
    }

    /**
     * @test read
     */
    public function test_read_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();

        $dbSousNiveau = $this->sousNiveauRepo->find($sousNiveau->id);

        $dbSousNiveau = $dbSousNiveau->toArray();
        $this->assertModelData($sousNiveau->toArray(), $dbSousNiveau);
    }

    /**
     * @test update
     */
    public function test_update_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();
        $fakeSousNiveau = SousNiveau::factory()->make()->toArray();

        $updatedSousNiveau = $this->sousNiveauRepo->update($fakeSousNiveau, $sousNiveau->id);

        $this->assertModelData($fakeSousNiveau, $updatedSousNiveau->toArray());
        $dbSousNiveau = $this->sousNiveauRepo->find($sousNiveau->id);
        $this->assertModelData($fakeSousNiveau, $dbSousNiveau->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();

        $resp = $this->sousNiveauRepo->delete($sousNiveau->id);

        $this->assertTrue($resp);
        $this->assertNull(SousNiveau::find($sousNiveau->id), 'SousNiveau should not exist in DB');
    }
}
