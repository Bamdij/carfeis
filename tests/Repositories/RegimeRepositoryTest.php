<?php namespace Tests\Repositories;

use App\Models\Regime;
use App\Repositories\RegimeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RegimeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RegimeRepository
     */
    protected $regimeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->regimeRepo = \App::make(RegimeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_regime()
    {
        $regime = Regime::factory()->make()->toArray();

        $createdRegime = $this->regimeRepo->create($regime);

        $createdRegime = $createdRegime->toArray();
        $this->assertArrayHasKey('id', $createdRegime);
        $this->assertNotNull($createdRegime['id'], 'Created Regime must have id specified');
        $this->assertNotNull(Regime::find($createdRegime['id']), 'Regime with given id must be in DB');
        $this->assertModelData($regime, $createdRegime);
    }

    /**
     * @test read
     */
    public function test_read_regime()
    {
        $regime = Regime::factory()->create();

        $dbRegime = $this->regimeRepo->find($regime->id);

        $dbRegime = $dbRegime->toArray();
        $this->assertModelData($regime->toArray(), $dbRegime);
    }

    /**
     * @test update
     */
    public function test_update_regime()
    {
        $regime = Regime::factory()->create();
        $fakeRegime = Regime::factory()->make()->toArray();

        $updatedRegime = $this->regimeRepo->update($fakeRegime, $regime->id);

        $this->assertModelData($fakeRegime, $updatedRegime->toArray());
        $dbRegime = $this->regimeRepo->find($regime->id);
        $this->assertModelData($fakeRegime, $dbRegime->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_regime()
    {
        $regime = Regime::factory()->create();

        $resp = $this->regimeRepo->delete($regime->id);

        $this->assertTrue($resp);
        $this->assertNull(Regime::find($regime->id), 'Regime should not exist in DB');
    }
}
