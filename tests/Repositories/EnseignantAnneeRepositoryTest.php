<?php namespace Tests\Repositories;

use App\Models\EnseignantAnnee;
use App\Repositories\EnseignantAnneeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EnseignantAnneeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EnseignantAnneeRepository
     */
    protected $enseignantAnneeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->enseignantAnneeRepo = \App::make(EnseignantAnneeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->make()->toArray();

        $createdEnseignantAnnee = $this->enseignantAnneeRepo->create($enseignantAnnee);

        $createdEnseignantAnnee = $createdEnseignantAnnee->toArray();
        $this->assertArrayHasKey('id', $createdEnseignantAnnee);
        $this->assertNotNull($createdEnseignantAnnee['id'], 'Created EnseignantAnnee must have id specified');
        $this->assertNotNull(EnseignantAnnee::find($createdEnseignantAnnee['id']), 'EnseignantAnnee with given id must be in DB');
        $this->assertModelData($enseignantAnnee, $createdEnseignantAnnee);
    }

    /**
     * @test read
     */
    public function test_read_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();

        $dbEnseignantAnnee = $this->enseignantAnneeRepo->find($enseignantAnnee->id);

        $dbEnseignantAnnee = $dbEnseignantAnnee->toArray();
        $this->assertModelData($enseignantAnnee->toArray(), $dbEnseignantAnnee);
    }

    /**
     * @test update
     */
    public function test_update_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();
        $fakeEnseignantAnnee = EnseignantAnnee::factory()->make()->toArray();

        $updatedEnseignantAnnee = $this->enseignantAnneeRepo->update($fakeEnseignantAnnee, $enseignantAnnee->id);

        $this->assertModelData($fakeEnseignantAnnee, $updatedEnseignantAnnee->toArray());
        $dbEnseignantAnnee = $this->enseignantAnneeRepo->find($enseignantAnnee->id);
        $this->assertModelData($fakeEnseignantAnnee, $dbEnseignantAnnee->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();

        $resp = $this->enseignantAnneeRepo->delete($enseignantAnnee->id);

        $this->assertTrue($resp);
        $this->assertNull(EnseignantAnnee::find($enseignantAnnee->id), 'EnseignantAnnee should not exist in DB');
    }
}
