<?php namespace Tests\Repositories;

use App\Models\DepenseCategorie;
use App\Repositories\DepenseCategorieRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DepenseCategorieRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DepenseCategorieRepository
     */
    protected $depenseCategorieRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->depenseCategorieRepo = \App::make(DepenseCategorieRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->make()->toArray();

        $createdDepenseCategorie = $this->depenseCategorieRepo->create($depenseCategorie);

        $createdDepenseCategorie = $createdDepenseCategorie->toArray();
        $this->assertArrayHasKey('id', $createdDepenseCategorie);
        $this->assertNotNull($createdDepenseCategorie['id'], 'Created DepenseCategorie must have id specified');
        $this->assertNotNull(DepenseCategorie::find($createdDepenseCategorie['id']), 'DepenseCategorie with given id must be in DB');
        $this->assertModelData($depenseCategorie, $createdDepenseCategorie);
    }

    /**
     * @test read
     */
    public function test_read_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();

        $dbDepenseCategorie = $this->depenseCategorieRepo->find($depenseCategorie->id);

        $dbDepenseCategorie = $dbDepenseCategorie->toArray();
        $this->assertModelData($depenseCategorie->toArray(), $dbDepenseCategorie);
    }

    /**
     * @test update
     */
    public function test_update_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();
        $fakeDepenseCategorie = DepenseCategorie::factory()->make()->toArray();

        $updatedDepenseCategorie = $this->depenseCategorieRepo->update($fakeDepenseCategorie, $depenseCategorie->id);

        $this->assertModelData($fakeDepenseCategorie, $updatedDepenseCategorie->toArray());
        $dbDepenseCategorie = $this->depenseCategorieRepo->find($depenseCategorie->id);
        $this->assertModelData($fakeDepenseCategorie, $dbDepenseCategorie->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();

        $resp = $this->depenseCategorieRepo->delete($depenseCategorie->id);

        $this->assertTrue($resp);
        $this->assertNull(DepenseCategorie::find($depenseCategorie->id), 'DepenseCategorie should not exist in DB');
    }
}
