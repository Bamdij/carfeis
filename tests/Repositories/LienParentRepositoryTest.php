<?php namespace Tests\Repositories;

use App\Models\LienParent;
use App\Repositories\LienParentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LienParentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LienParentRepository
     */
    protected $lienParentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->lienParentRepo = \App::make(LienParentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_lien_parent()
    {
        $lienParent = LienParent::factory()->make()->toArray();

        $createdLienParent = $this->lienParentRepo->create($lienParent);

        $createdLienParent = $createdLienParent->toArray();
        $this->assertArrayHasKey('id', $createdLienParent);
        $this->assertNotNull($createdLienParent['id'], 'Created LienParent must have id specified');
        $this->assertNotNull(LienParent::find($createdLienParent['id']), 'LienParent with given id must be in DB');
        $this->assertModelData($lienParent, $createdLienParent);
    }

    /**
     * @test read
     */
    public function test_read_lien_parent()
    {
        $lienParent = LienParent::factory()->create();

        $dbLienParent = $this->lienParentRepo->find($lienParent->id);

        $dbLienParent = $dbLienParent->toArray();
        $this->assertModelData($lienParent->toArray(), $dbLienParent);
    }

    /**
     * @test update
     */
    public function test_update_lien_parent()
    {
        $lienParent = LienParent::factory()->create();
        $fakeLienParent = LienParent::factory()->make()->toArray();

        $updatedLienParent = $this->lienParentRepo->update($fakeLienParent, $lienParent->id);

        $this->assertModelData($fakeLienParent, $updatedLienParent->toArray());
        $dbLienParent = $this->lienParentRepo->find($lienParent->id);
        $this->assertModelData($fakeLienParent, $dbLienParent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_lien_parent()
    {
        $lienParent = LienParent::factory()->create();

        $resp = $this->lienParentRepo->delete($lienParent->id);

        $this->assertTrue($resp);
        $this->assertNull(LienParent::find($lienParent->id), 'LienParent should not exist in DB');
    }
}
