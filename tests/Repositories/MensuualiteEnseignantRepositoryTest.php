<?php namespace Tests\Repositories;

use App\Models\MensuualiteEnseignant;
use App\Repositories\MensuualiteEnseignantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MensuualiteEnseignantRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MensuualiteEnseignantRepository
     */
    protected $mensuualiteEnseignantRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->mensuualiteEnseignantRepo = \App::make(MensuualiteEnseignantRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->make()->toArray();

        $createdMensuualiteEnseignant = $this->mensuualiteEnseignantRepo->create($mensuualiteEnseignant);

        $createdMensuualiteEnseignant = $createdMensuualiteEnseignant->toArray();
        $this->assertArrayHasKey('id', $createdMensuualiteEnseignant);
        $this->assertNotNull($createdMensuualiteEnseignant['id'], 'Created MensuualiteEnseignant must have id specified');
        $this->assertNotNull(MensuualiteEnseignant::find($createdMensuualiteEnseignant['id']), 'MensuualiteEnseignant with given id must be in DB');
        $this->assertModelData($mensuualiteEnseignant, $createdMensuualiteEnseignant);
    }

    /**
     * @test read
     */
    public function test_read_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();

        $dbMensuualiteEnseignant = $this->mensuualiteEnseignantRepo->find($mensuualiteEnseignant->id);

        $dbMensuualiteEnseignant = $dbMensuualiteEnseignant->toArray();
        $this->assertModelData($mensuualiteEnseignant->toArray(), $dbMensuualiteEnseignant);
    }

    /**
     * @test update
     */
    public function test_update_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();
        $fakeMensuualiteEnseignant = MensuualiteEnseignant::factory()->make()->toArray();

        $updatedMensuualiteEnseignant = $this->mensuualiteEnseignantRepo->update($fakeMensuualiteEnseignant, $mensuualiteEnseignant->id);

        $this->assertModelData($fakeMensuualiteEnseignant, $updatedMensuualiteEnseignant->toArray());
        $dbMensuualiteEnseignant = $this->mensuualiteEnseignantRepo->find($mensuualiteEnseignant->id);
        $this->assertModelData($fakeMensuualiteEnseignant, $dbMensuualiteEnseignant->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();

        $resp = $this->mensuualiteEnseignantRepo->delete($mensuualiteEnseignant->id);

        $this->assertTrue($resp);
        $this->assertNull(MensuualiteEnseignant::find($mensuualiteEnseignant->id), 'MensuualiteEnseignant should not exist in DB');
    }
}
