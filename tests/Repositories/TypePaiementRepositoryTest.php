<?php namespace Tests\Repositories;

use App\Models\TypePaiement;
use App\Repositories\TypePaiementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypePaiementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypePaiementRepository
     */
    protected $typePaiementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typePaiementRepo = \App::make(TypePaiementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->make()->toArray();

        $createdTypePaiement = $this->typePaiementRepo->create($typePaiement);

        $createdTypePaiement = $createdTypePaiement->toArray();
        $this->assertArrayHasKey('id', $createdTypePaiement);
        $this->assertNotNull($createdTypePaiement['id'], 'Created TypePaiement must have id specified');
        $this->assertNotNull(TypePaiement::find($createdTypePaiement['id']), 'TypePaiement with given id must be in DB');
        $this->assertModelData($typePaiement, $createdTypePaiement);
    }

    /**
     * @test read
     */
    public function test_read_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();

        $dbTypePaiement = $this->typePaiementRepo->find($typePaiement->id);

        $dbTypePaiement = $dbTypePaiement->toArray();
        $this->assertModelData($typePaiement->toArray(), $dbTypePaiement);
    }

    /**
     * @test update
     */
    public function test_update_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();
        $fakeTypePaiement = TypePaiement::factory()->make()->toArray();

        $updatedTypePaiement = $this->typePaiementRepo->update($fakeTypePaiement, $typePaiement->id);

        $this->assertModelData($fakeTypePaiement, $updatedTypePaiement->toArray());
        $dbTypePaiement = $this->typePaiementRepo->find($typePaiement->id);
        $this->assertModelData($fakeTypePaiement, $dbTypePaiement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();

        $resp = $this->typePaiementRepo->delete($typePaiement->id);

        $this->assertTrue($resp);
        $this->assertNull(TypePaiement::find($typePaiement->id), 'TypePaiement should not exist in DB');
    }
}
