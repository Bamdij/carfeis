<?php namespace Tests\Repositories;

use App\Models\TypeEnseignant;
use App\Repositories\TypeEnseignantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeEnseignantRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeEnseignantRepository
     */
    protected $typeEnseignantRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeEnseignantRepo = \App::make(TypeEnseignantRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->make()->toArray();

        $createdTypeEnseignant = $this->typeEnseignantRepo->create($typeEnseignant);

        $createdTypeEnseignant = $createdTypeEnseignant->toArray();
        $this->assertArrayHasKey('id', $createdTypeEnseignant);
        $this->assertNotNull($createdTypeEnseignant['id'], 'Created TypeEnseignant must have id specified');
        $this->assertNotNull(TypeEnseignant::find($createdTypeEnseignant['id']), 'TypeEnseignant with given id must be in DB');
        $this->assertModelData($typeEnseignant, $createdTypeEnseignant);
    }

    /**
     * @test read
     */
    public function test_read_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();

        $dbTypeEnseignant = $this->typeEnseignantRepo->find($typeEnseignant->id);

        $dbTypeEnseignant = $dbTypeEnseignant->toArray();
        $this->assertModelData($typeEnseignant->toArray(), $dbTypeEnseignant);
    }

    /**
     * @test update
     */
    public function test_update_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();
        $fakeTypeEnseignant = TypeEnseignant::factory()->make()->toArray();

        $updatedTypeEnseignant = $this->typeEnseignantRepo->update($fakeTypeEnseignant, $typeEnseignant->id);

        $this->assertModelData($fakeTypeEnseignant, $updatedTypeEnseignant->toArray());
        $dbTypeEnseignant = $this->typeEnseignantRepo->find($typeEnseignant->id);
        $this->assertModelData($fakeTypeEnseignant, $dbTypeEnseignant->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();

        $resp = $this->typeEnseignantRepo->delete($typeEnseignant->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeEnseignant::find($typeEnseignant->id), 'TypeEnseignant should not exist in DB');
    }
}
