<?php namespace Tests\Repositories;

use App\Models\AproposCarfeis;
use App\Repositories\AproposCarfeisRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AproposCarfeisRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AproposCarfeisRepository
     */
    protected $aproposCarfeisRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->aproposCarfeisRepo = \App::make(AproposCarfeisRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->make()->toArray();

        $createdAproposCarfeis = $this->aproposCarfeisRepo->create($aproposCarfeis);

        $createdAproposCarfeis = $createdAproposCarfeis->toArray();
        $this->assertArrayHasKey('id', $createdAproposCarfeis);
        $this->assertNotNull($createdAproposCarfeis['id'], 'Created AproposCarfeis must have id specified');
        $this->assertNotNull(AproposCarfeis::find($createdAproposCarfeis['id']), 'AproposCarfeis with given id must be in DB');
        $this->assertModelData($aproposCarfeis, $createdAproposCarfeis);
    }

    /**
     * @test read
     */
    public function test_read_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();

        $dbAproposCarfeis = $this->aproposCarfeisRepo->find($aproposCarfeis->id);

        $dbAproposCarfeis = $dbAproposCarfeis->toArray();
        $this->assertModelData($aproposCarfeis->toArray(), $dbAproposCarfeis);
    }

    /**
     * @test update
     */
    public function test_update_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();
        $fakeAproposCarfeis = AproposCarfeis::factory()->make()->toArray();

        $updatedAproposCarfeis = $this->aproposCarfeisRepo->update($fakeAproposCarfeis, $aproposCarfeis->id);

        $this->assertModelData($fakeAproposCarfeis, $updatedAproposCarfeis->toArray());
        $dbAproposCarfeis = $this->aproposCarfeisRepo->find($aproposCarfeis->id);
        $this->assertModelData($fakeAproposCarfeis, $dbAproposCarfeis->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();

        $resp = $this->aproposCarfeisRepo->delete($aproposCarfeis->id);

        $this->assertTrue($resp);
        $this->assertNull(AproposCarfeis::find($aproposCarfeis->id), 'AproposCarfeis should not exist in DB');
    }
}
