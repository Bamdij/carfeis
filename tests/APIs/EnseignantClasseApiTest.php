<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EnseignantClasse;

class EnseignantClasseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/enseignant_classes', $enseignantClasse
        );

        $this->assertApiResponse($enseignantClasse);
    }

    /**
     * @test
     */
    public function test_read_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/enseignant_classes/'.$enseignantClasse->id
        );

        $this->assertApiResponse($enseignantClasse->toArray());
    }

    /**
     * @test
     */
    public function test_update_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();
        $editedEnseignantClasse = EnseignantClasse::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/enseignant_classes/'.$enseignantClasse->id,
            $editedEnseignantClasse
        );

        $this->assertApiResponse($editedEnseignantClasse);
    }

    /**
     * @test
     */
    public function test_delete_enseignant_classe()
    {
        $enseignantClasse = EnseignantClasse::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/enseignant_classes/'.$enseignantClasse->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/enseignant_classes/'.$enseignantClasse->id
        );

        $this->response->assertStatus(404);
    }
}
