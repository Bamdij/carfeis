<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Regime;

class RegimeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_regime()
    {
        $regime = Regime::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/regimes', $regime
        );

        $this->assertApiResponse($regime);
    }

    /**
     * @test
     */
    public function test_read_regime()
    {
        $regime = Regime::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/regimes/'.$regime->id
        );

        $this->assertApiResponse($regime->toArray());
    }

    /**
     * @test
     */
    public function test_update_regime()
    {
        $regime = Regime::factory()->create();
        $editedRegime = Regime::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/regimes/'.$regime->id,
            $editedRegime
        );

        $this->assertApiResponse($editedRegime);
    }

    /**
     * @test
     */
    public function test_delete_regime()
    {
        $regime = Regime::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/regimes/'.$regime->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/regimes/'.$regime->id
        );

        $this->response->assertStatus(404);
    }
}
