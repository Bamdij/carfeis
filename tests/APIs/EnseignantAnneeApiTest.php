<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EnseignantAnnee;

class EnseignantAnneeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/enseignant_annees', $enseignantAnnee
        );

        $this->assertApiResponse($enseignantAnnee);
    }

    /**
     * @test
     */
    public function test_read_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/enseignant_annees/'.$enseignantAnnee->id
        );

        $this->assertApiResponse($enseignantAnnee->toArray());
    }

    /**
     * @test
     */
    public function test_update_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();
        $editedEnseignantAnnee = EnseignantAnnee::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/enseignant_annees/'.$enseignantAnnee->id,
            $editedEnseignantAnnee
        );

        $this->assertApiResponse($editedEnseignantAnnee);
    }

    /**
     * @test
     */
    public function test_delete_enseignant_annee()
    {
        $enseignantAnnee = EnseignantAnnee::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/enseignant_annees/'.$enseignantAnnee->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/enseignant_annees/'.$enseignantAnnee->id
        );

        $this->response->assertStatus(404);
    }
}
