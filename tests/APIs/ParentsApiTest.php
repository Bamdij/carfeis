<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Parents;

class ParentsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_parents()
    {
        $parents = Parents::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/parents', $parents
        );

        $this->assertApiResponse($parents);
    }

    /**
     * @test
     */
    public function test_read_parents()
    {
        $parents = Parents::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/parents/'.$parents->id
        );

        $this->assertApiResponse($parents->toArray());
    }

    /**
     * @test
     */
    public function test_update_parents()
    {
        $parents = Parents::factory()->create();
        $editedParents = Parents::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/parents/'.$parents->id,
            $editedParents
        );

        $this->assertApiResponse($editedParents);
    }

    /**
     * @test
     */
    public function test_delete_parents()
    {
        $parents = Parents::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/parents/'.$parents->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/parents/'.$parents->id
        );

        $this->response->assertStatus(404);
    }
}
