<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CategoryDepense;

class CategoryDepenseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/category_depenses', $categoryDepense
        );

        $this->assertApiResponse($categoryDepense);
    }

    /**
     * @test
     */
    public function test_read_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/category_depenses/'.$categoryDepense->id
        );

        $this->assertApiResponse($categoryDepense->toArray());
    }

    /**
     * @test
     */
    public function test_update_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();
        $editedCategoryDepense = CategoryDepense::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/category_depenses/'.$categoryDepense->id,
            $editedCategoryDepense
        );

        $this->assertApiResponse($editedCategoryDepense);
    }

    /**
     * @test
     */
    public function test_delete_category_depense()
    {
        $categoryDepense = CategoryDepense::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/category_depenses/'.$categoryDepense->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/category_depenses/'.$categoryDepense->id
        );

        $this->response->assertStatus(404);
    }
}
