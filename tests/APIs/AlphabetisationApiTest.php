<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Alphabetisation;

class AlphabetisationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/alphabetisations', $alphabetisation
        );

        $this->assertApiResponse($alphabetisation);
    }

    /**
     * @test
     */
    public function test_read_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/alphabetisations/'.$alphabetisation->id
        );

        $this->assertApiResponse($alphabetisation->toArray());
    }

    /**
     * @test
     */
    public function test_update_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();
        $editedAlphabetisation = Alphabetisation::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/alphabetisations/'.$alphabetisation->id,
            $editedAlphabetisation
        );

        $this->assertApiResponse($editedAlphabetisation);
    }

    /**
     * @test
     */
    public function test_delete_alphabetisation()
    {
        $alphabetisation = Alphabetisation::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/alphabetisations/'.$alphabetisation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/alphabetisations/'.$alphabetisation->id
        );

        $this->response->assertStatus(404);
    }
}
