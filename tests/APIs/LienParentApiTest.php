<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LienParent;

class LienParentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_lien_parent()
    {
        $lienParent = LienParent::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/lien_parents', $lienParent
        );

        $this->assertApiResponse($lienParent);
    }

    /**
     * @test
     */
    public function test_read_lien_parent()
    {
        $lienParent = LienParent::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/lien_parents/'.$lienParent->id
        );

        $this->assertApiResponse($lienParent->toArray());
    }

    /**
     * @test
     */
    public function test_update_lien_parent()
    {
        $lienParent = LienParent::factory()->create();
        $editedLienParent = LienParent::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/lien_parents/'.$lienParent->id,
            $editedLienParent
        );

        $this->assertApiResponse($editedLienParent);
    }

    /**
     * @test
     */
    public function test_delete_lien_parent()
    {
        $lienParent = LienParent::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/lien_parents/'.$lienParent->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/lien_parents/'.$lienParent->id
        );

        $this->response->assertStatus(404);
    }
}
