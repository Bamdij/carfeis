<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EvaluationSemestre;

class EvaluationSemestreApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/evaluation_semestres', $evaluationSemestre
        );

        $this->assertApiResponse($evaluationSemestre);
    }

    /**
     * @test
     */
    public function test_read_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/evaluation_semestres/'.$evaluationSemestre->id
        );

        $this->assertApiResponse($evaluationSemestre->toArray());
    }

    /**
     * @test
     */
    public function test_update_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();
        $editedEvaluationSemestre = EvaluationSemestre::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/evaluation_semestres/'.$evaluationSemestre->id,
            $editedEvaluationSemestre
        );

        $this->assertApiResponse($editedEvaluationSemestre);
    }

    /**
     * @test
     */
    public function test_delete_evaluation_semestre()
    {
        $evaluationSemestre = EvaluationSemestre::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/evaluation_semestres/'.$evaluationSemestre->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/evaluation_semestres/'.$evaluationSemestre->id
        );

        $this->response->assertStatus(404);
    }
}
