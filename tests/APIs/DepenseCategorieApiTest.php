<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DepenseCategorie;

class DepenseCategorieApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/depense_categories', $depenseCategorie
        );

        $this->assertApiResponse($depenseCategorie);
    }

    /**
     * @test
     */
    public function test_read_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/depense_categories/'.$depenseCategorie->id
        );

        $this->assertApiResponse($depenseCategorie->toArray());
    }

    /**
     * @test
     */
    public function test_update_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();
        $editedDepenseCategorie = DepenseCategorie::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/depense_categories/'.$depenseCategorie->id,
            $editedDepenseCategorie
        );

        $this->assertApiResponse($editedDepenseCategorie);
    }

    /**
     * @test
     */
    public function test_delete_depense_categorie()
    {
        $depenseCategorie = DepenseCategorie::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/depense_categories/'.$depenseCategorie->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/depense_categories/'.$depenseCategorie->id
        );

        $this->response->assertStatus(404);
    }
}
