<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EvaluationMensuel;

class EvaluationMensuelApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/evaluation_mensuels', $evaluationMensuel
        );

        $this->assertApiResponse($evaluationMensuel);
    }

    /**
     * @test
     */
    public function test_read_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/evaluation_mensuels/'.$evaluationMensuel->id
        );

        $this->assertApiResponse($evaluationMensuel->toArray());
    }

    /**
     * @test
     */
    public function test_update_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();
        $editedEvaluationMensuel = EvaluationMensuel::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/evaluation_mensuels/'.$evaluationMensuel->id,
            $editedEvaluationMensuel
        );

        $this->assertApiResponse($editedEvaluationMensuel);
    }

    /**
     * @test
     */
    public function test_delete_evaluation_mensuel()
    {
        $evaluationMensuel = EvaluationMensuel::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/evaluation_mensuels/'.$evaluationMensuel->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/evaluation_mensuels/'.$evaluationMensuel->id
        );

        $this->response->assertStatus(404);
    }
}
