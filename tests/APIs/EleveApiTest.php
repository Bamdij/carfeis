<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Eleve;

class EleveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_eleve()
    {
        $eleve = Eleve::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/eleves', $eleve
        );

        $this->assertApiResponse($eleve);
    }

    /**
     * @test
     */
    public function test_read_eleve()
    {
        $eleve = Eleve::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/eleves/'.$eleve->id
        );

        $this->assertApiResponse($eleve->toArray());
    }

    /**
     * @test
     */
    public function test_update_eleve()
    {
        $eleve = Eleve::factory()->create();
        $editedEleve = Eleve::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/eleves/'.$eleve->id,
            $editedEleve
        );

        $this->assertApiResponse($editedEleve);
    }

    /**
     * @test
     */
    public function test_delete_eleve()
    {
        $eleve = Eleve::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/eleves/'.$eleve->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/eleves/'.$eleve->id
        );

        $this->response->assertStatus(404);
    }
}
