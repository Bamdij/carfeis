<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\InfoParent;

class InfoParentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_info_parent()
    {
        $infoParent = InfoParent::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/info_parents', $infoParent
        );

        $this->assertApiResponse($infoParent);
    }

    /**
     * @test
     */
    public function test_read_info_parent()
    {
        $infoParent = InfoParent::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/info_parents/'.$infoParent->id
        );

        $this->assertApiResponse($infoParent->toArray());
    }

    /**
     * @test
     */
    public function test_update_info_parent()
    {
        $infoParent = InfoParent::factory()->create();
        $editedInfoParent = InfoParent::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/info_parents/'.$infoParent->id,
            $editedInfoParent
        );

        $this->assertApiResponse($editedInfoParent);
    }

    /**
     * @test
     */
    public function test_delete_info_parent()
    {
        $infoParent = InfoParent::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/info_parents/'.$infoParent->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/info_parents/'.$infoParent->id
        );

        $this->response->assertStatus(404);
    }
}
