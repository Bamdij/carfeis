<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AproposCarfeis;

class AproposCarfeisApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/apropos_carfeis', $aproposCarfeis
        );

        $this->assertApiResponse($aproposCarfeis);
    }

    /**
     * @test
     */
    public function test_read_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/apropos_carfeis/'.$aproposCarfeis->id
        );

        $this->assertApiResponse($aproposCarfeis->toArray());
    }

    /**
     * @test
     */
    public function test_update_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();
        $editedAproposCarfeis = AproposCarfeis::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/apropos_carfeis/'.$aproposCarfeis->id,
            $editedAproposCarfeis
        );

        $this->assertApiResponse($editedAproposCarfeis);
    }

    /**
     * @test
     */
    public function test_delete_apropos_carfeis()
    {
        $aproposCarfeis = AproposCarfeis::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/apropos_carfeis/'.$aproposCarfeis->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/apropos_carfeis/'.$aproposCarfeis->id
        );

        $this->response->assertStatus(404);
    }
}
