<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Sourate;

class SourateApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sourate()
    {
        $sourate = Sourate::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sourates', $sourate
        );

        $this->assertApiResponse($sourate);
    }

    /**
     * @test
     */
    public function test_read_sourate()
    {
        $sourate = Sourate::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/sourates/'.$sourate->id
        );

        $this->assertApiResponse($sourate->toArray());
    }

    /**
     * @test
     */
    public function test_update_sourate()
    {
        $sourate = Sourate::factory()->create();
        $editedSourate = Sourate::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sourates/'.$sourate->id,
            $editedSourate
        );

        $this->assertApiResponse($editedSourate);
    }

    /**
     * @test
     */
    public function test_delete_sourate()
    {
        $sourate = Sourate::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sourates/'.$sourate->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sourates/'.$sourate->id
        );

        $this->response->assertStatus(404);
    }
}
