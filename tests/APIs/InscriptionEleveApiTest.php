<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\InscriptionEleve;

class InscriptionEleveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/inscription_eleves', $inscriptionEleve
        );

        $this->assertApiResponse($inscriptionEleve);
    }

    /**
     * @test
     */
    public function test_read_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/inscription_eleves/'.$inscriptionEleve->id
        );

        $this->assertApiResponse($inscriptionEleve->toArray());
    }

    /**
     * @test
     */
    public function test_update_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();
        $editedInscriptionEleve = InscriptionEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/inscription_eleves/'.$inscriptionEleve->id,
            $editedInscriptionEleve
        );

        $this->assertApiResponse($editedInscriptionEleve);
    }

    /**
     * @test
     */
    public function test_delete_inscription_eleve()
    {
        $inscriptionEleve = InscriptionEleve::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/inscription_eleves/'.$inscriptionEleve->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/inscription_eleves/'.$inscriptionEleve->id
        );

        $this->response->assertStatus(404);
    }
}
