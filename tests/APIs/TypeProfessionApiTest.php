<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypeProfession;

class TypeProfessionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_profession()
    {
        $typeProfession = TypeProfession::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_professions', $typeProfession
        );

        $this->assertApiResponse($typeProfession);
    }

    /**
     * @test
     */
    public function test_read_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/type_professions/'.$typeProfession->id
        );

        $this->assertApiResponse($typeProfession->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();
        $editedTypeProfession = TypeProfession::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_professions/'.$typeProfession->id,
            $editedTypeProfession
        );

        $this->assertApiResponse($editedTypeProfession);
    }

    /**
     * @test
     */
    public function test_delete_type_profession()
    {
        $typeProfession = TypeProfession::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_professions/'.$typeProfession->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_professions/'.$typeProfession->id
        );

        $this->response->assertStatus(404);
    }
}
