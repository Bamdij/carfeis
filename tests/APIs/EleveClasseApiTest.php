<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EleveClasse;

class EleveClasseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/eleve_classes', $eleveClasse
        );

        $this->assertApiResponse($eleveClasse);
    }

    /**
     * @test
     */
    public function test_read_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/eleve_classes/'.$eleveClasse->id
        );

        $this->assertApiResponse($eleveClasse->toArray());
    }

    /**
     * @test
     */
    public function test_update_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();
        $editedEleveClasse = EleveClasse::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/eleve_classes/'.$eleveClasse->id,
            $editedEleveClasse
        );

        $this->assertApiResponse($editedEleveClasse);
    }

    /**
     * @test
     */
    public function test_delete_eleve_classe()
    {
        $eleveClasse = EleveClasse::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/eleve_classes/'.$eleveClasse->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/eleve_classes/'.$eleveClasse->id
        );

        $this->response->assertStatus(404);
    }
}
