<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PaiementEleve;

class PaiementEleveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/paiement_eleves', $paiementEleve
        );

        $this->assertApiResponse($paiementEleve);
    }

    /**
     * @test
     */
    public function test_read_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/paiement_eleves/'.$paiementEleve->id
        );

        $this->assertApiResponse($paiementEleve->toArray());
    }

    /**
     * @test
     */
    public function test_update_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();
        $editedPaiementEleve = PaiementEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/paiement_eleves/'.$paiementEleve->id,
            $editedPaiementEleve
        );

        $this->assertApiResponse($editedPaiementEleve);
    }

    /**
     * @test
     */
    public function test_delete_paiement_eleve()
    {
        $paiementEleve = PaiementEleve::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/paiement_eleves/'.$paiementEleve->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/paiement_eleves/'.$paiementEleve->id
        );

        $this->response->assertStatus(404);
    }
}
