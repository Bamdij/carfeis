<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Niveau;

class NiveauApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_niveau()
    {
        $niveau = Niveau::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/niveaux', $niveau
        );

        $this->assertApiResponse($niveau);
    }

    /**
     * @test
     */
    public function test_read_niveau()
    {
        $niveau = Niveau::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/niveaux/'.$niveau->id
        );

        $this->assertApiResponse($niveau->toArray());
    }

    /**
     * @test
     */
    public function test_update_niveau()
    {
        $niveau = Niveau::factory()->create();
        $editedNiveau = Niveau::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/niveaux/'.$niveau->id,
            $editedNiveau
        );

        $this->assertApiResponse($editedNiveau);
    }

    /**
     * @test
     */
    public function test_delete_niveau()
    {
        $niveau = Niveau::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/niveaux/'.$niveau->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/niveaux/'.$niveau->id
        );

        $this->response->assertStatus(404);
    }
}
