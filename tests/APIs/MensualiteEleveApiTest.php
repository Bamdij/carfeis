<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MensualiteEleve;

class MensualiteEleveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/mensualite_eleves', $mensualiteEleve
        );

        $this->assertApiResponse($mensualiteEleve);
    }

    /**
     * @test
     */
    public function test_read_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/mensualite_eleves/'.$mensualiteEleve->id
        );

        $this->assertApiResponse($mensualiteEleve->toArray());
    }

    /**
     * @test
     */
    public function test_update_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();
        $editedMensualiteEleve = MensualiteEleve::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/mensualite_eleves/'.$mensualiteEleve->id,
            $editedMensualiteEleve
        );

        $this->assertApiResponse($editedMensualiteEleve);
    }

    /**
     * @test
     */
    public function test_delete_mensualite_eleve()
    {
        $mensualiteEleve = MensualiteEleve::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/mensualite_eleves/'.$mensualiteEleve->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/mensualite_eleves/'.$mensualiteEleve->id
        );

        $this->response->assertStatus(404);
    }
}
