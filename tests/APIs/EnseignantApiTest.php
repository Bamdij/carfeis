<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Enseignant;

class EnseignantApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_enseignant()
    {
        $enseignant = Enseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/enseignants', $enseignant
        );

        $this->assertApiResponse($enseignant);
    }

    /**
     * @test
     */
    public function test_read_enseignant()
    {
        $enseignant = Enseignant::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/enseignants/'.$enseignant->id
        );

        $this->assertApiResponse($enseignant->toArray());
    }

    /**
     * @test
     */
    public function test_update_enseignant()
    {
        $enseignant = Enseignant::factory()->create();
        $editedEnseignant = Enseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/enseignants/'.$enseignant->id,
            $editedEnseignant
        );

        $this->assertApiResponse($editedEnseignant);
    }

    /**
     * @test
     */
    public function test_delete_enseignant()
    {
        $enseignant = Enseignant::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/enseignants/'.$enseignant->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/enseignants/'.$enseignant->id
        );

        $this->response->assertStatus(404);
    }
}
