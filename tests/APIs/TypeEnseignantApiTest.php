<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypeEnseignant;

class TypeEnseignantApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_enseignants', $typeEnseignant
        );

        $this->assertApiResponse($typeEnseignant);
    }

    /**
     * @test
     */
    public function test_read_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/type_enseignants/'.$typeEnseignant->id
        );

        $this->assertApiResponse($typeEnseignant->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();
        $editedTypeEnseignant = TypeEnseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_enseignants/'.$typeEnseignant->id,
            $editedTypeEnseignant
        );

        $this->assertApiResponse($editedTypeEnseignant);
    }

    /**
     * @test
     */
    public function test_delete_type_enseignant()
    {
        $typeEnseignant = TypeEnseignant::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_enseignants/'.$typeEnseignant->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_enseignants/'.$typeEnseignant->id
        );

        $this->response->assertStatus(404);
    }
}
