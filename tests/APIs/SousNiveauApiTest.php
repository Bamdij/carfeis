<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SousNiveau;

class SousNiveauApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sous_niveaus', $sousNiveau
        );

        $this->assertApiResponse($sousNiveau);
    }

    /**
     * @test
     */
    public function test_read_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/sous_niveaus/'.$sousNiveau->id
        );

        $this->assertApiResponse($sousNiveau->toArray());
    }

    /**
     * @test
     */
    public function test_update_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();
        $editedSousNiveau = SousNiveau::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sous_niveaus/'.$sousNiveau->id,
            $editedSousNiveau
        );

        $this->assertApiResponse($editedSousNiveau);
    }

    /**
     * @test
     */
    public function test_delete_sous_niveau()
    {
        $sousNiveau = SousNiveau::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sous_niveaus/'.$sousNiveau->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sous_niveaus/'.$sousNiveau->id
        );

        $this->response->assertStatus(404);
    }
}
