<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MensuualiteEnseignant;

class MensuualiteEnseignantApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/mensuualite_enseignants', $mensuualiteEnseignant
        );

        $this->assertApiResponse($mensuualiteEnseignant);
    }

    /**
     * @test
     */
    public function test_read_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/mensuualite_enseignants/'.$mensuualiteEnseignant->id
        );

        $this->assertApiResponse($mensuualiteEnseignant->toArray());
    }

    /**
     * @test
     */
    public function test_update_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();
        $editedMensuualiteEnseignant = MensuualiteEnseignant::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/mensuualite_enseignants/'.$mensuualiteEnseignant->id,
            $editedMensuualiteEnseignant
        );

        $this->assertApiResponse($editedMensuualiteEnseignant);
    }

    /**
     * @test
     */
    public function test_delete_mensuualite_enseignant()
    {
        $mensuualiteEnseignant = MensuualiteEnseignant::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/mensuualite_enseignants/'.$mensuualiteEnseignant->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/mensuualite_enseignants/'.$mensuualiteEnseignant->id
        );

        $this->response->assertStatus(404);
    }
}
