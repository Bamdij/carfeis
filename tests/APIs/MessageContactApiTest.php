<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MessageContact;

class MessageContactApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_message_contact()
    {
        $messageContact = MessageContact::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/message_contacts', $messageContact
        );

        $this->assertApiResponse($messageContact);
    }

    /**
     * @test
     */
    public function test_read_message_contact()
    {
        $messageContact = MessageContact::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/message_contacts/'.$messageContact->id
        );

        $this->assertApiResponse($messageContact->toArray());
    }

    /**
     * @test
     */
    public function test_update_message_contact()
    {
        $messageContact = MessageContact::factory()->create();
        $editedMessageContact = MessageContact::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/message_contacts/'.$messageContact->id,
            $editedMessageContact
        );

        $this->assertApiResponse($editedMessageContact);
    }

    /**
     * @test
     */
    public function test_delete_message_contact()
    {
        $messageContact = MessageContact::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/message_contacts/'.$messageContact->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/message_contacts/'.$messageContact->id
        );

        $this->response->assertStatus(404);
    }
}
