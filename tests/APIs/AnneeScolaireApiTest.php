<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AnneeScolaire;

class AnneeScolaireApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/annee_scolaires', $anneeScolaire
        );

        $this->assertApiResponse($anneeScolaire);
    }

    /**
     * @test
     */
    public function test_read_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/annee_scolaires/'.$anneeScolaire->id
        );

        $this->assertApiResponse($anneeScolaire->toArray());
    }

    /**
     * @test
     */
    public function test_update_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();
        $editedAnneeScolaire = AnneeScolaire::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/annee_scolaires/'.$anneeScolaire->id,
            $editedAnneeScolaire
        );

        $this->assertApiResponse($editedAnneeScolaire);
    }

    /**
     * @test
     */
    public function test_delete_annee_scolaire()
    {
        $anneeScolaire = AnneeScolaire::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/annee_scolaires/'.$anneeScolaire->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/annee_scolaires/'.$anneeScolaire->id
        );

        $this->response->assertStatus(404);
    }
}
