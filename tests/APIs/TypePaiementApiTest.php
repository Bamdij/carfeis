<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypePaiement;

class TypePaiementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_paiements', $typePaiement
        );

        $this->assertApiResponse($typePaiement);
    }

    /**
     * @test
     */
    public function test_read_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/type_paiements/'.$typePaiement->id
        );

        $this->assertApiResponse($typePaiement->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();
        $editedTypePaiement = TypePaiement::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_paiements/'.$typePaiement->id,
            $editedTypePaiement
        );

        $this->assertApiResponse($editedTypePaiement);
    }

    /**
     * @test
     */
    public function test_delete_type_paiement()
    {
        $typePaiement = TypePaiement::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_paiements/'.$typePaiement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_paiements/'.$typePaiement->id
        );

        $this->response->assertStatus(404);
    }
}
