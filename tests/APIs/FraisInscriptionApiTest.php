<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FraisInscription;

class FraisInscriptionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/frais_inscriptions', $fraisInscription
        );

        $this->assertApiResponse($fraisInscription);
    }

    /**
     * @test
     */
    public function test_read_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/frais_inscriptions/'.$fraisInscription->id
        );

        $this->assertApiResponse($fraisInscription->toArray());
    }

    /**
     * @test
     */
    public function test_update_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();
        $editedFraisInscription = FraisInscription::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/frais_inscriptions/'.$fraisInscription->id,
            $editedFraisInscription
        );

        $this->assertApiResponse($editedFraisInscription);
    }

    /**
     * @test
     */
    public function test_delete_frais_inscription()
    {
        $fraisInscription = FraisInscription::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/frais_inscriptions/'.$fraisInscription->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/frais_inscriptions/'.$fraisInscription->id
        );

        $this->response->assertStatus(404);
    }
}
