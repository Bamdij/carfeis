<?php

namespace Database\Factories;

use App\Models\EnseignantAnnee;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnseignantAnneeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnseignantAnnee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enseignant_id' => $this->faker->word,
        'annee_scolaire_id' => $this->faker->word,
        'type_enseignant_id' => $this->faker->word,
        'niveau_id' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
