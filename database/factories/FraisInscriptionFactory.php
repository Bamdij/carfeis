<?php

namespace Database\Factories;

use App\Models\FraisInscription;
use Illuminate\Database\Eloquent\Factories\Factory;

class FraisInscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FraisInscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'regime_id' => $this->faker->word,
        'annee_scolaire_id' => $this->faker->word,
        'montant' => $this->faker->word,
        'observation' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
