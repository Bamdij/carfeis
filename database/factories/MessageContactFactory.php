<?php

namespace Database\Factories;

use App\Models\MessageContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MessageContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'obje' => $this->faker->word,
        'description' => $this->faker->text,
        'user_id' => $this->faker->randomDigitNotNull,
        'is_admin' => $this->faker->word,
        'slug' => $this->faker->word,
        'is_parent_sending' => $this->faker->word,
        'admin_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
