<?php

namespace Database\Factories;

use App\Models\PaiementEleve;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaiementEleveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaiementEleve::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'eleve_id' => $this->faker->word,
        'frais_inscription_id' => $this->faker->word,
        'annee_scolaire_id' => $this->faker->word,
        'mois_1' => $this->faker->word,
        'montant_1' => $this->faker->word,
        'mois_2' => $this->faker->word,
        'montant_2' => $this->faker->word,
        'mois_3' => $this->faker->word,
        'montant_3' => $this->faker->word,
        'mois_4' => $this->faker->word,
        'montant_5' => $this->faker->word,
        'mois_6' => $this->faker->word,
        'montant_6' => $this->faker->word,
        'mois_7' => $this->faker->word,
        'montant_7' => $this->faker->word,
        'mois_8' => $this->faker->word,
        'montant_8' => $this->faker->word,
        'mois_9' => $this->faker->word,
        'montant_9' => $this->faker->word,
        'mois_10' => $this->faker->word,
        'montant_10' => $this->faker->word,
        'mois_11' => $this->faker->word,
        'montant_11' => $this->faker->word,
        'mois_12' => $this->faker->word,
        'montant_12' => $this->faker->word,
        'obseration_1' => $this->faker->text,
        'obseration_2' => $this->faker->text,
        'obseration_3' => $this->faker->text,
        'obseration_4' => $this->faker->text,
        'obseration_5' => $this->faker->text,
        'obseration_6' => $this->faker->text,
        'obseration_7' => $this->faker->text,
        'obseration_8' => $this->faker->text,
        'obseration_9' => $this->faker->text,
        'obseration_10' => $this->faker->text,
        'obseration_11' => $this->faker->text,
        'obseration_12' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
