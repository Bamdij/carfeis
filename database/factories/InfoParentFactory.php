<?php

namespace Database\Factories;

use App\Models\InfoParent;
use Illuminate\Database\Eloquent\Factories\Factory;

class InfoParentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InfoParent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'eleve_id' => $this->faker->randomDigitNotNull,
        'name_pere' => $this->faker->word,
        'phone_pere' => $this->faker->word,
        'email_pere' => $this->faker->word,
        'type_profession_id' => $this->faker->randomDigitNotNull,
        'name_mere' => $this->faker->word,
        'phone_mere' => $this->faker->word,
        'email_mere' => $this->faker->word,
        'type_profession_mere_id' => $this->faker->randomDigitNotNull,
        'name_tuteur' => $this->faker->word,
        'lien_parent_id' => $this->faker->randomDigitNotNull,
        'phone_tuteur' => $this->faker->word,
        'email_tuteur' => $this->faker->word,
        'contact_urgence' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
