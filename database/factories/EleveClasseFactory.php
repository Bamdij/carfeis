<?php

namespace Database\Factories;

use App\Models\EleveClasse;
use Illuminate\Database\Eloquent\Factories\Factory;

class EleveClasseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EleveClasse::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'niveau_id' => $this->faker->randomDigitNotNull,
        'classe_id' => $this->faker->randomDigitNotNull,
        'eleve_id' => $this->faker->randomDigitNotNull,
        'observation' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
