<?php

namespace Database\Factories;

use App\Models\EvaluationMensuel;
use Illuminate\Database\Eloquent\Factories\Factory;

class EvaluationMensuelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EvaluationMensuel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'eleve_id' => $this->faker->randomDigitNotNull,
        'user_id' => $this->faker->randomDigitNotNull,
        'annee_scolaire_id' => $this->faker->randomDigitNotNull,
        'month' => $this->faker->word,
        'date_evaluation' => $this->faker->word,
        'sourate' => $this->faker->word,
        'last_lesson' => $this->faker->word,
        'first_party' => $this->faker->word,
        'appreciation_first_party' => $this->faker->word,
        'second_party' => $this->faker->word,
        'appreciation_second_party' => $this->faker->word,
        'third_party' => $this->faker->word,
        'appreciation_third_party' => $this->faker->word,
        'comportement_fr' => $this->faker->word,
        'comportement_ar' => $this->faker->word,
        'appreciation' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
