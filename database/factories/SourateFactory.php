<?php

namespace Database\Factories;

use App\Models\Sourate;
use Illuminate\Database\Eloquent\Factories\Factory;

class SourateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sourate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'num_sourate' => $this->faker->word,
        'libelle_sourate_fr' => $this->faker->word,
        'libelle_sourate_ar' => $this->faker->word,
        'nbre_verset' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
