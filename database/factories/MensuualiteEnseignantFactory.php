<?php

namespace Database\Factories;

use App\Models\MensuualiteEnseignant;
use Illuminate\Database\Eloquent\Factories\Factory;

class MensuualiteEnseignantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MensuualiteEnseignant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enseignant_id' => $this->faker->word,
        'identity_card' => $this->faker->word,
        'month' => $this->faker->word,
        'montant' => $this->faker->word,
        'annee_scolaire_id' => $this->faker->word,
        'type_paiement_id' => $this->faker->word,
        'observation' => $this->faker->word,
        'user_id' => $this->faker->word,
        'month_end' => $this->faker->word,
        'is_payed' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
