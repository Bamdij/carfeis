<?php

namespace Database\Factories;

use App\Models\EnseignantClasse;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnseignantClasseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnseignantClasse::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enseignant_id' => $this->faker->randomDigitNotNull,
        'sous_niveau_id' => $this->faker->randomDigitNotNull,
        'is_teacher_arab' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
