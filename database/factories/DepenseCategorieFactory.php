<?php

namespace Database\Factories;

use App\Models\DepenseCategorie;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepenseCategorieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DepenseCategorie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_depense_id' => $this->faker->randomDigitNotNull,
        'date_depense' => $this->faker->word,
        'designation_depense' => $this->faker->text,
        'montant_depense' => $this->faker->word,
        'user_id' => $this->faker->word,
        'description' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
