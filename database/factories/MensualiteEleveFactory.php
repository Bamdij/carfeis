<?php

namespace Database\Factories;

use App\Models\MensualiteEleve;
use Illuminate\Database\Eloquent\Factories\Factory;

class MensualiteEleveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MensualiteEleve::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'paiement_eleve_id' => $this->faker->word,
        'eleve_id' => $this->faker->word,
        'annee_scolaire_id' => $this->faker->word,
        'month' => $this->faker->word,
        'mensualite' => $this->faker->word,
        'observation' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
