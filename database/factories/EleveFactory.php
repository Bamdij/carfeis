<?php

namespace Database\Factories;

use App\Models\Eleve;
use Illuminate\Database\Eloquent\Factories\Factory;

class EleveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Eleve::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->word,
        'last_name' => $this->faker->word,
        'date_naissance' => $this->faker->word,
        'sexe' => $this->faker->word,
        'groupe_sanguin' => $this->faker->word,
        'is_malade' => $this->faker->word,
        'quelle_maladie' => $this->faker->text,
        'is_traitement' => $this->faker->word,
        'quel_traitement' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
