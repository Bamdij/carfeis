<?php

namespace Database\Factories;

use App\Models\ParentPaiementAnnuelle;
use Illuminate\Database\Eloquent\Factories\Factory;

class ParentPaiementAnnuelleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ParentPaiementAnnuelle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'annee_scloaire_id' => $this->faker->randomDigitNotNull,
        'parent_id' => $this->faker->randomDigitNotNull,
        'janvier' => $this->faker->word,
        'payed_janv' => $this->faker->word,
        'fevrier' => $this->faker->word,
        'payed_fev' => $this->faker->word,
        'mars' => $this->faker->word,
        'payed_mars' => $this->faker->word,
        'avril' => $this->faker->word,
        'payed_avril' => $this->faker->word,
        'mai' => $this->faker->word,
        'mai_payed' => $this->faker->word,
        'juin' => $this->faker->word,
        'juin_payed' => $this->faker->word,
        'juillett' => $this->faker->word,
        'payed_jul' => $this->faker->word,
        'aout' => $this->faker->word,
        'payed_aout' => $this->faker->word,
        'septembre' => $this->faker->word,
        'payed_sept' => $this->faker->word,
        'octobre' => $this->faker->word,
        'payed_octobre' => $this->faker->word,
        'novembre' => $this->faker->word,
        'payed_nov' => $this->faker->word,
        'decembre' => $this->faker->word,
        'payed_dec' => $this->faker->word,
        'status' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
