<?php

namespace Database\Factories;

use App\Models\Alphabetisation;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlphabetisationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Alphabetisation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name_alphabetisation' => $this->faker->word,
        'daara_frequente' => $this->faker->word,
        'inteligence' => $this->faker->word,
        'person_inscript' => $this->faker->word,
        'cap_finance_resp' => $this->faker->word,
        'date_inscription' => $this->faker->word,
        'eleve_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
