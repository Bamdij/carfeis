<?php

namespace Database\Factories;

use App\Models\EvaluationSemestre;
use Illuminate\Database\Eloquent\Factories\Factory;

class EvaluationSemestreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EvaluationSemestre::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->word,
        'eleve_id' => $this->faker->randomDigitNotNull,
        'annee_scolaire_id' => $this->faker->randomDigitNotNull,
        'start_date' => $this->faker->word,
        'end_date' => $this->faker->word,
        'date_evaluation' => $this->faker->word,
        'monday_app' => $this->faker->word,
        'app_mond_1' => $this->faker->word,
        'app_mond_2' => $this->faker->word,
        'app_mon_3' => $this->faker->word,
        'thuesday_app' => $this->faker->word,
        'app_thues_1' => $this->faker->word,
        'app_thues_2' => $this->faker->word,
        'app_thues_3' => $this->faker->word,
        'wednesday_app' => $this->faker->word,
        'app_wed_1' => $this->faker->word,
        'app_wed_2' => $this->faker->word,
        'app_wed_3' => $this->faker->word,
        'thursday_app' => $this->faker->word,
        'app_thurd_1' => $this->faker->word,
        'app_thurd_2' => $this->faker->word,
        'app_thurd_3' => $this->faker->word,
        'friday_app' => $this->faker->word,
        'app_frid_1' => $this->faker->word,
        'app_frid_2' => $this->faker->word,
        'app_frid_3' => $this->faker->word,
        'last_lesson' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
