<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield11151210ToMensualiteEleves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mensualite_eleves', function (Blueprint $table) {
            $table->string('number_month')->nullable();
            $table->string('montant_attendu')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mensualite_eleves', function (Blueprint $table) {
            //
        });
    }
}
