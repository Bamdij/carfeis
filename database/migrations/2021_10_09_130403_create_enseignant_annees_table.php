<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnseignantAnneesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enseignant_annees', function (Blueprint $table) {
            $table->id('id');
            $table->string('enseignant_id');
            $table->string('annee_scolaire_id');
            $table->string('type_enseignant_id');
            $table->string('niveau_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enseignant_annees');
    }
}
