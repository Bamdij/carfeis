<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield936ToSousNiveaus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sous_niveaus', function (Blueprint $table) {
            $table->bigInteger('niveau_id')->unsigned()->index()->nullable();
            $table->foreign('niveau_id')
                ->references('id')->on('niveaux')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sous_niveaus', function (Blueprint $table) {
            //
        });
    }
}
