<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield06101401ToInfoParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_parents', function (Blueprint $table) {
            $table->string('name_tuteur')->nullable()->change();
            $table->string('email_pere')->nullable()->change();
            $table->string('email_mere')->nullable()->change();
            $table->integer('lien_parent_id')->nullable()->change();
            $table->string('phone_tuteur')->nullable()->change();
            $table->string('email_tuteur')->nullable()->change();
            $table->string('contact_urgence')->nullable()->change();
            $table->boolean('is_parent_tuteur')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_parents', function (Blueprint $table) {
            //
        });
    }
}
