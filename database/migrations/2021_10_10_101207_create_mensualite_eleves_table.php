<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensualiteElevesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensualite_eleves', function (Blueprint $table) {
            $table->id('id');
            $table->string('paiement_eleve_id');
            $table->string('eleve_id');
            $table->string('annee_scolaire_id');
            $table->string('month');
            $table->string('mensualite');
            $table->string('observation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mensualite_eleves');
    }
}
