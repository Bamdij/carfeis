<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParentPaiementAnnuellesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_paiement_annuelles', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('annee_scloaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scloaire_id')
                        ->references('id')->on('annee_scolaires')
                        ->onDelete('cascade');
            $table->bigInteger('parent_id')->unsigned()->index()->nullable();
            $table->foreign('parent_id')
                        ->references('id')->on('users')
                        ->onDelete('cascade');
            $table->string('janvier')->nullable();
            $table->boolean('payed_janv')->default(false);
            $table->string('fevrier')->nullable();
            $table->boolean('payed_fev')->default(false);
            $table->string('mars')->nullable();
            $table->boolean('payed_mars')->default(false);
            $table->string('avril')->nullable();
            $table->boolean('payed_avril')->default(false);
            $table->string('mai')->nullable();
            $table->boolean('mai_payed')->default(false);
            $table->string('juin')->nullable();
            $table->boolean('juin_payed')->default(false);
            $table->string('juillett')->nullable();
            $table->boolean('payed_jul')->default(false);
            $table->string('aout')->nullable();
            $table->boolean('payed_aout')->default(false);
            $table->string('septembre')->nullable();
            $table->boolean('payed_sept')->default(false);
            $table->string('octobre')->nullable();
            $table->boolean('payed_octobre')->default(false);
            $table->string('novembre')->nullable();
            $table->boolean('payed_nov')->default(false);
            $table->string('decembre')->nullable();
            $table->boolean('payed_dec')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parent_paiement_annuelles');
    }
}
