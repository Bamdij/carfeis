<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield3001205ToEleveClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                    ->references('id')->on('annee_scolaires')
                    ->onDelete('cascade');
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            //
        });
    }
}
