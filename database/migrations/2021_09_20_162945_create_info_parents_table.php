<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoParentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_parents', function (Blueprint $table) {
            $table->id('id');
            $table->integer('eleve_id');
            $table->string('name_pere');
            $table->string('phone_pere');
            $table->string('email_pere');
            $table-> bigInteger('type_profession_id')->unsigned()->index()->nullable();
            $table->string('name_mere');
            $table->string('phone_mere');
            $table->string('email_mere');
            $table-> bigInteger('type_profession_mere_id')->unsigned()->index()->nullable();
            $table->string('name_tuteur');
            $table->integer('lien_parent_id');
            $table->string('phone_tuteur');
            $table->string('email_tuteur');
            $table->string('contact_urgence');
            $table->foreign('type_profession_id')
                            ->references('id')->on('type_professions')
                            ->onDelete('cascade');
            $table->foreign('type_profession_mere_id')
            ->references('id')->on('type_professions')
            ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_parents');
    }
}
