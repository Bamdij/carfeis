<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaiementElevesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paiement_eleves', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable();
            $table->foreign('eleve_id')
                ->references('id')->on('eleves')
                ->onDelete('cascade');
            $table->bigInteger('frais_inscription_id')->unsigned()->index()->nullable();
            $table->foreign('frais_inscription_id')
                ->references('id')->on('frais_inscriptions')
                ->onDelete('cascade');
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                ->references('id')->on('annee_scolaires')
                ->onDelete('cascade');
            $table->string('mois_1')->nullable();
            $table->string('montant_1')->nullable();
            $table->string('mois_2')->nullable();
            $table->string('montant_2')->nullable();
            $table->string('mois_3')->nullable();
            $table->string('montant_3')->nullable();
            $table->string('mois_4')->nullable();
            $table->string('montant_5')->nullable();
            $table->string('mois_6')->nullable();
            $table->string('montant_6')->nullable();
            $table->string('mois_7')->nullable();
            $table->string('montant_7')->nullable();
            $table->string('mois_8')->nullable();
            $table->string('montant_8')->nullable();
            $table->string('mois_9')->nullable();
            $table->string('montant_9')->nullable();
            $table->string('mois_10')->nullable();
            $table->string('montant_10')->nullable();
            $table->string('mois_11')->nullable();
            $table->string('montant_11')->nullable();
            $table->string('mois_12')->nullable();
            $table->string('montant_12')->nullable();
            $table->longtext('observation_1')->nullable();
            $table->longtext('observation_2')->nullable();
            $table->longtext('observation_3')->nullable();
            $table->longtext('observation_4')->nullable();
            $table->longtext('observation_5')->nullable();
            $table->longtext('observation_6')->nullable();
            $table->longtext('observation_7')->nullable();
            $table->longtext('observation_8')->nullable();
            $table->longtext('observation_9')->nullable();
            $table->longtext('observation_10')->nullable();
            $table->longtext('observation_11')->nullable();
            $table->longtext('observation_12')->nullable();
            $table->longtext('montant_inscription')->nullable();
            $table->longtext('observation_inscription')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paiement_eleves');
    }
}
