<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageContactsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_contacts', function (Blueprint $table) {
            $table->id('id');
            $table->string('obje');
            $table->longtext('description');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                        ->references('id')->on('users')
                        ->onDelete('cascade');
            $table->string('admin_id')->nullable();
            $table->boolean('is_admin')->default(true);
            $table->boolean('slug')->default(true);
            $table->boolean('is_parent_sendind')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message_contacts');
    }
}
