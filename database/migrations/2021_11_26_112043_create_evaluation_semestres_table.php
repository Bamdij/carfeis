<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationSemestresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_semestres', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->bigInteger('enseignant_id')->unsigned()->index()->nullable();
            $table->foreign('enseignant_id')
                    ->references('id')->on('enseignants')
                    ->onDelete('cascade');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable();
            $table->foreign('eleve_id')
                    ->references('id')->on('eleves')
                    ->onDelete('cascade');
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                ->references('id')->on('annee_scolaires')
                ->onDelete('cascade');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('date_evaluation');
            $table->string('monday_app')->nullable();
            $table->string('app_mond_1')->nullable();
            $table->string('app_mond_2')->nullable();
            $table->string('app_mon_3')->nullable();
            $table->string('thuesday_app')->nullable();
            $table->string('app_thues_1')->nullable();
            $table->string('app_thues_2')->nullable();
            $table->string('app_thues_3')->nullable();
            $table->string('wednesday_app')->nullable();
            $table->string('app_wed_1')->nullable();
            $table->string('app_wed_2')->nullable();
            $table->string('app_wed_3')->nullable();
            $table->string('thursday_app')->nullable();
            $table->string('app_thurd_1')->nullable();
            $table->string('app_thurd_2')->nullable();
            $table->string('app_thurd_3')->nullable();
            $table->string('friday_app')->nullable();
            $table->string('app_frid_1')->nullable();
            $table->string('app_frid_2')->nullable();
            $table->string('app_frid_3')->nullable();
            $table->string('last_lesson')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluation_semestres');
    }
}
