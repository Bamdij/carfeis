<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield1209721102021ToEnseignantAnnees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enseignant_annees', function (Blueprint $table) {
            $table->bigInteger('sous_niveau_id')->unsigned()->index()->nullable()->after('niveau_id');
            $table->foreign('sous_niveau_id')
                ->references('id')->on('sous_niveaus')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enseignant_annees', function (Blueprint $table) {
            //
        });
    }
}
