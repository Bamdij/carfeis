<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepenseCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depense_categories', function (Blueprint $table) {
            $table->id('id');
            $table->integer('category_depense_id');
            $table->date('date_depense');
            $table->string('annee_scolaire_id');
            $table->longtext('designation_depense');
            $table->string('montant_depense');
            $table->string('user_id');
            $table->longtext('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('depense_categories');
    }
}
