<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield30092ToEleveClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            $table->bigInteger('enseignant_id')->unsigned()->index()->nullable();
            $table->foreign('enseignant_id')
                    ->references('id')->on('enseignants')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            //
        });
    }
}
