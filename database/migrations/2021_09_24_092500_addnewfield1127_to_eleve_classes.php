<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield1127ToEleveClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            $table->string('observation')->nullable()->change();
            $table->bigInteger('niveau_id')->unsigned()->index()->nullable()->change();
            $table->foreign('niveau_id')
                    ->references('id')->on('niveaux')
                    ->onDelete('cascade');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable()->change();
            $table->foreign('eleve_id')
                    ->references('id')->on('eleves')
                    ->onDelete('cascade');
            $table->bigInteger('classe_id')->unsigned()->index()->nullable()->change();
            $table->foreign('classe_id')
                    ->references('id')->on('sous_niveaus')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eleve_classes', function (Blueprint $table) {
            //
        });
    }
}
