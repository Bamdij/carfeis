<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElevesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleves', function (Blueprint $table) {
            $table->id('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_naissance');
            $table->string('sexe');
            $table->string('avatar_eleve');
            $table->string('groupe_sanguin');
            $table->boolean('is_malade')->default(false);
            $table->text('quelle_maladie')->nullable();
            $table->boolean('is_traitement')->default(false);
            $table->text('quel_traitement')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eleves');
    }
}
