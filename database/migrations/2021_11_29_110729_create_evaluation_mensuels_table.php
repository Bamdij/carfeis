<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationMensuelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_mensuels', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->bigInteger('enseignant_id')->unsigned()->index()->nullable();
            $table->foreign('enseignant_id')
                    ->references('id')->on('enseignants')
                    ->onDelete('cascade');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable();
            $table->foreign('eleve_id')
                    ->references('id')->on('eleves')
                    ->onDelete('cascade');
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                ->references('id')->on('annee_scolaires');
            $table->string('month');
            $table->date('date_evaluation');
            $table->string('sourate');
            $table->string('last_lesson');
            $table->string('first_party');
            $table->string('appreciation_first_party');
            $table->string('second_party');
            $table->string('appreciation_second_party');
            $table->string('third_party');
            $table->string('appreciation_third_party');
            $table->string('comportement_fr')->nullable();
            $table->string('comportement_ar')->nullable();
            $table->string('appreciation');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluation_mensuels');
    }
}
