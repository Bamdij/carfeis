<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSouratesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sourates', function (Blueprint $table) {
            $table->id('id');
            $table->string('num_sourate');
            $table->string('libelle_sourate_fr')->nullable();
            $table->string('libelle_sourate_ar')->nullable();
            $table->string('nbre_verset')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sourates');
    }
}
