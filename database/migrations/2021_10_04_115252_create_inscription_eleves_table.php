<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscriptionElevesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscription_eleves', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable();
            $table->foreign('eleve_id')
                    ->references('id')->on('eleves')
                    ->onDelete('cascade');
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                ->references('id')->on('annee_scolaires')
                ->onDelete('cascade');
            $table->text('classe_id');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inscription_eleves');
    }
}
