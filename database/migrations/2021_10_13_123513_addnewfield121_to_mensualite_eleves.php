<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield121ToMensualiteEleves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mensualite_eleves', function (Blueprint $table) {
            $table->date('periode_debut')->nullable();
            $table->date('periode_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mensualite_eleves', function (Blueprint $table) {
            //
        });
    }
}
