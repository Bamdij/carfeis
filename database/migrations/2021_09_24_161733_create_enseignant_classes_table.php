<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnseignantClassesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enseignant_classes', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('enseignant_id')->unsigned()->index()->nullable();
            $table->boolean('is_teacher_arab')->default(false);
            $table->bigInteger('sous_niveau_id')->unsigned()->index()->nullable();
            $table->foreign('sous_niveau_id')
                    ->references('id')->on('sous_niveaus')
                    ->onDelete('cascade');
            $table->foreign('enseignant_id')
                    ->references('id')->on('enseignants')
                    ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }
   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enseignant_classes');
    }
}
