<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensuualiteEnseignantsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensuualite_enseignants', function (Blueprint $table) {
            $table->id('id');
            $table->string('enseignant_id');
            $table->string('identity_card');
            $table->string('month');
            $table->string('montant');
            $table->string('annee_scolaire_id');
            $table->string('type_paiement_id');
            $table->string('observation')->nullable();
            $table->string('user_id');
            $table->string('month_end');
            $table->boolean('is_payed')->default(false);
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mensuualite_enseignants');
    }
}
