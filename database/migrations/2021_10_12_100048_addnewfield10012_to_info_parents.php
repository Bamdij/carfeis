<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield10012ToInfoParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_parents', function (Blueprint $table) {
            $table-> bigInteger('type_profession_tuteur_id')->unsigned()->index()->nullable();
            $table->foreign('type_profession_tuteur_id')
                        ->references('id')->on('type_professions')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_parents', function (Blueprint $table) {
            //
        });
    }
}
