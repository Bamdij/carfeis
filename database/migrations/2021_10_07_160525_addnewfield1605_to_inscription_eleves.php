<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addnewfield1605ToInscriptionEleves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inscription_eleves', function (Blueprint $table) {
            $table->bigInteger('regime_id')->unsigned()->index()->nullable();
            $table->foreign('regime_id')
                ->references('id')->on('regimes')
                ->onDelete('cascade');
            $table->longText('observation')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscription_eleves', function (Blueprint $table) {
            //
        });
    }
}
