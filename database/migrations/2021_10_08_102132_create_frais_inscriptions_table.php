<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFraisInscriptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frais_inscriptions', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('regime_id')->unsigned()->index()->nullable();
            $table->foreign('regime_id')
                    ->references('id')->on('regimes')
                    ->onDelete('cascade');
            $table->bigInteger('annee_scolaire_id')->unsigned()->index()->nullable();
            $table->foreign('annee_scolaire_id')
                ->references('id')->on('annee_scolaires')
                ->onDelete('cascade');
            $table->string('montant');
            $table->longtext('observation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('frais_inscriptions');
    }
}
