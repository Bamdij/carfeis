<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlphabetisationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alphabetisations', function (Blueprint $table) {
            $table->id('id');
            $table->string('name_alphabetisation');
            $table->string('daara_frequente');
            $table->string('inteligence');
            $table->string('person_inscript');
            $table->string('cap_finance_resp');
            $table->date('date_inscription');
            $table->bigInteger('eleve_id')->unsigned()->index()->nullable();
            $table->foreign('eleve_id')
                    ->references('id')->on('eleves')
                    ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alphabetisations');
    }
}
