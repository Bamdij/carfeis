<?php

namespace App\Repositories;

use App\Models\EvaluationMensuel;
use App\Repositories\BaseRepository;

/**
 * Class EvaluationMensuelRepository
 * @package App\Repositories
 * @version November 29, 2021, 11:07 am UTC
*/

class EvaluationMensuelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'eleve_id',
        'user_id',
        'annee_scolaire_id',
        'month',
        'date_evaluation',
        'sourate',
        'last_lesson',
        'first_party',
        'appreciation_first_party',
        'second_party',
        'appreciation_second_party',
        'third_party',
        'appreciation_third_party',
        'comportement_fr',
        'comportement_ar',
        'appreciation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EvaluationMensuel::class;
    }
}
