<?php

namespace App\Repositories;

use App\Models\TypePaiement;
use App\Repositories\BaseRepository;

/**
 * Class TypePaiementRepository
 * @package App\Repositories
 * @version September 19, 2021, 4:35 pm UTC
*/

class TypePaiementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypePaiement::class;
    }
}
