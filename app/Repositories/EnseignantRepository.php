<?php

namespace App\Repositories;

use App\Models\Enseignant;
use App\Repositories\BaseRepository;

/**
 * Class EnseignantRepository
 * @package App\Repositories
 * @version September 21, 2021, 11:10 am UTC
*/

class EnseignantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'phone',
        'adresse',
        'niveau_etude',
        'date_recrutement',
        'type_enseignant_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Enseignant::class;
    }
}
