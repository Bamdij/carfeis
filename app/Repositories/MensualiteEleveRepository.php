<?php

namespace App\Repositories;

use App\Models\MensualiteEleve;
use App\Repositories\BaseRepository;

/**
 * Class MensualiteEleveRepository
 * @package App\Repositories
 * @version October 10, 2021, 10:12 am UTC
*/

class MensualiteEleveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'paiement_eleve_id',
        'eleve_id',
        'annee_scolaire_id',
        'month',
        'mensualite',
        'observation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MensualiteEleve::class;
    }
}
