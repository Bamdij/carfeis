<?php

namespace App\Repositories;

use App\Models\Alphabetisation;
use App\Repositories\BaseRepository;

/**
 * Class AlphabetisationRepository
 * @package App\Repositories
 * @version September 20, 2021, 5:16 pm UTC
*/

class AlphabetisationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_alphabetisation',
        'daara_frequente',
        'inteligence',
        'person_inscript',
        'cap_finance_resp',
        'date_inscription',
        'eleve_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Alphabetisation::class;
    }
}
