<?php

namespace App\Repositories;

use App\Models\Niveau;
use App\Repositories\BaseRepository;

/**
 * Class NiveauRepository
 * @package App\Repositories
 * @version September 19, 2021, 5:18 pm UTC
*/

class NiveauRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Niveau::class;
    }
}
