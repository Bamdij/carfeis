<?php

namespace App\Repositories;

use App\Models\EvaluationSemestre;
use App\Repositories\BaseRepository;

/**
 * Class EvaluationSemestreRepository
 * @package App\Repositories
 * @version November 26, 2021, 11:20 am UTC
*/

class EvaluationSemestreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'eleve_id',
        'annee_scolaire_id',
        'start_date',
        'end_date',
        'date_evaluation',
        'monday_app',
        'app_mond_1',
        'app_mond_2',
        'app_mon_3',
        'thuesday_app',
        'app_thues_1',
        'app_thues_2',
        'app_thues_3',
        'wednesday_app',
        'app_wed_1',
        'app_wed_2',
        'app_wed_3',
        'thursday_app',
        'app_thurd_1',
        'app_thurd_2',
        'app_thurd_3',
        'friday_app',
        'app_frid_1',
        'app_frid_2',
        'app_frid_3',
        'last_lesson'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EvaluationSemestre::class;
    }
}
