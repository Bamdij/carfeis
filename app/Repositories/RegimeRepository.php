<?php

namespace App\Repositories;

use App\Models\Regime;
use App\Repositories\BaseRepository;

/**
 * Class RegimeRepository
 * @package App\Repositories
 * @version October 7, 2021, 3:56 pm UTC
*/

class RegimeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Regime::class;
    }
}
