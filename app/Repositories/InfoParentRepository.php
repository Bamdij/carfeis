<?php

namespace App\Repositories;

use App\Models\InfoParent;
use App\Repositories\BaseRepository;

/**
 * Class InfoParentRepository
 * @package App\Repositories
 * @version September 20, 2021, 4:29 pm UTC
*/

class InfoParentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'eleve_id',
        'name_pere',
        'phone_pere',
        'email_pere',
        'type_profession_id',
        'name_mere',
        'phone_mere',
        'email_mere',
        'type_profession_mere_id',
        'name_tuteur',
        'lien_parent_id',
        'phone_tuteur',
        'email_tuteur',
        'contact_urgence'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InfoParent::class;
    }
}
