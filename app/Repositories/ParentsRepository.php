<?php

namespace App\Repositories;

use App\Models\Parents;
use App\Repositories\BaseRepository;

/**
 * Class ParentsRepository
 * @package App\Repositories
 * @version December 9, 2021, 11:02 am UTC
*/

class ParentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'eleve_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Parents::class;
    }
}
