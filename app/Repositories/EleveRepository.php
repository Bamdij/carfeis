<?php

namespace App\Repositories;

use App\Models\Eleve;
use App\Repositories\BaseRepository;

/**
 * Class EleveRepository
 * @package App\Repositories
 * @version September 20, 2021, 2:27 pm UTC
*/

class EleveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'date_naissance',
        'sexe',
        'groupe_sanguin',
        'is_malade',
        'quelle_maladie',
        'is_traitement',
        'avatar_eleve',
        'quel_traitement'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Eleve::class;
    }
}
