<?php

namespace App\Repositories;

use App\Models\AproposCarfeis;
use App\Repositories\BaseRepository;

/**
 * Class AproposCarfeisRepository
 * @package App\Repositories
 * @version December 11, 2021, 5:23 pm UTC
*/

class AproposCarfeisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AproposCarfeis::class;
    }
}
