<?php

namespace App\Repositories;

use App\Models\Sourate;
use App\Repositories\BaseRepository;

/**
 * Class SourateRepository
 * @package App\Repositories
 * @version December 4, 2021, 1:23 pm UTC
*/

class SourateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'num_sourate',
        'libelle_sourate_fr',
        'libelle_sourate_ar',
        'nbre_verset'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sourate::class;
    }
}
