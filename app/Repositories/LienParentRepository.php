<?php

namespace App\Repositories;

use App\Models\LienParent;
use App\Repositories\BaseRepository;

/**
 * Class LienParentRepository
 * @package App\Repositories
 * @version September 18, 2021, 5:41 pm UTC
*/

class LienParentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LienParent::class;
    }
}
