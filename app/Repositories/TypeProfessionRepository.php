<?php

namespace App\Repositories;

use App\Models\TypeProfession;
use App\Repositories\BaseRepository;

/**
 * Class TypeProfessionRepository
 * @package App\Repositories
 * @version September 17, 2021, 10:42 pm UTC
*/

class TypeProfessionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeProfession::class;
    }
}
