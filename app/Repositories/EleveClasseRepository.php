<?php

namespace App\Repositories;

use App\Models\EleveClasse;
use App\Repositories\BaseRepository;

/**
 * Class EleveClasseRepository
 * @package App\Repositories
 * @version September 23, 2021, 6:03 pm UTC
*/

class EleveClasseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'niveau_id',
        'classe_id',
        'eleve_id',
        'observation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EleveClasse::class;
    }
}
