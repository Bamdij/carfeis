<?php

namespace App\Repositories;

use App\Models\DepenseCategorie;
use App\Repositories\BaseRepository;

/**
 * Class DepenseCategorieRepository
 * @package App\Repositories
 * @version October 13, 2021, 1:31 pm UTC
*/

class DepenseCategorieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_depense_id',
        'date_depense',
        'designation_depense',
        'montant_depense',
        'user_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DepenseCategorie::class;
    }
}
