<?php

namespace App\Repositories;

use App\Models\EnseignantAnnee;
use App\Repositories\BaseRepository;

/**
 * Class EnseignantAnneeRepository
 * @package App\Repositories
 * @version October 9, 2021, 1:04 pm UTC
*/

class EnseignantAnneeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'enseignant_id',
        'annee_scolaire_id',
        'type_enseignant_id',
        'niveau_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EnseignantAnnee::class;
    }
}
