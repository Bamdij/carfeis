<?php

namespace App\Repositories;

use App\Models\ParentPaiementAnnuelle;
use App\Repositories\BaseRepository;

/**
 * Class ParentPaiementAnnuelleRepository
 * @package App\Repositories
 * @version December 29, 2021, 11:43 am UTC
*/

class ParentPaiementAnnuelleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'annee_scloaire_id',
        'parent_id',
        'janvier',
        'payed_janv',
        'fevrier',
        'payed_fev',
        'mars',
        'payed_mars',
        'avril',
        'payed_avril',
        'mai',
        'mai_payed',
        'juin',
        'juin_payed',
        'juillett',
        'payed_jul',
        'aout',
        'payed_aout',
        'septembre',
        'payed_sept',
        'octobre',
        'payed_octobre',
        'novembre',
        'payed_nov',
        'decembre',
        'payed_dec',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ParentPaiementAnnuelle::class;
    }
}
