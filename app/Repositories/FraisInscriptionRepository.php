<?php

namespace App\Repositories;

use App\Models\FraisInscription;
use App\Repositories\BaseRepository;

/**
 * Class FraisInscriptionRepository
 * @package App\Repositories
 * @version October 8, 2021, 10:21 am UTC
*/

class FraisInscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'regime_id',
        'annee_scolaire_id',
        'montant',
        'observation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FraisInscription::class;
    }
}
