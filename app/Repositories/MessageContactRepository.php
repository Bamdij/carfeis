<?php

namespace App\Repositories;

use App\Models\MessageContact;
use App\Repositories\BaseRepository;

/**
 * Class MessageContactRepository
 * @package App\Repositories
 * @version December 10, 2021, 5:55 pm UTC
*/

class MessageContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'obje',
        'description',
        'user_id',
        'is_admin',
        'slug',
        'is_parent_sending',
        'admin_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MessageContact::class;
    }
}
