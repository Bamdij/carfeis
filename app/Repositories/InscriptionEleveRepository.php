<?php

namespace App\Repositories;

use App\Models\InscriptionEleve;
use App\Repositories\BaseRepository;

/**
 * Class InscriptionEleveRepository
 * @package App\Repositories
 * @version October 4, 2021, 11:52 am UTC
*/

class InscriptionEleveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'eleve_id',
        'annee_scolaire_id',
        'classe_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InscriptionEleve::class;
    }
}
