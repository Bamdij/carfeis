<?php

namespace App\Repositories;

use App\Models\PaiementEleve;
use App\Repositories\BaseRepository;

/**
 * Class PaiementEleveRepository
 * @package App\Repositories
 * @version October 8, 2021, 12:31 pm UTC
*/

class PaiementEleveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'eleve_id',
        'frais_inscription_id',
        'annee_scolaire_id',
        'mois_1',
        'montant_1',
        'mois_2',
        'montant_2',
        'mois_3',
        'montant_3',
        'mois_4',
        'montant_5',
        'mois_6',
        'montant_6',
        'mois_7',
        'montant_7',
        'mois_8',
        'montant_8',
        'mois_9',
        'montant_9',
        'mois_10',
        'montant_10',
        'mois_11',
        'montant_11',
        'mois_12',
        'montant_12',
        'obseration_1',
        'obseration_2',
        'obseration_3',
        'obseration_4',
        'obseration_5',
        'obseration_6',
        'obseration_7',
        'obseration_8',
        'obseration_9',
        'obseration_10',
        'obseration_11',
        'obseration_12'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaiementEleve::class;
    }
}
