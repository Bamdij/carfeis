<?php

namespace App\Repositories;

use App\Models\EnseignantClasse;
use App\Repositories\BaseRepository;

/**
 * Class EnseignantClasseRepository
 * @package App\Repositories
 * @version September 24, 2021, 4:17 pm UTC
*/

class EnseignantClasseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'enseignant_id',
        'sous_niveau_id',
        'is_teacher_arab'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EnseignantClasse::class;
    }
}
