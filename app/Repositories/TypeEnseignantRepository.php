<?php

namespace App\Repositories;

use App\Models\TypeEnseignant;
use App\Repositories\BaseRepository;

/**
 * Class TypeEnseignantRepository
 * @package App\Repositories
 * @version September 19, 2021, 4:04 pm UTC
*/

class TypeEnseignantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeEnseignant::class;
    }
}
