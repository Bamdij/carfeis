<?php

namespace App\Repositories;

use App\Models\CategoryDepense;
use App\Repositories\BaseRepository;

/**
 * Class CategoryDepenseRepository
 * @package App\Repositories
 * @version September 19, 2021, 4:56 pm UTC
*/

class CategoryDepenseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryDepense::class;
    }
}
