<?php

namespace App\Repositories;

use App\Models\MensuualiteEnseignant;
use App\Repositories\BaseRepository;

/**
 * Class MensuualiteEnseignantRepository
 * @package App\Repositories
 * @version October 14, 2021, 11:39 am UTC
*/

class MensuualiteEnseignantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'enseignant_id',
        'identity_card',
        'month',
        'montant',
        'annee_scolaire_id',
        'type_paiement_id',
        'observation',
        'user_id',
        'month_end',
        'is_payed'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MensuualiteEnseignant::class;
    }
}
