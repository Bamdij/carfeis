<?php

namespace App\Repositories;

use App\Models\AnneeScolaire;
use App\Repositories\BaseRepository;

/**
 * Class AnneeScolaireRepository
 * @package App\Repositories
 * @version September 30, 2021, 9:24 am UTC
*/

class AnneeScolaireRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AnneeScolaire::class;
    }
}
