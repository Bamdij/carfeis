<?php

namespace App\Repositories;

use App\Models\SousNiveau;
use App\Repositories\BaseRepository;

/**
 * Class SousNiveauRepository
 * @package App\Repositories
 * @version September 19, 2021, 6:10 pm UTC
*/

class SousNiveauRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SousNiveau::class;
    }
}
