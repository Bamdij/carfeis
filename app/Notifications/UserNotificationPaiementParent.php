<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;
use Carbon\Carbon;


class UserNotificationPaiementParent extends Notification
{
    use Queueable;
    use Queueable;
    public $use_parent;
    public $month_update;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($use_parent,$month_update)
    {
        $this->use_parent = $use_parent;
        $this->month_update = $month_update;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('GRH] - Paiement Mensuel de l\'eleve '.$this->month_update->eleve->first_name.'   '.$this->month_update->eleve->last_name.' sur GRH')
                ->greeting('Bonjour M. '. $this->use_parent->name.' ')
                ->line($this->month_update->user->name.'  Votre paiement d\'un '.$this->month_update->mensualite.' vient d\'être reglé avec success')
                ->action('Telecharger votre Facture', url($this->month_update->invoice_url))
                ->line('Nous vous souhaitons une belle expérience sur l\'App GRH!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
