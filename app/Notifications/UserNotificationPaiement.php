<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;
use Carbon\Carbon;


class UserNotificationPaiement extends Notification
{
    use Queueable;
    use Queueable;
    public $user_name;
    public $month_update;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_name,$month_update)
    {
        $this->user_name = $user_name;
        $this->month_update = $month_update;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('GRH] - Paiement Mensuel de l\'eleve '.$this->month_update->eleve->first_name.'   '.$this->month_update->eleve->last_name.' sur GRH')
                ->greeting('Bonjour M. '. $this->user_name->name.' ')
                ->line($this->month_update->user->name.'  vient d\'éffectuer un paiement mensuel d\'une valeur de   '.$this->month_update->mensualite.' pour la periode de'
                    .$this->month_update->periode_debut.'  à  '.$this->month_update->periode_end.' 
                    dont le montant attendu est '  .$this->month_update->montant_attendu.'
                  de l\'eleve '  .$this->month_update->eleve->first_name.'   '.$this->month_update->eleve->last_name.'
                 née le '.$this->month_update->eleve->date_naissance. ' à ' . $this->month_update->eleve->lieu_naissance.' de Matricule ' .$this->month_update->eleve->numero. ' Date de paiement : '.$this->month_update->created_at. ' Numero de recu :   '. $this->month_update->numero .'')
                ->action('Connectez-vous', url('/login'))
                ->line('Nous vous souhaitons une belle expérience sur l\'App GRH!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
