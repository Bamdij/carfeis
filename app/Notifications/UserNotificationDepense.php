<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;
use Carbon\Carbon;


class UserNotificationDepense extends Notification
{
    use Queueable;
    use Queueable;
    public $user_name;
    public $depenseCategorie;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_name,$depenseCategorie)
    {
        $this->user_name = $user_name;
        $this->depenseCategorie = $depenseCategorie;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('GRH] - Depenses Effectuer '.$this->depenseCategorie->categorie->name. ' sur GRH')
                ->greeting('Bonjour M. '. $this->user_name->name.' ')
                ->line($this->depenseCategorie->user->name.'  vient de donner une somme de  '.$this->depenseCategorie->montant_depense.' pour une depense en  '
                    .$this->depenseCategorie->categorie->name.'  avec comme designation '.$this->depenseCategorie->designation_depense.' 
                    dont la date est le  '  .$this->depenseCategorie->date_depense.'
                  mais ajouté dans la plateforme le  '  .$this->depenseCategorie->created_at.' '
                  )
                ->action('Connectez-vous', url('/login'))
                ->line('Nous vous souhaitons une belle expérience sur l\'App GRH!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
