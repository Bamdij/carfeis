<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;

class UserNotification extends Notification
{
    use Queueable;
    use Queueable;
    public $user;
    public $generatedPassword;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$generatedPassword)
    {
        $this->user = $user;
        $this->generatedPassword = $generatedPassword;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('GRH] - Votre compte '.$this->user->status.' sur GRH')
                ->greeting('Hello '. $this->user->name.' ')
                ->line('Un adminstrateur de GRH vient de vous créer un compte '.$this->user->status.' avec les identifiants suivants :
                         Email : '.$this->user->email.'
                         Mot de passe :  '. $this->generatedPassword .'')
                ->action('Connectez-vous', url('/login'))
                ->line('Nous vous souhaitons une belle expérience sur l\'App GRH!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
