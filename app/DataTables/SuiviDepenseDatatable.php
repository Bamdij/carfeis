<?php
namespace App\DataTables;

use App\Models\DepenseCategorie;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class SuiviDepenseDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);

            return  $dataTable->addColumn('action', 'depense_categories.datatables_actions');           
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\DepenseCategorie $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DepenseCategorie $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            // ['data' => 'annee_scolaire_id', 'name' => 'annee_scolaire_id', 'title' => 'Année scloaire'],
            ['data' => 'category_depense_id', 'category_depense_id' => 'name', 'title' => 'Type de Dépense'],
            ['data' => 'designation_depense', 'name' => 'designation_depense', 'title' => 'Designation'],
            ['data' => 'montant_depense', 'name' => 'montant_depense', 'title' => 'Montant'],
            ['data' => 'date_depense', 'name' => 'date_depense', 'title' => 'Date'],
            ['data' => 'user_id', 'name' => 'user_id', 'title' => 'Prenom et nom'],
            ['data' => 'category_depense_id', 'name' => 'name', 'title' => 'Type de Dépense'],
            
        ]);
    } 

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sen_gt__' . time(); 
    } 

}
 