<?php
namespace App\DataTables;

use App\Models\FraisInscription;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class FraisInscriptionDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

            return $dataTable->addColumn('regime_id', function($data){
                if(!empty($data->regime_id)){
                    return $data->regime->name.' '.$data->regime->status;
                }else{
                    return '<span class="badge badge-info">No renseigner</span>';
                }
            })
            ->addColumn('annee_scolaire_id', function($data){
                if(!empty($data->annee_scolaire_id)){
                    return $data->annee_scolaire->name;
                }else{
                    return '<span class="badge badge-info">No renseigner</span>';
                }           
             })
            ->addColumn('action', 'frais_inscriptions.datatables_actions')
            ->rawColumns(['action', 'regime_id','annee_scolaire_id']);
           
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\FraisInscription $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FraisInscription $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            ['data' => 'annee_scolaire_id', 'name' => 'annee_scolaire_id', 'title' => 'Annee Scolaire'],
            ['data' => 'regime_id', 'name' => 'regime_id', 'title' => 'Regime'],
            ['data' => 'montant', 'name' => 'montant', 'title' => 'montant'],
            // ['data' => 'observation', 'name' => 'observation', 'title' => 'Observation'],
            
        ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sen_gt__' . time();
    }

}
