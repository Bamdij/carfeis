<?php
namespace App\DataTables;

use App\Models\Regime;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class RegimeDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
            
            $dataTable = new EloquentDataTable($query);

            return $dataTable->addColumn('status', function($data){
                if ($data->status == null){
                    return '
                    <span class="badge badge-secondary">Info non disponible</span>';
                }
                if ($data->status == "Paiement-integral"){
                    return '
                    <span class="badge badge-success">Paiement Integral</span>';
                }
                if ($data->status == "Paiement-partiel"){
                    return '
                    <span class="badge badge-warning">Paiement Partiel</span>';
                }
                if ($data->status == "Prise-en-charge"){
                    return '
                    <span class="badge badge-info">Prise En Charge</span>';
                }
                if ($data->status == "social"){
                    return '
                    <span class="badge badge-danger">Cas social</span>';
                }
            })
        ->addColumn('action', 'regimes.datatables_actions')
        ->rawColumns(['action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Regime $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Regime $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            ['data' => 'name', 'name' => 'name', 'title' => 'Nom du Regime'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Type De Regime'],
            ['data' => 'mensualite', 'name' => 'mensualite', 'title' => 'Paiement Mensuel'],
            
        ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sen_gt__' . time();
    }

}
