<?php
namespace App\DataTables;

use App\Models\Enseignant;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class EnseignantDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query->orderBy('id','desc'));

            return $dataTable->addColumn('date_recrutement', function($data){
                    return \Carbon\Carbon::parse($data->date_recrutement)->format('d/m/Y');

            })   
            
            ->addColumn('action', 'enseignants.datatables_actions')
            ->rawColumns(['action', 'is_active_access','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Enseignant $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Enseignant $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            ['data' => 'numero', 'name' => 'numero', 'title' => 'Matricule'],
            ['data' => 'first_name', 'name' => 'first_name', 'title' => 'Prenom'],
            ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Nom'],
            ['data' => 'niveau_etude', 'name' => 'niveau_etude', 'title' => 'Niveau d\'etude'],
            ['data' => 'date_recrutement', 'name' => 'date_recrutement', 'title' => 'Date de recrutement'],
            
        ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sen_gt__' . time();
    }

}
