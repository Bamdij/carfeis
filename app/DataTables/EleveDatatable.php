<?php
namespace App\DataTables;

use App\Models\Eleve;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class EleveDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable =new EloquentDataTable($query->orderBy('id','desc'));
        return $dataTable->addColumn('avatar_eleve',function($data){
            if($data->avatar_eleve != null){
             return '<img src="'.asset($data->avatar_eleve).'" height="50" width="50" border="0"  class="img-rounded" align="center" />';
            }else{
                return '<span class="label label-info ">No image</span>';
            }
            })
            ->addColumn('date_naissance', function($data){
                return \Carbon\Carbon::parse($data->date_naissance)->format('d/m/Y');
                })
            // ->addColumn('created_at', function($data){
            //         return \Carbon\Carbon::parse($data->crated_at);
            //         })
            ->addColumn('action', 'eleves.datatables_actions')
            ->rawColumns(['avatar_eleve','date_naissance','created_at','action']);         
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Eleve $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Eleve $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            ['data' => 'numero', 'name' => 'numero', 'title' => 'Matricule'],
            ['data' => 'first_name', 'name' => 'first_name', 'title' => 'Prenom'],
            ['data' => 'last_name', 'name' => 'last_name', 'title' => 'Nom'],
            ['data' => 'date_naissance', 'name' => 'date_naissance', 'title' => 'Date naissance'],
            ['data' => 'lieu_naissance', 'name' => 'lieu_naissance', 'title' => 'Lieu de naissance'],
            // ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date\'inscription'],
            ['data' => 'avatar_eleve', 'name' => 'avatar_eleve', 'title' => 'Photo'],
            
        ]);
    }

    /**  
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sen_gt__' . time();
    }

}
