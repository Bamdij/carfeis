<?php
namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class ParentDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query->orderBy('id','desc')->where('status','Parent'));

        return $dataTable->addColumn('status', function($data){
                if ($data->status == "superadmin"){
                    return 'Administrateur';
                }
                if ($data->status == "directeur"){
                    return 'Directeur Institut';
                }
                if ($data->status == "responsable_pedagogique"){
                    return 'Responsable Pédagogique';
                }
                if ($data->status == "responsable_finance"){
                    return 'Responsable Financier';
                }
                if ($data->status == "Parent"){
                    return 'Parent';
                }
                if ($data->status == "responsable_professeur"){
                    return 'Responsable Evaluation';
                }
                if ($data->status == "1"){
                    return 'Enseignant';
                }
            })

            ->addColumn('action', 'parents.datatables_actions')
            ->rawColumns(['action', 'status']);
            
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    // ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ([
            ['data' => 'name', 'name' => 'name', 'title' => 'Prénom et Nom'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email.'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Profil Utilisateur'],
        ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'export_entreprise_' . time();
    }

}
