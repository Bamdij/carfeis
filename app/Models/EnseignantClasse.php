<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Cviebrock\EloquentSluggable\Sluggable; 
/**
 * Class EnseignantClasse
 * @package App\Models
 * @version September 24, 2021, 4:17 pm UTC
 *
 * @property integer $enseignant_id
 * @property integer $sous_niveau_id
 * @property boolean $is_teacher_arab
 */
class EnseignantClasse extends Model
{
    use SoftDeletes;
    // use Sluggable;

    use HasFactory;

    public $table = 'enseignant_classes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'enseignant_id',
        'sous_niveau_id',
        'is_teacher_arab'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'enseignant_id' => 'integer',
        'sous_niveau_id' => 'integer',
        'is_teacher_arab' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'enseignant_id' => 'required'
    ];

    //      /**
    //  * Return the sluggable configuration array for this model.
    //  *
    //  * @return array
    //  */
    // public function sluggable(): array
    // {
    //     return [
    //         'slug' => [
    //             'source' => ['enseignant_id','sous_niveau_id','is_teacher_arab'],
    //         ]
    //     ];
    // }
}
