<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class TypeEnseignant
 * @package App\Models
 * @version September 19, 2021, 4:04 pm UTC
 *
 * @property string $name
 * @property string $slug
 */
class TypeEnseignant extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'type_enseignants';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => ''
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
     /**
     * Get all of the candidatureOffres for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enseignant()
    {
        return $this->hasMany(Enseignant::class, 'type_enseignant_id');
    }
     /**
     * Get all of the candidatureOffres for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function niveaux()
    {
        return $this->hasMany(Niveau::class, 'type_enseignant_id');
    }
       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enseignant_annee()
    {
        return $this->hasMany(EnseignantAnnee::class, 'type_enseignant_id');
    }
}
