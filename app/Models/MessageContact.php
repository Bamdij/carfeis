<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class MessageContact
 * @package App\Models
 * @version December 10, 2021, 5:55 pm UTC
 *
 * @property string $obje
 * @property string $description
 * @property integer $user_id
 * @property boolean $is_admin
 * @property string $slug
 * @property boolean $is_parent_sending
 * @property integer $admin_id
 */
class MessageContact extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'message_contacts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'obje',
        'description',
        'user_id',
        'is_admin',
        'slug',
        'is_parent_sending',
        'admin_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'obje' => 'string',
        'description' => 'string',
        'user_id' => 'integer',
        'is_admin' => 'boolean',
        'slug' => 'string',
        'is_parent_sending' => 'boolean',
        'admin_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'obje' => 'required',
        'user_id' => 'required'
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                 'source' => 'objet',
            ]
        ];
    }
}
