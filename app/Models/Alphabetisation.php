<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Cviebrock\EloquentSluggable\Sluggable;
/**
 * Class Alphabetisation
 * @package App\Models
 * @version September 20, 2021, 5:16 pm UTC
 *
 * @property string $name_alphabetisation
 * @property string $daara_frequente
 * @property string $inteligence
 * @property string $person_inscript
 * @property string $cap_finance_resp
 * @property string $date_inscription
 * @property integer $eleve_id
 */
class Alphabetisation extends Model
{
    use SoftDeletes;
    // use Sluggable;

    use HasFactory;

    public $table = 'alphabetisations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name_alphabetisation',
        'daara_frequente',
        'inteligence',
        'person_inscript',
        'cap_finance_resp',
        'date_inscription',
        'eleve_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name_alphabetisation' => 'string',
        'daara_frequente' => 'string',
        'inteligence' => 'string',
        'person_inscript' => 'string',
        'cap_finance_resp' => 'string',
        'date_inscription' => 'date',
        'eleve_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name_alphabetisation' => 'required',
        'eleve_id' => 'required'
    ];

     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eleve()
    {
        return $this->belongs(Eleve::class, 'eleve_id');
    }

      /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    // public function sluggable(): array
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'name_alphabetisation',
    //         ]
    //     ];
    // } 
  
}
