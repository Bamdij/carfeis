<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;   


/**
 * Class PaiementEleve
 * @package App\Models
 * @version October 8, 2021, 12:31 pm UTC
 *
 * @property string $eleve_id
 * @property string $frais_inscription_id
 * @property string $annee_scolaire_id
 * @property string $mois_1
 * @property string $montant_1
 * @property string $mois_2
 * @property string $montant_2
 * @property string $mois_3
 * @property string $montant_3
 * @property string $mois_4
 * @property string $montant_5
 * @property string $mois_6
 * @property string $montant_6
 * @property string $mois_7
 * @property string $montant_7
 * @property string $mois_8
 * @property string $montant_8
 * @property string $mois_9
 * @property string $montant_9
 * @property string $mois_10
 * @property string $montant_10
 * @property string $mois_11
 * @property string $montant_11
 * @property string $mois_12
 * @property string $montant_12
 * @property string $obseration_1
 * @property string $obseration_2
 * @property string $obseration_3
 * @property string $obseration_4
 * @property string $obseration_5
 * @property string $obseration_6
 * @property string $obseration_7
 * @property string $obseration_8
 * @property string $obseration_9
 * @property string $obseration_10
 * @property string $obseration_11
 * @property string $obseration_12
 */
class PaiementEleve extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'paiement_eleves';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'eleve_id',
        'frais_inscription_id',
        'annee_scolaire_id',
        'mois_1',
        'montant_1',
        'mois_2',
        'montant_2',
        'mois_3',
        'montant_3',
        'mois_4',
        'montant_5',
        'mois_6',
        'montant_6',
        'mois_7',
        'montant_7',
        'mois_8',
        'montant_8',
        'mois_9',
        'montant_9',
        'mois_10',
        'montant_10',
        'mois_11',
        'montant_11',
        'mois_12',
        'montant_12',
        'observation_1',
        'observation_2',
        'observation_3',
        'observation_4',
        'observation_5',
        'observation_6',
        'observation_7',
        'observation_8',
        'observation_9',
        'observation_10',
        'observation_11',
        'montant_inscription',
        'observation_inscription',
        'slug',
        'is_confirme',
        'observation_12',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'eleve_id' => 'string',
        'frais_inscription_id' => 'string',
        'annee_scolaire_id' => 'string',
        'mois_1' => 'string',
        'montant_1' => 'string',
        'mois_2' => 'string',
        'montant_2' => 'string',
        'mois_3' => 'string',
        'montant_3' => 'string',
        'mois_4' => 'string',
        'montant_5' => 'string',
        'mois_6' => 'string',
        'montant_6' => 'string',
        'mois_7' => 'string',
        'montant_7' => 'string',
        'mois_8' => 'string',
        'montant_8' => 'string',
        'mois_9' => 'string',
        'montant_9' => 'string',
        'mois_10' => 'string',
        'montant_10' => 'string',
        'mois_11' => 'string',
        'montant_11' => 'string',
        'mois_12' => 'string',
        'montant_12' => 'string',
        'observation_1' => 'string',
        'observation_2' => 'string',
        'observation_3' => 'string',
        'observation_4' => 'string',
        'observation_5' => 'string',
        'observation_6' => 'string',
        'observation_7' => 'string',
        'observation_8' => 'string',
        'observation_9' => 'string',
        'observation_10' => 'string',
        'observation_11' => 'string',
        'observation_12' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'eleve_id' => 'required',
        'frais_inscription_id' => 'required',
        'annee_scolaire_id' => 'required'
    ];
   /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function frais_inscription()
    {
        return $this->belongsTo(FraisInscription::class, 'frais_inscription_id');
    }
    /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eleve()
    {
        return $this->belongsTo(Eleve::class, 'eleve_id');
    }
     /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    }
 /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

         /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['anne_scolaire_id','eleve_id','frais_inscription_id'],
            ]
        ];
    } 
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensualite_eleve()
    {
        return $this->hasMany(MensualiteEleve::class, 'paiement_eleve_id');
    }  

    
}
