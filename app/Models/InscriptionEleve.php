<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class InscriptionEleve
 * @package App\Models
 * @version October 4, 2021, 11:52 am UTC
 *
 * @property integer $eleve_id
 * @property integer $annee_scolaire_id
 * @property string $classe_id
 * @property string $description
 */
class InscriptionEleve extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'inscription_eleves';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'eleve_id',
        'annee_scolaire_id',
        'classe_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'eleve_id' => 'integer',
        'annee_scolaire_id' => 'integer',
        'classe_id' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'eleve_id' => 'required',
        'annee_scolaire_id' => 'required'
        
    ];

    
}
