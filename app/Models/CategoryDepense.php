<?php

namespace App\Models;

use Eloquent as Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CategoryDepense
 * @package App\Models
 * @version September 19, 2021, 4:56 pm UTC
 *
 * @property string $name
 * @property string $slug
 */
class CategoryDepense extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'category_depenses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => ''
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depense()
    {
        return $this->hasMany(DepenseCategory::class, 'category_depense_id');
    }

}
