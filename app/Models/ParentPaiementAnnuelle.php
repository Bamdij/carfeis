<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ParentPaiementAnnuelle
 * @package App\Models
 * @version December 29, 2021, 11:43 am UTC
 *
 * @property integer $annee_scloaire_id
 * @property integer $parent_id
 * @property string $janvier
 * @property boolean $payed_janv
 * @property string $fevrier
 * @property boolean $payed_fev
 * @property string $mars
 * @property boolean $payed_mars
 * @property string $avril
 * @property boolean $payed_avril
 * @property string $mai
 * @property boolean $mai_payed
 * @property string $juin
 * @property boolean $juin_payed
 * @property string $juillett
 * @property boolean $payed_jul
 * @property string $aout
 * @property boolean $payed_aout
 * @property string $septembre
 * @property boolean $payed_sept
 * @property sting $octobre
 * @property boolean $payed_octobre
 * @property string $novembre
 * @property boolean $payed_nov
 * @property string $decembre
 * @property boolean $payed_dec
 * @property boolean $status
 */
class ParentPaiementAnnuelle extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'parent_paiement_annuelles';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'eleve_id',
        'annee_scloaire_id',
        'parent_id',
        'janvier',
        'payed_janv',
        'fevrier',
        'payed_fev',
        'mars',
        'payed_mars',
        'avril',
        'payed_avril',
        'mai',
        'mai_payed',
        'juin',
        'juin_payed',
        'juillett',
        'payed_jul',
        'aout',
        'payed_aout',
        'septembre',
        'payed_sept',
        'octobre',
        'payed_octobre',
        'novembre',
        'payed_nov',
        'decembre',
        'payed_dec',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'annee_scloaire_id' => 'integer',
        'parent_id' => 'integer',
        'janvier' => 'string',
        'payed_janv' => 'boolean',
        'fevrier' => 'string',
        'payed_fev' => 'boolean',
        'mars' => 'string',
        'payed_mars' => 'boolean',
        'avril' => 'string',
        'payed_avril' => 'boolean',
        'mai' => 'string',
        'mai_payed' => 'boolean',
        'juin' => 'string',
        'juin_payed' => 'boolean',
        'juillett' => 'string',
        'payed_jul' => 'boolean',
        'aout' => 'string',
        'payed_aout' => 'boolean',
        'septembre' => 'string',
        'payed_sept' => 'boolean',
        'payed_octobre' => 'boolean',
        'novembre' => 'string',
        'payed_nov' => 'boolean',
        'decembre' => 'string',
        'payed_dec' => 'boolean',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'annee_scloaire_id' => 'required',
        'parent_id' => 'required',
        // 'mai' => 'mai_payed boolean string'
    ];

    
}
