<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class InfoParent
 * @package App\Models
 * @version September 20, 2021, 4:29 pm UTC
 *
 * @property integer $eleve_id
 * @property string $name_pere
 * @property string $phone_pere
 * @property string $email_pere
 * @property integer $type_profession_id
 * @property string $name_mere
 * @property string $phone_mere
 * @property string $email_mere
 * @property integer $type_profession_mere_id
 * @property string $name_tuteur
 * @property integer $lien_parent_id
 * @property string $phone_tuteur
 * @property string $email_tuteur
 * @property string $contact_urgence
 */
class InfoParent extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'info_parents';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'eleve_id',
        'name_pere',
        'phone_pere',
        'email_pere',
        'type_profession_id',
        'name_mere',
        'phone_mere',
        'email_mere',
        'type_profession_mere_id',
        'name_tuteur',
        'lien_parent_id',
        'phone_tuteur',
        'email_tuteur',
        'contact_urgence',
        'is_parent_tuteur',
        'type_profession_tuteur_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'eleve_id' => 'integer',
        'name_pere' => 'string',
        'phone_pere' => 'string',
        'email_pere' => 'string',
        'type_profession_id' => 'integer',
        'name_mere' => 'string',
        'phone_mere' => 'string',
        'email_mere' => 'string',
        'type_profession_mere_id' => 'integer',
        'name_tuteur' => 'string',
        'lien_parent_id' => 'integer',
        'phone_tuteur' => 'string',
        'email_tuteur' => 'string',
        'contact_urgence' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'eleve_id' => 'required'
    ];

     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eleve()
    {
        return $this->belongsTo(Eleve::class, 'eleve_id');
    }
     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type_profession()
    {
        return $this->belongsTo(TypeProfession::class, 'type_profession_id');
    }

      
}
