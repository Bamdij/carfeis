<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable; 

/**
 * Class Enseignant
 * @package App\Models
 * @version September 21, 2021, 11:10 am UTC
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $adresse
 * @property string $niveau_etude
 * @property string $date_recrutement
 * @property integer $type_enseignant_id
 */
class Enseignant extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'enseignants';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'first_name',
        'last_name',
        'phone',
        'adresse',
        'niveau_etude',
        'date_recrutement',
        'type_enseignant_id',
        'annee_scolaire_id',
        'numero',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'first_name' => 'string',
        'last_name' => 'string',
        'phone' => 'string',
        'adresse' => 'string',
        'niveau_etude' => 'string',
        'date_recrutement' => 'date',
        'type_enseignant_id' => 'integer',
        'annee_scolaire_id'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required'
    ];

    /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type_enseignant()
    {
        return $this->belongsTo(TypeEnseignant::class, 'type_enseignant_id');
    }


          /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['first_name','last_name'],
            ]
        ];
    } 

      /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensualite_enseignant()
    {
        return $this->hasMany(MensuualiteEnseignat::class, 'enseignant_id');
    }

        /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enseignant_annee()
    {
        return $this->hasMany(EnseignantAnnee::class, 'enseignant_id');
    }
}
