<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class EvaluationSemestre
 * @package App\Models
 * @version November 26, 2021, 11:20 am UTC
 *
 * @property string $user_id
 * @property integer $eleve_id
 * @property integer $annee_scolaire_id
 * @property string $start_date
 * @property string $end_date
 * @property string $date_evaluation
 * @property string $monday_app
 * @property string $app_mond_1
 * @property string $app_mond_2
 * @property string $app_mon_3
 * @property string $thuesday_app
 * @property string $app_thues_1
 * @property string $app_thues_2
 * @property string $app_thues_3
 * @property string $wednesday_app
 * @property string $app_wed_1
 * @property string $app_wed_2
 * @property string $app_wed_3
 * @property string $thursday_app
 * @property string $app_thurd_1
 * @property string $app_thurd_2
 * @property string $app_thurd_3
 * @property string $friday_app
 * @property string $app_frid_1
 * @property string $app_frid_2
 * @property string $app_frid_3
 * @property string $last_lesson
 */
class EvaluationSemestre extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'evaluation_semestres';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'eleve_id',
        'annee_scolaire_id',
        'start_date',
        'end_date',
        'date_evaluation',
        'monday_app',
        'app_mond_1',
        'app_mond_2',
        'app_mon_3',
        'thuesday_app',
        'app_thues_1',
        'app_thues_2',
        'app_thues_3',
        'wednesday_app',
        'app_wed_1',
        'app_wed_2',
        'app_wed_3',
        'thursday_app',
        'app_thurd_1',
        'app_thurd_2',
        'app_thurd_3',
        'friday_app',
        'app_frid_1',
        'app_frid_2',
        'app_frid_3',
        'last_lesson'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'string',
        'eleve_id' => 'integer',
        'annee_scolaire_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'date_evaluation' => 'date',
        'monday_app' => 'string',
        'app_mond_1' => 'string',
        'app_mond_2' => 'string',
        'app_mon_3' => 'string',
        'thuesday_app' => 'string',
        'app_thues_1' => 'string',
        'app_thues_2' => 'string',
        'app_thues_3' => 'string',
        'wednesday_app' => 'string',
        'app_wed_1' => 'string',
        'app_wed_2' => 'string',
        'app_wed_3' => 'string',
        'thursday_app' => 'string',
        'app_thurd_1' => 'string',
        'app_thurd_2' => 'string',
        'app_thurd_3' => 'string',
        'friday_app' => 'string',
        'app_frid_1' => 'string',
        'app_frid_2' => 'string',
        'app_frid_3' => 'string',
        'last_lesson' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'eleve_id' => 'required',
        'annee_scolaire_id' => 'required',
        'last_lesson' => 'required'
    ];

    /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    } 

     /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
