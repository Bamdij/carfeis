<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;   

/**
 * Class DepenseCategorie
 * @package App\Models
 * @version October 13, 2021, 1:31 pm UTC
 *
 * @property integer $category_depense_id
 * @property string $date_depense
 * @property string $designation_depense
 * @property string $montant_depense
 * @property string $user_id
 * @property string $description
 */
class DepenseCategorie extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'depense_categories';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'category_depense_id',
        'date_depense',
        'designation_depense',
        'montant_depense',
        'user_id',
        'description',
        'annee_scolaire_id',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_depense_id' => 'integer',
        'date_depense' => 'date',
        'designation_depense' => 'string',
        'montant_depense' => 'string',
        'user_id' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_depense_id' => 'required',
        'date_depense' => 'required',
        'designation_depense' => 'required',
        'montant_depense' => 'required',
        'user_id' => 'required'
    ];

      /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                // 'source' => 'first_name',
                 'source' => ['date_depense','montant_depense','designation_depense']
            ]
        ];
    }
     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorie()
    {
        return $this->belongsTo(CategoryDepense::class, 'category_depense_id');
    }
}
