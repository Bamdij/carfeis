<?php

namespace App\Models;

use Eloquent as Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SousNiveau
 * @package App\Models
 * @version September 19, 2021, 6:10 pm UTC
 *
 * @property string $name
 * @property string $slug
 */
class SousNiveau extends Model
{
    use SoftDeletes;
    use Sluggable;


    use HasFactory;

    public $table = 'sous_niveaus';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'niveau_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

     /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function niveau()
    {
        return $this->belongsTo(Niveau::class, 'niveau_id');
    }
      /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enseignant_annee()
    {
        return $this->hasMany(EnseignantAnnee::class, 'sous_niveau_id');
    }
}
