<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;   

/**
 * Class MensuualiteEnseignant
 * @package App\Models
 * @version October 14, 2021, 11:39 am UTC
 *
 * @property string $enseignant_id
 * @property string $identity_card
 * @property string $month
 * @property string $montant
 * @property string $annee_scolaire_id
 * @property string $type_paiement_id
 * @property string $observation
 * @property string $user_id
 * @property string $month_end
 * @property boolean $is_payed
 */
class MensuualiteEnseignant extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'mensuualite_enseignants';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'enseignant_id',
        'identity_card',
        'month',
        'montant',
        'annee_scolaire_id',
        'type_paiement_id',
        'observation',
        'user_id',
        'month_end',
        'is_payed',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'enseignant_id' => 'string',
        'identity_card' => 'string',
        'month' => 'string',
        'montant' => 'string',
        'annee_scolaire_id' => 'string',
        'type_paiement_id' => 'string',
        'observation' => 'string',
        'user_id' => 'string',
        'month_end' => 'string',
        'is_payed' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'enseignant_id' => 'required',
        'identity_card' => 'required',
        'month' => 'required',
        'montant' => 'required',
        'annee_scolaire_id' => 'required',
        'type_paiement_id' => 'required',
        'user_id' => 'required',
        'month_end' => 'required'
    ];

        /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['month','montant','identity_card'],
            ]
        ];
    } 

      /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

      /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enseigant()
    {
        return $this->belongsTo(Enseigant::class, 'enseignant_id');
    }

      /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type_paiement()
    {
        return $this->belongsTo(TypePaiement::class, 'type_paiement_id');
    }

       /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    }
}
