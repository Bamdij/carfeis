<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Regime
 * @package App\Models
 * @version October 7, 2021, 3:56 pm UTC
 *
 * @property string $name
 * @property string $status
 * @property string $description
 */
class Regime extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'regimes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'status',
        'description',
        'mensualite',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'status' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

      /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frais_inscription()
    {
        return $this->hasMany(FraisInscription::class, 'regime_id');
    }  
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paiement()
    {
        return $this->hasMany(PaiementEleve::class, 'regime_id');
    }  
}
