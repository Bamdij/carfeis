<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Eleve
 * @package App\Models
 * @version September 20, 2021, 2:27 pm UTC
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $date_naissance
 * @property string $sexe
 * @property string $groupe_sanguin
 * @property boolean $is_malade
 * @property string $quelle_maladie
 * @property boolean $is_traitement
 * @property string $quel_traitement
 */
class Eleve extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'eleves';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'first_name',
        'last_name',
        'date_naissance',
        'sexe',
        'groupe_sanguin',
        'is_malade',
        'quelle_maladie',
        'is_traitement',
        'quel_traitement',
        'avatar_eleve',
        'lieu_naissance',
        'is_added',
        'numero',
        'descripttion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'first_name' => 'string',
        'last_name' => 'string',
        'date_naissance' => 'date',
        'sexe' => 'string',
        'groupe_sanguin' => 'string',
        'is_malade' => 'boolean',
        'quelle_maladie' => 'string',
        'is_traitement' => 'boolean',
        'quel_traitement' => 'string',
        'slug',
        'is_added',
        'numero'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'date_naissance' => 'required'
    ];

          /**
     * Get all of the candidatureOffres for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function info_parents()
    {
        return $this->hasMany(InfoParent::class, 'eleve_id');
    }

        /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alphabetisations()
    {
        return $this->hasMany(Alphabetisation::class, 'eleve_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                // 'source' => 'first_name',
                 'source' => ['first_name','last_name','date_naissance']
            ]
        ];
    }
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paiement()
    {
        return $this->hasMany(PaiementEleve::class, 'eleve_id');
    }  

    
       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensualite_eleve()
    {
        return $this->hasMany(MensualiteEleve::class, 'eleve_id');
    }
}
