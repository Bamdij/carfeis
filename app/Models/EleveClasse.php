<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class EleveClasse
 * @package App\Models
 * @version September 23, 2021, 6:03 pm UTC
 *
 * @property integer $niveau_id
 * @property integer $classe_id
 * @property integer $eleve_id
 * @property string $observation
 */
class EleveClasse extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'eleve_classes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'niveau_id',
        'classe_id',
        'eleve_id',
        'observation',
        'enseignant_id',
        'annee_scolaire_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'niveau_id' => 'integer',
        'classe_id' => 'integer',
        'eleve_id' => 'integer',
        'observation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'niveau_id' => 'required',
        'eleve_id' => 'required'
    ];

     
}
