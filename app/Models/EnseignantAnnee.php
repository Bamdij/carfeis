<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class EnseignantAnnee
 * @package App\Models
 * @version October 9, 2021, 1:04 pm UTC
 *
 * @property string $enseignant_id
 * @property string $annee_scolaire_id
 * @property string $type_enseignant_id
 * @property string $niveau_id
 */
class EnseignantAnnee extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'enseignant_annees';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'enseignant_id',
        'annee_scolaire_id',
        'type_enseignant_id',
        'niveau_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'enseignant_id' => 'string',
        'annee_scolaire_id' => 'string',
        'type_enseignant_id' => 'string',
        'niveau_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'enseignant_id' => 'required',
        'annee_scolaire_id' => 'required',
        'type_enseignant_id' => 'required'
    ];

      /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type_enseignant()
    {
        return $this->belongsTo(TypeEnseignant::class, 'type_enseignant_id');
    }

      /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enseignant()
    {
        return $this->belongsTo(Enseignant::class, 'enseignant_id');
    }

     /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    }
     /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sous_niveau()
    {
        return $this->belongsTo(SousNiveau::class, 'sous_niveau_id');
    }
}
