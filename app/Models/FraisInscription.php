<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class FraisInscription
 * @package App\Models
 * @version October 8, 2021, 10:21 am UTC
 *
 * @property string $regime_id
 * @property string $annee_scolaire_id
 * @property string $montant
 * @property string $observation
 */
class FraisInscription extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'frais_inscriptions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'regime_id',
        'annee_scolaire_id',
        'montant',
        'observation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'regime_id' => 'string',
        'annee_scolaire_id' => 'string',
        'montant' => 'string',
        'observation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'regime_id' => 'required',
        'annee_scolaire_id' => 'required',
        'montant' => 'required'
    ];

      /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regime()
    {
        return $this->belongsTo(Regime::class, 'regime_id');
    }
      /**
     * Get the candidat that owns the CandidatureOffre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    }
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paiement()
    {
        return $this->hasMany(PaiementEleve::class, 'frais_inscription_id');
    }  
}
