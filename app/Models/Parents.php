<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Parents
 * @package App\Models
 * @version December 9, 2021, 11:02 am UTC
 *
 * @property integer $user_id
 * @property integer $eleve_id
 * @property string $description
 */
class Parents extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'parents';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'eleve_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'eleve_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'eleve_id' => 'required'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                // 'source' => 'first_name',
                 'source' => ['user_id','eleve_id']
            ]
        ];
    }
}
