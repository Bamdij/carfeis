<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class TypeProfession
 * @package App\Models
 * @version September 17, 2021, 10:42 pm UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $description
 */
class TypeProfession extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasFactory;

    public $table = 'type_professions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => ''
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
        /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function info_parent()
    {
        return $this->hasMany(InfoParent::class, 'type_profession_id');
    }
}
