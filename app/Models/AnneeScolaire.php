<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class AnneeScolaire
 * @package App\Models
 * @version September 30, 2021, 9:24 am UTC
 *
 * @property string $name
 * @property string $slug
 */
class AnneeScolaire extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'annee_scolaires';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        // 'slug' => 'required'
    ];

      /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ]
        ];
    } 
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frais_inscription()
    {
        return $this->hasMany(FraisInscription::class, 'annee_scolaire_id');
    }  
         /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paiement()
    {
        return $this->hasMany(PaiementEleve::class, 'annee_scolaire_id');
    }  
       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensualite_enseignant()
    {
        return $this->hasMany(MensuualiteEnseignat::class, 'enseignant_id');
    }

       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function enseignant_annee()
    {
        return $this->hasMany(EnseignantAnnee::class, 'annee_scolaire_id');
    }

       /**
     * Get all of the Alphabetisation for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensualite_eleve()
    {
        return $this->hasMany(MensualiteEleve::class, 'annee_scolaire_id');
    }

       /**
     * Get all of the Regime for the Niveau
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluation_eleve()
    {
        return $this->hasMany(EvaluationEleve::class, 'annee_scolaire_id');
    }  
}
