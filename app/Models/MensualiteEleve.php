<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class MensualiteEleve
 * @package App\Models
 * @version October 10, 2021, 10:12 am UTC
 *
 * @property string $paiement_eleve_id
 * @property string $eleve_id
 * @property string $annee_scolaire_id
 * @property string $month
 * @property string $mensualite
 * @property string $observation
 */
class MensualiteEleve extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'mensualite_eleves';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'paiement_eleve_id',
        'eleve_id',
        'annee_scolaire_id',
        'month',
        'mensualite',
        'observation',
        'montant_attendu',
        'number_month',
        'slug',
        'user_id',
        'periode_end',
        'periode_debut',
        'numero'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'paiement_eleve_id' => 'string',
        'eleve_id' => 'string',
        'annee_scolaire_id' => 'string',
        'month' => 'string',
        'mensualite' => 'string',
        'observation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'paiement_eleve_id' => 'required',
        'eleve_id' => 'required',
        'annee_scolaire_id' => 'required',
        'month' => 'required',
        'mensualite' => 'required',
        'user_id' => 'required'
    ];

        /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['month','mensualite','montant_attendu'],
            ]
        ];
    } 

      /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eleve()
    {
        return $this->belongsTo(Eleve::class, 'eleve_id');
    }

     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function annee_scolaire()
    {
        return $this->belongsTo(AnneeScolaire::class, 'annee_scolaire_id');
    }
     /**
     * Get the candidat that owns the eleve
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paiement()
    {
        return $this->belongsTo(PaiementEleve::class, 'paiement_eleve_id');
    }
}
