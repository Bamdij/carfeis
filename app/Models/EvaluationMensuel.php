<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class EvaluationMensuel
 * @package App\Models
 * @version November 29, 2021, 11:07 am UTC
 *
 * @property integer $eleve_id
 * @property integer $user_id
 * @property integer $annee_scolaire_id
 * @property string $month
 * @property string $date_evaluation
 * @property string $sourate
 * @property string $last_lesson
 * @property string $first_party
 * @property string $appreciation_first_party
 * @property string $second_party
 * @property string $appreciation_second_party
 * @property string $third_party
 * @property string $appreciation_third_party
 * @property string $comportement_fr
 * @property string $comportement_ar
 * @property string $appreciation
 */
class EvaluationMensuel extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'evaluation_mensuels';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'eleve_id',
        'user_id',
        'annee_scolaire_id',
        'month',
        'date_evaluation',
        'sourate',
        'last_lesson',
        'first_party',
        'appreciation_first_party',
        'second_party',
        'appreciation_second_party',
        'third_party',
        'appreciation_third_party',
        'comportement_fr',
        'comportement_ar',
        'appreciation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'eleve_id' => 'integer',
        'user_id' => 'integer',
        'annee_scolaire_id' => 'integer',
        'month' => 'string',
        'date_evaluation' => 'date',
        'sourate' => 'string',
        'last_lesson' => 'string',
        'first_party' => 'string',
        'appreciation_first_party' => 'string',
        'second_party' => 'string',
        'appreciation_second_party' => 'string',
        'third_party' => 'string',
        'appreciation_third_party' => 'string',
        'comportement_fr' => 'string',
        'comportement_ar' => 'string',
        'appreciation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'eleve_id' => 'required',
        'annee_scolaire_id' => 'required',
        'month' => 'required'
    ];

    
}
