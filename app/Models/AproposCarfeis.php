<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class AproposCarfeis
 * @package App\Models
 * @version December 11, 2021, 5:23 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $slug
 */
class AproposCarfeis extends Model
{
    use SoftDeletes;
    use Sluggable;

    use HasFactory;

    public $table = 'apropos_carfeis';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                 'source' => 'title',
            ]
        ];
    }
}
