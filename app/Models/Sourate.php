<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Sourate
 * @package App\Models
 * @version December 4, 2021, 1:23 pm UTC
 *
 * @property string $num_sourate
 * @property string $libelle_sourate_fr
 * @property string $libelle_sourate_ar
 * @property string $nbre_verset
 */
class Sourate extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sourates';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'num_sourate',
        'libelle_sourate_fr',
        'libelle_sourate_ar',
        'nbre_verset'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'num_sourate' => 'string',
        'libelle_sourate_fr' => 'string',
        'libelle_sourate_ar' => 'string',
        'nbre_verset' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'num_sourate' => 'required'
    ];

    
}
