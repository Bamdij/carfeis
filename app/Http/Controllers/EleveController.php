<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEleveRequest;
use App\Http\Requests\UpdateEleveRequest;
use App\Repositories\EleveRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\EleveDatatable;
use DataTables;
use App\Models\MensualiteEleve;
use App\Http\Controllers\AppHelper;
use App\Models\Eleve;
use App\Models\InfoParent;
use App\Models\Alphabetisation;
use App\Models\TypeProfession;
use App\Models\TypeEnseignant;
use App\Models\LienParent;
use App\Models\Niveau;
use App\Models\SousNiveau;
use App\Models\EleveClasse;
use App\Models\AnneeScolaire;
use App\Models\Enseignant;
use App\Models\InscriptionEleve;
use App\Models\PaiementEleve;
use App\Models\FraisInscription;
use App\Models\User;
use App\Models\EvaluationMensuel;
use App\Models\EvaluationSemestre;
use Flash;
use App\Notifications\UserNotificationPaiement;
use App\Models\Regime;
use Response;
use Auth;
class EleveController extends AppBaseController
{
    /** @var  EleveRepository */
    private $eleveRepository;

    public function __construct(EleveRepository $eleveRepo)
    {
        $this->eleveRepository = $eleveRepo;
    }


     /**
     * Display a listing of the .
     *
     * @param EleveDatatable $eleveDatatable
     * @return Response
     */

    public function index(EleveDatatable $eleveDatatable)
    {
	
        return $eleveDatatable->render('eleves.index');
    }

    /**
     * Show the form for creating a new Eleve.
     *
     * @return Response
     */
    public function create()
    {
        // return view('eleves.create');
	
         return view('eleves.askpage');

    }
    public function getFormtoInscrire(){
        // $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->get();
        // $regime = Regime::OrderBy('name','ASC')->get();

        $annee_niveau = AnneeScolaire::where('status',true)->first();
        // $regime = Regime::OrderBy('name','ASC')->get();
        $regime = FraisInscription::where('annee_scolaire_id',$annee_niveau->id)->get();
        if(!$regime->isEmpty()){
            foreach ($regime as  $value) {
                $detail_regime = Regime::find($value->regime_id);
                $value->name = $detail_regime->name;
                $value->status = $detail_regime->status;
                $value->mensualite = $detail_regime->mensualite;
            }
        }

        return view('eleves.eleve_search',compact('annee_niveau','regime'))->render();
    }
    public function getFormtoReInscrire(){
		
        // $annee_niveau = AnneeScolaire::OrderBy('id','DESC')->where('status',true)->get();
        return view('eleves.create');
    }

    // public function getEleveWithNumero(Request $request){
    //     $eleve = Eleve::where('numero',$request->numero)->first();
    //        if(empty($eleve)){
    //         // $annee_niveau = AnneeScolaire::OrderBy('id','DESC')->where('status',true)->get();
    //         // dd('baba');
    //          return redirect(route('eleve.new_reinscription'))->withFail('Desolé! Aucun eleve ne dispose le numero de cette carte!Veuillez entrer un numero de carte valide s\'il vous plait.');;
    //     }else{
    //        return $this->getEleveToreinscrire($eleve->id);
		
    //      }
        
    // }
    public function getEleveNumeroPost(Request $request){
        $eleve = Eleve::where('numero',$request->numero)->first();
        if(empty($eleve)){
         // $annee_niveau = AnneeScolaire::OrderBy('id','DESC')->where('status',true)->get();
         // dd('baba');
          return redirect(route('eleve.new_reinscription'))->withFail('Desolé! Aucun eleve ne dispose le numero de cette carte!Veuillez entrer un numero de carte valide s\'il vous plait.');;
     }else{
        return $this->getEleveToreinscrire($eleve->id);
     
      }
    }
    public function getEleveToreinscrire($eleve_id){
        $eleve = Eleve::find($eleve_id);
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        // $regime = Regime::OrderBy('name','ASC')->get();
        $regime = FraisInscription::where('annee_scolaire_id',$annee_niveau->id)->get();
        if(!$regime->isEmpty()){
            foreach ($regime as  $value) {
                $detail_regime = Regime::find($value->regime_id);
                $value->name = $detail_regime->name;
                $value->status = $detail_regime->status;
                $value->mensualite = $detail_regime->mensualite;
            }
        }
        // dd($regime);

        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        if(!$eleve_class_by_year->isEmpty()){
            foreach ($eleve_class_by_year as $key ) {
                
                if(!empty($key->classe_id)){
                    $classe = SousNiveau::find($key->classe_id); 
                    $key->classe = $classe->name;
                }else{
                    $key->classe ="Information Non disponible";
                }
                if(!empty($key->annee_scolaire_id)){
                    $annee_scolaire = AnneeScolaire::find($key->annee_scolaire_id);
                    $key->annee_scolaire = $annee_scolaire->name;
                }else{
                    $annee_scolaire ="Information Non disponible";
                    $key->annee_scolaire ="Information Non disponible";
                }
                if(!empty($key->enseignant_id)){
                    $enseignant = Enseignant::find($key->enseignant_id);
                    $key->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
                }else{
                    $key->enseignant ="Information Non disponible";
                }
            }
        }
         // $arr = array('status' => true,'data'=>$eleve_numero );
       return view('eleves.addclass_search',compact('eleve','regime','type_enseig','annee_niveau','eleve_class_by_year')); 
    }
    public function getInfoParent($eleve_id){
        $eleve_id = Eleve::find($eleve_id);
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();
        return view('info_parents.create')->with('type_prof', $type_prof)
                                             ->with('eleve_id', $eleve_id)
                                             ->with('lien_parent', $lien_parent);

    }
    /**
     * Store a newly created Eleve in storage.
     *
     * @param CreateEleveRequest $request
     *
     * @return Response
     */
    public function store(CreateEleveRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('avatar_eleve')){
            $file = $request->avatar_eleve;
            $extension = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $filename = 'eleve'.uniqid().'.'.$extension;                
            $destinationPathImg = public_path().'/uploads/eleves/';
            if($file->move($destinationPathImg, $filename)){
                $path = '/uploads/eleves/'.$filename;
                $input['avatar_eleve'] = $path; 
            }
        }else{
            $input['avatar_eleve'] = '/images/avatar.jpg';
        }


        $eleve = $this->eleveRepository->create($input);
        if($request->continuer == 'Ajouter et Continuer'){
            Flash::success('Eleve ajouté(e) avec success');
            $helper = new AppHelper();
            $generatedNumero = $helper->generateNumeroCode(4, true, 'ud');
            $inscription = new InscriptionEleve();
            $inscription->eleve_id =$eleve->id;
            $inscription->annee_scolaire_id =$request->annee_scolaire_id;
            $inscription->regime_id =$request->regime_id;
            $inscription->raison_inscription=$request->descripttion;
            $inscription->save();
            $eleve_upate = Eleve::find($eleve->id);
            $eleve_upate->numero = $generatedNumero;
            $eleve_upate->update();
            return $this->getInfoParent($eleve->id);
         }
        //  if($request->terminer == 'Terminer'){
        //     Flash::success('Eleve ajouté(e) avec success');
        //     return redirect(route('eleves.index'));
        //     }
 

        Flash::success('Eleve ajouté(e) avec success.');

        return redirect(route('eleves.index'));
    }

    /**
     * Display the specified Eleve.
     *
     * @param int $id
     *
     * @return Response 
     */
    public function show($id)
    {
        $eleve = $this->eleveRepository->find($id);
        if (empty($eleve)) {
            Flash::error('Eleve  non trouvé(e)');

            return redirect(route('eleves.index'));
        }
        $eleve_id = Eleve::find($id);
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        if(!$eleve_class_by_year->isEmpty()){
            foreach ($eleve_class_by_year as $key ) {
                
                if(!empty($key->classe_id)){
                    $classe = SousNiveau::find($key->classe_id);
                    $key->classe = $classe->name;
                }else{
                    $key->classe ="Information Non disponible";
                }
                if(!empty($key->annee_scolaire_id)){
                    $annee_scolaire = AnneeScolaire::find($key->annee_scolaire_id);
                    $key->annee_scolaire = $annee_scolaire->name;
                }else{
                    $annee_scolaire ="Information Non disponible";
                    $key->annee_scolaire ="Information Non disponible";
                }
                if(!empty($key->enseignant_id)){
                    $enseignant = Enseignant::find($key->enseignant_id);
                    $key->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
                }else{
                    $key->enseignant ="Information Non disponible";
                }
                if(!empty($key->regime_id)){
                    $regime = Regime::find($key->regime_id);
                    $key->regime =  $regime->name;
                }else{
                    $key->regime ="Information Non disponible";
                }
            }
        }
        // dd($eleve_class_by_year);
        $infoparent = InfoParent::where('eleve_id',$eleve->id)->first();
        $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->first();
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();

        return view('eleves.show')->with('eleve', $eleve)
                                    ->with('eleve_id', $eleve_id)
                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                    ->with('type_prof', $type_prof)
                                    ->with('infoParent', $infoparent)
                                    ->with('alphbetisation', $alphbetisation)
                                    ->with('lien_parent', $lien_parent);
    }

    /**
     * Show the form for editing the specified Eleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eleve = $this->eleveRepository->find($id);
        $eleve_id = Eleve::find($id);
        $infoparent = InfoParent::where('eleve_id',$eleve->id)->first();
        $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->first();
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();
        if (empty($eleve)) {
            Flash::error('Eleve  non trouvé(e)');

            return redirect(route('eleves.index'));
        }

        return view('eleves.edit')->with('eleve', $eleve)
                                ->with('eleve_id', $eleve_id)
                                ->with('type_prof', $type_prof)
                                ->with('infoparent', $infoparent)
                                ->with('alphbetisation', $alphbetisation)
                                ->with('lien_parent', $lien_parent);
    }

    /**
     * Update the specified Eleve in storage.
     *
     * @param int $id
     * @param UpdateEleveRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEleveRequest $request)
    {
        $eleve = $this->eleveRepository->find($id);
        $input = $request->all();
        // dd( $input);

        if (empty($eleve)) {
            Flash::error('Eleve  non trouvé(e)');

            return redirect(route('eleves.index'));
        }
        if($request->hasFile('avatar_eleve')){
            $file = $request->avatar_eleve;
            $extension = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $filename = 'eleve'.uniqid().'.'.$extension;                
            $destinationPathImg = public_path().'/uploads/eleves/update/';
            if($file->move($destinationPathImg, $filename)) {
                $path = '/uploads/eleves/update/'.$filename;
                $input['avatar_eleve'] = $path; 
            }
        }
        $eleve = $this->eleveRepository->update($input, $id);
        Flash::success('Eleve modifié avec success successfully.');

        // return redirect(route('eleves.index'));
        return back();
    }

    /**
     * Remove the specified Eleve from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $eleve = $this->eleveRepository->find($id);
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->first();
        if (empty($eleve)) {
            Flash::error('Eleve  non trouvé(e)');

            return redirect(route('eleves.index'));
        }

        $this->eleveRepository->delete($id);
        if(!empty($eleve_class_by_year)){
            $eleve_class_by_year->delete();
            $mensualite = MensualiteEleve::where('eleve_id',$eleve->id)->delete();
            $mensualite = PaiementEleve::where('eleve_id',$eleve->id)->delete();
            $infoparent = InfoParent::where('eleve_id',$eleve->id)->delete();
            $evalution_semestre = EvaluationSemestre::where('eleve_id',$eleve->id)->delete();
            $evalution_semestre = EvaluationMensuel::where('eleve_id',$eleve->id)->delete();
            $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->delete();

        }
        Flash::success('Eleve supprime avec success.');

        return redirect(route('eleves.index'));
    }

    /**
     * Add Eleve to class
     * @param int $id
     * @return view
     */

     public function getEleveAddClass($id){
         $eleve = Eleve::find($id);
         $elev_inscription = InscriptionEleve::where('eleve_id',$eleve->id)
                            ->first();
         $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        //  $annee_niveau = AnneeScolaire::OrderBy('name','ASC')->get();
         $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->get();
         return view('eleves.addclass',compact('eleve','type_enseig','annee_niveau','elev_inscription'));
    }

    public function getClasseByNiveaau($id){
        $type_enseignant = TypeEnseignant::where('id',$id)->first();
        // dd($type_enseignant->enseignant);
        $niveau_eleve = Niveau::where('type_enseignant_id',$id)->get();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('eleves.addniveauclass',compact('type_enseig','niveau_eleve','type_enseignant'))->render();
        
    }
    public function getClasseByNiveaaAdd($id){
        $niveau_eleve = Niveau::where('id',$id)->first();
        $class_eleve = SousNiveau::where('niveau_id',$id)->get();
        return view('eleves.niveauadd',compact('class_eleve','niveau_eleve'))->render();
        
    }

    public function PostClasseEleve(Request $request,$id){
        // dd($request->all());
        $eleve = Eleve::find($id);
        $classe_elev_test = EleveClasse::where('eleve_id',$eleve->id)
                            ->where('annee_scolaire_id',$request->annee_scolaire_id)
                            ->get();
        if(!$classe_elev_test->isEmpty() ){
            $arr = array('status' => false,'data'=> $classe_elev_test,'msg'=>'noo good');
        }else{
            $class_eleve = new EleveClasse();
            $class_eleve->eleve_id = $eleve->id;
            $class_eleve->niveau_id = $request->niveau_id;
            $class_eleve->classe_id = $request->classe_id;
            $class_eleve->annee_scolaire_id = $request->annee_scolaire_id;
            if($request->observation){
                $class_eleve->observation = $request->observation;
            }
            $class_eleve->save();
            if($class_eleve){
                $eleve_update = Eleve::where('id',$id)->first();
                $eleve_update->is_added = true;
                $eleve_update->update();
                $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'good');
                $inscription_test = InscriptionEleve::where('eleve_id',$eleve->id)
                                                    ->where('annee_scolaire_id',$request->annee_scolaire_id)
                                                    ->get();
                if(!$inscription_test->isEmpty()){
                    $inscription_1 = InscriptionEleve::where('eleve_id',$eleve->id)
                    ->where('annee_scolaire_id',$request->annee_scolaire_id)
                    ->first();
                    $inscription_1->annee_scolaire_id =$request->annee_scolaire_id;
                    $inscription_1->enseignant_id =$request->enseignant_id;
                    $inscription_1->classe_id =$request->classe_id;
                    $inscription_1->update();
                    $arr = array('data'=> $inscription_1,'msg'=>'noooo'); 
                }else{
                    $inscription = new InscriptionEleve();
                    $inscription->eleve_id = $eleve->id;
                    $inscription->annee_scolaire_id =$request->annee_scolaire_id;
                    $inscription->regime_id =$request->regime_id;
                    $inscription->raison_inscription ="reinscption";
                    $inscription->save();
                    $arr = array('data'=> $inscription_test,'msg'=>'noooo'); 
                }
            }
            $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'noo good');
        }
        Flash::success('Eleve ajouter avec success.');

        //  return redirect(route('eleves.index'));
        return Response()->json($arr);        
   
    }

    public function PostClasseEleveWithProf(Request $request,$id){
      //  dd($request->all());
        $eleve = Eleve::find($id);
        $class_student_exist = EleveClasse::where('eleve_id', $eleve->id)
                                            ->where('annee_scolaire_id',$request->annee_scolaire_id)->get();
        if(!$class_student_exist->isEmpty()){
            $arr = array('status' => false,'data'=> $class_student_exist,'msg'=>'Oups erreur');
        }else{
        $class_eleve = new EleveClasse();
        $class_eleve->eleve_id = $eleve->id;
        $class_eleve->niveau_id = $request->niveau_id;
        $class_eleve->classe_id = $request->classe_id;
        $class_eleve->annee_scolaire_id = $request->annee_scolaire_id;
        $class_eleve->enseignant_id = $request->enseignant_id;
        $class_eleve->save();
        if($class_eleve){
            $eleve_update = Eleve::where('id',$id)->first();
            $eleve_update->is_added = true;
            $eleve_update->update();
            $inscription_test = InscriptionEleve::where('eleve_id',$eleve->id)
                                ->where('annee_scolaire_id',$request->annee_scolaire_id)
                                ->get();
            if(!$inscription_test->isEmpty()){
                $inscription_2 = InscriptionEleve::where('eleve_id',$eleve->id)
                ->where('annee_scolaire_id',$request->annee_scolaire_id)
                ->first();
                $inscription_2->annee_scolaire_id =$request->annee_scolaire_id;
                $inscription_2->enseignant_id =$request->enseignant_id;
                $inscription_2->classe_id =$request->classe_id;
                $inscription_2->regime_id =$inscription_2->regime_id;
                $inscription_2->raison_inscription ="reinscption";
                $inscription_2->update();
            $arr = array('data'=> $inscription_2,'msg'=>'noooo'); 
            }else{
                $inscription = new InscriptionEleve();
                $inscription->eleve_id = $eleve->id;
                $inscription->annee_scolaire_id =$request->annee_scolaire_id;
                $inscription->enseignant_id =$request->enseignant_id;
                $inscription->classe_id =$request->classe_id;
                $inscription->regime_id =$request->regime_id;
                $inscription->raison_inscription ="reinscption";
                $inscription->save();
                $arr = array('data'=> $inscription_test,'msg'=>'gogogogog'); 
            }
            $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'Oups cette classe est dejà associer à un  enseignant');
        }
    }
        Flash::success('Eleve ajouter avec success.');

        //  return redirect(route('eleves.index'));
        return Response()->json($arr);        
   
    }

     //Carte Eleve printable

     public function getStudentCard($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        // $paiement = PaiementEleve::where('eleve_id',$eleve->id)->first();
        // dd($eleve);
        // $frais = FraisInscription::find($paiement->frais_inscription_id);
        $annee = AnneeScolaire::where('status',true)->first();
        // dd($annee,$paiement);

        return view('eleves.carte_for_print',compact('eleve','annee'));
        
    }
    //Carte Imprimer par année
    public function getCarteStudentPrinting($id){
        $eleve_class_by_year = InscriptionEleve::where('id',$id)->orderBy('id','DESC')->first();
        $classe = SousNiveau::find($eleve_class_by_year->classe_id);
        $annee_scolaire = AnneeScolaire::find($eleve_class_by_year->annee_scolaire_id);
        $eleve = Eleve::find($eleve_class_by_year->eleve_id);
        return view('eleves.print_all_carte',compact('eleve','eleve_class_by_year','classe','annee_scolaire'));
    }

    //Ajout eleve classe au moment de l'inscription

    public function AddClasseEleveContinuons(Request $request,$id){
        //  dd($request->all());
        $eleve = Eleve::find($id);
        $inscription = InscriptionEleve::where('eleve_id',$eleve->id)->first();
        $classe_elev_test = EleveClasse::where('eleve_id',$eleve->id)
                            ->where('annee_scolaire_id',$request->annee_scolaire_id)
                            ->get();
        if(!$classe_elev_test->isEmpty()){
            $arr = array('status' => false,'data'=> $classe_elev_test,'msg'=>'noo good');
        }else{
                $class_eleve = new EleveClasse();
                $class_eleve->eleve_id = $eleve->id;
                $class_eleve->niveau_id = $request->niveau_id;
                $class_eleve->classe_id = $request->classe_id;
                $class_eleve->annee_scolaire_id =$inscription->annee_scolaire_id;
                    if($request->observation){
                        $class_eleve->observation = $request->observation;
                    }
                $class_eleve->save();
                if($class_eleve){
                    $eleve_update = Eleve::where('id',$id)->first();
                    $eleve_update->is_added = true;
                    $eleve_update->update();
                    $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'good');
                        $inscription = InscriptionEleve::where('eleve_id',$eleve->id)->first();
                        $inscription->eleve_id = $eleve->id;
                        $inscription->annee_scolaire_id =$inscription->annee_scolaire_id;
                        $inscription->classe_id =$request->classe_id;
                        $inscription->raison_inscription ="insciprion";
                        $inscription->update();
                        $arr = array('data'=> $inscription,'msg'=>'noooo'); 
                }
            }
            $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'noo good');
        //  return redirect(route('eleves.index'));
        return Response()->json($arr);        
   
    }


    //Classe Professeur avec eleve type arabe
    public function AddClasseEleveContinuonsArabe(Request $request,$id){
        // dd($request->all());
        $eleve = Eleve::find($id);
        $inscription = InscriptionEleve::where('eleve_id',$eleve->id)->first();
        $classe_elev_test = EleveClasse::where('eleve_id',$eleve->id)
                            ->where('annee_scolaire_id',$request->annee_scolaire_id)
                            ->get();
        if(!$classe_elev_test->isEmpty()){
            $arr = array('status' => false,'data'=> $classe_elev_test,'msg'=>'noo good');
        }else{
            $class_eleve = new EleveClasse();
            $class_eleve->eleve_id = $eleve->id;
            $class_eleve->niveau_id = $request->niveau_id;
            $class_eleve->classe_id = $request->classe_id;
            $class_eleve->annee_scolaire_id =$inscription->annee_scolaire_id;
            if($request->observation){
                $class_eleve->observation = $request->observation;
            }
            $class_eleve->save();
            if($class_eleve){
                $eleve_update = Eleve::where('id',$id)->first();
                $eleve_update->is_added = true;
                $eleve_update->update();
                $arr = array('status' => true,'data'=> $class_eleve,'msg'=>'good');
                    $inscription = InscriptionEleve::where('eleve_id',$eleve->id)->first();
                    $inscription->eleve_id = $eleve->id;
                    $inscription->annee_scolaire_id =$inscription->annee_scolaire_id;
                    $inscription->enseignant_id =$request->enseignant_id;
                    $inscription->type_enseignant_id =$request->type_enseignant_id;
                    $inscription->classe_id =$request->classe_id;
                    $inscription->raison_inscription ="inscription";
                    $inscription->update();
                    $arr = array('data'=> $inscription,'msg'=>'noooo'); 
                }
                
            }

            $arr = array('status' => true,'data'=> $inscription,'msg'=>'noo good');
        Flash::success('Eleve ajouter avec success.');

        //  return redirect(route('eleves.index'));
        return Response()->json($arr);        
   
    }

    public function getEleveToAddInscription($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        $inscription = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        // dd($inscription);
        $regime = Regime::find($inscription->regime_id);
        $frais_inscript = FraisInscription::where('regime_id',$regime->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $montant_inscription = intval($frais_inscript->montant + $regime->mensualite);
        return view('eleves.get_page_to_paie',compact('eleve','inscription','montant_inscription','regime','frais_inscript'));
    }

     public function getPaiementEleveByYear(){
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        $eleve_paiement = PaiementEleve::where('annee_scolaire_id',$annee_niveau->id)->get();
         return view('eleves.eleve_get_paiement',compact('annee_niveau','eleve_paiement'));
     }

     public function getEleveToAddPaiement($id){
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('id',$id)->first();
        $eleve_paiement = PaiementEleve::where('annee_scolaire_id',$annee_niveau->id)->get();
        return view('eleves.page_elev_paie_month',compact('annee_niveau','eleve_paiement'));
     }

     public function SearchingElevePaiement(Request $request){
        $eleve = Eleve::where('numero',$request->numero)->first();
        if(empty($eleve)){
            $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
            return back()->withFail('Desolé! Aucun eleve ne dispose le numero de cette carte Oubien eleve non disponible pour cette annéé!Veuillez entrer un numero de carte valide s\'il vous plait.');;
        }else{
            $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$request->annee_scolaire_id)->first();
           
		if(empty($eleve_class_by_year)){
                return back()->withFail('Desolé! Aucun eleve ne dispose le numero de cette carte Oubien eleve non disponible pour cette annéé!Veuillez entrer un numero de carte valide s\'il vous plait.');;
            }else{

                return $this->getEleveTorPaie($eleve->id);
            }
         }

        // return Response()->json($arr); 
    }

    public function getEleveTorPaie($eleve_id){
        $eleve = Eleve::find($eleve_id);
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        $status_paiement = PaiementEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $regime = Regime::OrderBy('name','ASC')->get();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $eleve_regime = Regime::find($eleve_class_by_year->regime_id);
        $frais = FraisInscription::where('regime_id',$eleve_regime->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $mensualite = MensualiteEleve::where('eleve_id',$eleve_id)->where('annee_scolaire_id',$annee_niveau->id)->get();
         // $arr = array('status' => true,'data'=>$eleve_numero );
       return view('eleves.get_paiement_elve',compact('eleve','mensualite','status_paiement','frais','eleve_regime','regime','type_enseig','annee_niveau','eleve_class_by_year'))->render(); 
    }

    public function ChangeStatusPaiement(Request $request,$id){
        $status_paiement = PaiementEleve::where('id',$id)->first();
        if($request->is_confirme){
            $status_paiement->is_confirme = $request->is_confirme;
            $status_paiement->update();
            if($status_paiement){
                Flash::success('Status paiement modifie avec success');

                $arr = array('status' => true,'data'=> $status_paiement,'msg'=>'good');
            }else{
                $arr = array('status' => false,'data'=> $status_paiement,'msg'=>'noooo');

            }
        }
        // $arr = array('status' => false,'data'=> $status_paiement,'msg'=>'noooo');
        return Response()->json($arr);        

    }

    //Paiement mensuel de l'eleve
    public function PaiementMonthPosted(Request $request,$id){
        $paiement = PaiementEleve::find($id);
        $elev = Eleve::find($paiement->eleve_id);
        $paiement_month = 12;
        $x = 1;
            $new_month = new MensualiteEleve();
            $s_month = 's_month1';
            $s_mensualite = 's_mensualite1';
            $s_observation = 's_observation1';
            $s_montant_attendu = 's_montant_attendu1';
            $s_number_month = 's_number_month1';
            if(!is_null($request->$s_month) && !is_null($request->$s_mensualite)){
                $new_month->paiement_eleve_id = $paiement->id;
                $new_month->eleve_id = $paiement->eleve_id;
                $new_month->annee_scolaire_id = $paiement->annee_scolaire_id;
                $new_month->month = $request->$s_month;
                $new_month->mensualite = $request->$s_mensualite;
                $new_month->observation = $request->$s_observation;
                $new_month->montant_attendu = $request->$s_montant_attendu;
                $new_month->number_month = $request->$s_number_month;
                $new_month->periode_end = $request->periode_end;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->user_id = Auth::user()->id;
                $new_month->save();
                if($new_month){
                    $helper = new AppHelper();
                    $generatedNumero = $helper->generateNumeroCode(4, true, 'ud');
                    $month_update = MensualiteEleve::find($new_month->id);
                    $month_update->numero=$generatedNumero;
                    $month_update->update();
                    if($month_update){ 
                        $user_name = User::where('status','superadmin')->first();
                            $notif = $user_name->notify(new UserNotificationPaiement($user_name,$month_update));
                    }
                    $arr = array('status' => true,'data'=> $new_month,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
              
        }
        Flash::success('Mensualite  ajouter avec success.');
        //  return $this->getInfoPaiementMonth($elev->slug);
           

        //  return redirect(route('infor_paiement.mensual',$elev->slug));  
    }

    //Paiement avec Paydunya
    public function PaiementMonthPostedPaydunua(Request $request,$id){
        $paiement = PaiementEleve::find($id);
        $elev = Eleve::find($paiement->eleve_id);
        $paiement_month = 12;
        $x = 1;
            $new_month = new MensualiteEleve();
            $s_month = 's_month1';
            $s_mensualite = 's_mensualite1';
            $s_observation = 's_observation1';
            $s_montant_attendu = 's_montant_attendu1';
            $s_number_month = 's_number_month1';
            if(!is_null($request->$s_month) && !is_null($request->$s_mensualite)){
                $new_month->paiement_eleve_id = $paiement->id;
                $new_month->eleve_id = $paiement->eleve_id;
                $new_month->annee_scolaire_id = $paiement->annee_scolaire_id;
                $new_month->month = $request->$s_month;
                $new_month->mensualite = $request->$s_mensualite;
                $new_month->observation = $request->$s_observation;
                $new_month->montant_attendu = $request->$s_montant_attendu;
                $new_month->number_month = $request->$s_number_month;
                $new_month->periode_end = $request->periode_end;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->user_id = Auth::user()->id;
                $new_month->save();
                if($new_month){
                    $helper = new AppHelper();
                    $generatedNumero = $helper->generateNumeroCode(4, true, 'ud');
                    $month_update = MensualiteEleve::find($new_month->id);
                    $month_update->numero=$generatedNumero;
                    $month_update->update();
                    // if($month_update){ 
                    //     $user_name = User::where('status','superadmin')->first();
                    //         $notif = $user_name->notify(new UserNotificationPaiement($user_name,$month_update));
                    // }
                    $arr = array('status' => true,'data'=> $new_month,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
              
        }
        Flash::success('Mensualite  ajouter avec success.');
        //  return $this->getInfoPaiementMonth($elev->slug);
           

        //  return redirect(route('infor_paiement.mensual',$elev->slug));  
    }
    //Fin Paiement

    public function getInfoPaiementMonth($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        if(!empty($eleve_class_by_year)){
            $status_paiement = PaiementEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
            $eleve_regime = Regime::find($eleve_class_by_year->regime_id);
            $mensualite = MensualiteEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
            return view('eleves.infos_after_paiement',compact('eleve','status_paiement','eleve_regime','annee_niveau','eleve_class_by_year','mensualite'));
        }else{
            $status_paiement = PaiementEleve::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
            $mensualite = MensualiteEleve::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
            return view('eleves.infos_no_paiement',compact('eleve','status_paiement','annee_niveau','eleve_class_by_year','mensualite'));
        }
       
    }

    public function getEleveAllByPaiement(){
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        $eleves = InscriptionEleve::where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        $mensualites = MensualiteEleve::where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        foreach ($eleves as $eleve) {
            $eleve->eleve_id = $eleve->id;
            $eleve->info = Eleve::find($eleve->id);
            $eleve->eleve_regime = Regime::find($eleve->regime_id);
            foreach ($mensualites as $key) {
                $eleve_mensuel =  $key->eleve_id;
                $elev_montananant = MensualiteEleve::where('eleve_id', $eleve->eleve_id)->count();
                if($elev_montananant>0){
                    if($eleve->eleve_id == $key->eleve_id){
                            $eleve->elev_mont = MensualiteEleve::where('eleve_id', $key->eleve_id)->get();
                            $eleve->eleve_cont = count($eleve->elev_mont);
                        
                        }
                }else{
                     $eleve->elev_mont = "Aucun Paiement"; 
                     $eleve->eleve_cont = 0;
                     $eleve->elev_inform = "0";
                     $eleve->eleve_regime = Regime::find($eleve->regime_id);

                }
                }
            }
            //  dd($eleves);
            return view('eleves.getAlleleve_paiements',compact('annee_niveau','eleves','mensualites'));
    }


    public function getListOfEleveNonPaiement($query){
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        $eleves = InscriptionEleve::where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        $mensualites = MensualiteEleve::where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        foreach ($eleves as $eleve) {
            $eleve->eleve_id = $eleve->id;
            $eleve->info = Eleve::find($eleve->id);
            $eleve->eleve_regime = Regime::find($eleve->regime_id);
            foreach ($mensualites as $key) {
                $eleve_mensuel =  $key->eleve_id;
                $elev_montananant = MensualiteEleve::where('eleve_id', $eleve->eleve_id)->count();
                if($elev_montananant>0){
                    if($eleve->eleve_id == $key->eleve_id){
                            $eleve->elev_mont = MensualiteEleve::where('eleve_id', $key->eleve_id)->get();
                            $eleve->eleve_cont = count($eleve->elev_mont);
                            $eleve->status = "OUI";
                        
                        }
                }else{
                     $eleve->elev_mont = "Aucun Paiement"; 
                     $eleve->elev_inform = "0";
                     $eleve->status = "NON";
                     $eleve->eleve_cont = 0;
                }
                }
            }
            if($query == "eleve_nonpayed"){
                return view('eleves.getAlleleve_modal_non_payed',compact('annee_niveau','eleves','mensualites'))->render();
            }
            if($query == "eleve_payed"){
                return view('eleves.getAlleleve_modal_payed',compact('annee_niveau','eleves','mensualites'))->render();
            }
            if($query == "one_month"){
                return view('eleves.getAlleleve_on_month_modal_payed',compact('annee_niveau','eleves','mensualites'))->render();
            }
    }
  

}

