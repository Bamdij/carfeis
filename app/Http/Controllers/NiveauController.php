<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNiveauRequest;
use App\Http\Requests\UpdateNiveauRequest;
use App\Repositories\NiveauRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\NiveauDatatable;
use App\Models\SousNiveau;
use App\Models\TypeEnseignant;
use DataTables;
use Flash;
use Response;

class NiveauController extends AppBaseController
{
    /** @var  NiveauRepository */
    private $niveauRepository;

    public function __construct(NiveauRepository $niveauRepo)
    {
        $this->niveauRepository = $niveauRepo;
    }

      /**
     * Display a listing of the .
     *
     * @param NiveauDatatable $niveauDatatable
     * @return Response
     */

    public function index(NiveauDatatable $niveauDatatable)
    {
        return $niveauDatatable->render('niveaux.index');
    }



    /**
     * Show the form for creating a new Niveau.
     *
     * @return Response
     */
    public function create()
    {
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('niveaux.create',compact('type_enseig'));
    }

    /**
     * Store a newly created Niveau in storage.
     *
     * @param CreateNiveauRequest $request
     *
     * @return Response
     */
    public function store(CreateNiveauRequest $request)
    {
        $input = $request->all();

        $niveau = $this->niveauRepository->create($input);

        $nbreSousniveaux = 10000;
        $x = 1;
        while($x < $nbreSousniveaux){
            $sous_niveaux = new SousNiveau();
            $s_niveau = 's_niveau'.$x;
            if(!is_null($request->$s_niveau)){
                $sous_niveaux->niveau_id = $niveau->id;
                $sous_niveaux->name = $request->$s_niveau;
                $sous_niveaux->save();
            }
            $x++;
        }

        Flash::success('Niveau enregistrer avec success.');

        return redirect(route('niveaux.index'));
    }

    /**
     * Display the specified Niveau.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            Flash::error('Niveau  non trouvé(e)');

            return redirect(route('niveaux.index'));
        }
        $s_niveau = $niveau->sousniveaux;

        return view('niveaux.show')->with('niveau', $niveau)
                                    ->with('s_niveau', $s_niveau);
    }

    /**
     * Show the form for editing the specified Niveau.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $niveau = $this->niveauRepository->find($id);
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        if (empty($niveau)) {
            Flash::error('Niveau  non trouvé(e)');

            return redirect(route('niveaux.index'));
        }
        $s_niveau = $niveau->sousniveaux;
        return view('niveaux.edit')
                                ->with('niveau', $niveau)
                                ->with('type_enseig', $type_enseig)
                                ->with('s_niveau', $s_niveau);
    }

    /**
     * Update the specified Niveau in storage.
     *
     * @param int $id
     * @param UpdateNiveauRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNiveauRequest $request)
    {
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            Flash::error('Niveau  non trouvé(e)');

            return redirect(route('niveaux.index'));
        }

        $nbreSousniveaux = 10000;
        $x = 1;
        while($x < $nbreSousniveaux){
            $sous_niveaux = new SousNiveau();
            $s_niveau = 's_niveau'.$x;
            if(!is_null($request->$s_niveau)){
                $sous_niveaux->niveau_id = $niveau->id;
                $sous_niveaux->name = $request->$s_niveau;
                $sous_niveaux->save();
            }
            $x++;
        }
        $niveau = $this->niveauRepository->update($request->all(), $id);
        Flash::success('Niveau modifié(e) avec success.');

        return redirect(route('niveaux.index'));
    }

    /**
     * Remove the specified Niveau from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            Flash::error('Niveau  non trouvé(e)');

            return redirect(route('niveaux.index'));
        }

        $this->niveauRepository->delete($id);

        Flash::success('Niveau deleted successfully.');

        return redirect(route('niveaux.index'));
    }
}
