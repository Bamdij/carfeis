<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlphabetisationRequest;
use App\Http\Requests\UpdateAlphabetisationRequest;
use App\Repositories\AlphabetisationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Eleve;
use App\Models\TypeEnseignant;
use App\Models\AnneeScolaire;
use Flash;
use Response;

class AlphabetisationController extends AppBaseController
{
    /** @var  AlphabetisationRepository */
    private $alphabetisationRepository;

    public function __construct(AlphabetisationRepository $alphabetisationRepo)
    {
        $this->alphabetisationRepository = $alphabetisationRepo;
    }

    /**
     * Display a listing of the Alphabetisation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $alphabetisations = $this->alphabetisationRepository->all();

        return view('alphabetisations.index')
            ->with('alphabetisations', $alphabetisations);
    }

    /**
     * Show the form for creating a new Alphabetisation.
     *
     * @return Response
     */
    public function create()
    {
        return view('alphabetisations.create');
    }

    public function getClasseEleveInscription($eleve_id){
        $eleve = Eleve::find($eleve_id);
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_niveau = AnneeScolaire::OrderBy('id','ASC')->get();
        return view('eleves.create_classe')->with('type_enseig', $type_enseig)
                                            ->with('annee_niveau', $annee_niveau)
                                            ->with('eleve', $eleve);
    }
    /**
     * Store a newly created Alphabetisation in storage.
     *
     * @param CreateAlphabetisationRequest $request
     *
     * @return Response
     */
    public function store(CreateAlphabetisationRequest $request)
    {
        $input = $request->all();

        $alphabetisation = $this->alphabetisationRepository->create($input);
        if($request->continuer == 'Ajouter et Continuer'){
            Flash::success('Eleve ajouté(e) avec success');
            return $this->getClasseEleveInscription($input['eleve_id']);
         }
         if($request->terminer == 'Terminer'){
            Flash::success('Eleve ajouté avec success.');
            return redirect(route('eleves.index'));
            }
        // return redirect(route('alphabetisations.index'));
    }

    /**
     * Display the specified Alphabetisation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            Flash::error('Alphabetisation non trouvé');

            return redirect(route('alphabetisations.index'));
        }

        return view('alphabetisations.show')->with('alphabetisation', $alphabetisation);
    }

    /**
     * Show the form for editing the specified Alphabetisation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            Flash::error('Alphabetisation non trouvé');

            return redirect(route('alphabetisations.index'));
        }

        return view('alphabetisations.edit')->with('alphabetisation', $alphabetisation);
    }

    /**
     * Update the specified Alphabetisation in storage.
     *
     * @param int $id
     * @param UpdateAlphabetisationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlphabetisationRequest $request)
    {
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            Flash::error('Alphabetisation non trouvé');

            return redirect(route('alphabetisations.index'));
        }

        $alphabetisation = $this->alphabetisationRepository->update($request->all(), $id);

        Flash::success('Alphabetisation modifié(e) avec success.');
        return redirect(route('eleves.index'));

        // return redirect(route('alphabetisations.index'));
    }

    /**
     * Remove the specified Alphabetisation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            Flash::error('Alphabetisation non trouvé');

            return redirect(route('alphabetisations.index'));
        }

        $this->alphabetisationRepository->delete($id);

        Flash::success('Alphabetisation supprime(e) avec success.');

        return redirect(route('alphabetisations.index'));
    }
}
