<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEleveClasseRequest;
use App\Http\Requests\UpdateEleveClasseRequest;
use App\Repositories\EleveClasseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Eleve;
use Flash;
use Response;

class EleveClasseController extends AppBaseController
{
    /** @var  EleveClasseRepository */
    private $eleveClasseRepository;

    public function __construct(EleveClasseRepository $eleveClasseRepo)
    {
        $this->eleveClasseRepository = $eleveClasseRepo;
    }

    /**
     * Display a listing of the EleveClasse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $eleveClasses = $this->eleveClasseRepository->all();

        return view('eleve_classes.index')
            ->with('eleveClasses', $eleveClasses);
    }

    /**
     * Show the form for creating a new EleveClasse.
     *
     * @return Response
     */
    public function create()
    {
        return view('eleve_classes.create');
    }

    /**
     * Store a newly created EleveClasse in storage.
     *
     * @param CreateEleveClasseRequest $request
     *
     * @return Response
     */
    public function store(CreateEleveClasseRequest $request)
    {
        $input = $request->all();

        $eleveClasse = $this->eleveClasseRepository->create($input);

        Flash::success('Eleve Classe ajouté(e) avec success.');

        return redirect(route('eleveClasses.index'));
    }

    /**
     * Display the specified EleveClasse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            Flash::error('Eleve Classe  non trouvé(e)');

            return redirect(route('eleveClasses.index'));
        }

        return view('eleve_classes.show')->with('eleveClasse', $eleveClasse);
    }

    /**
     * Show the form for editing the specified EleveClasse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            Flash::error('Eleve Classe  non trouvé(e)');

            return redirect(route('eleveClasses.index'));
        }

        return view('eleve_classes.edit')->with('eleveClasse', $eleveClasse);
    }

    /**
     * Update the specified EleveClasse in storage.
     *
     * @param int $id
     * @param UpdateEleveClasseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEleveClasseRequest $request)
    {
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            Flash::error('Eleve Classe  non trouvé(e)');

            return redirect(route('eleveClasses.index'));
        }

        $eleveClasse = $this->eleveClasseRepository->update($request->all(), $id);

        Flash::success('Eleve Classe modifié(e) avec success.');

        return redirect(route('eleveClasses.index'));
    }

    /**
     * Remove the specified EleveClasse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $eleveClasse = $this->eleveClasseRepository->find($id);
        $eleve = Eleve::find($eleveClasse->eleve_id);
        if (empty($eleveClasse)) {
            Flash::error('Eleve Classe non trouvé(e)');

            return redirect(route('get_eleve_by_class.classe'));
        }

        $this->eleveClasseRepository->delete($id);
        $eleve->is_added = false;
        $eleve->update();
        Flash::success('Eleve Classe supprime(e) avec success.');

        return redirect(route('get_eleve_by_class.classe'));
    }
}
