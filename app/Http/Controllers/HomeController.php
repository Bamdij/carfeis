<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Enseignant;
use App\Models\Eleve;
use App\Models\InscriptionEleve;
use App\Models\AnneeScolaire;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::count();
        $enseignant = Enseignant::count();
        $eleve = Eleve::count();
        $inscription = InscriptionEleve::orderBy('id','ASC')->get();
        $annee_scolaire= Anneescolaire::orderBy('id','ASC')->get();
            foreach ($annee_scolaire as $item) {
                $nomannee = InscriptionEleve::where('annee_scolaire_id',$item->id)->count();
                $item->count = $nomannee;
                $anne_name= Anneescolaire::find($item->id);
                $item->name = $anne_name->name;
            }

        return view('home',compact('user','enseignant','eleve','inscription','annee_scolaire'));
    }

}
