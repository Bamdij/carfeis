<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnseignantRequest;
use App\Http\Requests\UpdateEnseignantRequest;
use App\Repositories\EnseignantRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\EnseignantDatatable;
use App\Http\Controllers\AppHelper;
use App\Models\EnseignantClasse;
use App\Models\EnseignantAnnee;
use App\Models\AnneeScolaire;
use App\Models\Alphabetisation;
use App\Models\TypeProfession;
use App\Models\TypeEnseignant;
use Illuminate\Http\Request;
use App\Models\EleveClasse;
use App\Models\SousNiveau;
use App\Models\Enseignant;
use App\Models\LienParent;
use App\Models\InfoParent;
use App\Models\Niveau;
use App\Models\Eleve;
use DataTables;
use Response;
use Flash;


class EnseignantController extends AppBaseController
{
    /** @var  EnseignantRepository */
    private $enseignantRepository;

    public function __construct(EnseignantRepository $enseignantRepo)
    {
        $this->enseignantRepository = $enseignantRepo;
    }

     /**
     * Display a listing of the .
     *
     * @param EnseignantDatatable $EnseignantDatatable
     * @return Response
     */

    public function index(EnseignantDatatable $enseignantDatatable)
    {
        return $enseignantDatatable->render('enseignants.index');
    }
    /**
     * Show the form for creating a new Enseignant.
     *
     * @return Response
     */
    public function create()
    {
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        return view('enseignants.create')->with('type_enseig',$type_enseig)
                                        ->with('annee_scolaire',$annee_scolaire);
    }

    /**
     * Store a newly created Enseignant in storage.
     *
     * @param CreateEnseignantRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantRequest $request)
    {
        $input = $request->all();
        $enseignant = $this->enseignantRepository->create($input);
        if($enseignant){
            $helper = new AppHelper();
            $generatedNumero = $helper->generateNumeroCodeForTeacher(6, true, 'ud');
            $enseignant_upd = Enseignant::find($enseignant->id);
            $enseignant_upd->numero = $generatedNumero;
            $enseignant_upd->update();
        }
            if($request->type_enseignant_id){
            $enseignant_year = new EnseignantAnnee();
            $enseignant_year->enseignant_id = $enseignant->id;
            $enseignant_year->annee_scolaire_id = $request->annee_scolaire_id;
            $enseignant_year->type_enseignant_id = $request->type_enseignant_id;
            $enseignant_year->niveau_id = $request->niveau_id;
            $enseignant_year->sous_niveau_id = $request->sous_niveau_id;
            $enseignant_year->save();
       
        if($input['type_enseignant_id'] =="2" ){
            $enseignant_class = new EnseignantClasse();
            $enseignant_class->enseignant_id = $enseignant->id;
            $enseignant_class->sous_niveau_id = $request->sous_niveau_id;
            $enseignant_class->is_teacher_arab = true;
            $enseignant_class->save();
           Flash::success('Enseignant ajouté(e) avec success');
           return redirect(route('enseignants.index'));
        }else{
            $enseignant_class = new EnseignantClasse();
            $enseignant_class->enseignant_id = $enseignant->id;
            $enseignant_class->sous_niveau_id = $request->sous_niveau_id;
            $enseignant_class->is_teacher_arab = false;
            $enseignant_class->save();
            Flash::success('Enseignant ajouté(e) avec success');
            return redirect(route('enseignants.index'));
        }

    } 
    Flash::success('Enseignant ajouté(e) avec success');
    return redirect(route('enseignants.index'));

    }

    /**
     * Display the specified Enseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $enseignant = $this->enseignantRepository->find($id);
        $enseignant_year = EnseignantAnnee::where('enseignant_id',$enseignant->id)->get();
        foreach ($enseignant_year  as $key) {
            if(!empty($key->enseignant_id)){
                $key->name = EnseignantAnnee::find($key->id)->enseignant;
            }else{
                $key->name = "Information non disponible";
            }
            if(!empty($key->type_enseignant_id)){
                $key->type = EnseignantAnnee::find($key->id)->type_enseignant;
            }else{
                $key->type = "Information non disponible";
            }
            if(!empty($key->annee_scolaire_id)){
              $key->annee = EnseignantAnnee::find($key->id)->annee_scolaire;
            }else{
                $key->annee = "Information non disponible";
            }
            if(!empty($key->sous_niveau_id)){
                $key->sous_niveau = EnseignantAnnee::find($key->id)->sous_niveau;
                $key->name_sous =  $key->sous_niveau ->name;
            }else{
                $key->name_sous = "Information non disponible";
            }
        }
        if (empty($enseignant)) {
            Flash::error('Enseignant  non trouvé(e)');

            return redirect(route('enseignants.index'));
        }

        return view('enseignants.show')->with('enseignant_year', $enseignant_year)
                                    ->with('enseignant', $enseignant);
    }

    /**
     * Show the form for editing the specified Enseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $enseignant = $this->enseignantRepository->find($id);
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        // $enseignant_class = EnseignantClasse::where('enseignant_id',$enseignant->id)->get();
        $enseignant_class = EnseignantAnnee::where('enseignant_id',$enseignant->id)->get();
        if(!empty($enseignant_class)){
            foreach ($enseignant_class  as $key) {
                if(!empty($key->sous_niveau_id)){
                    $id = $key->sous_niveau_id;
                    $class = SousNiveau::find($id);
                    $key->name = $class->name;
                    $niveau = Niveau::find($class->niveau_id);
                    $type_ens = TypeEnseignant::find($niveau->type_enseignant_id);
                    $key->type = $type_ens->name;
                }else{
                    $key->name = "No classe";
                    $key->type = "Arabe";
                }
                
            }
        }
        // dd($enseignant_class);
        if (empty($enseignant)) {
            Flash::error('Enseignant  non trouvé(e)');

            return redirect(route('enseignants.index'));
        }
    
        return view('enseignants.edit')->with('type_enseig', $type_enseig)
                                        ->with('enseignant_class', $enseignant_class)
                                        ->with('enseignant', $enseignant)
                                        ->with('annee_scolaire', $annee_scolaire);
    }

    /**
     * Update the specified Enseignant in storage.
     *
     * @param int $id
     * @param UpdateEnseignantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantRequest $request)
    {
        $enseignant = $this->enseignantRepository->find($id);

        if (empty($enseignant)) {
            Flash::error('Enseignant  non trouvé(e)');

            return redirect(route('enseignants.index'));
        }

        $enseignant = $this->enseignantRepository->update($request->all(), $id);

        Flash::success('Enseignant modifié(e) avec success.');

        return redirect(route('enseignants.index'));
    }

    /**
     * Remove the specified Enseignant from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $enseignant = $this->enseignantRepository->find($id);

        if (empty($enseignant)) {
            Flash::error('Enseignant  non trouvé(e)');

            return redirect(route('enseignants.index'));
        }

        $this->enseignantRepository->delete($id);

        Flash::success('Enseignant deleted successfully.');

        return redirect(route('enseignants.index'));
    }

    public function getNiveauEnseignantByTypeEnseignant($id){
        $type_enseignant = TypeEnseignant::where('id',$id)->first();
        $niveau_eleve = Niveau::where('type_enseignant_id',$id)->get();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('enseignants.addniveauclass',compact('type_enseig','niveau_eleve','type_enseignant'))->render();
    }


    //Associer enseignant dans des classes

    public function AddClassToEnseignant($id){
        $niveau_eleve = Niveau::where('id',$id)->first();
        $class_eleve = SousNiveau::where('niveau_id',$id)->get();
        return view('enseignants.addclasse_enseignant',compact('class_eleve','niveau_eleve'))->render();
    }

    //Add classe edit professeur

    public function editClassePro($id){
        $enseignant = Enseignant::find($id);
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('enseignants.edit_status',compact('enseignant','type_enseig','annee_scolaire'))->render();
    }

     //Add classe edit professeur

     public function editClasseProfesseurClasse($id){
        $enseignant = Enseignant::find($id);
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('enseignants.edit_status_classe',compact('enseignant','type_enseig','annee_scolaire'))->render();
    }

    public function EditClasseProfesseur($id){
        $type_enseignant = TypeEnseignant::where('id',$id)->first();
        // dd($type_enseignant->enseignant);
        $niveau_eleve = Niveau::where('type_enseignant_id',$id)->get();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('enseignants.add_niveau',compact('type_enseig','niveau_eleve','type_enseignant'))->render();
        
    }
    public function EditClasseProf($id){
        $niveau_eleve = Niveau::where('id',$id)->first();
        $class_eleve = SousNiveau::where('niveau_id',$id)->get();
        return view('enseignants.niveau_classe_add',compact('class_eleve','niveau_eleve'))->render();
        
    }

    public function updateClasseEnseignant(Request $request,$id){
        $enseignant = Enseignant::find($id);
             $enseignant_class = new EnseignantClasse();
             $enseignant_class->enseignant_id = $enseignant->id;
             $enseignant_class->sous_niveau_id = $request->sous_niveau_id;
            //  $class_exist = EnseignantClasse::where('sous_niveau_id', $request->sous_niveau_id)->where('enseignant_id',$enseignant->id)->count();
             $class_exist = EnseignantAnnee::where('sous_niveau_id', $request->sous_niveau_id)->where('annee_scolaire_id', $request->annee_scolaire_id)->count();
            if($class_exist>0){
                $arr = array('status' => false,'data'=> $class_exist,'msg'=>'Oups cette classe est dejà associer à un  enseignant');
            }else{
                $enseignant_class->is_teacher_arab = false;
                $enseignant_class->save();
                if($enseignant_class){
                    $enseignant_year = new EnseignantAnnee();
                    $enseignant_year->enseignant_id = $enseignant->id;
                    $enseignant_year->annee_scolaire_id = $request->annee_scolaire_id;
                    $enseignant_year->type_enseignant_id = $request->type_enseignant_id;
                    $enseignant_year->niveau_id = $request->niveau_id;
                    $enseignant_year->sous_niveau_id = $request->sous_niveau_id;
                    $enseignant_year->save();
                }
                Flash::success('Enseignant ajouté(e) avec success');
                $arr = array('status' => true,'data'=> $class_exist);
            }
             return Response()->json($arr); 
            //  return back();
    }

    public function AddEnseignantOnly(Request $request,$id){
        $enseignant = Enseignant::find($id);
        $enseignant_class = EnseignantClasse::where('enseignant_id',$enseignant->id)->count();
        // $class_exist = EnseignantClasse::where('is_teacher_arab', true)->where('enseignant_id',$enseignant->id)->count();
        $class_exist = EnseignantAnnee::where('sous_niveau_id', $request->sous_niveau_id)->where('annee_scolaire_id', $request->annee_scolaire_id)->count();
        if($class_exist>0){
            $arr = array('status' => false,'data'=> $class_exist,'msg'=>'Oups cet enseignant est dejà associer à ce type d\'enseignement'); 
        }else{
            $enseignant_clas = new EnseignantClasse();
            $enseignant_clas->enseignant_id = $enseignant->id;
            $enseignant_clas->is_teacher_arab = true;
            $enseignant_clas->save();
            if($enseignant_clas){
                $enseignant_year = new EnseignantAnnee();
                $enseignant_year->enseignant_id = $enseignant->id;
                $enseignant_year->annee_scolaire_id = $request->annee_scolaire_id;
                $enseignant_year->type_enseignant_id = $request->type_enseignant_id;
                $enseignant_year->niveau_id = $request->niveau_id;
                $enseignant_year->sous_niveau_id = $request->sous_niveau_id;
                $enseignant_year->save();
            }
            Flash::success('Enseignant ajouté(e) avec success');
            // return back();
            $arr = array('status' => true,'data'=> $enseignant_clas);

            }
            return Response()->json($arr); 
         }

    public function deleteSousniveau($id){
        $enseignant_class = EnseignantClasse::find($id);
            if (empty($enseignant_class)) {
                Flash::error('Enseignant  non trouvé(e)');
    
                return back();
            }
    
            $enseignant_class->delete();
            Flash::success('Enseignant Classe supprimé avec success.');
    
            return back();
    }


    //Recuperation List des eleve par Classe

    public function getClassByEleve(){
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
         return view('eleve_classes.eleve_class',compact('type_enseig'));
    }


    public function getClasseByEleve($id){
        $type_enseignant = TypeEnseignant::where('id',$id)->first();
        // dd($type_enseignant->enseignant);
        $niveau_eleve = Niveau::where('type_enseignant_id',$id)->get();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('eleve_classes.addniveauclass',compact('type_enseig','niveau_eleve','type_enseignant'))->render();
        
    }
    public function getClasseByNiveauEleve($id){
        $niveau_eleve = Niveau::where('id',$id)->first();
        $class_eleve = SousNiveau::where('niveau_id',$id)->get();
        return view('eleve_classes.niveauadd',compact('class_eleve','niveau_eleve'))->render();
        
    }

    public function getEleveByClasse($id){
        $class_eleve = SousNiveau::find($id);
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        $eleve_class = EleveClasse::where('classe_id',$class_eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->get();
        if(!$eleve_class->isEmpty()){
            foreach ($eleve_class as $key) {
                if(!empty($key->eleve_id)){
                    $id = $key->eleve_id;
                    $elev = Eleve::find($id);
                    $key->name = $elev;
                   
                // $niveau = Niveau::find($class->niveau_id);
                // $type_ens = TypeEnseignant::find($niveau->type_enseignant_id);
                // $key->type = $type_ens->name;
            }
        }
        }
        // dd($eleve_class);
        return view('eleve_classes.table_class',compact('class_eleve','eleve_class'))->render();
    }


     /**
     * Display the specified Elevedetails.
     *
     * @param int $id
     *
     * @return Response
     */
    public function showdetailseleve($id)
    {
        $eleve = Eleve::find($id);

        if (empty($eleve)) {
            Flash::error('Eleve  non trouvé(e)');

            return redirect(route('eleves.index'));
        }
        $eleve_id = Eleve::find($id);
        $infoparent = InfoParent::where('eleve_id',$eleve->id)->first();
        $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->first();
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();
        $eleve_class = EleveClasse::where('eleve_id',$eleve->id)->first();
        $s_class =SousNiveau::find($eleve_class->classe_id);
        return view('eleve_classes.show_details')->with('eleve', $eleve)
                                    ->with('eleve_id', $eleve_id)
                                    ->with('type_prof', $type_prof)
                                    ->with('infoParent', $infoparent)
                                    ->with('alphbetisation', $alphbetisation)
                                    ->with('eleve_class', $eleve_class)
                                    ->with('s_class', $s_class)
                                    ->with('lien_parent', $lien_parent);
    }


    public function changeEleveClasse($id){
        $eleve_class_edit = EleveClasse::find($id);
        if (empty($eleve_class_edit)) {
            Flash::error('Enseignant  non trouvé(e)');

            return back();
        }
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('eleve_classes.edit_class',compact('eleve_class_edit', 'type_enseig'));
    }

    public function EditClasseEl($id){ 
        $type_enseignant = TypeEnseignant::where('id',$id)->first();
        // dd($type_enseignant->enseignant);
        $niveau_eleve = Niveau::where('type_enseignant_id',$id)->get();
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        return view('eleve_classes.elev_nivo',compact('type_enseig','niveau_eleve','type_enseignant'))->render();
        
    }

    public function getNiveaEleveAdd($id){
        $niveau_eleve = Niveau::where('id',$id)->first();
        $class_eleve = SousNiveau::where('niveau_id',$id)->get();
        return view('eleve_classes.niveau_choice',compact('class_eleve','niveau_eleve'))->render();
        
    }

    public function updateClasseStudent(Request $request,$id){
        $classe_elev = EleveClasse::find($id);
            // if($classe_elev>0){
            //     $arr = array('status' => false,'data'=> $classe_elev,'msg'=>'Oups cette classe est dejà associer à un  enseignant');
            // }else{
                $classe_elev->classe_id = $request->sous_niveau_id;
                $classe_elev->eleve_id = $classe_elev->eleve_id;
                $classe_elev->update();
                Flash::success('Enseignant ajouté(e) avec success');
                $arr = array('status' => true,'data'=> $classe_elev);
            // }
             return Response()->json($arr); 
            //  return back();
    }

    public function getNiveauEnseignantByClasseEleve($id){
        $classe_enseignant = SousNiveau::where('id',$id)->first();
        // $niveau_eleve = EnseignantClasse::where('sous_niveau_id',$classe_enseignant->id)->get();
        $niveau_eleve = EnseignantAnnee::where('sous_niveau_id',$classe_enseignant->id)->get();
        foreach ($niveau_eleve as  $value) {
           $id = $value->id;
           if(!empty($value->enseignant_id)){
                $enseignant = Enseignant::find($value->enseignant_id);
                $value->name = $enseignant->first_name.' '.$enseignant->last_name;
                $value->id = $enseignant->id;
           }
        }
        return view('enseignants.add_class_prof',compact('classe_enseignant','niveau_eleve'))->render();
    }
    public function getClasseByTeacher(){
        $type_enseig = TypeEnseignant::OrderBy('name','ASC')->get();
        $annee_niveau = AnneeScolaire::OrderBy('name','ASC')->get();
        return view('enseignants.get_Class',compact('type_enseig','annee_niveau'));
    }

    public function getEnseignantByTypeEnseignement($id){
        $type_enseig = TypeEnseignant::find($id);
        $annee_niveau = AnneeScolaire::where('status',true)->first();
        // $niveau_enseignant= EnseignantClasse::where('type_enseignant_id',$type_enseig->id)->where('annee_scolaire_id',$annee_niveau ->id)->get();
        $niveau_enseignant= EnseignantAnnee::where('type_enseignant_id',$type_enseig->id)->where('annee_scolaire_id',$annee_niveau ->id)->get();
        if(!$niveau_enseignant->isEmpty()){
            foreach ($niveau_enseignant as $value) {
                $id = $value->enseignant_id;
                $enseignant = Enseignant::find($id);
                $value->name  =$enseignant->first_name.' '.$enseignant->last_name;
            }
        }
       // dd($niveau_enseignant);
        return view('enseignants.get_enseignant',compact('type_enseig','niveau_enseignant'))->render();
    }
    // public function getEnseignantByAnneeScolaire($id){
    //     $annee_enseig = AnneeScolaire::find($id);
    //     $niveau_enseignant= Enseignant::where('annee_scolaire_id',$annee_enseig ->id)->get();
    //     return view('enseignants.get_enseignant',compact('annee_enseig','niveau_enseignant'))->render();
    // }
    public function getEnseignantByClasseEnseign($id){
        $type_enseig = Enseignant::find($id);
        // $niveau_enseignant= EnseignantClasse::where('enseignant_id',$type_enseig->id)->get();
        $niveau_enseignant= EnseignantAnnee::where('enseignant_id',$type_enseig->id)->get();
        foreach ($niveau_enseignant as  $value) {
            $id = $value->id;
            if(!empty($value->sous_niveau_id)){
                 $sous_niveau_id = SousNiveau::find($value->sous_niveau_id);
                 $value->name = $sous_niveau_id->name;
                 $value->id = $sous_niveau_id->id;
            }else{
                $value->name = "No Information";
            }
         }
        //  dd($niveau_enseignant);
        return view('enseignants.get_enseignant_class',compact('type_enseig','niveau_enseignant'))->render();
    }

    public function getEleveByClasseAndEnseignant($id){
        $sous_niv = SousNiveau::find($id);
        // $niveau_enseignant= EnseignantClasse::where('sous_niveau_id',$sous_niv->id)->first();
        $niveau_enseignant= EnseignantAnnee::where('sous_niveau_id',$sous_niv->id)->first();
        $enseigant_class = Enseignant::find($niveau_enseignant->enseignant_id);
        $eleve_class_edit = EleveClasse::where('classe_id',$sous_niv->id)->get();
        foreach ($eleve_class_edit as  $value) {
            $id = $value->id;
            if(!empty($value->eleve_id)){
                 $sous_niveau_id = Eleve::find($value->eleve_id);
                 if(!empty($value->name)){
                    $value->name = $sous_niveau_id;
                 }else{
                    $value->name ="Information non disponible";

                 }
            }
         }
        return view('enseignants.get_details_class',compact('sous_niv','eleve_class_edit','niveau_enseignant','enseigant_class'))->render();
    }

   
}
