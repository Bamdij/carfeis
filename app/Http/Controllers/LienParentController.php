<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLienParentRequest;
use App\Http\Requests\UpdateLienParentRequest;
use App\Repositories\LienParentRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\LienParentDatatable;
use Illuminate\Http\Request;
use DataTables;
use Flash;
use Response;

class LienParentController extends AppBaseController
{
    /** @var  LienParentRepository */
    private $lienParentRepository;

    public function __construct(LienParentRepository $lienParentRepo)
    {
        $this->lienParentRepository = $lienParentRepo;
    }

  
  /**
     * Display a listing of the .
     *
     * @param LienParentDatatable $lienParentDatatable
     * @return Response
     */

    public function index(LienParentDatatable $lienParentDatatable)
    {
        return $lienParentDatatable->render('lien_parents.index');
    }
    /**
     * Show the form for creating a new LienParent.
     *
     * @return Response
     */
    public function create()
    {
        return view('lien_parents.create');
    }

    /**
     * Store a newly created LienParent in storage.
     *
     * @param CreateLienParentRequest $request
     *
     * @return Response
     */
    public function store(CreateLienParentRequest $request)
    {
        $input = $request->all();

        $lienParent = $this->lienParentRepository->create($input);

        Flash::success('Lien Parent ajouté(e) avec success');

        return redirect(route('lienParents.index'));
    }

    /**
     * Display the specified LienParent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            Flash::error('Lien Parent  non trouvé(e)');

            return redirect(route('lienParents.index'));
        }

        return view('lien_parents.show')->with('lienParent', $lienParent);
    }

    /**
     * Show the form for editing the specified LienParent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            Flash::error('Lien Parent  non trouvé(e)');

            return redirect(route('lienParents.index'));
        }

        return view('lien_parents.edit')->with('lienParent', $lienParent);
    }

    /**
     * Update the specified LienParent in storage.
     *
     * @param int $id
     * @param UpdateLienParentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLienParentRequest $request)
    {
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            Flash::error('Lien Parent  non trouvé(e)');

            return redirect(route('lienParents.index'));
        }

        $lienParent = $this->lienParentRepository->update($request->all(), $id);

        Flash::success('Lien Parent modifié(e) avec success.');

        return redirect(route('lienParents.index'));
    }

    /**
     * Remove the specified LienParent from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            Flash::error('Lien Parent  non trouvé(e)');

            return redirect(route('lienParents.index'));
        }

        $this->lienParentRepository->delete($id);

        Flash::success('Lien Parent deleted successfully.');

        return redirect(route('lienParents.index'));
    }
}
