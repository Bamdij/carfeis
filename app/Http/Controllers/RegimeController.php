<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegimeRequest;
use App\Http\Requests\UpdateRegimeRequest;
use App\Repositories\RegimeRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\RegimeDatatable;
use Illuminate\Http\Request;
use Flash;
use Response;

class RegimeController extends AppBaseController
{
    /** @var  RegimeRepository */
    private $regimeRepository;

    public function __construct(RegimeRepository $regimeRepo)
    {
        $this->regimeRepository = $regimeRepo;
    }

    /**
     * Display a listing of the Regime.
     *
     * @param Request $request
     *
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $regimes = $this->regimeRepository->all();

    //     return view('regimes.index')
    //         ->with('regimes', $regimes);
    // }
      /**
     * Display a listing of the .
     *
     * @param RegimeDatatable $utilisateurDatatable
     * @return Response
     */

    public function index(RegimeDatatable $regimeDatatable)
    {
        return $regimeDatatable->render('regimes.index');
    }

    /**
     * Show the form for creating a new Regime.
     *
     * @return Response
     */
    public function create()
    {
        return view('regimes.create');
    }

    /**
     * Store a newly created Regime in storage.
     *
     * @param CreateRegimeRequest $request
     *
     * @return Response
     */
    public function store(CreateRegimeRequest $request)
    {
        $input = $request->all();

        $regime = $this->regimeRepository->create($input);

        Flash::success('Regime ajouté(e) avec success');

        return redirect(route('regimes.index'));
    }

    /**
     * Display the specified Regime.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            Flash::error('Regime  non trouvé(e)');

            return redirect(route('regimes.index'));
        }

        return view('regimes.show')->with('regime', $regime);
    }

    /**
     * Show the form for editing the specified Regime.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            Flash::error('Regime  non trouvé(e)');

            return redirect(route('regimes.index'));
        }

        return view('regimes.edit')->with('regime', $regime);
    }

    /**
     * Update the specified Regime in storage.
     *
     * @param int $id
     * @param UpdateRegimeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegimeRequest $request)
    {
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            Flash::error('Regime  non trouvé(e)');

            return redirect(route('regimes.index'));
        }

        $regime = $this->regimeRepository->update($request->all(), $id);

        Flash::success('Regime modifié(e) avec success.');

        return redirect(route('regimes.index'));
    }

    /**
     * Remove the specified Regime from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            Flash::error('Regime  non trouvé(e)');

            return redirect(route('regimes.index'));
        }

        $this->regimeRepository->delete($id);

        Flash::success('Regime deleted successfully.');

        return redirect(route('regimes.index'));
    }
}
