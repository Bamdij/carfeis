<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParentsRequest;
use App\Http\Requests\UpdateParentsRequest;
use App\Repositories\ParentsRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\ParentDatatable;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Parents;
use App\Models\Eleve;
use App\Models\AnneeScolaire;
use App\Models\SousNiveau;
use App\Models\InscriptionEleve;
use App\Models\Enseignant;
use App\Models\Regime;
use App\Models\InfoParent;
use App\Models\Alphabetisation;
use App\Models\TypeProfession;
use App\Models\PaiementEleve;
use App\Models\LienParent;
use App\Models\EvaluationMensuel;
use App\Models\EvaluationSemestre;
use App\Models\MensualiteEleve;
use App\Models\ParentPaiementAnnuelle;
use Flash;
use Auth;
use Response;

class ParentsController extends AppBaseController
{
    /** @var  ParentsRepository */
    private $parentsRepository;

    public function __construct(ParentsRepository $parentsRepo)
    {
        $this->parentsRepository = $parentsRepo;
    }


           /**
     * Display a listing of the .
     *
     * @param ParentDatatable $parentDatatable
     * @return Response
     */

    public function index(ParentDatatable $parentDatatable)
    {
        return $parentDatatable->render('parents.index');
    }
  

    /**
     * Show the form for creating a new Parents.
     *
     * @return Response
     */
    public function create()
    {
        return view('parents.create');
    }

    /**
     * Store a newly created Parents in storage.
     *
     * @param CreateParentsRequest $request
     *
     * @return Response
     */
    public function store(CreateParentsRequest $request)
    {
        $input = $request->all();

        $parents = $this->parentsRepository->create($input);

        Flash::success('Parents saved successfully.');

        return redirect(route('parents.index'));
    }

    /**
     * Display the specified Parents.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $parents =User::find($id);
        $parent_eleve = Parents::where('user_id',$parents->id)->get();
        if (empty($parents)) {
            Flash::error('Parents not found');

            return redirect(route('parents.index'));
        }
        if(!$parent_eleve->isEmpty()){
            foreach ($parent_eleve as $key) {
                $key->eleve= Eleve::find($key->eleve_id);
            }
        }
       
        return view('parents.show')->with('parents', $parents)
                                    ->with('parent_eleve', $parent_eleve);
    }

    /**
     * Show the form for editing the specified Parents.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $parents =User::find($id);
        $eleves = Eleve::orderBy('last_name','asc')->get();
        $eleves_parent = Parents::where('user_id',$parents->id)->get();

        if (empty($parents)) {
            Flash::error('Parents not found');

            return redirect(route('parents.index'));
        }

        return view('parents.edit')->with('eleves', $eleves)
                                    ->with('parents', $parents)
                                    ->with('eleves_parent', $eleves_parent);
    }

    /**
     * Update the specified Parents in storage.
     *
     * @param int $id
     * @param UpdateParentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParentsRequest $request)
    {
        $parents = $this->parentsRepository->find($id);
        
        if (empty($parents)) {
            Flash::error('Parents not found');

            return redirect(route('parents.index'));
        }

        $parents = $this->parentsRepository->update($request->all(), $id);

        Flash::success('Parents updated successfully.');

        return redirect(route('parents.index'));
    }

    /**
     * Remove the specified Parents from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $parents = Parents::where('user_id',$user->id)->get();

        if (empty($parents)) {
            Flash::error('Parents not found');

            return redirect(route('parents.index'));
        }
        $user->delete();
        if(!$parents->isEmpty()){
            $parents = Parents::where('user_id',$user->id)->delete();

        }

        Flash::success('Parents deleted successfully.');

        return redirect(route('parents.index'));
    }

    public function GetParentEleve(){
        $user = Auth::user();
        if(!empty($user)){
            $parent_eleve =  Parents::where('user_id',$user->id)->get();
            if(!$parent_eleve->isEmpty()){
                foreach ($parent_eleve as $key) {
                    $key->eleve= Eleve::find($key->eleve_id);
                }
            }
            return view('parents.eleve_parent',compact('user','parent_eleve'));
        }else{
            return view('errors.419');

        }
       
    }
    public function GetParentEleveHebdomadaire(){
        $user = Auth::user();
        if(!empty($user)){
            $parent_eleve =  Parents::where('user_id',$user->id)->get();
            if(!$parent_eleve->isEmpty()){
                foreach ($parent_eleve as $key) {
                    $key->eleve= Eleve::find($key->eleve_id);
                }
            }
            return view('parents.eleve_hebdomadaire',compact('user','parent_eleve'));
        }else{
            return view('errors.419');

        }
       
    }
    public function GetParentEleveMensuel(){
        $user = Auth::user();
        if(!empty($user)){
            $parent_eleve =  Parents::where('user_id',$user->id)->get();
            if(!$parent_eleve->isEmpty()){
                foreach ($parent_eleve as $key) {
                    $key->eleve= Eleve::find($key->eleve_id);
                }
            }
            return view('parents.eleve_mensuel',compact('user','parent_eleve'));
        }else{
            return view('errors.419');

        }
       
    }
    public function GetInfoEleveByParent($slug){
        $eleve =  Eleve::where('slug',$slug)->first();
        $eleve_id = Eleve::find($eleve->id);
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        if(!$eleve_class_by_year->isEmpty()){
            foreach ($eleve_class_by_year as $key ) {
                
                if(!empty($key->classe_id)){
                    $classe = SousNiveau::find($key->classe_id);
                    $key->classe = $classe->name;
                }else{
                    $key->classe ="Information Non disponible";
                }
                if(!empty($key->annee_scolaire_id)){
                    $annee_scolaire = AnneeScolaire::find($key->annee_scolaire_id);
                    $key->annee_scolaire = $annee_scolaire->name;
                }else{
                    $annee_scolaire ="Information Non disponible";
                    $key->annee_scolaire ="Information Non disponible";
                }
                if(!empty($key->enseignant_id)){
                    $enseignant = Enseignant::find($key->enseignant_id);
                    $key->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
                }else{
                    $key->enseignant ="Information Non disponible";
                }
                if(!empty($key->regime_id)){
                    $regime = Regime::find($key->regime_id);
                    $key->regime =  $regime->name;
                }else{
                    $key->regime ="Information Non disponible";
                }
            }
        }

        $infoparent = InfoParent::where('eleve_id',$eleve->id)->first();
        $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->first();
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();
        return view('parents.show_eleve')->with('eleve', $eleve)
                                    ->with('eleve_id', $eleve_id)
                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                    ->with('type_prof', $type_prof)
                                    ->with('infoParent', $infoparent)
                                    ->with('alphbetisation', $alphbetisation)
                                    ->with('lien_parent', $lien_parent)
                                   ;
    }

    public function GetEValuationSemestreByParent($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $evaluation = EvaluationSemestre::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->first();
       if(!empty($eleve_class_by_year)){ 
               if(!empty($eleve_class_by_year->classe_id)){
                   $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                   $eleve_class_by_year->classe = $classe->name;
               }else{
                   $eleve_class_by_year->classe ="Information Non disponible";
               }
               if(!empty($eleve_class_by_year->enseignant_id)){
                   $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                   $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
               }else{
                   $eleve_class_by_year->enseignant ="Information Non disponible";
               }
      
       }
       foreach ($evaluation as $value) {
        if(!empty($value->annee_scolaire_id)){
            $classe = Anneescolaire::find($value->annee_scolaire_id);
            $value->annee = $classe->name;
        }else{
            $value->annee ="Information Non disponible";
        } 
        if(!empty($value->user_id)){
            $classe = User::find($value->user_id);
            $value->user = $classe->name;
        }else{
            $value->user ="Information Non disponible";
        } 
       }
        return view('parents.show_evaluation')->with('evaluation', $evaluation)
                                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                                    ->with('eleve', $eleve);
    }

    public function GetEvaluationMensuelEleveByParent($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $evaluationMensuels = EvaluationMensuel::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_scolaire->id)->first();
       if(!empty($eleve_class_by_year)){ 
               if(!empty($eleve_class_by_year->classe_id)){
                   $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                   $eleve_class_by_year->classe = $classe->name;
               }else{
                   $eleve_class_by_year->classe ="Information Non disponible";
               }
               if(!empty($eleve_class_by_year->enseignant_id)){
                   $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                   $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
               }else{
                   $eleve_class_by_year->enseignant ="Information Non disponible";
               }
      
       }
       foreach ($evaluationMensuels as $value) {
        if(!empty($value->annee_scolaire_id)){
            $classe = Anneescolaire::find($value->annee_scolaire_id);
            $value->annee = $classe->name;
        }else{
            $value->annee ="Information Non disponible";
        } 
        if(!empty($value->user_id)){
            $classe = User::find($value->user_id);
            $value->user = $classe->name;
        }else{
            $value->user ="Information Non disponible";
        } 
       }
        return view('parents.show_info_mensuel')->with('evaluationMensuels', $evaluationMensuels)
                                                    ->with('annee_scolaire', $annee_scolaire)
                                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                                    ->with('eleve', $eleve);
    }

    //Pour le Paiement des Eleves Par les Parents
    public function GetParentElevePaiement(){
        $user = Auth::user();
        if(!empty($user)){
            $parent_eleve =  Parents::where('user_id',$user->id)->get();
            if(!$parent_eleve->isEmpty()){
                foreach ($parent_eleve as $key) {
                    $key->eleve= Eleve::find($key->eleve_id);
                    $key->month = MensualiteEleve::where('eleve_id',$key->eleve_id)->get();
                }
            }
            // dd($parent_eleve);

            return view('parents.eleve_Paiement',compact('user','parent_eleve'));
        }else{
            return view('errors.419');

        }
    }

    public function getInfoPaiementMonthByparent($slug){
        $user = Auth::user();
        if(!empty($user)){
        $eleve = Eleve::where('slug',$slug)->first();
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        $status_paiement = PaiementEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $eleve_regime = Regime::find($eleve_class_by_year->regime_id);
        $mensualite = MensualiteEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        $eleve_situat = ParentPaiementAnnuelle::where('eleve_id',$eleve->id)->where('annee_scloaire_id',$annee_niveau->id)->orderBy('id','DESC')->first();
        return view('parents.infos_after_paiement_parent',compact('eleve','status_paiement','eleve_situat','eleve_regime','annee_niveau','eleve_class_by_year','mensualite'));
         }else{
            return view('errors.419');

        }
    }


    //Ajout des Boutons col sm 4 pour les 12 mois de paiements

    // public function getAllMonthToPaiement(){
    //     $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        
    // }
    public function UpdateMonthPaiementByParent(Request $request,$id){
        $eleve_situat = ParentPaiementAnnuelle::find($id);
        if($request->payed_janv){
            $eleve_situat->payed_janv =$request->payed_janv;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($request->payed_fev){
            $eleve_situat->payed_fev =$request->payed_fev;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_mars){
            $eleve_situat->payed_mars =$request->payed_mars;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($request->payed_avril){
            $eleve_situat->payed_avril =$request->payed_avril;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($request->mai_payed){
            $eleve_situat->mai_payed =$request->mai_payed;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($request->juin_payed){
            $eleve_situat->juin_payed =$request->juin_payed;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_jul){
            $eleve_situat->payed_jul =$request->payed_jul;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_aout){
            $eleve_situat->payed_aout =$request->payed_aout;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($request->payed_sept){
            $eleve_situat->payed_sept =$request->payed_sept;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_octobre){
            $eleve_situat->payed_octobre =$request->payed_octobre;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_nov){
            $eleve_situat->payed_nov =$request->payed_nov;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($request->payed_dec){
            $eleve_situat->payed_dec =$request->payed_dec;
            $eleve_situat->update();
                if($eleve_situat){
                    $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                    return Response()->json($arr);   
                    }else{
                        $arr = array('status' => false,'msg'=>'nogood');
                        return Response()->json($arr);    
                    }

        }

       
    }


    public function GetEleveMontPaiement($query,$slug,$name_status){
        $user = Auth::user();
        if(!empty($user)){
        $eleve = Eleve::where('slug',$slug)->first();
        $month_paiement = $query;
        $status_name = $name_status;
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        $status_paiement = PaiementEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->first();
        $eleve_regime = Regime::find($eleve_class_by_year->regime_id);
        $mensualite = MensualiteEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_niveau->id)->orderBy('id','DESC')->get();
        $eleve_situat = ParentPaiementAnnuelle::where('eleve_id',$eleve->id)->where('annee_scloaire_id',$annee_niveau->id)->orderBy('id','DESC')->first();
        return view('parents.paiement_status_parent',compact('eleve','status_paiement','status_name','month_paiement','eleve_situat','eleve_regime','annee_niveau','eleve_class_by_year','mensualite'));
         }else{
            return view('errors.419');

        }
    }

    public function changeStatusMonth($name_status,$id){
        $eleve_situat = ParentPaiementAnnuelle::find($id);
        if($name_status=='payed_janv'){
            $eleve_situat->payed_janv =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($name_status=='payed_fev'){
            $eleve_situat->payed_fev =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_mars'){
            $eleve_situat->payed_mars =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($name_status=='payed_avril'){
            $eleve_situat->payed_avril =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($name_status=='mai_payed'){
            $eleve_situat->mai_payed =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($name_status=='juin_payed'){
            $eleve_situat->juin_payed =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_jul'){
            $eleve_situat->payed_jul =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_aout'){
            $eleve_situat->payed_aout =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
        }
        if($name_status=='payed_sept'){
            $eleve_situat->payed_sept =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_octobre'){
            $eleve_situat->payed_octobre =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_nov'){
            $eleve_situat->payed_nov =false;
            $eleve_situat->update();
            if($eleve_situat){
                $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }

        }
        if($name_status=='payed_dec'){
            $eleve_situat->payed_dec =false;
            $eleve_situat->update();
                if($eleve_situat){
                    $arr = array('status' => true,'data'=> $eleve_situat,'msg'=>'good');
                    return Response()->json($arr);   
                    }else{
                        $arr = array('status' => false,'msg'=>'nogood');
                        return Response()->json($arr);    
                    }

        }
    }



     //Paiement avec Paydunya
     public function PaiementMonthPostedPaydunuaParent(Request $request,$id){
        $paiement = PaiementEleve::find($id);
        $elev = Eleve::find($paiement->eleve_id);
        $paiement_month = 12;
        $x = 1;
            $new_month = new MensualiteEleve();
            $s_month = 's_month1';
            $s_mensualite = 's_mensualite1';
            $s_observation = 's_observation1';
            $s_montant_attendu = 's_montant_attendu1';
            $s_number_month = 's_number_month1';
            if(!is_null($request->$s_month) && !is_null($request->$s_mensualite)){
                $new_month->paiement_eleve_id = $paiement->id;
                $new_month->eleve_id = $paiement->eleve_id;
                $new_month->annee_scolaire_id = $paiement->annee_scolaire_id;
                $new_month->month = $request->$s_month;
                $new_month->mensualite = $request->$s_mensualite;
                $new_month->observation = $request->$s_observation;
                $new_month->montant_attendu = $request->$s_montant_attendu;
                $new_month->number_month = $request->$s_number_month;
                $new_month->periode_end = $request->periode_end;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->periode_debut = $request->periode_debut;
                $new_month->user_id = Auth::user()->id;
                $new_month->save();
                if($new_month){
                    $helper = new AppHelper();
                    $generatedNumero = $helper->generateNumeroCode(4, true, 'ud');
                    $month_update = MensualiteEleve::find($new_month->id);
                    $month_update->numero=$generatedNumero;
                    $month_update->update();
                    // if($month_update){ 
                    //     $user_name = User::where('status','superadmin')->first();
                    //         $notif = $user_name->notify(new UserNotificationPaiement($user_name,$month_update));
                    // }
                    $arr = array('status' => true,'data'=> $new_month,'msg'=>'good');
                return Response()->json($arr);   
                }else{
                    $arr = array('status' => false,'msg'=>'nogood');
                    return Response()->json($arr);    
                }
              
        }
        Flash::success('Mensualite  ajouter avec success.');
        //  return $this->getInfoPaiementMonth($elev->slug);
           

        //  return redirect(route('infor_paiement.mensual',$elev->slug));  
    }
    //Fin Paiement
}
