<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFraisInscriptionRequest;
use App\Http\Requests\UpdateFraisInscriptionRequest;
use App\Repositories\FraisInscriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use App\Models\Regime;
use App\Models\AnneeScolaire;
use App\Models\FraisInscription;
use App\DataTables\FraisInscriptionDatatable;
use Response;

class FraisInscriptionController extends AppBaseController
{
    /** @var  FraisInscriptionRepository */
    private $fraisInscriptionRepository;

    public function __construct(FraisInscriptionRepository $fraisInscriptionRepo)
    {
        $this->fraisInscriptionRepository = $fraisInscriptionRepo;
    }

    /**
     * Display a listing of the FraisInscription.
     *
     * @param Request $request
     *
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $fraisInscriptions = $this->fraisInscriptionRepository->all();

    //     return view('frais_inscriptions.index')
    //         ->with('fraisInscriptions', $fraisInscriptions);
    // }

     /**
     * Display a listing of the .
     *
     * @param FraisInscriptionDatatable $fraisInscriptionDatatable
     * @return Response
     */

    public function index(FraisInscriptionDatatable $fraisInscriptionDatatable)
    {
        return $fraisInscriptionDatatable->render('frais_inscriptions.index');
    }

    /**
     * Show the form for creating a new FraisInscription.
     *
     * @return Response
     */
    public function create()
    {
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->get();
        $regime = Regime::OrderBy('name','ASC')->get();
        return view('frais_inscriptions.create',compact('annee_niveau','regime'));
    }

    /**
     * Store a newly created FraisInscription in storage.
     *
     * @param CreateFraisInscriptionRequest $request
     *
     * @return Response
     */
    public function store(CreateFraisInscriptionRequest $request)
    {
        $input = $request->all();
        $frais = FraisInscription::where('annee_scolaire_id',$request->annee_scolaire_id)
                                    ->where('regime_id',$request->regime_id)->get();
        if(count($frais)> 0 ){
            return back()->withFail('Vous ne pouvez pas ajouter le même frais inscription pour une même année et le même regime! Veuillez choisir un autre regime s\'il vous plait.');;
        }
        $fraisInscription = $this->fraisInscriptionRepository->create($input);

        Flash::success('Frais Inscription ajouté avec  success.');

        return redirect(route('fraisInscriptions.index'));
    }

    /**
     * Display the specified FraisInscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            Flash::error('Frais Inscription  non trouvé(e)');

            return redirect(route('fraisInscriptions.index'));
        }

        return view('frais_inscriptions.show')->with('fraisInscription', $fraisInscription);
    }

    /**
     * Show the form for editing the specified FraisInscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->get();
        $regime = Regime::OrderBy('name','ASC')->get();
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            Flash::error('Frais Inscription  non trouvé(e)');

            return redirect(route('fraisInscriptions.index'));
        }

        return view('frais_inscriptions.edit')->with('annee_niveau', $annee_niveau)
                                        ->with('regime', $regime)
                                        ->with('fraisInscription', $fraisInscription);
    }

    /**
     * Update the specified FraisInscription in storage.
     *
     * @param int $id
     * @param UpdateFraisInscriptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFraisInscriptionRequest $request)
    {
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            Flash::error('Frais Inscription  non trouvé(e)');

            return redirect(route('fraisInscriptions.index'));
        }

        $fraisInscription = $this->fraisInscriptionRepository->update($request->all(), $id);

        Flash::success('Frais Inscription modifié(e) avec success.');

        return redirect(route('fraisInscriptions.index'));
    }

    /**
     * Remove the specified FraisInscription from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            Flash::error('Frais Inscription  non trouvé(e)');

            return redirect(route('fraisInscriptions.index'));
        }

        $this->fraisInscriptionRepository->delete($id);

        Flash::success('Frais Inscription deleted successfully.');

        return redirect(route('fraisInscriptions.index'));
    }
}
