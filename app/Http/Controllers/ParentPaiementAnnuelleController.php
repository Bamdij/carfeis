<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParentPaiementAnnuelleRequest;
use App\Http\Requests\UpdateParentPaiementAnnuelleRequest;
use App\Repositories\ParentPaiementAnnuelleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ParentPaiementAnnuelleController extends AppBaseController
{
    /** @var  ParentPaiementAnnuelleRepository */
    private $parentPaiementAnnuelleRepository;

    public function __construct(ParentPaiementAnnuelleRepository $parentPaiementAnnuelleRepo)
    {
        $this->parentPaiementAnnuelleRepository = $parentPaiementAnnuelleRepo;
    }

    /**
     * Display a listing of the ParentPaiementAnnuelle.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $parentPaiementAnnuelles = $this->parentPaiementAnnuelleRepository->all();

        return view('parent_paiement_annuelles.index')
            ->with('parentPaiementAnnuelles', $parentPaiementAnnuelles);
    }

    /**
     * Show the form for creating a new ParentPaiementAnnuelle.
     *
     * @return Response
     */
    public function create()
    {
        return view('parent_paiement_annuelles.create');
    }

    /**
     * Store a newly created ParentPaiementAnnuelle in storage.
     *
     * @param CreateParentPaiementAnnuelleRequest $request
     *
     * @return Response
     */
    public function store(CreateParentPaiementAnnuelleRequest $request)
    {
        $input = $request->all();

        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->create($input);

        Flash::success('Parent Paiement Annuelle saved successfully.');

        return redirect(route('parentPaiementAnnuelles.index'));
    }

    /**
     * Display the specified ParentPaiementAnnuelle.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->find($id);

        if (empty($parentPaiementAnnuelle)) {
            Flash::error('Parent Paiement Annuelle not found');

            return redirect(route('parentPaiementAnnuelles.index'));
        }

        return view('parent_paiement_annuelles.show')->with('parentPaiementAnnuelle', $parentPaiementAnnuelle);
    }

    /**
     * Show the form for editing the specified ParentPaiementAnnuelle.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->find($id);

        if (empty($parentPaiementAnnuelle)) {
            Flash::error('Parent Paiement Annuelle not found');

            return redirect(route('parentPaiementAnnuelles.index'));
        }

        return view('parent_paiement_annuelles.edit')->with('parentPaiementAnnuelle', $parentPaiementAnnuelle);
    }

    /**
     * Update the specified ParentPaiementAnnuelle in storage.
     *
     * @param int $id
     * @param UpdateParentPaiementAnnuelleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParentPaiementAnnuelleRequest $request)
    {
        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->find($id);

        if (empty($parentPaiementAnnuelle)) {
            Flash::error('Parent Paiement Annuelle not found');

            return redirect(route('parentPaiementAnnuelles.index'));
        }

        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->update($request->all(), $id);

        Flash::success('Parent Paiement Annuelle updated successfully.');

        return redirect(route('parentPaiementAnnuelles.index'));
    }

    /**
     * Remove the specified ParentPaiementAnnuelle from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $parentPaiementAnnuelle = $this->parentPaiementAnnuelleRepository->find($id);

        if (empty($parentPaiementAnnuelle)) {
            Flash::error('Parent Paiement Annuelle not found');

            return redirect(route('parentPaiementAnnuelles.index'));
        }

        $this->parentPaiementAnnuelleRepository->delete($id);

        Flash::success('Parent Paiement Annuelle deleted successfully.');

        return redirect(route('parentPaiementAnnuelles.index'));
    }
}
