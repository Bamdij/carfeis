<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnseignantAnneeRequest;
use App\Http\Requests\UpdateEnseignantAnneeRequest;
use App\Repositories\EnseignantAnneeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EnseignantAnneeController extends AppBaseController
{
    /** @var  EnseignantAnneeRepository */
    private $enseignantAnneeRepository;

    public function __construct(EnseignantAnneeRepository $enseignantAnneeRepo)
    {
        $this->enseignantAnneeRepository = $enseignantAnneeRepo;
    }

    /**
     * Display a listing of the EnseignantAnnee.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $enseignantAnnees = $this->enseignantAnneeRepository->all();

        return view('enseignant_annees.index')
            ->with('enseignantAnnees', $enseignantAnnees);
    }

    /**
     * Show the form for creating a new EnseignantAnnee.
     *
     * @return Response
     */
    public function create()
    {
        return view('enseignant_annees.create');
    }

    /**
     * Store a newly created EnseignantAnnee in storage.
     *
     * @param CreateEnseignantAnneeRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantAnneeRequest $request)
    {
        $input = $request->all();

        $enseignantAnnee = $this->enseignantAnneeRepository->create($input);

        Flash::success('Enseignant Annee ajouté(e) avec success');

        return redirect(route('enseignantAnnees.index'));
    }

    /**
     * Display the specified EnseignantAnnee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            Flash::error('Enseignant Annee  non trouvé(e)');

            return redirect(route('enseignantAnnees.index'));
        }

        return view('enseignant_annees.show')->with('enseignantAnnee', $enseignantAnnee);
    }

    /**
     * Show the form for editing the specified EnseignantAnnee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            Flash::error('Enseignant Annee  non trouvé(e)');

            return redirect(route('enseignantAnnees.index'));
        }

        return view('enseignant_annees.edit')->with('enseignantAnnee', $enseignantAnnee);
    }

    /**
     * Update the specified EnseignantAnnee in storage.
     *
     * @param int $id
     * @param UpdateEnseignantAnneeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantAnneeRequest $request)
    {
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            Flash::error('Enseignant Annee  non trouvé(e)');

            return redirect(route('enseignantAnnees.index'));
        }

        $enseignantAnnee = $this->enseignantAnneeRepository->update($request->all(), $id);

        Flash::success('Enseignant Annee modifié(e) avec success.');

        return redirect(route('enseignantAnnees.index'));
    }

    /**
     * Remove the specified EnseignantAnnee from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */ 
    public function destroy($id)
    {
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            Flash::error('Enseignant Annee  non trouvé(e)');

            return back();
        }

        $this->enseignantAnneeRepository->delete($id);

        Flash::success('Enseignant Annee suppimee avec  success.');

        return back();
    }
}
