<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypePaiementRequest;
use App\Http\Requests\UpdateTypePaiementRequest;
use App\Repositories\TypePaiementRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\TypePaiementDatatable;
use DataTables;
use Illuminate\Http\Request;
use Flash;
use Response;

class TypePaiementController extends AppBaseController
{
    /** @var  TypePaiementRepository */
    private $typePaiementRepository;

    public function __construct(TypePaiementRepository $typePaiementRepo)
    {
        $this->typePaiementRepository = $typePaiementRepo;
    }

      /**
     * Display a listing of the .
     *
     * @param TypePaiementDatatable $typePaiementDatatable
     * @return Response
     */

    public function index(TypePaiementDatatable $typePaiementDatatable)
    {
        return $typePaiementDatatable->render('type_paiements.index');
    }


    /**
     * Show the form for creating a new TypePaiement.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_paiements.create');
    }

    /**
     * Store a newly created TypePaiement in storage.
     *
     * @param CreateTypePaiementRequest $request
     *
     * @return Response
     */
    public function store(CreateTypePaiementRequest $request)
    {
        $input = $request->all();

        $typePaiement = $this->typePaiementRepository->create($input);

        Flash::success('Type Paiement ajouté(e) avec success');

        return redirect(route('type_paiements.index'));
    }

    /**
     * Display the specified TypePaiement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            Flash::error('Type Paiement  non trouvé(e)');

            return redirect(route('type_paiements.index'));
        }

        return view('type_paiements.show')->with('typePaiement', $typePaiement);
    }

    /**
     * Show the form for editing the specified TypePaiement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            Flash::error('Type Paiement  non trouvé(e)');

            return redirect(route('type_paiements.index'));
        }

        return view('type_paiements.edit')->with('typePaiement', $typePaiement);
    }

    /**
     * Update the specified TypePaiement in storage.
     *
     * @param int $id
     * @param UpdateTypePaiementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypePaiementRequest $request)
    {
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            Flash::error('Type Paiement  non trouvé(e)');

            return redirect(route('type_paiements.index'));
        }

        $typePaiement = $this->typePaiementRepository->update($request->all(), $id);

        Flash::success('Type Paiement modifié(e) avec success.');

        return redirect(route('type_paiements.index'));
    }

    /**
     * Remove the specified TypePaiement from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            Flash::error('Type Paiement  non trouvé(e)');

            return redirect(route('type_paiements.index'));
        }

        $this->typePaiementRepository->delete($id);

        Flash::success('Type Paiement deleted successfully.');

        return redirect(route('type_paiements.index'));
    }
}
