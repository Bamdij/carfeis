<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryDepenseRequest;
use App\Http\Requests\UpdateCategoryDepenseRequest;
use App\Repositories\CategoryDepenseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\CategoryDepenseDatatable;
use DataTables;
use Flash;
use Response;

class CategoryDepenseController extends AppBaseController
{
    /** @var  CategoryDepenseRepository */
    private $categoryDepenseRepository;

    public function __construct(CategoryDepenseRepository $categoryDepenseRepo)
    {
        $this->categoryDepenseRepository = $categoryDepenseRepo;
    }
    
     /**
     * Display a listing of the .
     *
     * @param CategoryDepenseDatatable $categoryDepenseDatatable
     * @return Response
     */

    public function index(CategoryDepenseDatatable $categoryDepenseDatatable)
    {
        return $categoryDepenseDatatable->render('category_depenses.index');
    }

    /**
     * Show the form for creating a new CategoryDepense.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_depenses.create');
    }

    /**
     * Store a newly created CategoryDepense in storage.
     *
     * @param CreateCategoryDepenseRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryDepenseRequest $request)
    {
        $input = $request->all();

        $categoryDepense = $this->categoryDepenseRepository->create($input);

        Flash::success('Category Depense ajouté(e) avec success.');

        return redirect(route('categoryDepenses.index'));
    }

    /**
     * Display the specified CategoryDepense.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            Flash::error('Category Depense  non trouvé(e)');

            return redirect(route('categoryDepenses.index'));
        }

        return view('category_depenses.show')->with('categoryDepense', $categoryDepense);
    }

    /**
     * Show the form for editing the specified CategoryDepense.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            Flash::error('Category Depense non trouvé(e)');

            return redirect(route('categoryDepenses.index'));
        }

        return view('category_depenses.edit')->with('categoryDepense', $categoryDepense);
    }

    /**
     * Update the specified CategoryDepense in storage.
     *
     * @param int $id
     * @param UpdateCategoryDepenseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryDepenseRequest $request)
    {
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            Flash::error('Category Depenset  non trouvé(e)');

            return redirect(route('categoryDepenses.index'));
        }

        $categoryDepense = $this->categoryDepenseRepository->update($request->all(), $id);

        Flash::success('Category Depense modifié(e) avec success.');

        return redirect(route('categoryDepenses.index'));
    }

    /**
     * Remove the specified CategoryDepense from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            Flash::error('Category Depense  non trouvé(e)');

            return redirect(route('categoryDepenses.index'));
        }

        $this->categoryDepenseRepository->delete($id);

        Flash::success('Category Depense supprime(e) avec success.');

        return redirect(route('categoryDepenses.index'));
    }
}
