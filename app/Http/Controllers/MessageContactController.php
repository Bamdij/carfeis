<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessageContactRequest;
use App\Http\Requests\UpdateMessageContactRequest;
use App\Repositories\MessageContactRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MessageContactController extends AppBaseController
{
    /** @var  MessageContactRepository */
    private $messageContactRepository;

    public function __construct(MessageContactRepository $messageContactRepo)
    {
        $this->messageContactRepository = $messageContactRepo;
    }

    /**
     * Display a listing of the MessageContact.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $messageContacts = $this->messageContactRepository->all();

        return view('message_contacts.index')
            ->with('messageContacts', $messageContacts);
    }

    /**
     * Show the form for creating a new MessageContact.
     *
     * @return Response
     */
    public function create()
    {
        return view('message_contacts.create');
    }

    /**
     * Store a newly created MessageContact in storage.
     *
     * @param CreateMessageContactRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageContactRequest $request)
    {
        $input = $request->all();

        $messageContact = $this->messageContactRepository->create($input);

        Flash::success('Message Contact saved successfully.');

        return redirect(route('messageContacts.index'));
    }

    /**
     * Display the specified MessageContact.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            Flash::error('Message Contact not found');

            return redirect(route('messageContacts.index'));
        }

        return view('message_contacts.show')->with('messageContact', $messageContact);
    }

    /**
     * Show the form for editing the specified MessageContact.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            Flash::error('Message Contact not found');

            return redirect(route('messageContacts.index'));
        }

        return view('message_contacts.edit')->with('messageContact', $messageContact);
    }

    /**
     * Update the specified MessageContact in storage.
     *
     * @param int $id
     * @param UpdateMessageContactRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageContactRequest $request)
    {
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            Flash::error('Message Contact not found');

            return redirect(route('messageContacts.index'));
        }

        $messageContact = $this->messageContactRepository->update($request->all(), $id);

        Flash::success('Message Contact updated successfully.');

        return redirect(route('messageContacts.index'));
    }

    /**
     * Remove the specified MessageContact from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            Flash::error('Message Contact not found');

            return redirect(route('messageContacts.index'));
        }

        $this->messageContactRepository->delete($id);

        Flash::success('Message Contact deleted successfully.');

        return redirect(route('messageContacts.index'));
    }
}
