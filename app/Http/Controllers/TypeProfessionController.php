<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateTypeProfessionRequest;
use App\Http\Requests\UpdateTypeProfessionRequest;
use App\Repositories\TypeProfessionRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\TypeProfessionDatatable;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use Response;

class TypeProfessionController extends AppBaseController
{
    /** @var  TypeProfessionRepository */
    private $typeProfessionRepository;

    public function __construct(TypeProfessionRepository $typeProfessionRepo)
    {
        $this->typeProfessionRepository = $typeProfessionRepo;
    }

    
     /**
     * Display a listing of the .
     *
     * @param TypeProfessionDatatable $typeProfessionDatatable
     * @return Response
     */

    public function index(TypeProfessionDatatable $typeProfessionDatatable)
    {
        return $typeProfessionDatatable->render('type_professions.index');
    }
    /**
     * Show the form for creating a new TypeProfession.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_professions.create');
    }

    /**
     * Store a newly created TypeProfession in storage.
     *
     * @param CreateTypeProfessionRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeProfessionRequest $request)
    {
        $input = $request->all();

        $typeProfession = $this->typeProfessionRepository->create($input);

        Flash::success('Type Profession ajouté(e) avec success');

        return redirect(route('typeProfessions.index'));
    }

    /**
     * Display the specified TypeProfession.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            Flash::error('Type Profession  non trouvé(e)');

            return redirect(route('typeProfessions.index'));
        }

        return view('type_professions.show')->with('typeProfession', $typeProfession);
    }

    /**
     * Show the form for editing the specified TypeProfession.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            Flash::error('Type Profession  non trouvé(e)');

            return redirect(route('typeProfessions.index'));
        }

        return view('type_professions.edit')->with('typeProfession', $typeProfession);
    }

    /**
     * Update the specified TypeProfession in storage.
     *
     * @param int $id
     * @param UpdateTypeProfessionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeProfessionRequest $request)
    {
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            Flash::error('Type Profession  non trouvé(e)');

            return redirect(route('typeProfessions.index'));
        }

        $typeProfession = $this->typeProfessionRepository->update($request->all(), $id);

        Flash::success('Type Profession modifié(e) avec success.');

        return redirect(route('typeProfessions.index'));
    }

    /**
     * Remove the specified TypeProfession from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            Flash::error('Type Profession  non trouvé(e)');

            return redirect(route('typeProfessions.index'));
        }

        $this->typeProfessionRepository->delete($id);

        Flash::success('Type Profession deleted successfully.');

        return redirect(route('typeProfessions.index'));
    }
}
