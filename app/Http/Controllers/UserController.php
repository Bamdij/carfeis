<?php

namespace App\Http\Controllers;
use App\Http\Controllers\AppHelper;
use App\DataTables\UtilisateurDatatable;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Eleve;
use App\Models\Parents;
use App\Models\AnneeScolaire;
use App\Models\InscriptionEleve;
use App\Models\TypeEnseignant;
use App\Models\AproposCarfeis;
use App\Models\ParentPaiementAnnuelle;
use DataTables;
use Response;
use Flash;
use Auth;

class UserController extends Controller
{
   
      /**
     * Display a listing of the .
     *
     * @param UtilisateurDatatable $utilisateurDatatable
     * @return Response
     */

    public function index(UtilisateurDatatable $utilisateurDatatable)
    {
        return $utilisateurDatatable->render('users.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }


    public function dashboardParent(){
        $user = Auth::user();
        $apropos = AproposCarfeis::orderBy('id','desc')->first();
       if(!empty($user)){ 
        $parent = Parents::where('user_id',$user->id)->count();
        $annee_scolaire= AnneeScolaire::orderBy('id','desc')->get();
            foreach ($annee_scolaire as $item) {
                $nomannee = InscriptionEleve::where('annee_scolaire_id',$item->id)->count();
                $item->count = $nomannee;
                $anne_name= Anneescolaire::find($item->id);
                $item->name = $anne_name->name;
            }
        return view('users.Parent_dashbord',compact('user','parent','annee_scolaire','apropos'));
    }else{
            return view('errors.419');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helper = new AppHelper();
        $generatedPassword = $helper->generateStrongCode(10, true, 'luds');

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->pseudo = $request->pseudo;
        $user->status = $request->status;
        $user->is_active_access == true;
        $user->password =  Hash::make($generatedPassword);
        $user->save();
        if($user){
            $notif = $user->notify(new UserNotification($user, $generatedPassword));
         }
            Flash::success('Utilisateur enregistré avec success.');
             return redirect(route('user.index'));  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $user = User::find($user);
        
        //dd($user);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $user = User::find($user);
        
        //dd($user);
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $user =User::find($user);
        if(empty($user)){
            Flash::error('Une erreur est survenue.Veuillez reessayer svp!.');
            return redirect(route('user.index'));   
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->pseudo = $request->pseudo;
        $user->status = $request->status;
        $user->is_active_access =$request->is_active_access;
        $user->update();
       // dd($user);
        Flash::success('Utilisateur modifié avec  success.');
        return redirect(route('user.index'));   
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::find($user);
        $user->delete();
        Flash::success('Utilisateur supprimé avec  success.');
        return redirect(route('user.index'));   
    }


    public function loginAdmin(Request $request){
    
        if (Auth::attempt(['email' => $request->type, 'password' => $request->password]) || Auth::attempt(['pseudo' => $request->type, 'password' => $request->password])) {
            if(Auth::user()->is_active_access == 1){
                if(Auth::user()->status == 'superadmin' || Auth::user()->status == 'directeur' || Auth::user()->status == 'responsable_finance' || Auth::user()->status == 'responsable_pedagogique' || Auth::user()->status == 'responsable_professeur'  ){
                    return redirect()->route('home');
                }else if(Auth::user()->status == 'Parent'){
                    return redirect()->route('dashboard.parent');
                }
                else{
                    Auth::logout();
                    return redirect()->back()->withFail('Vous n\'êtes pas autorisé à accéder au dashboard.');
                }
            }else{
                Auth::logout();
                return redirect()->back()->withFail('Vos accès sont désactivés. Veuillez contacter un administrateur.');
                
             }
        }else{
            Auth::logout();
            return redirect()->back()->withFail('Ouppsss! Une erreur est survenue');
        }
    }

    public function getmodalstatus($id){
        $user = User::find($id);
        return view('users.status',compact('user'))->render();
    }

    public function UpdateStatusUser(Request $request,$id){
        $user = User::find($id);
        $user->is_active_access =$request->is_active_access;
        $user->update();
        Flash::success('Status  modifié avec  success.');
        return redirect(route('user.index'));  

    }

    public function ChangeInfoProfile($slug){
        $user = User::where('slug',$slug)->first();
        return view('users.edit_profil',compact('user'));
    }

    public function UpdateProfileAdmin(Request $request){
         $user = Auth::user();
	    $user->name = $request->name;
        $user->pseudo = $request->pseudo;
        if($request->hasFile('image_path')){
            $file = $request->image_path;
            $extension = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $filename = 'user'.uniqid().'.'.$extension;
            $destinationPathImg = public_path().'/upload/users/image/';
                if($file->move($destinationPathImg,$filename)){
                    $path = '/upload/users/image/'.$filename;
                    $user->image_path = $path; 
                    $user->save();
                        if($user){
                            return back()->withSuccess('Profil modifiée avec success');
                        }
                    }
            }
        if($user->save()){
            return back()->withSuccess('Status modifiée avec success');
        }
    return back()->withFail('profil non changé');
        return back()->withSuccess('Profil modifiée avec success');

}

    public function ChangePassword(Request $request){
        $user = Auth::user();
        $password = $user->password;
        if($request->old_password){
            if(!Hash::check($request->old_password,$password)){
                return back()->withFail('Vous devez renseigner votre ancien mot de passe avant de modifier le profil');
            }else{
                if($request->new_password ){
                if($request->new_password== $request->confirm_password){
                    $user->password = Hash::make($request->new_password);
                    $user->save();
                    return back()->withSuccess('Mot de passe modifiée avec success');
                }else{
                    return back()->withFail('Nouveau mot de passe et confirmation mot de passe non conforme');
                }
            }
            }
        }
    }

    public function ChangePasswordUser(Request $request,$id){
        $user =User::find($id);
        if($request->new_password== $request->confirm_password){
            $user->password = Hash::make($request->new_password);
            $user->save();
            return back()->withSuccess('Mot de passe modifiée avec success');
        }else{
            return back()->withFail('Nouveau mot de passe et confirmation mot de passe non conforme'); 
        }
    }
    /**Function pour creation parent connect */
    public function GetTocreateParent(){
        $eleves = Eleve::orderBy('last_name','asc')->get();
        return view('parents.create_parent',compact('eleves'));
    }
    public function CreateParent(Request $request){
        $user = User::where('email',$request->email)->count();
        if($user >0){
            return back()->withFail('Parent dejà enregistré ou email dejà utilisé');

        }else{
        $helper = new AppHelper();
        $generatedPassword = $helper->generateStrongCode(10, true, 'luds');
        $anneeScolaire = AnneeScolaire::where('status',true)->first();

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->pseudo = $request->pseudo;
        $user->status = $request->status;
        $user->is_active_access == true;
        $user->password =  Hash::make($generatedPassword);
        $user->save();
        if($user){
            $notif = $user->notify(new UserNotification($user, $generatedPassword));
         }
         if($user){
                if(isset($request->eleve_id)){
                foreach($request->eleve_id as $item){
                    $parent = Parents::create([
                        'user_id' => $user->id,
                        'eleve_id' => $item
                    ]);
                
                $paiement = ParentPaiementAnnuelle::create([
                    'parent_id' => $user->id,
                    'annee_scloaire_id' => $anneeScolaire->id,
                    'eleve_id' => $item,
                    'janvier' =>"JANVIER",
                    'fevrier' => "FEVRIER",
                    'mars' => "Mars",
                    'avril' => "AVRIL",
                    'mai' => "MAI",
                    'juin' => "JUIN",
                    'juillett' => "JUILLET",
                    'aout' => "AOUT",
                    'septembre' => "SEPTEMBRE",
                    'octobre' => "OCTOBRE",
                    'novembre' => "NOVEMBRE",
                    'decembre' => "DECEMBRE"

                ]);
            }
            }
            Flash::success('Parent enregistré avec success.');
             return redirect(route('parents.index'));  
        }
    }
    }


    public function updateParent($id,Request $request){
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $user->email;
        $user->pseudo = $request->pseudo;
        $user->status = $request->status;
        $user->save();
         if($user){
                if(isset($request->eleve_id)){
                $parent = Parents::where('user_id',$user->id)->delete();
                if($parent){
                foreach($request->eleve_id as $item){
                    $parent = Parents::create([
                        'user_id' => $user->id,
                        'eleve_id' => $item
                    ]);
                }
            }
         }
            Flash::success('Parent modifié avec success.');
             return redirect(route('parents.index'));  
        }
    }

}


