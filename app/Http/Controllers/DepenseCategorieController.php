<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDepenseCategorieRequest;
use App\Http\Requests\UpdateDepenseCategorieRequest;
use App\Repositories\DepenseCategorieRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\SuiviDepenseDatatable;
use App\Models\AnneeScolaire;
use App\Models\CategoryDepense;
use App\Models\DepenseCategorie;
use App\Models\User;
use App\Notifications\UserNotificationDepense;
use Auth;
use DataTables;
use Flash;
use Response;

class DepenseCategorieController extends AppBaseController
{
    /** @var  DepenseCategorieRepository */
    private $depenseCategorieRepository;

    public function __construct(DepenseCategorieRepository $depenseCategorieRepo)
    {
        $this->depenseCategorieRepository = $depenseCategorieRepo;
    }

    /**
     * Display a listing of the DepenseCategorie.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categories= CategoryDepense::orderBy('id','DESC')->get();
        $depenseCategories = DepenseCategorie::all();
		// dd($depenseCategories);
        foreach($categories as $cate){
            foreach ($depenseCategories as $key) {
                 if($cate->id == $key->category_depense_id){
                    $cate->count =DepenseCategorie::where('category_depense_id',$key->category_depense_id)->count();
                 }
		}
            }
       
        //   dd($categories);
        return view('depense_categories.index')
                    ->with('categories', $categories)
                    ->with('depenseCategories', $depenseCategories);
   
	}
    
        public function getDeCategory($slug){

        $category = CategoryDepense::where('slug',$slug)->first();
        $depenses = DepenseCategorie::where('category_depense_id',$category->id)->orderBy('id','DESC')->get();
       return view('depense_categories.index_new')->with('category', $category)
                                                    ->with('depenses', $depenses);
    }
   /**
     * Display a listing of the .
     *
     * @param EleveDatatable $eleveDatatable
     * @return Response
     */

    // public function index(SuiviDepenseDatatable $suiviDepenseDatatable)
    // {
    //     return $suiviDepenseDatatable->render('depense_categories.index');
    // }


    /**
     * Show the form for creating a new DepenseCategorie.
     *
     * @return Response
     */
    public function create()
    {
        $categories = CategoryDepense::orderBy('id','DESC')->get();
        $annee_scolaire = Anneescolaire::where('status',true)->first();
        return view('depense_categories.create',compact('categories','annee_scolaire'));
    }

    /**
     * Store a newly created DepenseCategorie in storage.
     *
     * @param CreateDepenseCategorieRequest $request
     *
     * @return Response
     */
    public function store(CreateDepenseCategorieRequest $request)
    {
    
        $input = $request->all();
        $user = Auth::user();
        $category = CategoryDepense::find($request->category_depense_id);
        $depenseCategorie = $this->depenseCategorieRepository->create($input);
        if($depenseCategorie){
                $user_name = User::where('status','superadmin')->first();
                    $notif = $user_name->notify(new UserNotificationDepense($user_name,$depenseCategorie));
                
        }
        Flash::success('Depense Categorie ajouté(e) avec success.');

        return redirect(route('depense.category_depense',$category->slug));
    }

    /**
     * Display the specified DepenseCategorie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            Flash::error('Depense Categorie  non trouvé(e)');

            return redirect(route('depenseCategories.index'));
        }

        return view('depense_categories.show')->with('depenseCategorie', $depenseCategorie);
    }

    /**
     * Show the form for editing the specified DepenseCategorie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $depenseCategorie = $this->depenseCategorieRepository->find($id);
        $annee_scolaire = Anneescolaire::where('status',true)->first();
        $categories= CategoryDepense::orderBy('id','DESC')->get();

        if (empty($depenseCategorie)) {
            Flash::error('Depense Categorie non trouvé(e)');

            return redirect(route('depenseCategories.index'));
        }

        return view('depense_categories.edit')->with('annee_scolaire', $annee_scolaire)
                                                ->with('categories', $categories)
                                            ->with('depenseCategorie', $depenseCategorie);
    }

    /**
     * Update the specified DepenseCategorie in storage.
     *
     * @param int $id
     * @param UpdateDepenseCategorieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDepenseCategorieRequest $request)
    {
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            Flash::error('Depense Categorie  non trouvé(e)');

            return redirect(route('depenseCategories.index'));
        }

        $depenseCategorie = $this->depenseCategorieRepository->update($request->all(), $id);

        Flash::success('Depense Categorie modifié(e) avec success.');

        return redirect(route('depenseCategories.index'));
    }

    /**
     * Remove the specified DepenseCategorie from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            Flash::error('Depense Categorie  non trouvé(e)');

            return redirect(route('depenseCategories.index'));
        }

        $this->depenseCategorieRepository->delete($id);

        Flash::success('Depense Categorie supprime(e) avec success.');

        return redirect(route('depenseCategories.index'));
    }
}
