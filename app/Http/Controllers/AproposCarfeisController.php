<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAproposCarfeisRequest;
use App\Http\Requests\UpdateAproposCarfeisRequest;
use App\Repositories\AproposCarfeisRepository;
use App\Http\Controllers\AppBaseController;
use App\DataTables\AproposDatatable;
use DataTables;
use Illuminate\Http\Request;
use Flash;
use Response;

class AproposCarfeisController extends AppBaseController
{
    /** @var  AproposCarfeisRepository */
    private $aproposCarfeisRepository;

    public function __construct(AproposCarfeisRepository $aproposCarfeisRepo)
    {
        $this->aproposCarfeisRepository = $aproposCarfeisRepo;
    }

    /**
     * Display a listing of the AproposCarfeis.
     *
     * @param Request $request
     *
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $aproposCarfeis = $this->aproposCarfeisRepository->all();

    //     return view('apropos_carfeis.index')
    //         ->with('aproposCarfeis', $aproposCarfeis);
    // }
     /**
     * Display a listing of the .
     *
     * @param AnneeScolaireDatatable $anneeScolaireDatatable
     * @return Response
     */

    public function index(AproposDatatable $aproposDatatable)
    {
        return $aproposDatatable->render('apropos_carfeis.index');
    }

    /**
     * Show the form for creating a new AproposCarfeis.
     *
     * @return Response
     */
    public function create()
    {
        return view('apropos_carfeis.create');
    }

    /**
     * Store a newly created AproposCarfeis in storage.
     *
     * @param CreateAproposCarfeisRequest $request
     *
     * @return Response
     */
    public function store(CreateAproposCarfeisRequest $request)
    {
        $input = $request->all();

        $aproposCarfeis = $this->aproposCarfeisRepository->create($input);

        Flash::success('Apropos Carfeis saved successfully.');

        return redirect(route('aproposCarfeis.index'));
    }

    /**
     * Display the specified AproposCarfeis.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            Flash::error('Apropos Carfeis not found');

            return redirect(route('aproposCarfeis.index'));
        }

        return view('apropos_carfeis.show')->with('aproposCarfeis', $aproposCarfeis);
    }

    /**
     * Show the form for editing the specified AproposCarfeis.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            Flash::error('Apropos Carfeis not found');

            return redirect(route('aproposCarfeis.index'));
        }

        return view('apropos_carfeis.edit')->with('aproposCarfeis', $aproposCarfeis);
    }

    /**
     * Update the specified AproposCarfeis in storage.
     *
     * @param int $id
     * @param UpdateAproposCarfeisRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAproposCarfeisRequest $request)
    {
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            Flash::error('Apropos Carfeis not found');

            return redirect(route('aproposCarfeis.index'));
        }

        $aproposCarfeis = $this->aproposCarfeisRepository->update($request->all(), $id);

        Flash::success('Apropos Carfeis updated successfully.');

        return redirect(route('aproposCarfeis.index'));
    }

    /**
     * Remove the specified AproposCarfeis from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            Flash::error('Apropos Carfeis not found');

            return redirect(route('aproposCarfeis.index'));
        }

        $this->aproposCarfeisRepository->delete($id);

        Flash::success('Apropos Carfeis deleted successfully.');

        return redirect(route('aproposCarfeis.index'));
    }
}
