<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSousNiveauRequest;
use App\Http\Requests\UpdateSousNiveauRequest;
use App\Repositories\SousNiveauRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SousNiveauController extends AppBaseController
{
    /** @var  SousNiveauRepository */
    private $sousNiveauRepository;

    public function __construct(SousNiveauRepository $sousNiveauRepo)
    {
        $this->sousNiveauRepository = $sousNiveauRepo;
    }

    /**
     * Display a listing of the SousNiveau.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sousNiveaus = $this->sousNiveauRepository->all();

        return view('sous_niveaus.index')
            ->with('sousNiveaus', $sousNiveaus);
    }

    /**
     * Show the form for creating a new SousNiveau.
     *
     * @return Response
     */
    public function create()
    {
        return view('sous_niveaus.create');
    }

    /**
     * Store a newly created SousNiveau in storage.
     *
     * @param CreateSousNiveauRequest $request
     *
     * @return Response
     */
    public function store(CreateSousNiveauRequest $request)
    {
        $input = $request->all();

        $sousNiveau = $this->sousNiveauRepository->create($input);

        Flash::success('Sous Niveau ajouté(e) avec success');

        return redirect(route('sousNiveaus.index'));
    }

    /**
     * Display the specified SousNiveau.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            Flash::error('Sous Niveau  non trouvé(e)');

            return redirect(route('sousNiveaus.index'));
        }

        return view('sous_niveaus.show')->with('sousNiveau', $sousNiveau);
    }

    /**
     * Show the form for editing the specified SousNiveau.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            Flash::error('Sous Niveau  non trouvé(e)');

            return redirect(route('sousNiveaus.index'));
        }

        return view('sous_niveaus.edit')->with('sousNiveau', $sousNiveau);
    }

    /**
     * Update the specified SousNiveau in storage.
     *
     * @param int $id
     * @param UpdateSousNiveauRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSousNiveauRequest $request)
    {
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            Flash::error('Sous Niveau  non trouvé(e)');

            return redirect(route('niveaux.index') );
        }

        $sousNiveau = $this->sousNiveauRepository->update($request->all(), $id);

        Flash::success('Sous Niveau modifié(e) avec success.');

        return redirect(route('niveaux.index') );
        
    }

    /**
     * Remove the specified SousNiveau from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            Flash::error('Sous Niveau  non trouvé(e)');

            return redirect(route('niveaux.index') );
        }

        $this->sousNiveauRepository->delete($id);

        Flash::success('Sous Niveau deleted successfully.');

        return redirect(route('niveaux.index') );
    }
}
