<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Flash;
use Response;
use App\Models\User;
use App\Models\Eleve;
use App\Models\Enseignant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class AppHelper extends AppBaseController{

     /**
     * Generate password
     */
    function generateStrongCode($length = 6, $add_dashes = false, $available_sets = 'ud'){
        $sets = array();
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        $all = '';
        $code = '';
        foreach($sets as $set)
        {
            $code .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $code .= $all[array_rand($all)];

        $code = str_shuffle($code);

        if(!$add_dashes)
            return $code;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($code) > $dash_len)
        {
            $dash_str .= substr($code, 0, $dash_len) . '-';
            $code = substr($code, $dash_len);
        }
        $dash_str .= $code;

        return $dash_str;
    }

    //Generate number eleve
    /**
     * Generate password
     */
    function generateNumeroCode($length = 4, $add_dashes = false, $available_sets = 'ud'){
        $sets = array();
        if(strpos($available_sets, 'u') !== false)
            $sets[] = '0123456789ABCDEFGHIJKLMNOPKRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        $all = '';
        $code = '';
        foreach($sets as $set)
        {
            $code .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $code .= $all[array_rand($all)];

        $code = str_shuffle($code);

        if(!$add_dashes)
            return $code;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($code) > $dash_len)
        {
            $dash_str .= substr($code, 0, $dash_len) . '';
            $code = substr($code, $dash_len);
        }
        $dash_str .= $code;

        return $dash_str;
    }

//Generate number eleve
    /**
     * Generate password
     */
    function generateNumeroCodeForTeacher($length = 6, $add_dashes = false, $available_sets = 'ud'){
        $sets = array();
        if(strpos($available_sets, 'u') !== false)
            $sets[] = '0123456789ABCDEFGHIJKLMNOPKRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        $all = '';
        $code = '';
        foreach($sets as $set)
        {
            $code .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $code .= $all[array_rand($all)];

        $code = str_shuffle($code);

        if(!$add_dashes)
            return $code;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($code) > $dash_len)
        {
            $dash_str .= substr($code, 0, $dash_len) . '';
            $code = substr($code, $dash_len);
        }
        $dash_str .= $code;

        return $dash_str;
    }

}

