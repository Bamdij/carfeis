<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMensualiteEleveRequest;
use App\Http\Requests\UpdateMensualiteEleveRequest;
use App\Repositories\MensualiteEleveRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MensualiteEleveController extends AppBaseController
{
    /** @var  MensualiteEleveRepository */
    private $mensualiteEleveRepository;

    public function __construct(MensualiteEleveRepository $mensualiteEleveRepo)
    {
        $this->mensualiteEleveRepository = $mensualiteEleveRepo;
    }

    /**
     * Display a listing of the MensualiteEleve.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $mensualiteEleves = $this->mensualiteEleveRepository->all();

        return view('mensualite_eleves.index')
            ->with('mensualiteEleves', $mensualiteEleves);
    }

    /**
     * Show the form for creating a new MensualiteEleve.
     *
     * @return Response
     */
    public function create()
    {
        return view('mensualite_eleves.create');
    }

    /**
     * Store a newly created MensualiteEleve in storage.
     *
     * @param CreateMensualiteEleveRequest $request
     *
     * @return Response
     */
    public function store(CreateMensualiteEleveRequest $request)
    {
        $input = $request->all();

        $mensualiteEleve = $this->mensualiteEleveRepository->create($input);

        Flash::success('Mensualite Eleve ajouté(e) avec success');

        return redirect(route('mensualiteEleves.index'));
    }

    /**
     * Display the specified MensualiteEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            Flash::error('Mensualite Eleve  non trouvé(e)');

            return redirect(route('mensualiteEleves.index'));
        }

        return view('mensualite_eleves.show')->with('mensualiteEleve', $mensualiteEleve);
    }

    /**
     * Show the form for editing the specified MensualiteEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            Flash::error('Mensualite Eleve  non trouvé(e)');

            return redirect(route('mensualiteEleves.index'));
        }

        return view('mensualite_eleves.edit')->with('mensualiteEleve', $mensualiteEleve);
    }

    /**
     * Update the specified MensualiteEleve in storage.
     *
     * @param int $id
     * @param UpdateMensualiteEleveRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMensualiteEleveRequest $request)
    {
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            Flash::error('Mensualite Eleve  non trouvé(e)');

            return redirect(route('mensualiteEleves.index'));
        }

        $mensualiteEleve = $this->mensualiteEleveRepository->update($request->all(), $id);

        Flash::success('Mensualite Eleve modifié(e) avec success.');

        return redirect(route('mensualiteEleves.index'));
    }

    /**
     * Remove the specified MensualiteEleve from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            Flash::error('Mensualite Eleve  non trouvé(e)');

            return redirect(route('mensualiteEleves.index'));
        }

        $this->mensualiteEleveRepository->delete($id);
        Flash::success('Mensualite Eleve deleted successfully.');
        return redirect(route('get_eleve_to_paiement.annee_scolaire'));
        // return redirect(route('mensualiteEleves.index'));
    }
}
