<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInfoParentRequest;
use App\Http\Requests\UpdateInfoParentRequest;
use App\Repositories\InfoParentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Eleve;
use Flash;
use Response;

class InfoParentController extends AppBaseController
{
    /** @var  InfoParentRepository */
    private $infoParentRepository;

    public function __construct(InfoParentRepository $infoParentRepo)
    {
        $this->infoParentRepository = $infoParentRepo;
    }

    /**
     * Display a listing of the InfoParent.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $infoParents = $this->infoParentRepository->all();

        return view('info_parents.index')
            ->with('infoParents', $infoParents);
    }

    /**
     * Show the form for creating a new InfoParent.
     *
     * @return Response
     */
    public function create()
    {
        return view('info_parents.create');
    }
    public function getAlphabetisation($eleve_id)
    {
        $eleve_id = Eleve::find($eleve_id);
        return view('alphabetisations.create')->with('eleve_id', $eleve_id);
    }
    /**
     * Store a newly created InfoParent in storage.
     *
     * @param CreateInfoParentRequest $request
     *
     * @return Response
     */
    public function store(CreateInfoParentRequest $request)
    {
        $input = $request->all();
        $infoParent = $this->infoParentRepository->create($input);

        if($request->continuer == 'Ajouter et Continuer'){
            Flash::success('Eleve ajouté(e) avec success');
            return $this->getAlphabetisation($input['eleve_id']);
         }
         if($request->terminer == 'Terminer'){
            Flash::success('Eleve ajouté(e) avec success');
            return redirect(route('eleves.index'));
            }
        Flash::success('Info Parent ajouté(e) avec success');
        return redirect(route('eleves.index'));

        // return redirect(route('infoParents.index'));
    }

    /**
     * Display the specified InfoParent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            Flash::error('Info Parent  non trouvé(e)');

            return redirect(route('infoParents.index'));
        }

        return view('info_parents.show')->with('infoParent', $infoParent);
    }

    /**
     * Show the form for editing the specified InfoParent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            Flash::error('Info Parent  non trouvé(e)');

            return redirect(route('infoParents.index'));
        }

        return view('info_parents.edit')->with('infoParent', $infoParent);
    }

    /**
     * Update the specified InfoParent in storage.
     *
     * @param int $id
     * @param UpdateInfoParentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInfoParentRequest $request)
    {
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            Flash::error('Info Parent  non trouvé(e)');

            return redirect(route('infoParents.index'));
        }

        $infoParent = $this->infoParentRepository->update($request->all(), $id);

        Flash::success('Info Parent modifié(e) avec success.');
        return redirect(route('eleves.index'));

        // return redirect(route('infoParents.index'));
    }

    /**
     * Remove the specified InfoParent from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            Flash::error('Info Parent  non trouvé(e)');

            return redirect(route('infoParents.index'));
        }

        $this->infoParentRepository->delete($id);

        Flash::success('Info Parent deleted successfully.');

        return redirect(route('infoParents.index'));
    }
}
