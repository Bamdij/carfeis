<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvaluationSemestreRequest;
use App\Http\Requests\UpdateEvaluationSemestreRequest;
use App\Repositories\EvaluationSemestreRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\EvaluationDatatable;
use DataTables;
use Flash;
use Response;
use App\Models\Eleve;
use App\Models\AnneeScolaire;
use App\Models\EvaluationSemestre;
use App\Models\EleveClasse;
use App\Models\InscriptionEleve;
use App\Models\SousNiveau;
use App\Models\User;
use App\Models\Enseignant;
use App\Models\EvaluationMensuel;



class EvaluationSemestreController extends AppBaseController
{
    /** @var  EvaluationSemestreRepository */
    private $evaluationSemestreRepository;

    public function __construct(EvaluationSemestreRepository $evaluationSemestreRepo)
    {
        $this->evaluationSemestreRepository = $evaluationSemestreRepo;
    }

     /**
     * Display a listing of the .
     *
     * @param EleveDatatable $eleveDatatable
     * @return Response
     */

    public function indexinfo(EvaluationDatatable $evaluationDatatable)
    {
	
        return $evaluationDatatable->render('evaluation_semestres.index_detail');
    }

    /**
     * Display a listing of the EvaluationSemestre.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $evaluationSemestres = $this->evaluationSemestreRepository->all();

        return view('evaluation_semestres.index')
            ->with('evaluationSemestres', $evaluationSemestres);
    }

    /**
     * Show the form for creating a new EvaluationSemestre.
     *
     * @return Response
     */
    public function create()
    {
        return view('evaluation_semestres.create');
    }

    /**
     * Store a newly created EvaluationSemestre in storage.
     *
     * @param CreateEvaluationSemestreRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluationSemestreRequest $request)
    {
        $input = $request->all();

        $evaluationSemestre = $this->evaluationSemestreRepository->create($input);

        Flash::success('Evaluation Semestre saved successfully.');

        return redirect(route('evaluationSemestres.index'));
    }

    /**
     * Display the specified EvaluationSemestre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);

        if (empty($evaluationSemestre)) {
            Flash::error('Evaluation Semestre not found');

            return redirect(route('evaluationSemestres.index'));
        }

        return view('evaluation_semestres.show')->with('evaluationSemestre', $evaluationSemestre);
    }

    /**
     * Show the form for editing the specified EvaluationSemestre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);
        $eleve = Eleve::find( $evaluationSemestre->eleve_id);
        $annee_scolaire = AnneeScolaire::where('status',true)->first();

        if (empty($evaluationSemestre)) {
            Flash::error('Evaluation Semestre not found');

            return redirect(route('evaluationSemestres.index'));
        }

        return view('evaluation_semestres.edit')->with('eleve', $eleve)
                                                 ->with('annee_scolaire', $annee_scolaire)
                                                ->with('evaluationSemestre', $evaluationSemestre);
    }

    /**
     * Update the specified EvaluationSemestre in storage.
     *
     * @param int $id
     * @param UpdateEvaluationSemestreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluationSemestreRequest $request)
    {
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);
        $eleve = Eleve::find( $evaluationSemestre->eleve_id);

        if (empty($evaluationSemestre)) {
            Flash::error('Evaluation Semestre not found');

            return redirect(route('evaluationSemestres.index'));
        }

        $evaluationSemestre = $this->evaluationSemestreRepository->update($request->all(), $id);

        Flash::success('Evaluation Semestre Modifier avec success.');

        return redirect(route('evaluation_detail.eleve',$eleve->slug));
    }

    /**
     * Remove the specified EvaluationSemestre from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);

        if (empty($evaluationSemestre)) {
            Flash::error('Evaluation Semestre not found');

            return redirect(route('evaluationSemestres.index'));
        }

        $this->evaluationSemestreRepository->delete($id);

        Flash::success('Evaluation Semestre Suppimée avec success.');

        return redirect(route('getinfos.eleve'));
    }
    
    /**
     * get infos evaluation 
     * @return view
     */
    public function GetEvaluationEleve($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $evaluation = EvaluationSemestre::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_scolaire->id)->first();
       if(!empty($eleve_class_by_year)){ 
               if(!empty($eleve_class_by_year->classe_id)){
                   $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                   $eleve_class_by_year->classe = $classe->name;
               }else{
                   $eleve_class_by_year->classe ="Information Non disponible";
               }
               if(!empty($eleve_class_by_year->enseignant_id)){
                   $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                   $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
               }else{
                   $eleve_class_by_year->enseignant ="Information Non disponible";
               }
      
       }
       foreach ($evaluation as $value) {
        if(!empty($value->annee_scolaire_id)){
            $classe = Anneescolaire::find($value->annee_scolaire_id);
            $value->annee = $classe->name;
        }else{
            $value->annee ="Information Non disponible";
        } 
        if(!empty($value->user_id)){
            $classe = User::find($value->user_id);
            $value->user = $classe->name;
        }else{
            $value->user ="Information Non disponible";
        } 
       }
        return view('evaluation_semestres.show_info')->with('evaluation', $evaluation)
                                                    ->with('annee_scolaire', $annee_scolaire)
                                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                                    ->with('eleve', $eleve);
    }

    /**
     * Get Form to add evaluation
     * @return view
     * @param $id
     */

     public function getEleveToEvaluated($id){
         $eleve = Eleve::find($id);
         $annee_scolaire = AnneeScolaire::where('status',true)->first();
         $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_scolaire->id)->first();
        if(!empty($eleve_class_by_year)){ 
                if(!empty($eleve_class_by_year->classe_id)){
                    $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                    $eleve_class_by_year->classe = $classe->name;
                }else{
                    $eleve_class_by_year->classe ="Information Non disponible";
                }
                if(!empty($eleve_class_by_year->enseignant_id)){
                    $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                    $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
                }else{
                    $eleve_class_by_year->enseignant ="Information Non disponible";
                }
       
        }
         return view('evaluation_semestres.status')->with('annee_scolaire', $annee_scolaire)
                                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                                    ->with('eleve', $eleve)->render();
     }


     /**Get Evaluation Mensuel 
      * @param $id
      *@return iew
      */

      public function getEleveToEvaluatedMensuel($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $evaluationMensuels = EvaluationMensuel::where('eleve_id',$eleve->id)->orderBy('id','DESC')->get();
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_scolaire->id)->first();
       if(!empty($eleve_class_by_year)){ 
               if(!empty($eleve_class_by_year->classe_id)){
                   $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                   $eleve_class_by_year->classe = $classe->name;
               }else{
                   $eleve_class_by_year->classe ="Information Non disponible";
               }
               if(!empty($eleve_class_by_year->enseignant_id)){
                   $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                   $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
               }else{
                   $eleve_class_by_year->enseignant ="Information Non disponible";
               }
      
       }
       foreach ($evaluationMensuels as $value) {
        if(!empty($value->annee_scolaire_id)){
            $classe = Anneescolaire::find($value->annee_scolaire_id);
            $value->annee = $classe->name;
        }else{
            $value->annee ="Information Non disponible";
        } 
        if(!empty($value->user_id)){
            $classe = User::find($value->user_id);
            $value->user = $classe->name;
        }else{
            $value->user ="Information Non disponible";
        } 
       }
        return view('evaluation_semestres.show_info_mensuel')->with('evaluationMensuels', $evaluationMensuels)
                                                    ->with('annee_scolaire', $annee_scolaire)
                                                    ->with('eleve_class_by_year', $eleve_class_by_year)
                                                    ->with('eleve', $eleve);
      }

      
    /**
     * Get Form to add evaluation
     * @return view
     * @param $id
     */

     public function getEleveToEvaluerModalMensuel($id){
        $eleve = Eleve::find($id);
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        $eleve_class_by_year = InscriptionEleve::where('eleve_id',$eleve->id)->where('annee_scolaire_id',$annee_scolaire->id)->first();
       if(!empty($eleve_class_by_year)){ 
               if(!empty($eleve_class_by_year->classe_id)){
                   $classe = SousNiveau::find($eleve_class_by_year->classe_id);
                   $eleve_class_by_year->classe = $classe->name;
               }else{
                   $eleve_class_by_year->classe ="Information Non disponible";
               }
               if(!empty($eleve_class_by_year->enseignant_id)){
                   $enseignant = Enseignant::find($eleve_class_by_year->enseignant_id);
                   $eleve_class_by_year->enseignant =  $enseignant->first_name.' '. $enseignant->last_name;
               }else{
                   $eleve_class_by_year->enseignant ="Information Non disponible";
               }
      
       }
        return view('evaluation_semestres.status_mensuel')->with('annee_scolaire', $annee_scolaire)
                                                   ->with('eleve_class_by_year', $eleve_class_by_year)
                                                   ->with('eleve', $eleve)->render();
    }
}
