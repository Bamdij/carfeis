<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvaluationMensuelRequest;
use App\Http\Requests\UpdateEvaluationMensuelRequest;
use App\Repositories\EvaluationMensuelRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Eleve;
use App\Models\AnneeScolaire;
use App\Models\EvaluationSemestre;
use App\Models\EleveClasse;
use App\Models\InscriptionEleve;
use App\Models\SousNiveau;
use App\Models\User;
use App\Models\Enseignant;
use App\Models\EvaluationMensuel;
use Flash;
use Response;

class EvaluationMensuelController extends AppBaseController
{
    /** @var  EvaluationMensuelRepository */
    private $evaluationMensuelRepository;

    public function __construct(EvaluationMensuelRepository $evaluationMensuelRepo)
    {
        $this->evaluationMensuelRepository = $evaluationMensuelRepo;
    }

    /**
     * Display a listing of the EvaluationMensuel.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $evaluationMensuels = $this->evaluationMensuelRepository->all();

        return view('evaluation_mensuels.index')
            ->with('evaluationMensuels', $evaluationMensuels);
    }

    /**
     * Show the form for creating a new EvaluationMensuel.
     *
     * @return Response
     */
    public function create()
    {
        return view('evaluation_mensuels.create');
    }

    /**
     * Store a newly created EvaluationMensuel in storage.
     *
     * @param CreateEvaluationMensuelRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluationMensuelRequest $request)
    {
        $input = $request->all();

        $evaluationMensuel = $this->evaluationMensuelRepository->create($input);

        Flash::success('Evaluation Mensuel ajouter avec success.');

        return redirect(route('evaluationMensuels.index'));
    }

    /**
     * Display the specified EvaluationMensuel.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);

        if (empty($evaluationMensuel)) {
            Flash::error('Evaluation Mensuel not found');

            return redirect(route('evaluationMensuels.index'));
        }

        return view('evaluation_mensuels.show')->with('evaluationMensuel', $evaluationMensuel);
    }

    /**
     * Show the form for editing the specified EvaluationMensuel.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);
        $eleve = Eleve::find( $evaluationMensuel->eleve_id);
        $annee_scolaire = AnneeScolaire::where('status',true)->first();
        if (empty($evaluationMensuel)) {
            Flash::error('Evaluation Mensuel not found');

            return redirect(route('evaluationMensuels.index'));
        }

        return view('evaluation_mensuels.edit') ->with('eleve', $eleve)
                                                    ->with('annee_scolaire', $annee_scolaire)
                                                ->with('evaluationMensuel', $evaluationMensuel);
    }

    /**
     * Update the specified EvaluationMensuel in storage.
     *
     * @param int $id
     * @param UpdateEvaluationMensuelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluationMensuelRequest $request)
    {
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);
        $eleve = Eleve::find( $evaluationMensuel->eleve_id);

        if (empty($evaluationMensuel)) {
            Flash::error('Evaluation Mensuel not found');

            return redirect(route('evaluationMensuels.index'));
        }

        $evaluationMensuel = $this->evaluationMensuelRepository->update($request->all(), $id);

        Flash::success('Evaluation Mensuel updated successfully.');

        return redirect(route('evaluation_mensuel_detail.eleve',$eleve->slug));
    }

    /**
     * Remove the specified EvaluationMensuel from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);

        if (empty($evaluationMensuel)) {
            Flash::error('Evaluation Mensuel not found');

            return redirect(route('evaluationMensuels.index'));
        }

        $this->evaluationMensuelRepository->delete($id);

        Flash::success('Evaluation Mensuel supprimer avec success.');
        return redirect(route('getinfos.eleve'));
    }
}
