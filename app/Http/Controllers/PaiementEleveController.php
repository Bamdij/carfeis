<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaiementEleveRequest;
use App\Http\Requests\UpdatePaiementEleveRequest;
use App\Repositories\PaiementEleveRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Eleve;
use App\Models\SousNiveau;
use App\Models\EleveClasse;
use App\Models\InfoParent ;
use App\Models\TypeProfession;
use App\Models\AnneeScolaire;
use App\Models\Regime;
use App\Models\FraisInscription;
use App\Models\Alphabetisation;
use App\Models\PaiementEleve;
use App\Models\LienParent;
use App\Models\MensualiteEleve;
use App\Models\TypeEnseignant;
use App\Models\CategoryDepense;
use App\Models\DepenseCategorie;
use Flash;
use Carbon\Carbon;
use Response;

class PaiementEleveController extends AppBaseController
{
    /** @var  PaiementEleveRepository */
    private $paiementEleveRepository;

    public function __construct(PaiementEleveRepository $paiementEleveRepo)
    {
        $this->paiementEleveRepository = $paiementEleveRepo;
    }

    /**
     * Display a listing of the PaiementEleve.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $paiementEleves = $this->paiementEleveRepository->all();

        return view('paiement_eleves.index')
            ->with('paiementEleves', $paiementEleves);
    }

    /**
     * Show the form for creating a new PaiementEleve.
     *
     * @return Response
     */
    public function create()
    {
        return view('paiement_eleves.create');
    }

    public function PrintRecuEleve($eleve_id,$annee_scolaire_id,$regime_id,$frais_inscription_id){
        $eleve = Eleve::find($eleve_id);
        $annee = AnneeScolaire::find($annee_scolaire_id);
        $regime = Regime::find($regime_id);
        $frais = FraisInscription::find($frais_inscription_id);
        $paie = PaiementEleve::where('eleve_id',$eleve->id)
                                    ->where('annee_scolaire_id',$annee->id)
                                    ->where('frais_inscription_id',$frais->id)->first();
        $infoparent = InfoParent::where('eleve_id',$eleve->id)->first();
        $alphbetisation = Alphabetisation::where('eleve_id',$eleve->id)->first();
        $type_prof = TypeProfession::OrderBy('name','ASC')->get();
        $lien_parent = LienParent::OrderBy('name','ASC')->get();
        return view('paiement_eleves.show_detais')->with('eleve', $eleve)
                                    ->with('eleve_id', $eleve_id)
                                    ->with('type_prof', $type_prof)
                                    ->with('annee', $annee)
                                    ->with('regime', $regime)
                                    ->with('frais', $frais)
                                    ->with('paie', $paie)
                                    ->with('infoParent', $infoparent)
                                    ->with('alphbetisation', $alphbetisation)
                                    ->with('lien_parent', $lien_parent);

    }
    /**
     * Store a newly created PaiementEleve in storage.
     *
     * @param CreatePaiementEleveRequest $request
     *
     * @return Response
     */
    public function store(CreatePaiementEleveRequest $request)
    {
        $input = $request->all();        
        $paiementEleve = $this->paiementEleveRepository->create($input);
            if($paiementEleve){
                Flash::success('Eleve ajouté(e) avec success');
                return $this->PrintRecuEleve($paiementEleve->eleve_id,$paiementEleve->annee_scolaire_id,$paiementEleve->regime_id,$paiementEleve->frais_inscription_id);     
             }
             return $this->PrintRecuEleve($paiementEleve->eleve_id,$paiementEleve->annee_scolaire_id,$paiementEleve->regime_id,$paiementEleve->frais_inscription_id);     

        // return redirect(route('eleves.index'));
    }

    /**
     * Display the specified PaiementEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            Flash::error('Paiement Eleve  non trouvé(e)');

            return redirect(route('paiementEleves.index'));
        }

        return view('paiement_eleves.show')->with('paiementEleve', $paiementEleve);
    }

    /**
     * Show the form for editing the specified PaiementEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            Flash::error('Paiement Eleve  non trouvé(e)');

            return redirect(route('paiementEleves.index'));
        }

        return view('paiement_eleves.edit')->with('paiementEleve', $paiementEleve);
    }

    /**
     * Update the specified PaiementEleve in storage.
     *
     * @param int $id
     * @param UpdatePaiementEleveRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaiementEleveRequest $request)
    {
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            Flash::error('Paiement Eleve  non trouvé(e)');

            return redirect(route('paiementEleves.index'));
        }

        $paiementEleve = $this->paiementEleveRepository->update($request->all(), $id);

        Flash::success('Paiement Eleve modifié(e) avec success.');

        return redirect(route('paiementEleves.index'));
    }

    /**
     * Remove the specified PaiementEleve from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            Flash::error('Paiement Eleve  non trouvé(e)');

            return redirect(route('paiementEleves.index'));
        }

        $this->paiementEleveRepository->delete($id);

        Flash::success('Paiement Eleve deleted successfully.');

        return redirect(route('paiementEleves.index'));
    }
    //Founction pour afficher le recu

    public function getPrinRecu($slug){
        $eleve = Eleve::where('slug',$slug)->first();
        $paiement = PaiementEleve::where('eleve_id',$eleve->id)->first();
        $frais = FraisInscription::find($paiement->frais_inscription_id);
        $annee = AnneeScolaire::find($paiement->annee_scolaire_id);
        // dd($annee,$paiement);

        return view('paiement_eleves.print_recue',compact('eleve','paiement','annee','frais'));
    }

    public function getPaiementMonthRecu($slug){
        $mensualite =MensualiteEleve::where('slug',$slug)->first();
        $eleve = Eleve::where('id',$mensualite->eleve_id)->first();
        $paiement = PaiementEleve::where('eleve_id',$eleve->id)->first();
        $frais = FraisInscription::find($paiement->frais_inscription_id);
        $regime = Regime::find($frais->regime_id);
        $annee = AnneeScolaire::find($paiement->annee_scolaire_id);
        //   dd( $mensualite,$regime );

        return view('paiement_eleves.print_recue_month',compact('eleve','paiement','annee','frais','mensualite','regime'));
    }

    // Recuperation de la liste des paiement
    public function getListePaiementStudent(){
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        return view('paiement_eleves.info_paiement',compact('annee_niveau'));
       
    }


    public function getStudentPaiementDayByyear(Request $request){
        $paiement = MensualiteEleve::where('annee_scolaire_id',$request->annee_scolaire_id)->get();
            $date_debut = $request->date_debut;
            $end_debut = $request->end_date;
            $annee_scolaire = AnneeScolaire::find($request->annee_scolaire_id);
        if($request->date_debut){
            $end_debut = "";
            $result = MensualiteEleve::where('annee_scolaire_id',$request->annee_scolaire_id)->whereDate('created_at',$request->date_debut)->orderBy('id','DESC')->get();
           $sum = collect($result)->sum('mensualite');
            if(!$result->isEmpty()){
                foreach ($result as $key) {
                   $eleve = EleveClasse::where('eleve_id',$key->eleve_id)->where('annee_scolaire_id',$request->annee_scolaire_id)->first();
                   if(!empty($eleve)){
                   $classe = SousNiveau::find($eleve->classe_id);
                   if(!empty($classe)){
                    $key->classe = $classe->name;
                   }
                   $key->classe = "Information non dispo";
                }
                }
            }
           //  $arr = array('status' => true,'data'=> $result,'msg'=>'noo good');
           if($request->end_date){
            $end_debut = $request->end_date;
            $result = MensualiteEleve::where('annee_scolaire_id',$request->annee_scolaire_id)->whereBetween('created_at',[$request->date_debut,$request->end_date])->orderBy('id','DESC')->get();
                    $sum = collect($result)->sum('mensualite');
                if(!$result->isEmpty()){
                    foreach ($result as $key) {
                    $eleve = EleveClasse::where('eleve_id',$key->eleve_id)->where('annee_scolaire_id',$request->annee_scolaire_id)->first();
                   if(!empty($eleve)){
                    $classe = SousNiveau::find($eleve->classe_id);
                    if(!empty($classe)){
                        $key->classe = $classe->name;
                    }
                    $key->classe = "Information non dispo";
                    }
                    $key->classe = "Information non dispo";
                }

                }
        }
        
        // if($request->date_debut && $request->end_date){
        //     $result = MensualiteEleve::where('annee_scolaire_id',$request->annee_scolaire_id)->whereBetween('created_at',[$request->date_debut,$request->end_date])->orderBy('id','DESC')->get();
        //     $sum = collect($results)->sum('mensualite');
        // if(!$results->isEmpty()){
        //     foreach ($result as $key) {
        //        $eleve = EleveClasse::where('eleve_id',$key->eleve_id)->where('annee_scolaire_id',$request->annee_scolaire_id)->first();
        //        $classe = SousNiveau::find($eleve->classe_id);
        //        if(!empty($classe)){
        //         $key->classe = $classe->name;
        //        }
        //        $key->classe = "Information non dispo";
        //     }

        // }
       // $arr = array('status' => true,'data'=> $results,'msg'=>'noo good');
             //  return view('paiement_eleves.status_betwen',compact('results','date_debut','end_debut','sum','annee_scolaire'))->render();

        }
        Flash::success('Eleve ajouter avec success.');
           return view('paiement_eleves.status',compact('result','date_debut','end_debut','sum','annee_scolaire'))->render();
          //return Response()->json($arr);    
    }

     // Recuperation de la liste des depenses
     public function getListeDepensesStatus(){
        $categories= CategoryDepense::orderBy('id','DESC')->get();
        // $depenses = DepenseCategorie::where('category_depense_id',$category->id)->orderBy('id','DESC')->get();
        $annee_niveau = AnneeScolaire::OrderBy('id','desc')->where('status',true)->first();
        return view('paiement_eleves.info_depense_paiement',compact('annee_niveau','categories'));
       
    }

    public function getlistofdepensebydate(Request $request){
        $depenses = DepenseCategorie::where('category_depense_id',$request->category_id)
                                    ->where('annee_scolaire_id',$request->annee_scolaire_id)
                                    ->orderBy('id','DESC')
                                    ->get();
        $date_debut = $request->date_debut;
        $annee_scolaire = AnneeScolaire::find($request->annee_scolaire_id);
        $category= CategoryDepense::find($request->category_id);

        if($request->date_debut){
            $end_debut = "";

            $result = DepenseCategorie::where('annee_scolaire_id',$request->annee_scolaire_id)->whereDate('created_at',$request->date_debut)
                                        ->where('category_depense_id',$request->category_id)
                                        ->orderBy('id','DESC')->get();
            $sum = collect($result)->sum('montant_depense');
            if($request->date_debut && $request->end_date){
                $end_debut = $request->end_date;
                $result = DepenseCategorie::where('annee_scolaire_id',$request->annee_scolaire_id)
                                            ->where('category_depense_id',$request->category_id)
                                            ->whereBetween('created_at',[$request->date_debut,$request->end_date])
                                            ->orderBy('id','DESC')->get();
                $sum = collect($result)->sum('montant_depense');
            // return view('paiement_eleves.status_depenses',compact('result','date_debut','category','end_debut','sum','annee_scolaire'))->render();
        }
            // if($request->date_debut && $request->end_date){
            //     $result = DepenseCategorie::where('annee_scolaire_id',$request->annee_scolaire_id)
            //                                 ->where('category_depense_id',$request->category_id)
            //                                 ->whereBetween('created_at',[$request->date_debut,$request->end_date])
            //                                 ->orderBy('id','DESC')->get();
            //     $sum = collect($result)->sum('montant_depense');
            //  $arr = array('status' => true,'data'=> $result,'msg'=>'noo good');
                // return view('paiement_eleves.status_depenses',compact('result','date_debut','category','end_debut','sum','annee_scolaire'))->render();
        }
        return view('paiement_eleves.status_depenses',compact('result','date_debut','category','end_debut','sum','annee_scolaire'))->render();

    }

}
 