<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMensuualiteEnseignantRequest;
use App\Http\Requests\UpdateMensuualiteEnseignantRequest;
use App\Repositories\MensuualiteEnseignantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\MensualiteEnseignantDatatable;
use App\Models\MensuualiteEnseignant;
use App\Models\Enseignant;
use App\Models\TypePaiement;
use App\Models\AnneeScolaire;
use DataTables;
use Flash;
use Response;

class MensuualiteEnseignantController extends AppBaseController
{
    /** @var  MensuualiteEnseignantRepository */
    private $mensuualiteEnseignantRepository;

    public function __construct(MensuualiteEnseignantRepository $mensuualiteEnseignantRepo)
    {
        $this->mensuualiteEnseignantRepository = $mensuualiteEnseignantRepo;
    }

      /**
     * Display a listing of the .
     *
     * @param MensualiteEnseignantDatatable $mensualiteEnseignantDatatable
     * @return Response
     */

    public function index(MensualiteEnseignantDatatable $mensualiteEnseignantDatatable)
    {
        return $mensualiteEnseignantDatatable->render('mensuualite_enseignants.index');
    }

    /**
     * Display a listing of the MensuualiteEnseignant.
     *
     * @param Request $request
     *
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $mensuualiteEnseignants = $this->mensuualiteEnseignantRepository->all();

    //     return view('mensuualite_enseignants.index')
    //         ->with('mensuualiteEnseignants', $mensuualiteEnseignants);
    // }

    /**
     * Show the form for creating a new MensuualiteEnseignant.
     *
     * @return Response
     */
    public function create()
    {
       
        return view('mensuualite_enseignants.create');
    }

    /**
     * Store a newly created MensuualiteEnseignant in storage.
     *
     * @param CreateMensuualiteEnseignantRequest $request
     *
     * @return Response
     */
    public function store(CreateMensuualiteEnseignantRequest $request)
    {
        $input = $request->all();
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->create($input);
        if($mensuualiteEnseignant){
            $enseignant =Enseignant::find($mensuualiteEnseignant->enseignant_id);
            Flash::success('Mensuualite Enseignant modifié(e) avec success.');
            return redirect(route('enseignant.paiement',$enseignant->slug));
        }

        Flash::success('Mensuualite Enseignant ajouté(e) avec success');

        return redirect(route('mensuualiteEnseignants.index'));
    }

    /**
     * Display the specified MensuualiteEnseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);

        if (empty($mensuualiteEnseignant)) {
            Flash::error('Mensuualite Enseignant  non trouvé(e)');

            return redirect(route('mensuualiteEnseignants.index'));
        }

        return view('mensuualite_enseignants.show')->with('mensuualiteEnseignant', $mensuualiteEnseignant);
    }

    /**
     * Show the form for editing the specified MensuualiteEnseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);
        $enseignant =Enseignant::find($mensuualiteEnseignant->enseignant_id);
        if (empty($mensuualiteEnseignant)) {
            Flash::error('Mensuualite Enseignant  non trouvé(e)');

            return redirect(route('mensuualiteEnseignants.index'));
        }

        return view('mensuualite_enseignants.edit')->with('enseignant',$enseignant)
                                                    ->with('mensuualiteEnseignant', $mensuualiteEnseignant);
    }

    /**
     * Update the specified MensuualiteEnseignant in storage.
     *
     * @param int $id
     * @param UpdateMensuualiteEnseignantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMensuualiteEnseignantRequest $request)
    {
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);
        $enseignant =Enseignant::find($mensuualiteEnseignant->enseignant_id);
        
        if (empty($mensuualiteEnseignant)) {
            Flash::error('Mensuualite Enseignant  non trouvé(e)');

            return redirect(route('mensuualiteEnseignants.index'));
        }

        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->update($request->all(), $id);

        Flash::success('Mensuualite Enseignant modifié(e) avec success.');

        return redirect(route('enseignant.paiement',$enseignant->slug));
    }

    /**
     * Remove the specified MensuualiteEnseignant from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);

        if (empty($mensuualiteEnseignant)) {
            Flash::error('Mensuualite Enseignant  non trouvé(e)');

            return redirect(route('mensuualiteEnseignants.index'));
        }

        $this->mensuualiteEnseignantRepository->delete($id);

        Flash::success('Mensuualite Enseignant deleted successfully.');

        return redirect(route('mensuualiteEnseignants.index'));
    }

    public function getInfoPaiementEnseignant($slug){
            $enseignant = Enseignant::where('slug',$slug)->first();
            $type_paiment = TypePaiement::orderBy('name','ASC')->get();
            $annee_scolaire = AnneeScolaire::where('status',true)->first();
            $paiement_month = MensuualiteEnseignant::where('enseignant_id',$enseignant->id)->get();
            return view('mensuualite_enseignants.show_info')->with('enseignant', $enseignant)
                                                            ->with('type_paiment', $type_paiment)
                                                            ->with('annee_scolaire', $annee_scolaire)
                                                            ->with('paiement_month', $paiement_month);
    }

    public function getRecuPaiement($slug){
        $mensualite = MensuualiteEnseignant::where('slug',$slug)->first();
        $enseignant = Enseignant::where('id',$mensualite->enseignant_id)->first();
        return view('mensuualite_enseignants.recu_paiement',compact('mensualite','enseignant'));
    }
}
