<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeEnseignantRequest;
use App\Http\Requests\UpdateTypeEnseignantRequest;
use App\Repositories\TypeEnseignantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\TypeEnseignantDatatable;
use DataTables;
use Flash;
use Response;

class TypeEnseignantController extends AppBaseController
{
    /** @var  TypeEnseignantRepository */
    private $typeEnseignantRepository;

    public function __construct(TypeEnseignantRepository $typeEnseignantRepo)
    {
        $this->typeEnseignantRepository = $typeEnseignantRepo;
    }

     /**
     * Display a listing of the .
     *
     * @param TypeEnseignantDatatable $typeEnseignantDatatable
     * @return Response
     */

    public function index(TypeEnseignantDatatable $typeEnseignantDatatable)
    {
        return $typeEnseignantDatatable->render('type_enseignants.index');
    }

    /**
     * Show the form for creating a new TypeEnseignant.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_enseignants.create');
    }

    /**
     * Store a newly created TypeEnseignant in storage.
     *
     * @param CreateTypeEnseignantRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeEnseignantRequest $request)
    {
        $input = $request->all();

        $typeEnseignant = $this->typeEnseignantRepository->create($input);

        Flash::success('Type Enseignant ajouté(e) avec success');

        return redirect(route('typeEnseignants.index'));
    }

    /**
     * Display the specified TypeEnseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            Flash::error('Type Enseignant  non trouvé(e)');

            return redirect(route('typeEnseignants.index'));
        }

        return view('type_enseignants.show')->with('typeEnseignant', $typeEnseignant);
    }

    /**
     * Show the form for editing the specified TypeEnseignant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            Flash::error('Type Enseignant  non trouvé(e)');

            return redirect(route('typeEnseignants.index'));
        }

        return view('type_enseignants.edit')->with('typeEnseignant', $typeEnseignant);
    }

    /**
     * Update the specified TypeEnseignant in storage.
     *
     * @param int $id
     * @param UpdateTypeEnseignantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeEnseignantRequest $request)
    {
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            Flash::error('Type Enseignant  non trouvé(e)');

            return redirect(route('typeEnseignants.index'));
        }

        $typeEnseignant = $this->typeEnseignantRepository->update($request->all(), $id);

        Flash::success('Type Enseignant modifié(e) avec success.');

        return redirect(route('typeEnseignants.index'));
    }

    /**
     * Remove the specified TypeEnseignant from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            Flash::error('Type Enseignant  non trouvé(e)');

            return redirect(route('typeEnseignants.index'));
        }

        $this->typeEnseignantRepository->delete($id);

        Flash::success('Type Enseignant deleted successfully.');

        return redirect(route('typeEnseignants.index'));
    }
}
