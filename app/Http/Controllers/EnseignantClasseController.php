<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnseignantClasseRequest;
use App\Http\Requests\UpdateEnseignantClasseRequest;
use App\Repositories\EnseignantClasseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\EnseignantAnnee;
use Flash;
use Response;

class EnseignantClasseController extends AppBaseController
{
    /** @var  EnseignantClasseRepository */
    private $enseignantClasseRepository;

    public function __construct(EnseignantClasseRepository $enseignantClasseRepo)
    {
        $this->enseignantClasseRepository = $enseignantClasseRepo;
    }

    /**
     * Display a listing of the EnseignantClasse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $enseignantClasses = $this->enseignantClasseRepository->all();

        return view('enseignant_classes.index')
            ->with('enseignantClasses', $enseignantClasses);
    }

    /**
     * Show the form for creating a new EnseignantClasse.
     *
     * @return Response
     */
    public function create()
    {
        return view('enseignant_classes.create');
    }

    /**
     * Store a newly created EnseignantClasse in storage.
     *
     * @param CreateEnseignantClasseRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantClasseRequest $request)
    {
        $input = $request->all();

        $enseignantClasse = $this->enseignantClasseRepository->create($input);

        Flash::success('Enseignant Classe ajouté(e) avec success');

        return redirect(route('enseignantClasses.index'));
    }

    /**
     * Display the specified EnseignantClasse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            Flash::error('Enseignant Classe  non trouvé(e)');

            return redirect(route('enseignantClasses.index'));
        }

        return view('enseignant_classes.show')->with('enseignantClasse', $enseignantClasse);
    }

    /**
     * Show the form for editing the specified EnseignantClasse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            Flash::error('Enseignant Classe  non trouvé(e)');

            return redirect(route('enseignantClasses.index'));
        }

        return view('enseignant_classes.edit')->with('enseignantClasse', $enseignantClasse);
    }

    /**
     * Update the specified EnseignantClasse in storage.
     *
     * @param int $id
     * @param UpdateEnseignantClasseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantClasseRequest $request)
    {
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            Flash::error('Enseignant Classe  non trouvé(e)');

            return redirect(route('enseignantClasses.index'));
        }

        $enseignantClasse = $this->enseignantClasseRepository->update($request->all(), $id);

        Flash::success('Enseignant Classe modifié(e) avec success.');

        return redirect(route('enseignantClasses.index'));
    }

    /**
     * Remove the specified EnseignantClasse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            Flash::error('Enseignant Classe  non trouvé(e)');

            return redirect(route('enseignantClasses.index'));
        }
        $this->enseignantClasseRepository->delete($id);

        Flash::success('Enseignant Classe deleted successfully.');

        return redirect(route('enseignantClasses.index'));
    }
}
