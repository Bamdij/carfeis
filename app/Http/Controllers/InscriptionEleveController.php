<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInscriptionEleveRequest;
use App\Http\Requests\UpdateInscriptionEleveRequest;
use App\Repositories\InscriptionEleveRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class InscriptionEleveController extends AppBaseController
{
    /** @var  InscriptionEleveRepository */
    private $inscriptionEleveRepository;

    public function __construct(InscriptionEleveRepository $inscriptionEleveRepo)
    {
        $this->inscriptionEleveRepository = $inscriptionEleveRepo;
    }

    /**
     * Display a listing of the InscriptionEleve.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $inscriptionEleves = $this->inscriptionEleveRepository->all();

        return view('inscription_eleves.index')
            ->with('inscriptionEleves', $inscriptionEleves);
    }

    /**
     * Show the form for creating a new InscriptionEleve.
     *
     * @return Response
     */
    public function create()
    {
        return view('inscription_eleves.create');
    }

    /**
     * Store a newly created InscriptionEleve in storage.
     *
     * @param CreateInscriptionEleveRequest $request
     *
     * @return Response
     */
    public function store(CreateInscriptionEleveRequest $request)
    {
        $input = $request->all();

        $inscriptionEleve = $this->inscriptionEleveRepository->create($input);

        Flash::success('Inscription Eleve ajouté(e) avec success');

        return redirect(route('inscriptionEleves.index'));
    }

    /**
     * Display the specified InscriptionEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            Flash::error('Inscription Eleve  non trouvé(e)');

            return redirect(route('inscriptionEleves.index'));
        }

        return view('inscription_eleves.show')->with('inscriptionEleve', $inscriptionEleve);
    }

    /**
     * Show the form for editing the specified InscriptionEleve.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            Flash::error('Inscription Eleve  non trouvé(e)');

            return redirect(route('inscriptionEleves.index'));
        }

        return view('inscription_eleves.edit')->with('inscriptionEleve', $inscriptionEleve);
    }

    /**
     * Update the specified InscriptionEleve in storage.
     *
     * @param int $id
     * @param UpdateInscriptionEleveRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInscriptionEleveRequest $request)
    {
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            Flash::error('Inscription Eleve  non trouvé(e)');

            return redirect(route('inscriptionEleves.index'));
        }

        $inscriptionEleve = $this->inscriptionEleveRepository->update($request->all(), $id);

        Flash::success('Inscription Eleve modifié(e) avec success.');

        return redirect(route('inscriptionEleves.index'));
    }

    /**
     * Remove the specified InscriptionEleve from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            Flash::error('Inscription Eleve  non trouvé(e)');

            return redirect(route('inscriptionEleves.index'));
        }

        $this->inscriptionEleveRepository->delete($id);

        Flash::success('Inscription Eleve deleted successfully.');

        return redirect(route('inscriptionEleves.index'));
    }
}
