<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paydunya\Setup;
use Paydunya\Checkout\CheckoutInvoice;
use Paydunya\Checkout\Store;
use App\Models\MensualiteEleve;
use App\Models\Eleve;
use App\Models\ParentPaiementAnnuelle;
use App\Models\User;
use App\Models\Parents;
use Auth;
use Flash;
use App\Notifications\UserNotificationPaiement;
use App\Notifications\UserNotificationPaiementParent;


class PaydunyaCarfeisController extends Controller
{
	public function __construct()
	{
		//Configuration de l'API de Paydunya
        Setup::setMasterKey('YkqaW4C9-VDqB-aUST-KAz7-bEuveSmeZAbQ');
        Setup::setPublicKey('test_public_fFsOBmBfvZKvt0AdI2mDFjYr6rU');
        Setup::setPrivateKey('test_private_9ju6vWcKB9M6g2GZ35XtxoDNSlD');
        Setup::setToken('J2G1Jlwugj2douN8XdqB');
        Setup::setMode('test');

	}

    /**
     * PayDunya Form view
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaydunyaPaymentView($slug)
    {
        $mensualite = MensualiteEleve::where('slug',$slug)->first();
        $eleve = Eleve::where('id',$mensualite->eleve_id)->first();
    	return view('paydunya-view.larapaydunya-form',compact('mensualite','eleve'));

    }

    /**
     * Send payment infos to Paydunya
     *
     * @return \Illuminate\Http\Response
     */
    public function postPayDunyaPaymentInfos(Request $request)
    {
    	$this->validate($request, [
    		'amount' => 'required'
    	]);
        $id = $request->mensualite_id;
        $month=MensualiteEleve::find($id);
        $eleve = Eleve::where('id',$month->eleve_id)->first();
    	$amount = $request->get('amount');

    	// Initialisation d'un Paiement Avec Redirection (PAR)

    	

        // Création d'une nouvelle instace de la classe Store
        $store = new Store();

        //Configuration des informations de votre service/entreprise
        $store::setName("carfeis");
		$store::setTagline("Paiement Mensuel");
		$store::setPhoneNumber("+221776208943");
		$store::setPostalAddress("Grand Yoff - Keur Serigne Fallou");
		$store::setWebsiteUrl("https://www.dev.carfeis.com");
		$store::setLogoUrl("https://www.dev.carfeis.com/images/logoCarfeisEd.png");
		//Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $store::setReturnUrl(route('paydunya.payment.status'));

        $invoice = new CheckoutInvoice();

        // Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $invoice->setReturnUrl(route('paydunya.payment.status'));

        // Configuration globale de l'URL de redirection après annulation de paiement
        $invoice->setCancelUrl(route('paydunya.payment.status'));

        /* L'ajout d'éléments à votre facture est très basique.
		Les paramètres attendus sont nom du produit, la quantité, le prix unitaire,
		le prix total et une description optionelle. */
        $invoice->addItem('Montant de paiement Mensuel eleve  '.$eleve->first_name.' '.$eleve->last_name,$month->number_month, $amount, $amount);

        // Ajouter une description de la transaction
        $invoice->setDescription("Paiement Mensuel");

        // Ajouter le montant total de la transaction
        $invoice->setTotalAmount($amount);

        // Les données personnalisées vous permettent d'ajouter des données supplémentaires à vos informations de facture
		// que pourrez récupérer plus tard à l'aide de l'action de callback Confirm
        $invoice->addCustomData("sup_data", 'Merci d\'avoir soutenu notre cause');
        $invoice->addCustomData("donate_value", $amount);
        $invoice->addCustomData("id", $id);

        //Cas où l'url de paiment n'est pas créé
        $no_create_url_alert = '';
        //Création de l'URL de redirection pour le paiement
        $transaction_url = '';

        if($invoice->create()) {  
            $transaction_url = $invoice->getInvoiceUrl();

        }else{
            echo $invoice->response_text;
            $no_create_url_alert = 'Erreur de création de l\'URL de redirection au paiement';
        }

    	return view('paydunya-view.larapaydunya-payment-infos', compact('amount', 'transaction_url','eleve','month'));
    }

    /**
     * Paydunya payment execution
     *
     * @return 
     */
    public function paydunyaPaymentStatus(Request $request)
    {
    	$store = new Store();
    	$store::setName("Carfeis"); 

    	//Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $store::setReturnUrl(route('paydunya.payment.status'));

    	// PayDunya rajoutera automatiquement le token de la facture sous forme de QUERYSTRING "token"
		// si vous avez configuré un "return_url" ou "cancel_url".
		// Récupérez donc le token en pur PHP via $_GET['token']
    	$token = $_GET['token'];

    	// Variable qui doit avoir comme valeur le statut de la transaction
        $payment_status = '';

        $invoice = new CheckoutInvoice();
        if ($invoice->confirm($token)) {
        	// Statut de la transaction
            $payment_status = $invoice->getStatus();

            // Vérification de l'état du paiement
            if($payment_status == 'completed'){ // Action si le paiement a été bien validé par paydunya

            	// Récupérer l'URL du reçu PDF électronique pour téléchargement
                $invoice_pdf = $invoice->getReceiptUrl();

                // Vous pouvez récupérer le nom, l'adresse email et le
			  	// numéro de téléphone du client en utilisant
			  	// les méthodes suivantes
			  	$giver_name =  $invoice->getCustomerInfo('name');
			  	$giver_email = $invoice->getCustomerInfo('email');
			  	$giver_phone = $invoice->getCustomerInfo('phone');
                  $giver_month_eleve = $invoice->getCustomData("id");
                //  dd($giver_month_eleve);      
                   $month_update=MensualiteEleve::find($giver_month_eleve);
                   $month_update->is_paid = true;
                   $month_update->invoice_url = $invoice_pdf;
                   $month_update->update();
                   $eleve = Eleve::where('id',$month_update->eleve_id)->first();
                   $user_name = User::where('status','superadmin')->first();
                   $use_parent = Auth::user();
                   $notif_parent = $use_parent->notify(new UserNotificationPaiementParent($use_parent,$month_update));
                   $notif = $user_name->notify(new UserNotificationPaiement($user_name,$month_update));
			  	// Récupérer n'importe laquelle des données personnalisées que
				// vous avez eu à rajouter précédemment à la facture.
				// Assurer à utiliser les mêmes clés que celles utilisées
				// lors de la configuration.
				$greating_message =  $invoice->getCustomData("sup_data");
				$donate_amount = $invoice->getCustomData("donate_value");

				// Vous pouvez aussi récupérer le montant total spécifié précédemment
                $donate_value = $invoice->getTotalAmount();
                // dd($eleve);
            	return view('paydunya-view.paydunya-payment-status', compact('payment_status', 'invoice_pdf', 'giver_name', 'giver_email', 'giver_phone', 'greating_message', 'donate_amount','eleve'));

            }
        }
        else{
			$payment_status = 'ANNULER';
			$message = 'Oupss... Dommage que vous ayez annulé votre le paiemeent.';
            $invoice->getCustomData("id");
            $month_update=MensualiteEleve::find($invoice->getCustomData("id"));
            $eleve = Eleve::where('id',$month_update->eleve_id)->first();
            $month_update->delete();
            Flash::error('Paiement Annuler');
            return redirect(route('infor_paiement.mensual',$eleve->slug));
			// return view('paydunya-view.paydunya-payment-status', compact('message', 'payment_status','eleve'));
		}

    }


    /**Pour le paiement inscription */
    public function getPaydunyaParent($payed_month,$status_payed,$paiemet_id,$mensualite_id){
        $mensualite = MensualiteEleve::where('slug',$mensualite_id)->first();
        $eleve = Eleve::where('id',$mensualite->eleve_id)->first();
    	return view('parents.paydunya-view.larapaydunya-form',compact('mensualite','eleve','payed_month','status_payed','paiemet_id'));
    }
    public function postPayDunyaPaymentInfosParents(Request $request)
    {
    	$this->validate($request, [
    		'amount' => 'required'
    	]);
        $id = $request->mensualite_id;
        $payed_month = $request->payed_month;
        $status_payed = $request->status_payed;
        $paiement= $request->paiemet_id;
        $month=MensualiteEleve::find($id);
        $paiement_mont=ParentPaiementAnnuelle::find($request->paiemet_id);
        $eleve = Eleve::where('id',$month->eleve_id)->first();
    	$amount = $request->get('amount');

    	// Initialisation d'un Paiement Avec Redirection (PAR)

    	

        // Création d'une nouvelle instace de la cla sse Store
        $store = new Store();

        //Configuration des informations de votre service/entreprise
        $store::setName("carfeis");
		$store::setTagline("Paiement Mensuel");
		$store::setPhoneNumber("+221776208943");
		$store::setPostalAddress("Grand Yoff - Keur Serigne Fallou");
		$store::setWebsiteUrl("https://www.dev.carfeis.com");
		$store::setLogoUrl("https://www.dev.carfeis.com/images/logoCarfeisEd.png");

		//Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $store::setReturnUrl(route('paydunya.payment.status_parent'));

        $invoice = new CheckoutInvoice();

        // Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $invoice->setReturnUrl(route('paydunya.payment.status_parent'));

        // Configuration globale de l'URL de redirection après annulation de paiement
        $invoice->setCancelUrl(route('paydunya.payment.status_parent'));

        /* L'ajout d'éléments à votre facture est très basique.
		Les paramètres attendus sont nom du produit, la quantité, le prix unitaire,
		le prix total et une description optionelle. */
        $invoice->addItem('Montant de paiement Mensuel eleve  '.$eleve->first_name.' '.$eleve->last_name,$month->number_month, $amount, $amount);

        // Ajouter une description de la transaction
        $invoice->setDescription("Paiement Mensuel");

        // Ajouter le montant total de la transaction
        $invoice->setTotalAmount($amount);

        // Les données personnalisées vous permettent d'ajouter des données supplémentaires à vos informations de facture
		// que pourrez récupérer plus tard à l'aide de l'action de callback Confirm
        $invoice->addCustomData("sup_data", 'Merci d\'avoir validé le paiement');
        $invoice->addCustomData("donate_value", $amount);
        $invoice->addCustomData("id", $id);
        $invoice->addCustomData("payed_month", $payed_month);
        $invoice->addCustomData("status_payed", $status_payed);
        $invoice->addCustomData("paiement_id", $paiement);

        //Cas où l'url de paiment n'est pas créé
        $no_create_url_alert = '';
        //Création de l'URL de redirection pour le paiement
        $transaction_url = '';

        if($invoice->create()) {  
            $transaction_url = $invoice->getInvoiceUrl();

        }else{
            echo $invoice->response_text;
            $no_create_url_alert = 'Erreur de création de l\'URL de redirection au paiement';
        }

    	return view('parents.paydunya-view.larapaydunya-payment-infos', compact('amount', 'transaction_url','eleve','month','paiement_mont'));
    }

    public function paydunyaPaymentStatusParent(Request $request)
    {
    	$store = new Store();
    	$store::setName("Carfeis"); 

    	//Configuration de l'URL de redirection après confirmation de paiement sur une instance de facture
        $store::setReturnUrl(route('paydunya.payment.status_parent'));

    	// PayDunya rajoutera automatiquement le token de la facture sous forme de QUERYSTRING "token"
		// si vous avez configuré un "return_url" ou "cancel_url".
		// Récupérez donc le token en pur PHP via $_GET['token']
    	$token = $_GET['token'];

    	// Variable qui doit avoir comme valeur le statut de la transaction
        $payment_status = '';

        $invoice = new CheckoutInvoice();
        if ($invoice->confirm($token)) {
        	// Statut de la transaction
            $payment_status = $invoice->getStatus();

            // Vérification de l'état du paiement
            if($payment_status == 'completed'){ // Action si le paiement a été bien validé par paydunya

            	// Récupérer l'URL du reçu PDF électronique pour téléchargement
                $invoice_pdf = $invoice->getReceiptUrl();

                // Vous pouvez récupérer le nom, l'adresse email et le
			  	// numéro de téléphone du client en utilisant
			  	// les méthodes suivantes
			  	$giver_name =  $invoice->getCustomerInfo('name');
			  	$giver_email = $invoice->getCustomerInfo('email');
			  	$giver_phone = $invoice->getCustomerInfo('phone');
                $giver_month_eleve = $invoice->getCustomData("id");
                $giver_month_parents = $invoice->getCustomData("payed_month");
                $giver_status_parents = $invoice->getCustomData("status_payed");
                $giver_paiement_parents = $invoice->getCustomData("paiement_id");
                
                //  dd($giver_month_eleve);      
                   $month_update=MensualiteEleve::find($giver_month_eleve);
                   $month_update->is_paid = true;
                   $month_update->invoice_url = $invoice_pdf;
                   $month_update->update();
                   $eleve = Eleve::where('id',$month_update->eleve_id)->first();
                   $user_name = User::where('status','superadmin')->first();
                   $notif = $user_name->notify(new UserNotificationPaiement($user_name,$month_update));
                   $paiement_mont=ParentPaiementAnnuelle::find($giver_paiement_parents);
                   if($giver_month_parents=='payed_janv'){
                    $paiement_mont->payed_janv = true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_fev'){
                    $paiement_mont->payed_fev =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_mars'){
                    $paiement_mont->payed_mars =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_avril'){
                    $paiement_mont->payed_avril =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='mai_payed'){
                    $paiement_mont->mai_payed =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='juin_payed'){
                    $paiement_mont->juin_payed =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_jul'){
                    $paiement_mont->payed_jul =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_aout'){
                    $paiement_mont->payed_aout =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_sept'){
                    $paiement_mont->payed_sept =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_octobre'){
                    $paiement_mont->payed_octobre =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_nov'){
                    $paiement_mont->payed_nov =true;
                    $paiement_mont->update();
                }
                if($giver_month_parents=='payed_dec'){
                    $paiement_mont->payed_dec =true;
                    $paiement_mont->update();
                }
                // dd($paiement_mont,$giver_status_parents, $giver_month_parents);

			  	// Récupérer n'importe laquelle des données personnalisées que
				// vous avez eu à rajouter précédemment à la facture.
				// Assurer à utiliser les mêmes clés que celles utilisées
				// lors de la configuration.
				$greating_message =  $invoice->getCustomData("sup_data");
				$donate_amount = $invoice->getCustomData("donate_value");

				// Vous pouvez aussi récupérer le montant total spécifié précédemment
                $donate_value = $invoice->getTotalAmount();
                // dd($eleve);
            	return view('parents.paydunya-view.paydunya-payment-status', compact('payment_status', 'invoice_pdf', 'giver_name', 'giver_email', 'giver_phone', 'greating_message', 'donate_amount','eleve'));

            }
        }
        else{
			$payment_status = 'ANNULER';
			$message = 'Oupss... Dommage que vous ayez annulé votre le paiemeent.';
            $invoice->getCustomData("id");
            $month_update=MensualiteEleve::find($invoice->getCustomData("id"));
            $eleve = Eleve::where('id',$month_update->eleve_id)->first();
            $month_update->delete();
            Flash::success('Paiement Annuler');

            return redirect(route('infor_paiement_parent.mensual',$eleve->slug));
			// return view('paydunya-view.paydunya-payment-status', compact('message', 'payment_status','eleve'));
		}

    }

    public function deleteMensualiteaftercancel($id){
        $mensualite = MensualiteEleve::find($id);
        $eleve = Eleve::where('id',$mensualite->eleve_id)->first();
        $mensualite->delete();
        if(empty($mensualite)){
            Flash::error('Erreur Annulation');
            return redirect(route('infor_paiement_parent.mensual',$eleve->slug));

        }
        Flash::success('Paiment annuller avec success');
        return redirect(route('infor_paiement_parent.mensual',$eleve->slug));
    }

    public function deleteMensualitebYAdmin($id){
        $mensualite = MensualiteEleve::find($id);
        $eleve = Eleve::where('id',$mensualite->eleve_id)->first();
        $mensualite->delete();
        if(empty($mensualite)){
            Flash::error('Erreur Annulation');
            return redirect(route('infor_paiement_parent.mensual',$eleve->slug));

        }
        Flash::success('Paiment annuller avec success');
        return redirect(route('infor_paiement.mensual',$eleve->slug));
    }
}
