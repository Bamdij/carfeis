<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAnneeScolaireRequest;
use App\Http\Requests\UpdateAnneeScolaireRequest;
use App\Repositories\AnneeScolaireRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\DataTables\AnneeScolaireDatatable;
use App\Models\Anneescolaire;
use App\Models\User;
use App\Models\Parents;
use App\Models\Eleve;
use App\Models\ParentPaiementAnnuelle;
use DataTables;
use Flash;
use Response;

class AnneeScolaireController extends AppBaseController
{
    /** @var  AnneeScolaireRepository */
    private $anneeScolaireRepository;

    public function __construct(AnneeScolaireRepository $anneeScolaireRepo)
    {
        $this->anneeScolaireRepository = $anneeScolaireRepo;
    }

  
    /**
     * Display a listing of the .
     *
     * @param AnneeScolaireDatatable $anneeScolaireDatatable
     * @return Response
     */

    public function index(AnneeScolaireDatatable $anneeScolaireDatatable)
    {
        return $anneeScolaireDatatable->render('annee_scolaires.index');
    }
    /**
     * Show the form for creating a new AnneeScolaire.
     *
     * @return Response
     */
    public function create()
    {
        return view('annee_scolaires.create');
    }

    /**
     * Store a newly created AnneeScolaire in storage.
     *
     * @param CreateAnneeScolaireRequest $request
     *
     * @return Response
     */
    public function store(CreateAnneeScolaireRequest $request)
    {
        $input = $request->all();

        $anneeScolaire = $this->anneeScolaireRepository->create($input);
        if($anneeScolaire){
            $users = User::where('status','Parent')->get();
            foreach($users  as $item){
                $parent_eleve = Parents::where('user_id',$item->id)->get();
                foreach ($parent_eleve as  $value) { 
                $parent = ParentPaiementAnnuelle::create([
                    'parent_id' => $item->id,
                    'annee_scloaire_id' => $anneeScolaire->id,
                    'eleve_id' => $value->eleve_id,
                    'janvier' =>"JANVIER",
                    'fevrier' => "FEVRIER",
                    'mars' => "Mars",
                    'avril' => "AVRIL",
                    'mai' => "MAI",
                    'juin' => "JUIN",
                    'juillett' => "JUILLET",
                    'aout' => "AOUT",
                    'septembre' => "SEPTEMBRE",
                    'octobre' => "OCTOBRE",
                    'novembre' => "NOVEMBRE",
                    'decembre' => "DECEMBRE"

                ]);
            }
        }
        }
      
        Flash::success('Annee Scolaire ajoutée avec success.');

        return redirect(route('anneeScolaires.index'));
    }

    /**
     * Display the specified AnneeScolaire.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            Flash::error('Annee Scolaire non trouvé(e)');

            return redirect(route('anneeScolaires.index'));
        }

        return view('annee_scolaires.show')->with('anneeScolaire', $anneeScolaire);
    }

    /**
     * Show the form for editing the specified AnneeScolaire.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            Flash::error('Annee Scolaire  non trouvé(e)');

            return redirect(route('anneeScolaires.index'));
        }

        return view('annee_scolaires.edit')->with('anneeScolaire', $anneeScolaire);
    }

    /**
     * Update the specified AnneeScolaire in storage.
     *
     * @param int $id
     * @param UpdateAnneeScolaireRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnneeScolaireRequest $request)
    {
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            Flash::error('Annee Scolaire non trouvé(e)');

            return redirect(route('anneeScolaires.index'));
        }

        $anneeScolaire = $this->anneeScolaireRepository->update($request->all(), $id);

        Flash::success('Annee Scolaire modifié(e) avec success.');

        return redirect(route('anneeScolaires.index'));
    }

    /**
     * Remove the specified AnneeScolaire from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $anneeScolaire = $this->anneeScolaireRepository->find($id);
        $paiement = ParentPaiementAnnuelle::where('annee_scolaire_id',$anneeScolaire->id)->get();
        if (empty($anneeScolaire)) {
            Flash::error('Annee Scolaire  non trouvé(e)');

            return redirect(route('anneeScolaires.index'));
        }
       
        $this->anneeScolaireRepository->delete($id);
        if(!empty($paiement)){
            $paiement->delete();
        }
        Flash::success('Annee Scolaire supprime(e) avec success.');

        return redirect(route('anneeScolaires.index'));
    }
}
