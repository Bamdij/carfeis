<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSourateRequest;
use App\Http\Requests\UpdateSourateRequest;
use App\Repositories\SourateRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SourateController extends AppBaseController
{
    /** @var  SourateRepository */
    private $sourateRepository;

    public function __construct(SourateRepository $sourateRepo)
    {
        $this->sourateRepository = $sourateRepo;
    }

    /**
     * Display a listing of the Sourate.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sourates = $this->sourateRepository->all();

        return view('sourates.index')
            ->with('sourates', $sourates);
    }

    /**
     * Show the form for creating a new Sourate.
     *
     * @return Response
     */
    public function create()
    {
        return view('sourates.create');
    }

    /**
     * Store a newly created Sourate in storage.
     *
     * @param CreateSourateRequest $request
     *
     * @return Response
     */
    public function store(CreateSourateRequest $request)
    {
        $input = $request->all();

        $sourate = $this->sourateRepository->create($input);

        Flash::success('Sourate saved successfully.');

        return redirect(route('sourates.index'));
    }

    /**
     * Display the specified Sourate.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            Flash::error('Sourate not found');

            return redirect(route('sourates.index'));
        }

        return view('sourates.show')->with('sourate', $sourate);
    }

    /**
     * Show the form for editing the specified Sourate.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            Flash::error('Sourate not found');

            return redirect(route('sourates.index'));
        }

        return view('sourates.edit')->with('sourate', $sourate);
    }

    /**
     * Update the specified Sourate in storage.
     *
     * @param int $id
     * @param UpdateSourateRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSourateRequest $request)
    {
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            Flash::error('Sourate not found');

            return redirect(route('sourates.index'));
        }

        $sourate = $this->sourateRepository->update($request->all(), $id);

        Flash::success('Sourate updated successfully.');

        return redirect(route('sourates.index'));
    }

    /**
     * Remove the specified Sourate from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            Flash::error('Sourate not found');

            return redirect(route('sourates.index'));
        }

        $this->sourateRepository->delete($id);

        Flash::success('Sourate deleted successfully.');

        return redirect(route('sourates.index'));
    }
}
