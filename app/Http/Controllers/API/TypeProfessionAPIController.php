<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeProfessionAPIRequest;
use App\Http\Requests\API\UpdateTypeProfessionAPIRequest;
use App\Models\TypeProfession;
use App\Repositories\TypeProfessionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TypeProfessionResource;
use Response;

/**
 * Class TypeProfessionController
 * @package App\Http\Controllers\API
 */

class TypeProfessionAPIController extends AppBaseController
{
    /** @var  TypeProfessionRepository */
    private $typeProfessionRepository;

    public function __construct(TypeProfessionRepository $typeProfessionRepo)
    {
        $this->typeProfessionRepository = $typeProfessionRepo;
    }

    /**
     * Display a listing of the TypeProfession.
     * GET|HEAD /typeProfessions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typeProfessions = $this->typeProfessionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(TypeProfessionResource::collection($typeProfessions), 'Type Professions retrieved successfully');
    }

    /**
     * Store a newly created TypeProfession in storage.
     * POST /typeProfessions
     *
     * @param CreateTypeProfessionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeProfessionAPIRequest $request)
    {
        $input = $request->all();

        $typeProfession = $this->typeProfessionRepository->create($input);

        return $this->sendResponse(new TypeProfessionResource($typeProfession), 'Type Profession saved successfully');
    }

    /**
     * Display the specified TypeProfession.
     * GET|HEAD /typeProfessions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypeProfession $typeProfession */
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            return $this->sendError('Type Profession  non trouvé(e)');
        }

        return $this->sendResponse(new TypeProfessionResource($typeProfession), 'Type Profession retrieved successfully');
    }

    /**
     * Update the specified TypeProfession in storage.
     * PUT/PATCH /typeProfessions/{id}
     *
     * @param int $id
     * @param UpdateTypeProfessionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeProfessionAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeProfession $typeProfession */
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            return $this->sendError('Type Profession  non trouvé(e)');
        }

        $typeProfession = $this->typeProfessionRepository->update($input, $id);

        return $this->sendResponse(new TypeProfessionResource($typeProfession), 'TypeProfession modifié(e) avec success');
    }

    /**
     * Remove the specified TypeProfession from storage.
     * DELETE /typeProfessions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypeProfession $typeProfession */
        $typeProfession = $this->typeProfessionRepository->find($id);

        if (empty($typeProfession)) {
            return $this->sendError('Type Profession  non trouvé(e)');
        }

        $typeProfession->delete();

        return $this->sendSuccess('Type Profession deleted successfully');
    }
}
