<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDepenseCategorieAPIRequest;
use App\Http\Requests\API\UpdateDepenseCategorieAPIRequest;
use App\Models\DepenseCategorie;
use App\Repositories\DepenseCategorieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DepenseCategorieResource;
use Response;

/**
 * Class DepenseCategorieController
 * @package App\Http\Controllers\API
 */

class DepenseCategorieAPIController extends AppBaseController
{
    /** @var  DepenseCategorieRepository */
    private $depenseCategorieRepository;

    public function __construct(DepenseCategorieRepository $depenseCategorieRepo)
    {
        $this->depenseCategorieRepository = $depenseCategorieRepo;
    }

    /**
     * Display a listing of the DepenseCategorie.
     * GET|HEAD /depenseCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $depenseCategories = $this->depenseCategorieRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DepenseCategorieResource::collection($depenseCategories), 'Depense Categories retrieved successfully');
    }

    /**
     * Store a newly created DepenseCategorie in storage.
     * POST /depenseCategories
     *
     * @param CreateDepenseCategorieAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDepenseCategorieAPIRequest $request)
    {
        $input = $request->all();

        $depenseCategorie = $this->depenseCategorieRepository->create($input);

        return $this->sendResponse(new DepenseCategorieResource($depenseCategorie), 'Depense Categorie saved successfully');
    }

    /**
     * Display the specified DepenseCategorie.
     * GET|HEAD /depenseCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DepenseCategorie $depenseCategorie */
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            return $this->sendError('Depense Categorie  non trouvé(e)');
        }

        return $this->sendResponse(new DepenseCategorieResource($depenseCategorie), 'Depense Categorie retrieved successfully');
    }

    /**
     * Update the specified DepenseCategorie in storage.
     * PUT/PATCH /depenseCategories/{id}
     *
     * @param int $id
     * @param UpdateDepenseCategorieAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDepenseCategorieAPIRequest $request)
    {
        $input = $request->all();

        /** @var DepenseCategorie $depenseCategorie */
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            return $this->sendError('Depense Categorie  non trouvé(e)');
        }

        $depenseCategorie = $this->depenseCategorieRepository->update($input, $id);

        return $this->sendResponse(new DepenseCategorieResource($depenseCategorie), 'DepenseCategorie modifié(e) avec success');
    }

    /**
     * Remove the specified DepenseCategorie from storage.
     * DELETE /depenseCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DepenseCategorie $depenseCategorie */
        $depenseCategorie = $this->depenseCategorieRepository->find($id);

        if (empty($depenseCategorie)) {
            return $this->sendError('Depense Categorie  non trouvé(e)');
        }

        $depenseCategorie->delete();

        return $this->sendSuccess('Depense Categorie deleted successfully');
    }
}
