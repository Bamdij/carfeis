<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRegimeAPIRequest;
use App\Http\Requests\API\UpdateRegimeAPIRequest;
use App\Models\Regime;
use App\Repositories\RegimeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RegimeResource;
use Response;

/**
 * Class RegimeController
 * @package App\Http\Controllers\API
 */

class RegimeAPIController extends AppBaseController
{
    /** @var  RegimeRepository */
    private $regimeRepository;

    public function __construct(RegimeRepository $regimeRepo)
    {
        $this->regimeRepository = $regimeRepo;
    }

    /**
     * Display a listing of the Regime.
     * GET|HEAD /regimes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $regimes = $this->regimeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RegimeResource::collection($regimes), 'Regimes retrieved successfully');
    }

    /**
     * Store a newly created Regime in storage.
     * POST /regimes
     *
     * @param CreateRegimeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRegimeAPIRequest $request)
    {
        $input = $request->all();

        $regime = $this->regimeRepository->create($input);

        return $this->sendResponse(new RegimeResource($regime), 'Regime saved successfully');
    }

    /**
     * Display the specified Regime.
     * GET|HEAD /regimes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Regime $regime */
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            return $this->sendError('Regime  non trouvé(e)');
        }

        return $this->sendResponse(new RegimeResource($regime), 'Regime retrieved successfully');
    }

    /**
     * Update the specified Regime in storage.
     * PUT/PATCH /regimes/{id}
     *
     * @param int $id
     * @param UpdateRegimeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegimeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Regime $regime */
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            return $this->sendError('Regime  non trouvé(e)');
        }

        $regime = $this->regimeRepository->update($input, $id);

        return $this->sendResponse(new RegimeResource($regime), 'Regime modifié(e) avec success');
    }

    /**
     * Remove the specified Regime from storage.
     * DELETE /regimes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Regime $regime */
        $regime = $this->regimeRepository->find($id);

        if (empty($regime)) {
            return $this->sendError('Regime  non trouvé(e)');
        }

        $regime->delete();

        return $this->sendSuccess('Regime deleted successfully');
    }
}
