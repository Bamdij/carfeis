<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMensualiteEleveAPIRequest;
use App\Http\Requests\API\UpdateMensualiteEleveAPIRequest;
use App\Models\MensualiteEleve;
use App\Repositories\MensualiteEleveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\MensualiteEleveResource;
use Response;

/**
 * Class MensualiteEleveController
 * @package App\Http\Controllers\API
 */

class MensualiteEleveAPIController extends AppBaseController
{
    /** @var  MensualiteEleveRepository */
    private $mensualiteEleveRepository;

    public function __construct(MensualiteEleveRepository $mensualiteEleveRepo)
    {
        $this->mensualiteEleveRepository = $mensualiteEleveRepo;
    }

    /**
     * Display a listing of the MensualiteEleve.
     * GET|HEAD /mensualiteEleves
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $mensualiteEleves = $this->mensualiteEleveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(MensualiteEleveResource::collection($mensualiteEleves), 'Mensualite Eleves retrieved successfully');
    }

    /**
     * Store a newly created MensualiteEleve in storage.
     * POST /mensualiteEleves
     *
     * @param CreateMensualiteEleveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMensualiteEleveAPIRequest $request)
    {
        $input = $request->all();

        $mensualiteEleve = $this->mensualiteEleveRepository->create($input);

        return $this->sendResponse(new MensualiteEleveResource($mensualiteEleve), 'Mensualite Eleve saved successfully');
    }

    /**
     * Display the specified MensualiteEleve.
     * GET|HEAD /mensualiteEleves/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MensualiteEleve $mensualiteEleve */
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            return $this->sendError('Mensualite Eleve  non trouvé(e)');
        }

        return $this->sendResponse(new MensualiteEleveResource($mensualiteEleve), 'Mensualite Eleve retrieved successfully');
    }

    /**
     * Update the specified MensualiteEleve in storage.
     * PUT/PATCH /mensualiteEleves/{id}
     *
     * @param int $id
     * @param UpdateMensualiteEleveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMensualiteEleveAPIRequest $request)
    {
        $input = $request->all();

        /** @var MensualiteEleve $mensualiteEleve */
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            return $this->sendError('Mensualite Eleve  non trouvé(e)');
        }

        $mensualiteEleve = $this->mensualiteEleveRepository->update($input, $id);

        return $this->sendResponse(new MensualiteEleveResource($mensualiteEleve), 'MensualiteEleve modifié(e) avec success');
    }

    /**
     * Remove the specified MensualiteEleve from storage.
     * DELETE /mensualiteEleves/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MensualiteEleve $mensualiteEleve */
        $mensualiteEleve = $this->mensualiteEleveRepository->find($id);

        if (empty($mensualiteEleve)) {
            return $this->sendError('Mensualite Eleve  non trouvé(e)');
        }

        $mensualiteEleve->delete();

        return $this->sendSuccess('Mensualite Eleve deleted successfully');
    }
}
