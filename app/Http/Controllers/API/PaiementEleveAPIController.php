<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaiementEleveAPIRequest;
use App\Http\Requests\API\UpdatePaiementEleveAPIRequest;
use App\Models\PaiementEleve;
use App\Repositories\PaiementEleveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PaiementEleveResource;
use Response;

/**
 * Class PaiementEleveController
 * @package App\Http\Controllers\API
 */

class PaiementEleveAPIController extends AppBaseController
{
    /** @var  PaiementEleveRepository */
    private $paiementEleveRepository;

    public function __construct(PaiementEleveRepository $paiementEleveRepo)
    {
        $this->paiementEleveRepository = $paiementEleveRepo;
    }

    /**
     * Display a listing of the PaiementEleve.
     * GET|HEAD /paiementEleves
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $paiementEleves = $this->paiementEleveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(PaiementEleveResource::collection($paiementEleves), 'Paiement Eleves retrieved successfully');
    }

    /**
     * Store a newly created PaiementEleve in storage.
     * POST /paiementEleves
     *
     * @param CreatePaiementEleveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePaiementEleveAPIRequest $request)
    {
        $input = $request->all();

        $paiementEleve = $this->paiementEleveRepository->create($input);

        return $this->sendResponse(new PaiementEleveResource($paiementEleve), 'Paiement Eleve saved successfully');
    }

    /**
     * Display the specified PaiementEleve.
     * GET|HEAD /paiementEleves/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PaiementEleve $paiementEleve */
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            return $this->sendError('Paiement Eleve  non trouvé(e)');
        }

        return $this->sendResponse(new PaiementEleveResource($paiementEleve), 'Paiement Eleve retrieved successfully');
    }

    /**
     * Update the specified PaiementEleve in storage.
     * PUT/PATCH /paiementEleves/{id}
     *
     * @param int $id
     * @param UpdatePaiementEleveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaiementEleveAPIRequest $request)
    {
        $input = $request->all();

        /** @var PaiementEleve $paiementEleve */
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            return $this->sendError('Paiement Eleve  non trouvé(e)');
        }

        $paiementEleve = $this->paiementEleveRepository->update($input, $id);

        return $this->sendResponse(new PaiementEleveResource($paiementEleve), 'PaiementEleve modifié(e) avec success');
    }

    /**
     * Remove the specified PaiementEleve from storage.
     * DELETE /paiementEleves/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PaiementEleve $paiementEleve */
        $paiementEleve = $this->paiementEleveRepository->find($id);

        if (empty($paiementEleve)) {
            return $this->sendError('Paiement Eleve  non trouvé(e)');
        }

        $paiementEleve->delete();

        return $this->sendSuccess('Paiement Eleve deleted successfully');
    }
}
