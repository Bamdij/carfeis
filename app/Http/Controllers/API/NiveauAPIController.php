<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNiveauAPIRequest;
use App\Http\Requests\API\UpdateNiveauAPIRequest;
use App\Models\Niveau;
use App\Repositories\NiveauRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\NiveauResource;
use Response;

/**
 * Class NiveauController
 * @package App\Http\Controllers\API
 */

class NiveauAPIController extends AppBaseController
{
    /** @var  NiveauRepository */
    private $niveauRepository;

    public function __construct(NiveauRepository $niveauRepo)
    {
        $this->niveauRepository = $niveauRepo;
    }

    /**
     * Display a listing of the Niveau.
     * GET|HEAD /niveaux
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $niveaux = $this->niveauRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(NiveauResource::collection($niveaux), 'Niveaux retrieved successfully');
    }

    /**
     * Store a newly created Niveau in storage.
     * POST /niveaux
     *
     * @param CreateNiveauAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNiveauAPIRequest $request)
    {
        $input = $request->all();

        $niveau = $this->niveauRepository->create($input);

        return $this->sendResponse(new NiveauResource($niveau), 'Niveau saved successfully');
    }

    /**
     * Display the specified Niveau.
     * GET|HEAD /niveaux/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Niveau $niveau */
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            return $this->sendError('Niveau  non trouvé(e)');
        }

        return $this->sendResponse(new NiveauResource($niveau), 'Niveau retrieved successfully');
    }

    /**
     * Update the specified Niveau in storage.
     * PUT/PATCH /niveaux/{id}
     *
     * @param int $id
     * @param UpdateNiveauAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNiveauAPIRequest $request)
    {
        $input = $request->all();

        /** @var Niveau $niveau */
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            return $this->sendError('Niveau  non trouvé(e)');
        }

        $niveau = $this->niveauRepository->update($input, $id);

        return $this->sendResponse(new NiveauResource($niveau), 'Niveau modifié(e) avec success');
    }

    /**
     * Remove the specified Niveau from storage.
     * DELETE /niveaux/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Niveau $niveau */
        $niveau = $this->niveauRepository->find($id);

        if (empty($niveau)) {
            return $this->sendError('Niveau  non trouvé(e)');
        }

        $niveau->delete();

        return $this->sendSuccess('Niveau deleted successfully');
    }
}
