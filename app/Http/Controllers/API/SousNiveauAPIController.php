<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSousNiveauAPIRequest;
use App\Http\Requests\API\UpdateSousNiveauAPIRequest;
use App\Models\SousNiveau;
use App\Repositories\SousNiveauRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\SousNiveauResource;
use Response;

/**
 * Class SousNiveauController
 * @package App\Http\Controllers\API
 */

class SousNiveauAPIController extends AppBaseController
{
    /** @var  SousNiveauRepository */
    private $sousNiveauRepository;

    public function __construct(SousNiveauRepository $sousNiveauRepo)
    {
        $this->sousNiveauRepository = $sousNiveauRepo;
    }

    /**
     * Display a listing of the SousNiveau.
     * GET|HEAD /sousNiveaus
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sousNiveaus = $this->sousNiveauRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(SousNiveauResource::collection($sousNiveaus), 'Sous Niveaus retrieved successfully');
    }

    /**
     * Store a newly created SousNiveau in storage.
     * POST /sousNiveaus
     *
     * @param CreateSousNiveauAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSousNiveauAPIRequest $request)
    {
        $input = $request->all();

        $sousNiveau = $this->sousNiveauRepository->create($input);

        return $this->sendResponse(new SousNiveauResource($sousNiveau), 'Sous Niveau saved successfully');
    }

    /**
     * Display the specified SousNiveau.
     * GET|HEAD /sousNiveaus/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SousNiveau $sousNiveau */
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            return $this->sendError('Sous Niveau  non trouvé(e)');
        }

        return $this->sendResponse(new SousNiveauResource($sousNiveau), 'Sous Niveau retrieved successfully');
    }

    /**
     * Update the specified SousNiveau in storage.
     * PUT/PATCH /sousNiveaus/{id}
     *
     * @param int $id
     * @param UpdateSousNiveauAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSousNiveauAPIRequest $request)
    {
        $input = $request->all();

        /** @var SousNiveau $sousNiveau */
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            return $this->sendError('Sous Niveau  non trouvé(e)');
        }

        $sousNiveau = $this->sousNiveauRepository->update($input, $id);

        return $this->sendResponse(new SousNiveauResource($sousNiveau), 'SousNiveau modifié(e) avec success');
    }

    /**
     * Remove the specified SousNiveau from storage.
     * DELETE /sousNiveaus/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SousNiveau $sousNiveau */
        $sousNiveau = $this->sousNiveauRepository->find($id);

        if (empty($sousNiveau)) {
            return $this->sendError('Sous Niveau  non trouvé(e)');
        }

        $sousNiveau->delete();

        return $this->sendSuccess('Sous Niveau deleted successfully');
    }
}
