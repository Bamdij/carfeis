<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnseignantAnneeAPIRequest;
use App\Http\Requests\API\UpdateEnseignantAnneeAPIRequest;
use App\Models\EnseignantAnnee;
use App\Repositories\EnseignantAnneeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EnseignantAnneeResource;
use Response;

/**
 * Class EnseignantAnneeController
 * @package App\Http\Controllers\API
 */

class EnseignantAnneeAPIController extends AppBaseController
{
    /** @var  EnseignantAnneeRepository */
    private $enseignantAnneeRepository;

    public function __construct(EnseignantAnneeRepository $enseignantAnneeRepo)
    {
        $this->enseignantAnneeRepository = $enseignantAnneeRepo;
    }

    /**
     * Display a listing of the EnseignantAnnee.
     * GET|HEAD /enseignantAnnees
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $enseignantAnnees = $this->enseignantAnneeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EnseignantAnneeResource::collection($enseignantAnnees), 'Enseignant Annees retrieved successfully');
    }

    /**
     * Store a newly created EnseignantAnnee in storage.
     * POST /enseignantAnnees
     *
     * @param CreateEnseignantAnneeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantAnneeAPIRequest $request)
    {
        $input = $request->all();

        $enseignantAnnee = $this->enseignantAnneeRepository->create($input);

        return $this->sendResponse(new EnseignantAnneeResource($enseignantAnnee), 'Enseignant Annee saved successfully');
    }

    /**
     * Display the specified EnseignantAnnee.
     * GET|HEAD /enseignantAnnees/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EnseignantAnnee $enseignantAnnee */
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            return $this->sendError('Enseignant Annee  non trouvé(e)');
        }

        return $this->sendResponse(new EnseignantAnneeResource($enseignantAnnee), 'Enseignant Annee retrieved successfully');
    }

    /**
     * Update the specified EnseignantAnnee in storage.
     * PUT/PATCH /enseignantAnnees/{id}
     *
     * @param int $id
     * @param UpdateEnseignantAnneeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantAnneeAPIRequest $request)
    {
        $input = $request->all();

        /** @var EnseignantAnnee $enseignantAnnee */
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            return $this->sendError('Enseignant Annee  non trouvé(e)');
        }

        $enseignantAnnee = $this->enseignantAnneeRepository->update($input, $id);

        return $this->sendResponse(new EnseignantAnneeResource($enseignantAnnee), 'EnseignantAnnee modifié(e) avec success');
    }

    /**
     * Remove the specified EnseignantAnnee from storage.
     * DELETE /enseignantAnnees/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EnseignantAnnee $enseignantAnnee */
        $enseignantAnnee = $this->enseignantAnneeRepository->find($id);

        if (empty($enseignantAnnee)) {
            return $this->sendError('Enseignant Annee  non trouvé(e)');
        }

        $enseignantAnnee->delete();

        return $this->sendSuccess('Enseignant Annee deleted successfully');
    }
}
