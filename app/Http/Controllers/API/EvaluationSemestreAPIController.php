<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEvaluationSemestreAPIRequest;
use App\Http\Requests\API\UpdateEvaluationSemestreAPIRequest;
use App\Models\EvaluationSemestre;
use App\Repositories\EvaluationSemestreRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EvaluationSemestreResource;
use Response;

/**
 * Class EvaluationSemestreController
 * @package App\Http\Controllers\API
 */

class EvaluationSemestreAPIController extends AppBaseController
{
    /** @var  EvaluationSemestreRepository */
    private $evaluationSemestreRepository;

    public function __construct(EvaluationSemestreRepository $evaluationSemestreRepo)
    {
        $this->evaluationSemestreRepository = $evaluationSemestreRepo;
    }

    /**
     * Display a listing of the EvaluationSemestre.
     * GET|HEAD /evaluationSemestres
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $evaluationSemestres = $this->evaluationSemestreRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EvaluationSemestreResource::collection($evaluationSemestres), 'Evaluation Semestres retrieved successfully');
    }

    /**
     * Store a newly created EvaluationSemestre in storage.
     * POST /evaluationSemestres
     *
     * @param CreateEvaluationSemestreAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluationSemestreAPIRequest $request)
    {
        $input = $request->all();

        $evaluationSemestre = $this->evaluationSemestreRepository->create($input);

        return $this->sendResponse(new EvaluationSemestreResource($evaluationSemestre), 'Evaluation Semestre saved successfully');
    }

    /**
     * Display the specified EvaluationSemestre.
     * GET|HEAD /evaluationSemestres/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EvaluationSemestre $evaluationSemestre */
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);

        if (empty($evaluationSemestre)) {
            return $this->sendError('Evaluation Semestre not found');
        }

        return $this->sendResponse(new EvaluationSemestreResource($evaluationSemestre), 'Evaluation Semestre retrieved successfully');
    }

    /**
     * Update the specified EvaluationSemestre in storage.
     * PUT/PATCH /evaluationSemestres/{id}
     *
     * @param int $id
     * @param UpdateEvaluationSemestreAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluationSemestreAPIRequest $request)
    {
        $input = $request->all();

        /** @var EvaluationSemestre $evaluationSemestre */
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);

        if (empty($evaluationSemestre)) {
            return $this->sendError('Evaluation Semestre not found');
        }

        $evaluationSemestre = $this->evaluationSemestreRepository->update($input, $id);

        return $this->sendResponse(new EvaluationSemestreResource($evaluationSemestre), 'EvaluationSemestre updated successfully');
    }

    /**
     * Remove the specified EvaluationSemestre from storage.
     * DELETE /evaluationSemestres/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EvaluationSemestre $evaluationSemestre */
        $evaluationSemestre = $this->evaluationSemestreRepository->find($id);

        if (empty($evaluationSemestre)) {
            return $this->sendError('Evaluation Semestre not found');
        }

        $evaluationSemestre->delete();

        return $this->sendSuccess('Evaluation Semestre deleted successfully');
    }
}
