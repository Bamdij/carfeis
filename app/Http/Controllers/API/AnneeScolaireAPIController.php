<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAnneeScolaireAPIRequest;
use App\Http\Requests\API\UpdateAnneeScolaireAPIRequest;
use App\Models\AnneeScolaire;
use App\Repositories\AnneeScolaireRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AnneeScolaireResource;
use Response;

/**
 * Class AnneeScolaireController
 * @package App\Http\Controllers\API
 */

class AnneeScolaireAPIController extends AppBaseController
{
    /** @var  AnneeScolaireRepository */
    private $anneeScolaireRepository;

    public function __construct(AnneeScolaireRepository $anneeScolaireRepo)
    {
        $this->anneeScolaireRepository = $anneeScolaireRepo;
    }

    /**
     * Display a listing of the AnneeScolaire.
     * GET|HEAD /anneeScolaires
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $anneeScolaires = $this->anneeScolaireRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(AnneeScolaireResource::collection($anneeScolaires), 'Annee Scolaires retrieved successfully');
    }

    /**
     * Store a newly created AnneeScolaire in storage.
     * POST /anneeScolaires
     *
     * @param CreateAnneeScolaireAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAnneeScolaireAPIRequest $request)
    {
        $input = $request->all();

        $anneeScolaire = $this->anneeScolaireRepository->create($input);

        return $this->sendResponse(new AnneeScolaireResource($anneeScolaire), 'Annee Scolaire saved successfully');
    }

    /**
     * Display the specified AnneeScolaire.
     * GET|HEAD /anneeScolaires/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AnneeScolaire $anneeScolaire */
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            return $this->sendError('Annee Scolaire  non trouvé(e)');
        }

        return $this->sendResponse(new AnneeScolaireResource($anneeScolaire), 'Annee Scolaire retrieved successfully');
    }

    /**
     * Update the specified AnneeScolaire in storage.
     * PUT/PATCH /anneeScolaires/{id}
     *
     * @param int $id
     * @param UpdateAnneeScolaireAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnneeScolaireAPIRequest $request)
    {
        $input = $request->all();

        /** @var AnneeScolaire $anneeScolaire */
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            return $this->sendError('Annee Scolaire  non trouvé(e)');
        }

        $anneeScolaire = $this->anneeScolaireRepository->update($input, $id);

        return $this->sendResponse(new AnneeScolaireResource($anneeScolaire), 'AnneeScolaire modifié(e) avec success');
    }

    /**
     * Remove the specified AnneeScolaire from storage.
     * DELETE /anneeScolaires/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AnneeScolaire $anneeScolaire */
        $anneeScolaire = $this->anneeScolaireRepository->find($id);

        if (empty($anneeScolaire)) {
            return $this->sendError('Annee Scolaire  non trouvé(e)');
        }

        $anneeScolaire->delete();

        return $this->sendSuccess('Annee Scolaire deleted successfully');
    }
}
