<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMessageContactAPIRequest;
use App\Http\Requests\API\UpdateMessageContactAPIRequest;
use App\Models\MessageContact;
use App\Repositories\MessageContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\MessageContactResource;
use Response;

/**
 * Class MessageContactController
 * @package App\Http\Controllers\API
 */

class MessageContactAPIController extends AppBaseController
{
    /** @var  MessageContactRepository */
    private $messageContactRepository;

    public function __construct(MessageContactRepository $messageContactRepo)
    {
        $this->messageContactRepository = $messageContactRepo;
    }

    /**
     * Display a listing of the MessageContact.
     * GET|HEAD /messageContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $messageContacts = $this->messageContactRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(MessageContactResource::collection($messageContacts), 'Message Contacts retrieved successfully');
    }

    /**
     * Store a newly created MessageContact in storage.
     * POST /messageContacts
     *
     * @param CreateMessageContactAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageContactAPIRequest $request)
    {
        $input = $request->all();

        $messageContact = $this->messageContactRepository->create($input);

        return $this->sendResponse(new MessageContactResource($messageContact), 'Message Contact saved successfully');
    }

    /**
     * Display the specified MessageContact.
     * GET|HEAD /messageContacts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MessageContact $messageContact */
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            return $this->sendError('Message Contact not found');
        }

        return $this->sendResponse(new MessageContactResource($messageContact), 'Message Contact retrieved successfully');
    }

    /**
     * Update the specified MessageContact in storage.
     * PUT/PATCH /messageContacts/{id}
     *
     * @param int $id
     * @param UpdateMessageContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageContactAPIRequest $request)
    {
        $input = $request->all();

        /** @var MessageContact $messageContact */
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            return $this->sendError('Message Contact not found');
        }

        $messageContact = $this->messageContactRepository->update($input, $id);

        return $this->sendResponse(new MessageContactResource($messageContact), 'MessageContact updated successfully');
    }

    /**
     * Remove the specified MessageContact from storage.
     * DELETE /messageContacts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MessageContact $messageContact */
        $messageContact = $this->messageContactRepository->find($id);

        if (empty($messageContact)) {
            return $this->sendError('Message Contact not found');
        }

        $messageContact->delete();

        return $this->sendSuccess('Message Contact deleted successfully');
    }
}
