<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEvaluationMensuelAPIRequest;
use App\Http\Requests\API\UpdateEvaluationMensuelAPIRequest;
use App\Models\EvaluationMensuel;
use App\Repositories\EvaluationMensuelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EvaluationMensuelResource;
use Response;

/**
 * Class EvaluationMensuelController
 * @package App\Http\Controllers\API
 */

class EvaluationMensuelAPIController extends AppBaseController
{
    /** @var  EvaluationMensuelRepository */
    private $evaluationMensuelRepository;

    public function __construct(EvaluationMensuelRepository $evaluationMensuelRepo)
    {
        $this->evaluationMensuelRepository = $evaluationMensuelRepo;
    }

    /**
     * Display a listing of the EvaluationMensuel.
     * GET|HEAD /evaluationMensuels
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $evaluationMensuels = $this->evaluationMensuelRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EvaluationMensuelResource::collection($evaluationMensuels), 'Evaluation Mensuels retrieved successfully');
    }

    /**
     * Store a newly created EvaluationMensuel in storage.
     * POST /evaluationMensuels
     *
     * @param CreateEvaluationMensuelAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluationMensuelAPIRequest $request)
    {
        $input = $request->all();

        $evaluationMensuel = $this->evaluationMensuelRepository->create($input);

        return $this->sendResponse(new EvaluationMensuelResource($evaluationMensuel), 'Evaluation Mensuel saved successfully');
    }

    /**
     * Display the specified EvaluationMensuel.
     * GET|HEAD /evaluationMensuels/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EvaluationMensuel $evaluationMensuel */
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);

        if (empty($evaluationMensuel)) {
            return $this->sendError('Evaluation Mensuel not found');
        }

        return $this->sendResponse(new EvaluationMensuelResource($evaluationMensuel), 'Evaluation Mensuel retrieved successfully');
    }

    /**
     * Update the specified EvaluationMensuel in storage.
     * PUT/PATCH /evaluationMensuels/{id}
     *
     * @param int $id
     * @param UpdateEvaluationMensuelAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluationMensuelAPIRequest $request)
    {
        $input = $request->all();

        /** @var EvaluationMensuel $evaluationMensuel */
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);

        if (empty($evaluationMensuel)) {
            return $this->sendError('Evaluation Mensuel not found');
        }

        $evaluationMensuel = $this->evaluationMensuelRepository->update($input, $id);

        return $this->sendResponse(new EvaluationMensuelResource($evaluationMensuel), 'EvaluationMensuel updated successfully');
    }

    /**
     * Remove the specified EvaluationMensuel from storage.
     * DELETE /evaluationMensuels/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EvaluationMensuel $evaluationMensuel */
        $evaluationMensuel = $this->evaluationMensuelRepository->find($id);

        if (empty($evaluationMensuel)) {
            return $this->sendError('Evaluation Mensuel not found');
        }

        $evaluationMensuel->delete();

        return $this->sendSuccess('Evaluation Mensuel deleted successfully');
    }
}
