<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypePaiementAPIRequest;
use App\Http\Requests\API\UpdateTypePaiementAPIRequest;
use App\Models\TypePaiement;
use App\Repositories\TypePaiementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TypePaiementResource;
use Response;

/**
 * Class TypePaiementController
 * @package App\Http\Controllers\API
 */

class TypePaiementAPIController extends AppBaseController
{
    /** @var  TypePaiementRepository */
    private $typePaiementRepository;

    public function __construct(TypePaiementRepository $typePaiementRepo)
    {
        $this->typePaiementRepository = $typePaiementRepo;
    }

    /**
     * Display a listing of the TypePaiement.
     * GET|HEAD /typePaiements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typePaiements = $this->typePaiementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(TypePaiementResource::collection($typePaiements), 'Type Paiements retrieved successfully');
    }

    /**
     * Store a newly created TypePaiement in storage.
     * POST /typePaiements
     *
     * @param CreateTypePaiementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypePaiementAPIRequest $request)
    {
        $input = $request->all();

        $typePaiement = $this->typePaiementRepository->create($input);

        return $this->sendResponse(new TypePaiementResource($typePaiement), 'Type Paiement saved successfully');
    }

    /**
     * Display the specified TypePaiement.
     * GET|HEAD /typePaiements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypePaiement $typePaiement */
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            return $this->sendError('Type Paiement  non trouvé(e)');
        }

        return $this->sendResponse(new TypePaiementResource($typePaiement), 'Type Paiement retrieved successfully');
    }

    /**
     * Update the specified TypePaiement in storage.
     * PUT/PATCH /typePaiements/{id}
     *
     * @param int $id
     * @param UpdateTypePaiementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypePaiementAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypePaiement $typePaiement */
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            return $this->sendError('Type Paiement  non trouvé(e)');
        }

        $typePaiement = $this->typePaiementRepository->update($input, $id);

        return $this->sendResponse(new TypePaiementResource($typePaiement), 'TypePaiement modifié(e) avec success');
    }

    /**
     * Remove the specified TypePaiement from storage.
     * DELETE /typePaiements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypePaiement $typePaiement */
        $typePaiement = $this->typePaiementRepository->find($id);

        if (empty($typePaiement)) {
            return $this->sendError('Type Paiement  non trouvé(e)');
        }

        $typePaiement->delete();

        return $this->sendSuccess('Type Paiement deleted successfully');
    }
}
