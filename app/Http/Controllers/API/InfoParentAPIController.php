<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInfoParentAPIRequest;
use App\Http\Requests\API\UpdateInfoParentAPIRequest;
use App\Models\InfoParent;
use App\Repositories\InfoParentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\InfoParentResource;
use Response;

/**
 * Class InfoParentController
 * @package App\Http\Controllers\API
 */

class InfoParentAPIController extends AppBaseController
{
    /** @var  InfoParentRepository */
    private $infoParentRepository;

    public function __construct(InfoParentRepository $infoParentRepo)
    {
        $this->infoParentRepository = $infoParentRepo;
    }

    /**
     * Display a listing of the InfoParent.
     * GET|HEAD /infoParents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $infoParents = $this->infoParentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(InfoParentResource::collection($infoParents), 'Info Parents retrieved successfully');
    }

    /**
     * Store a newly created InfoParent in storage.
     * POST /infoParents
     *
     * @param CreateInfoParentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInfoParentAPIRequest $request)
    {
        $input = $request->all();

        $infoParent = $this->infoParentRepository->create($input);

        return $this->sendResponse(new InfoParentResource($infoParent), 'Info Parent saved successfully');
    }

    /**
     * Display the specified InfoParent.
     * GET|HEAD /infoParents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InfoParent $infoParent */
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            return $this->sendError('Info Parent  non trouvé(e)');
        }

        return $this->sendResponse(new InfoParentResource($infoParent), 'Info Parent retrieved successfully');
    }

    /**
     * Update the specified InfoParent in storage.
     * PUT/PATCH /infoParents/{id}
     *
     * @param int $id
     * @param UpdateInfoParentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInfoParentAPIRequest $request)
    {
        $input = $request->all();

        /** @var InfoParent $infoParent */
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            return $this->sendError('Info Parent  non trouvé(e)');
        }

        $infoParent = $this->infoParentRepository->update($input, $id);

        return $this->sendResponse(new InfoParentResource($infoParent), 'InfoParent modifié(e) avec success');
    }

    /**
     * Remove the specified InfoParent from storage.
     * DELETE /infoParents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InfoParent $infoParent */
        $infoParent = $this->infoParentRepository->find($id);

        if (empty($infoParent)) {
            return $this->sendError('Info Parent  non trouvé(e)');
        }

        $infoParent->delete();

        return $this->sendSuccess('Info Parent deleted successfully');
    }
}
