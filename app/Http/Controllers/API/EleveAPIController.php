<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEleveAPIRequest;
use App\Http\Requests\API\UpdateEleveAPIRequest;
use App\Models\Eleve;
use App\Repositories\EleveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EleveResource;
use Response;

/**
 * Class EleveController
 * @package App\Http\Controllers\API
 */

class EleveAPIController extends AppBaseController
{
    /** @var  EleveRepository */
    private $eleveRepository;

    public function __construct(EleveRepository $eleveRepo)
    {
        $this->eleveRepository = $eleveRepo;
    }

    /**
     * Display a listing of the Eleve.
     * GET|HEAD /eleves
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $eleves = $this->eleveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EleveResource::collection($eleves), 'Eleves retrieved successfully');
    }

    /**
     * Store a newly created Eleve in storage.
     * POST /eleves
     *
     * @param CreateEleveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEleveAPIRequest $request)
    {
        $input = $request->all();

        $eleve = $this->eleveRepository->create($input);

        return $this->sendResponse(new EleveResource($eleve), 'Eleve saved successfully');
    }

    /**
     * Display the specified Eleve.
     * GET|HEAD /eleves/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Eleve $eleve */
        $eleve = $this->eleveRepository->find($id);

        if (empty($eleve)) {
            return $this->sendError('Eleve  non trouvé(e)');
        }

        return $this->sendResponse(new EleveResource($eleve), 'Eleve retrieved successfully');
    }

    /**
     * Update the specified Eleve in storage.
     * PUT/PATCH /eleves/{id}
     *
     * @param int $id
     * @param UpdateEleveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEleveAPIRequest $request)
    {
        $input = $request->all();

        /** @var Eleve $eleve */
        $eleve = $this->eleveRepository->find($id);

        if (empty($eleve)) {
            return $this->sendError('Eleve  non trouvé(e)');
        }

        $eleve = $this->eleveRepository->update($input, $id);

        return $this->sendResponse(new EleveResource($eleve), 'Eleve modifié(e) avec success');
    }

    /**
     * Remove the specified Eleve from storage.
     * DELETE /eleves/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Eleve $eleve */
        $eleve = $this->eleveRepository->find($id);

        if (empty($eleve)) {
            return $this->sendError('Eleve  non trouvé(e)');
        }

        $eleve->delete();

        return $this->sendSuccess('Eleve deleted successfully');
    }
}
