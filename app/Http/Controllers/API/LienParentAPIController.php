<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLienParentAPIRequest;
use App\Http\Requests\API\UpdateLienParentAPIRequest;
use App\Models\LienParent;
use App\Repositories\LienParentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\LienParentResource;
use Response;

/**
 * Class LienParentController
 * @package App\Http\Controllers\API
 */

class LienParentAPIController extends AppBaseController
{
    /** @var  LienParentRepository */
    private $lienParentRepository;

    public function __construct(LienParentRepository $lienParentRepo)
    {
        $this->lienParentRepository = $lienParentRepo;
    }

    /**
     * Display a listing of the LienParent.
     * GET|HEAD /lienParents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $lienParents = $this->lienParentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(LienParentResource::collection($lienParents), 'Lien Parents retrieved successfully');
    }

    /**
     * Store a newly created LienParent in storage.
     * POST /lienParents
     *
     * @param CreateLienParentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLienParentAPIRequest $request)
    {
        $input = $request->all();

        $lienParent = $this->lienParentRepository->create($input);

        return $this->sendResponse(new LienParentResource($lienParent), 'Lien Parent saved successfully');
    }

    /**
     * Display the specified LienParent.
     * GET|HEAD /lienParents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LienParent $lienParent */
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            return $this->sendError('Lien Parent  non trouvé(e)');
        }

        return $this->sendResponse(new LienParentResource($lienParent), 'Lien Parent retrieved successfully');
    }

    /**
     * Update the specified LienParent in storage.
     * PUT/PATCH /lienParents/{id}
     *
     * @param int $id
     * @param UpdateLienParentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLienParentAPIRequest $request)
    {
        $input = $request->all();

        /** @var LienParent $lienParent */
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            return $this->sendError('Lien Parent  non trouvé(e)');
        }

        $lienParent = $this->lienParentRepository->update($input, $id);

        return $this->sendResponse(new LienParentResource($lienParent), 'LienParent modifié(e) avec success');
    }

    /**
     * Remove the specified LienParent from storage.
     * DELETE /lienParents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LienParent $lienParent */
        $lienParent = $this->lienParentRepository->find($id);

        if (empty($lienParent)) {
            return $this->sendError('Lien Parent  non trouvé(e)');
        }

        $lienParent->delete();

        return $this->sendSuccess('Lien Parent deleted successfully');
    }
}
