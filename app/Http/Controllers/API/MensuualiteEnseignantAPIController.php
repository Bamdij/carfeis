<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMensuualiteEnseignantAPIRequest;
use App\Http\Requests\API\UpdateMensuualiteEnseignantAPIRequest;
use App\Models\MensuualiteEnseignant;
use App\Repositories\MensuualiteEnseignantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\MensuualiteEnseignantResource;
use Response;

/**
 * Class MensuualiteEnseignantController
 * @package App\Http\Controllers\API
 */

class MensuualiteEnseignantAPIController extends AppBaseController
{
    /** @var  MensuualiteEnseignantRepository */
    private $mensuualiteEnseignantRepository;

    public function __construct(MensuualiteEnseignantRepository $mensuualiteEnseignantRepo)
    {
        $this->mensuualiteEnseignantRepository = $mensuualiteEnseignantRepo;
    }

    /**
     * Display a listing of the MensuualiteEnseignant.
     * GET|HEAD /mensuualiteEnseignants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $mensuualiteEnseignants = $this->mensuualiteEnseignantRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(MensuualiteEnseignantResource::collection($mensuualiteEnseignants), 'Mensuualite Enseignants retrieved successfully');
    }

    /**
     * Store a newly created MensuualiteEnseignant in storage.
     * POST /mensuualiteEnseignants
     *
     * @param CreateMensuualiteEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMensuualiteEnseignantAPIRequest $request)
    {
        $input = $request->all();

        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->create($input);

        return $this->sendResponse(new MensuualiteEnseignantResource($mensuualiteEnseignant), 'Mensuualite Enseignant saved successfully');
    }

    /**
     * Display the specified MensuualiteEnseignant.
     * GET|HEAD /mensuualiteEnseignants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MensuualiteEnseignant $mensuualiteEnseignant */
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);

        if (empty($mensuualiteEnseignant)) {
            return $this->sendError('Mensuualite Enseignant  non trouvé(e)');
        }

        return $this->sendResponse(new MensuualiteEnseignantResource($mensuualiteEnseignant), 'Mensuualite Enseignant retrieved successfully');
    }

    /**
     * Update the specified MensuualiteEnseignant in storage.
     * PUT/PATCH /mensuualiteEnseignants/{id}
     *
     * @param int $id
     * @param UpdateMensuualiteEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMensuualiteEnseignantAPIRequest $request)
    {
        $input = $request->all();

        /** @var MensuualiteEnseignant $mensuualiteEnseignant */
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);

        if (empty($mensuualiteEnseignant)) {
            return $this->sendError('Mensuualite Enseignant  non trouvé(e)');
        }

        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->update($input, $id);

        return $this->sendResponse(new MensuualiteEnseignantResource($mensuualiteEnseignant), 'MensuualiteEnseignant modifié(e) avec success');
    }

    /**
     * Remove the specified MensuualiteEnseignant from storage.
     * DELETE /mensuualiteEnseignants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MensuualiteEnseignant $mensuualiteEnseignant */
        $mensuualiteEnseignant = $this->mensuualiteEnseignantRepository->find($id);

        if (empty($mensuualiteEnseignant)) {
            return $this->sendError('Mensuualite Enseignant  non trouvé(e)');
        }

        $mensuualiteEnseignant->delete();

        return $this->sendSuccess('Mensuualite Enseignant deleted successfully');
    }
}
