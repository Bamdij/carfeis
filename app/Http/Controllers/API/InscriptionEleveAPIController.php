<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInscriptionEleveAPIRequest;
use App\Http\Requests\API\UpdateInscriptionEleveAPIRequest;
use App\Models\InscriptionEleve;
use App\Repositories\InscriptionEleveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\InscriptionEleveResource;
use Response;

/**
 * Class InscriptionEleveController
 * @package App\Http\Controllers\API
 */

class InscriptionEleveAPIController extends AppBaseController
{
    /** @var  InscriptionEleveRepository */
    private $inscriptionEleveRepository;

    public function __construct(InscriptionEleveRepository $inscriptionEleveRepo)
    {
        $this->inscriptionEleveRepository = $inscriptionEleveRepo;
    }

    /**
     * Display a listing of the InscriptionEleve.
     * GET|HEAD /inscriptionEleves
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $inscriptionEleves = $this->inscriptionEleveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(InscriptionEleveResource::collection($inscriptionEleves), 'Inscription Eleves retrieved successfully');
    }

    /**
     * Store a newly created InscriptionEleve in storage.
     * POST /inscriptionEleves
     *
     * @param CreateInscriptionEleveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInscriptionEleveAPIRequest $request)
    {
        $input = $request->all();

        $inscriptionEleve = $this->inscriptionEleveRepository->create($input);

        return $this->sendResponse(new InscriptionEleveResource($inscriptionEleve), 'Inscription Eleve saved successfully');
    }

    /**
     * Display the specified InscriptionEleve.
     * GET|HEAD /inscriptionEleves/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InscriptionEleve $inscriptionEleve */
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            return $this->sendError('Inscription Eleve  non trouvé(e)');
        }

        return $this->sendResponse(new InscriptionEleveResource($inscriptionEleve), 'Inscription Eleve retrieved successfully');
    }

    /**
     * Update the specified InscriptionEleve in storage.
     * PUT/PATCH /inscriptionEleves/{id}
     *
     * @param int $id
     * @param UpdateInscriptionEleveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInscriptionEleveAPIRequest $request)
    {
        $input = $request->all();

        /** @var InscriptionEleve $inscriptionEleve */
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            return $this->sendError('Inscription Eleve  non trouvé(e)');
        }

        $inscriptionEleve = $this->inscriptionEleveRepository->update($input, $id);

        return $this->sendResponse(new InscriptionEleveResource($inscriptionEleve), 'InscriptionEleve modifié(e) avec success');
    }

    /**
     * Remove the specified InscriptionEleve from storage.
     * DELETE /inscriptionEleves/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InscriptionEleve $inscriptionEleve */
        $inscriptionEleve = $this->inscriptionEleveRepository->find($id);

        if (empty($inscriptionEleve)) {
            return $this->sendError('Inscription Eleve  non trouvé(e)');
        }

        $inscriptionEleve->delete();

        return $this->sendSuccess('Inscription Eleve deleted successfully');
    }
}
