<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeEnseignantAPIRequest;
use App\Http\Requests\API\UpdateTypeEnseignantAPIRequest;
use App\Models\TypeEnseignant;
use App\Repositories\TypeEnseignantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TypeEnseignantResource;
use Response;

/**
 * Class TypeEnseignantController
 * @package App\Http\Controllers\API
 */

class TypeEnseignantAPIController extends AppBaseController
{
    /** @var  TypeEnseignantRepository */
    private $typeEnseignantRepository;

    public function __construct(TypeEnseignantRepository $typeEnseignantRepo)
    {
        $this->typeEnseignantRepository = $typeEnseignantRepo;
    }

    /**
     * Display a listing of the TypeEnseignant.
     * GET|HEAD /typeEnseignants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typeEnseignants = $this->typeEnseignantRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(TypeEnseignantResource::collection($typeEnseignants), 'Type Enseignants retrieved successfully');
    }

    /**
     * Store a newly created TypeEnseignant in storage.
     * POST /typeEnseignants
     *
     * @param CreateTypeEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeEnseignantAPIRequest $request)
    {
        $input = $request->all();

        $typeEnseignant = $this->typeEnseignantRepository->create($input);

        return $this->sendResponse(new TypeEnseignantResource($typeEnseignant), 'Type Enseignant saved successfully');
    }

    /**
     * Display the specified TypeEnseignant.
     * GET|HEAD /typeEnseignants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypeEnseignant $typeEnseignant */
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            return $this->sendError('Type Enseignant  non trouvé(e)');
        }

        return $this->sendResponse(new TypeEnseignantResource($typeEnseignant), 'Type Enseignant retrieved successfully');
    }

    /**
     * Update the specified TypeEnseignant in storage.
     * PUT/PATCH /typeEnseignants/{id}
     *
     * @param int $id
     * @param UpdateTypeEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeEnseignantAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeEnseignant $typeEnseignant */
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            return $this->sendError('Type Enseignant  non trouvé(e)');
        }

        $typeEnseignant = $this->typeEnseignantRepository->update($input, $id);

        return $this->sendResponse(new TypeEnseignantResource($typeEnseignant), 'TypeEnseignant modifié(e) avec success');
    }

    /**
     * Remove the specified TypeEnseignant from storage.
     * DELETE /typeEnseignants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypeEnseignant $typeEnseignant */
        $typeEnseignant = $this->typeEnseignantRepository->find($id);

        if (empty($typeEnseignant)) {
            return $this->sendError('Type Enseignant  non trouvé(e)');
        }

        $typeEnseignant->delete();

        return $this->sendSuccess('Type Enseignant deleted successfully');
    }
}
