<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSourateAPIRequest;
use App\Http\Requests\API\UpdateSourateAPIRequest;
use App\Models\Sourate;
use App\Repositories\SourateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\SourateResource;
use Response;

/**
 * Class SourateController
 * @package App\Http\Controllers\API
 */

class SourateAPIController extends AppBaseController
{
    /** @var  SourateRepository */
    private $sourateRepository;

    public function __construct(SourateRepository $sourateRepo)
    {
        $this->sourateRepository = $sourateRepo;
    }

    /**
     * Display a listing of the Sourate.
     * GET|HEAD /sourates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sourates = $this->sourateRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(SourateResource::collection($sourates), 'Sourates retrieved successfully');
    }

    /**
     * Store a newly created Sourate in storage.
     * POST /sourates
     *
     * @param CreateSourateAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSourateAPIRequest $request)
    {
        $input = $request->all();

        $sourate = $this->sourateRepository->create($input);

        return $this->sendResponse(new SourateResource($sourate), 'Sourate saved successfully');
    }

    /**
     * Display the specified Sourate.
     * GET|HEAD /sourates/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sourate $sourate */
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            return $this->sendError('Sourate not found');
        }

        return $this->sendResponse(new SourateResource($sourate), 'Sourate retrieved successfully');
    }

    /**
     * Update the specified Sourate in storage.
     * PUT/PATCH /sourates/{id}
     *
     * @param int $id
     * @param UpdateSourateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSourateAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sourate $sourate */
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            return $this->sendError('Sourate not found');
        }

        $sourate = $this->sourateRepository->update($input, $id);

        return $this->sendResponse(new SourateResource($sourate), 'Sourate updated successfully');
    }

    /**
     * Remove the specified Sourate from storage.
     * DELETE /sourates/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sourate $sourate */
        $sourate = $this->sourateRepository->find($id);

        if (empty($sourate)) {
            return $this->sendError('Sourate not found');
        }

        $sourate->delete();

        return $this->sendSuccess('Sourate deleted successfully');
    }
}
