<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnseignantAPIRequest;
use App\Http\Requests\API\UpdateEnseignantAPIRequest;
use App\Models\Enseignant;
use App\Repositories\EnseignantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EnseignantResource;
use Response;

/**
 * Class EnseignantController
 * @package App\Http\Controllers\API
 */

class EnseignantAPIController extends AppBaseController
{
    /** @var  EnseignantRepository */
    private $enseignantRepository;

    public function __construct(EnseignantRepository $enseignantRepo)
    {
        $this->enseignantRepository = $enseignantRepo;
    }

    /**
     * Display a listing of the Enseignant.
     * GET|HEAD /enseignants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $enseignants = $this->enseignantRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EnseignantResource::collection($enseignants), 'Enseignants retrieved successfully');
    }

    /**
     * Store a newly created Enseignant in storage.
     * POST /enseignants
     *
     * @param CreateEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantAPIRequest $request)
    {
        $input = $request->all();

        $enseignant = $this->enseignantRepository->create($input);

        return $this->sendResponse(new EnseignantResource($enseignant), 'Enseignant saved successfully');
    }

    /**
     * Display the specified Enseignant.
     * GET|HEAD /enseignants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Enseignant $enseignant */
        $enseignant = $this->enseignantRepository->find($id);

        if (empty($enseignant)) {
            return $this->sendError('Enseignant  non trouvé(e)');
        }

        return $this->sendResponse(new EnseignantResource($enseignant), 'Enseignant retrieved successfully');
    }

    /**
     * Update the specified Enseignant in storage.
     * PUT/PATCH /enseignants/{id}
     *
     * @param int $id
     * @param UpdateEnseignantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantAPIRequest $request)
    {
        $input = $request->all();

        /** @var Enseignant $enseignant */
        $enseignant = $this->enseignantRepository->find($id);

        if (empty($enseignant)) {
            return $this->sendError('Enseignant  non trouvé(e)');
        }

        $enseignant = $this->enseignantRepository->update($input, $id);

        return $this->sendResponse(new EnseignantResource($enseignant), 'Enseignant modifié(e) avec success');
    }

    /**
     * Remove the specified Enseignant from storage.
     * DELETE /enseignants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Enseignant $enseignant */
        $enseignant = $this->enseignantRepository->find($id);

        if (empty($enseignant)) {
            return $this->sendError('Enseignant  non trouvé(e)');
        }

        $enseignant->delete();

        return $this->sendSuccess('Enseignant deleted successfully');
    }
}
