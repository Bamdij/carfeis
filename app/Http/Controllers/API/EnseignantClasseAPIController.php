<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnseignantClasseAPIRequest;
use App\Http\Requests\API\UpdateEnseignantClasseAPIRequest;
use App\Models\EnseignantClasse;
use App\Repositories\EnseignantClasseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EnseignantClasseResource;
use Response;

/**
 * Class EnseignantClasseController
 * @package App\Http\Controllers\API
 */

class EnseignantClasseAPIController extends AppBaseController
{
    /** @var  EnseignantClasseRepository */
    private $enseignantClasseRepository;

    public function __construct(EnseignantClasseRepository $enseignantClasseRepo)
    {
        $this->enseignantClasseRepository = $enseignantClasseRepo;
    }

    /**
     * Display a listing of the EnseignantClasse.
     * GET|HEAD /enseignantClasses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $enseignantClasses = $this->enseignantClasseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EnseignantClasseResource::collection($enseignantClasses), 'Enseignant Classes retrieved successfully');
    }

    /**
     * Store a newly created EnseignantClasse in storage.
     * POST /enseignantClasses
     *
     * @param CreateEnseignantClasseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEnseignantClasseAPIRequest $request)
    {
        $input = $request->all();

        $enseignantClasse = $this->enseignantClasseRepository->create($input);

        return $this->sendResponse(new EnseignantClasseResource($enseignantClasse), 'Enseignant Classe saved successfully');
    }

    /**
     * Display the specified EnseignantClasse.
     * GET|HEAD /enseignantClasses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EnseignantClasse $enseignantClasse */
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            return $this->sendError('Enseignant Classe  non trouvé(e)');
        }

        return $this->sendResponse(new EnseignantClasseResource($enseignantClasse), 'Enseignant Classe retrieved successfully');
    }

    /**
     * Update the specified EnseignantClasse in storage.
     * PUT/PATCH /enseignantClasses/{id}
     *
     * @param int $id
     * @param UpdateEnseignantClasseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnseignantClasseAPIRequest $request)
    {
        $input = $request->all();

        /** @var EnseignantClasse $enseignantClasse */
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            return $this->sendError('Enseignant Classe  non trouvé(e)');
        }

        $enseignantClasse = $this->enseignantClasseRepository->update($input, $id);

        return $this->sendResponse(new EnseignantClasseResource($enseignantClasse), 'EnseignantClasse modifié(e) avec success');
    }

    /**
     * Remove the specified EnseignantClasse from storage.
     * DELETE /enseignantClasses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EnseignantClasse $enseignantClasse */
        $enseignantClasse = $this->enseignantClasseRepository->find($id);

        if (empty($enseignantClasse)) {
            return $this->sendError('Enseignant Classe  non trouvé(e)');
        }

        $enseignantClasse->delete();

        return $this->sendSuccess('Enseignant Classe deleted successfully');
    }
}
