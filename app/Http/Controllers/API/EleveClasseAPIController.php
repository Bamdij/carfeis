<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEleveClasseAPIRequest;
use App\Http\Requests\API\UpdateEleveClasseAPIRequest;
use App\Models\EleveClasse;
use App\Repositories\EleveClasseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EleveClasseResource;
use Response;

/**
 * Class EleveClasseController
 * @package App\Http\Controllers\API
 */

class EleveClasseAPIController extends AppBaseController
{
    /** @var  EleveClasseRepository */
    private $eleveClasseRepository;

    public function __construct(EleveClasseRepository $eleveClasseRepo)
    {
        $this->eleveClasseRepository = $eleveClasseRepo;
    }

    /**
     * Display a listing of the EleveClasse.
     * GET|HEAD /eleveClasses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $eleveClasses = $this->eleveClasseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(EleveClasseResource::collection($eleveClasses), 'Eleve Classes retrieved successfully');
    }

    /**
     * Store a newly created EleveClasse in storage.
     * POST /eleveClasses
     *
     * @param CreateEleveClasseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEleveClasseAPIRequest $request)
    {
        $input = $request->all();

        $eleveClasse = $this->eleveClasseRepository->create($input);

        return $this->sendResponse(new EleveClasseResource($eleveClasse), 'Eleve Classe saved successfully');
    }

    /**
     * Display the specified EleveClasse.
     * GET|HEAD /eleveClasses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EleveClasse $eleveClasse */
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            return $this->sendError('Eleve Classe  non trouvé(e)');
        }

        return $this->sendResponse(new EleveClasseResource($eleveClasse), 'Eleve Classe retrieved successfully');
    }

    /**
     * Update the specified EleveClasse in storage.
     * PUT/PATCH /eleveClasses/{id}
     *
     * @param int $id
     * @param UpdateEleveClasseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEleveClasseAPIRequest $request)
    {
        $input = $request->all();

        /** @var EleveClasse $eleveClasse */
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            return $this->sendError('Eleve Classe  non trouvé(e)');
        }

        $eleveClasse = $this->eleveClasseRepository->update($input, $id);

        return $this->sendResponse(new EleveClasseResource($eleveClasse), 'EleveClasse modifié(e) avec success');
    }

    /**
     * Remove the specified EleveClasse from storage.
     * DELETE /eleveClasses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EleveClasse $eleveClasse */
        $eleveClasse = $this->eleveClasseRepository->find($id);

        if (empty($eleveClasse)) {
            return $this->sendError('Eleve Classe  non trouvé(e)');
        }

        $eleveClasse->delete();

        return $this->sendSuccess('Eleve Classe deleted successfully');
    }
}
