<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFraisInscriptionAPIRequest;
use App\Http\Requests\API\UpdateFraisInscriptionAPIRequest;
use App\Models\FraisInscription;
use App\Repositories\FraisInscriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FraisInscriptionResource;
use Response;

/**
 * Class FraisInscriptionController
 * @package App\Http\Controllers\API
 */

class FraisInscriptionAPIController extends AppBaseController
{
    /** @var  FraisInscriptionRepository */
    private $fraisInscriptionRepository;

    public function __construct(FraisInscriptionRepository $fraisInscriptionRepo)
    {
        $this->fraisInscriptionRepository = $fraisInscriptionRepo;
    }

    /**
     * Display a listing of the FraisInscription.
     * GET|HEAD /fraisInscriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $fraisInscriptions = $this->fraisInscriptionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(FraisInscriptionResource::collection($fraisInscriptions), 'Frais Inscriptions retrieved successfully');
    }

    /**
     * Store a newly created FraisInscription in storage.
     * POST /fraisInscriptions
     *
     * @param CreateFraisInscriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFraisInscriptionAPIRequest $request)
    {
        $input = $request->all();

        $fraisInscription = $this->fraisInscriptionRepository->create($input);

        return $this->sendResponse(new FraisInscriptionResource($fraisInscription), 'Frais Inscription saved successfully');
    }

    /**
     * Display the specified FraisInscription.
     * GET|HEAD /fraisInscriptions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FraisInscription $fraisInscription */
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            return $this->sendError('Frais Inscription  non trouvé(e)');
        }

        return $this->sendResponse(new FraisInscriptionResource($fraisInscription), 'Frais Inscription retrieved successfully');
    }

    /**
     * Update the specified FraisInscription in storage.
     * PUT/PATCH /fraisInscriptions/{id}
     *
     * @param int $id
     * @param UpdateFraisInscriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFraisInscriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var FraisInscription $fraisInscription */
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            return $this->sendError('Frais Inscription  non trouvé(e)');
        }

        $fraisInscription = $this->fraisInscriptionRepository->update($input, $id);

        return $this->sendResponse(new FraisInscriptionResource($fraisInscription), 'FraisInscription modifié(e) avec success');
    }

    /**
     * Remove the specified FraisInscription from storage.
     * DELETE /fraisInscriptions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FraisInscription $fraisInscription */
        $fraisInscription = $this->fraisInscriptionRepository->find($id);

        if (empty($fraisInscription)) {
            return $this->sendError('Frais Inscription  non trouvé(e)');
        }

        $fraisInscription->delete();

        return $this->sendSuccess('Frais Inscription deleted successfully');
    }
}
