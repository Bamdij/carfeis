<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAproposCarfeisAPIRequest;
use App\Http\Requests\API\UpdateAproposCarfeisAPIRequest;
use App\Models\AproposCarfeis;
use App\Repositories\AproposCarfeisRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AproposCarfeisResource;
use Response;

/**
 * Class AproposCarfeisController
 * @package App\Http\Controllers\API
 */

class AproposCarfeisAPIController extends AppBaseController
{
    /** @var  AproposCarfeisRepository */
    private $aproposCarfeisRepository;

    public function __construct(AproposCarfeisRepository $aproposCarfeisRepo)
    {
        $this->aproposCarfeisRepository = $aproposCarfeisRepo;
    }

    /**
     * Display a listing of the AproposCarfeis.
     * GET|HEAD /aproposCarfeis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $aproposCarfeis = $this->aproposCarfeisRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(AproposCarfeisResource::collection($aproposCarfeis), 'Apropos Carfeis retrieved successfully');
    }

    /**
     * Store a newly created AproposCarfeis in storage.
     * POST /aproposCarfeis
     *
     * @param CreateAproposCarfeisAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAproposCarfeisAPIRequest $request)
    {
        $input = $request->all();

        $aproposCarfeis = $this->aproposCarfeisRepository->create($input);

        return $this->sendResponse(new AproposCarfeisResource($aproposCarfeis), 'Apropos Carfeis saved successfully');
    }

    /**
     * Display the specified AproposCarfeis.
     * GET|HEAD /aproposCarfeis/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AproposCarfeis $aproposCarfeis */
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            return $this->sendError('Apropos Carfeis not found');
        }

        return $this->sendResponse(new AproposCarfeisResource($aproposCarfeis), 'Apropos Carfeis retrieved successfully');
    }

    /**
     * Update the specified AproposCarfeis in storage.
     * PUT/PATCH /aproposCarfeis/{id}
     *
     * @param int $id
     * @param UpdateAproposCarfeisAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAproposCarfeisAPIRequest $request)
    {
        $input = $request->all();

        /** @var AproposCarfeis $aproposCarfeis */
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            return $this->sendError('Apropos Carfeis not found');
        }

        $aproposCarfeis = $this->aproposCarfeisRepository->update($input, $id);

        return $this->sendResponse(new AproposCarfeisResource($aproposCarfeis), 'AproposCarfeis updated successfully');
    }

    /**
     * Remove the specified AproposCarfeis from storage.
     * DELETE /aproposCarfeis/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AproposCarfeis $aproposCarfeis */
        $aproposCarfeis = $this->aproposCarfeisRepository->find($id);

        if (empty($aproposCarfeis)) {
            return $this->sendError('Apropos Carfeis not found');
        }

        $aproposCarfeis->delete();

        return $this->sendSuccess('Apropos Carfeis deleted successfully');
    }
}
