<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateParentsAPIRequest;
use App\Http\Requests\API\UpdateParentsAPIRequest;
use App\Models\Parents;
use App\Repositories\ParentsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ParentsResource;
use Response;

/**
 * Class ParentsController
 * @package App\Http\Controllers\API
 */

class ParentsAPIController extends AppBaseController
{
    /** @var  ParentsRepository */
    private $parentsRepository;

    public function __construct(ParentsRepository $parentsRepo)
    {
        $this->parentsRepository = $parentsRepo;
    }

    /**
     * Display a listing of the Parents.
     * GET|HEAD /parents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $parents = $this->parentsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(ParentsResource::collection($parents), 'Parents retrieved successfully');
    }

    /**
     * Store a newly created Parents in storage.
     * POST /parents
     *
     * @param CreateParentsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateParentsAPIRequest $request)
    {
        $input = $request->all();

        $parents = $this->parentsRepository->create($input);

        return $this->sendResponse(new ParentsResource($parents), 'Parents saved successfully');
    }

    /**
     * Display the specified Parents.
     * GET|HEAD /parents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Parents $parents */
        $parents = $this->parentsRepository->find($id);

        if (empty($parents)) {
            return $this->sendError('Parents not found');
        }

        return $this->sendResponse(new ParentsResource($parents), 'Parents retrieved successfully');
    }

    /**
     * Update the specified Parents in storage.
     * PUT/PATCH /parents/{id}
     *
     * @param int $id
     * @param UpdateParentsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParentsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Parents $parents */
        $parents = $this->parentsRepository->find($id);

        if (empty($parents)) {
            return $this->sendError('Parents not found');
        }

        $parents = $this->parentsRepository->update($input, $id);

        return $this->sendResponse(new ParentsResource($parents), 'Parents updated successfully');
    }

    /**
     * Remove the specified Parents from storage.
     * DELETE /parents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Parents $parents */
        $parents = $this->parentsRepository->find($id);

        if (empty($parents)) {
            return $this->sendError('Parents not found');
        }

        $parents->delete();

        return $this->sendSuccess('Parents deleted successfully');
    }
}
