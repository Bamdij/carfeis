<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAlphabetisationAPIRequest;
use App\Http\Requests\API\UpdateAlphabetisationAPIRequest;
use App\Models\Alphabetisation;
use App\Repositories\AlphabetisationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AlphabetisationResource;
use Response;

/**
 * Class AlphabetisationController
 * @package App\Http\Controllers\API
 */

class AlphabetisationAPIController extends AppBaseController
{
    /** @var  AlphabetisationRepository */
    private $alphabetisationRepository;

    public function __construct(AlphabetisationRepository $alphabetisationRepo)
    {
        $this->alphabetisationRepository = $alphabetisationRepo;
    }

    /**
     * Display a listing of the Alphabetisation.
     * GET|HEAD /alphabetisations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $alphabetisations = $this->alphabetisationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(AlphabetisationResource::collection($alphabetisations), 'Alphabetisations retrieved successfully');
    }

    /**
     * Store a newly created Alphabetisation in storage.
     * POST /alphabetisations
     *
     * @param CreateAlphabetisationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAlphabetisationAPIRequest $request)
    {
        $input = $request->all();

        $alphabetisation = $this->alphabetisationRepository->create($input);

        return $this->sendResponse(new AlphabetisationResource($alphabetisation), 'Alphabetisation saved successfully');
    }

    /**
     * Display the specified Alphabetisation.
     * GET|HEAD /alphabetisations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Alphabetisation $alphabetisation */
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            return $this->sendError('Alphabetisation  non trouvé(e)');
        }

        return $this->sendResponse(new AlphabetisationResource($alphabetisation), 'Alphabetisation retrieved successfully');
    }

    /**
     * Update the specified Alphabetisation in storage.
     * PUT/PATCH /alphabetisations/{id}
     *
     * @param int $id
     * @param UpdateAlphabetisationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlphabetisationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Alphabetisation $alphabetisation */
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            return $this->sendError('Alphabetisation  non trouvé(e)');
        }

        $alphabetisation = $this->alphabetisationRepository->update($input, $id);

        return $this->sendResponse(new AlphabetisationResource($alphabetisation), 'Alphabetisation modifié(e) avec success');
    }

    /**
     * Remove the specified Alphabetisation from storage.
     * DELETE /alphabetisations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Alphabetisation $alphabetisation */
        $alphabetisation = $this->alphabetisationRepository->find($id);

        if (empty($alphabetisation)) {
            return $this->sendError('Alphabetisation  non trouvé(e)');
        }

        $alphabetisation->delete();

        return $this->sendSuccess('Alphabetisation deleted successfully');
    }
}
