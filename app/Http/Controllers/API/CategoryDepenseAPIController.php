<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryDepenseAPIRequest;
use App\Http\Requests\API\UpdateCategoryDepenseAPIRequest;
use App\Models\CategoryDepense;
use App\Repositories\CategoryDepenseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoryDepenseResource;
use Response;

/**
 * Class CategoryDepenseController
 * @package App\Http\Controllers\API
 */

class CategoryDepenseAPIController extends AppBaseController
{
    /** @var  CategoryDepenseRepository */
    private $categoryDepenseRepository;

    public function __construct(CategoryDepenseRepository $categoryDepenseRepo)
    {
        $this->categoryDepenseRepository = $categoryDepenseRepo;
    }

    /**
     * Display a listing of the CategoryDepense.
     * GET|HEAD /categoryDepenses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryDepenses = $this->categoryDepenseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(CategoryDepenseResource::collection($categoryDepenses), 'Category Depenses retrieved successfully');
    }

    /**
     * Store a newly created CategoryDepense in storage.
     * POST /categoryDepenses
     *
     * @param CreateCategoryDepenseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryDepenseAPIRequest $request)
    {
        $input = $request->all();

        $categoryDepense = $this->categoryDepenseRepository->create($input);

        return $this->sendResponse(new CategoryDepenseResource($categoryDepense), 'Category Depense saved successfully');
    }

    /**
     * Display the specified CategoryDepense.
     * GET|HEAD /categoryDepenses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryDepense $categoryDepense */
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            return $this->sendError('Category Depense  non trouvé(e)');
        }

        return $this->sendResponse(new CategoryDepenseResource($categoryDepense), 'Category Depense retrieved successfully');
    }

    /**
     * Update the specified CategoryDepense in storage.
     * PUT/PATCH /categoryDepenses/{id}
     *
     * @param int $id
     * @param UpdateCategoryDepenseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryDepenseAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryDepense $categoryDepense */
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            return $this->sendError('Category Depense  non trouvé(e)');
        }

        $categoryDepense = $this->categoryDepenseRepository->update($input, $id);

        return $this->sendResponse(new CategoryDepenseResource($categoryDepense), 'CategoryDepense modifié(e) avec success');
    }

    /**
     * Remove the specified CategoryDepense from storage.
     * DELETE /categoryDepenses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryDepense $categoryDepense */
        $categoryDepense = $this->categoryDepenseRepository->find($id);

        if (empty($categoryDepense)) {
            return $this->sendError('Category Depense  non trouvé(e)');
        }

        $categoryDepense->delete();

        return $this->sendSuccess('Category Depense deleted successfully');
    }
}
