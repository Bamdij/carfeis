<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InfoParentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'eleve_id' => $this->eleve_id,
            'name_pere' => $this->name_pere,
            'phone_pere' => $this->phone_pere,
            'email_pere' => $this->email_pere,
            'type_profession_id' => $this->type_profession_id,
            'name_mere' => $this->name_mere,
            'phone_mere' => $this->phone_mere,
            'email_mere' => $this->email_mere,
            'type_profession_mere_id' => $this->type_profession_mere_id,
            'name_tuteur' => $this->name_tuteur,
            'lien_parent_id' => $this->lien_parent_id,
            'phone_tuteur' => $this->phone_tuteur,
            'email_tuteur' => $this->email_tuteur,
            'contact_urgence' => $this->contact_urgence,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
