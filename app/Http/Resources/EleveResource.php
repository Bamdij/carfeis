<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EleveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'date_naissance' => $this->date_naissance,
            'sexe' => $this->sexe,
            'groupe_sanguin' => $this->groupe_sanguin,
            'is_malade' => $this->is_malade,
            'quelle_maladie' => $this->quelle_maladie,
            'is_traitement' => $this->is_traitement,
            'quel_traitement' => $this->quel_traitement,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
