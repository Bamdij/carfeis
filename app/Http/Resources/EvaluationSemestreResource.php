<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EvaluationSemestreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'eleve_id' => $this->eleve_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'date_evaluation' => $this->date_evaluation,
            'monday_app' => $this->monday_app,
            'app_mond_1' => $this->app_mond_1,
            'app_mond_2' => $this->app_mond_2,
            'app_mon_3' => $this->app_mon_3,
            'thuesday_app' => $this->thuesday_app,
            'app_thues_1' => $this->app_thues_1,
            'app_thues_2' => $this->app_thues_2,
            'app_thues_3' => $this->app_thues_3,
            'wednesday_app' => $this->wednesday_app,
            'app_wed_1' => $this->app_wed_1,
            'app_wed_2' => $this->app_wed_2,
            'app_wed_3' => $this->app_wed_3,
            'thursday_app' => $this->thursday_app,
            'app_thurd_1' => $this->app_thurd_1,
            'app_thurd_2' => $this->app_thurd_2,
            'app_thurd_3' => $this->app_thurd_3,
            'friday_app' => $this->friday_app,
            'app_frid_1' => $this->app_frid_1,
            'app_frid_2' => $this->app_frid_2,
            'app_frid_3' => $this->app_frid_3,
            'last_lesson' => $this->last_lesson,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
