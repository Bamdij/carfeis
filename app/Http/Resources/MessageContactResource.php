<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'obje' => $this->obje,
            'description' => $this->description,
            'user_id' => $this->user_id,
            'is_admin' => $this->is_admin,
            'slug' => $this->slug,
            'is_parent_sending' => $this->is_parent_sending,
            'admin_id' => $this->admin_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
