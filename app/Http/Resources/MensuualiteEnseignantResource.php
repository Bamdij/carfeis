<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MensuualiteEnseignantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'enseignant_id' => $this->enseignant_id,
            'identity_card' => $this->identity_card,
            'month' => $this->month,
            'montant' => $this->montant,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'type_paiement_id' => $this->type_paiement_id,
            'observation' => $this->observation,
            'user_id' => $this->user_id,
            'month_end' => $this->month_end,
            'is_payed' => $this->is_payed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
