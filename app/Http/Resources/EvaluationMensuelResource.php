<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EvaluationMensuelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'eleve_id' => $this->eleve_id,
            'user_id' => $this->user_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'month' => $this->month,
            'date_evaluation' => $this->date_evaluation,
            'sourate' => $this->sourate,
            'last_lesson' => $this->last_lesson,
            'first_party' => $this->first_party,
            'appreciation_first_party' => $this->appreciation_first_party,
            'second_party' => $this->second_party,
            'appreciation_second_party' => $this->appreciation_second_party,
            'third_party' => $this->third_party,
            'appreciation_third_party' => $this->appreciation_third_party,
            'comportement_fr' => $this->comportement_fr,
            'comportement_ar' => $this->comportement_ar,
            'appreciation' => $this->appreciation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
