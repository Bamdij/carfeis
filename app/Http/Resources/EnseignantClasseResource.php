<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnseignantClasseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'enseignant_id' => $this->enseignant_id,
            'sous_niveau_id' => $this->sous_niveau_id,
            'is_teacher_arab' => $this->is_teacher_arab,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
