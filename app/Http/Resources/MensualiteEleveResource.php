<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MensualiteEleveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'paiement_eleve_id' => $this->paiement_eleve_id,
            'eleve_id' => $this->eleve_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'month' => $this->month,
            'mensualite' => $this->mensualite,
            'observation' => $this->observation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
