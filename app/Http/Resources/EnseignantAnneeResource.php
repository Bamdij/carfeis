<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnseignantAnneeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'enseignant_id' => $this->enseignant_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'type_enseignant_id' => $this->type_enseignant_id,
            'niveau_id' => $this->niveau_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
