<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SourateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'num_sourate' => $this->num_sourate,
            'libelle_sourate_fr' => $this->libelle_sourate_fr,
            'libelle_sourate_ar' => $this->libelle_sourate_ar,
            'nbre_verset' => $this->nbre_verset,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
