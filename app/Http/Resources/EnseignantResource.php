<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnseignantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'adresse' => $this->adresse,
            'niveau_etude' => $this->niveau_etude,
            'date_recrutement' => $this->date_recrutement,
            'type_enseignant_id' => $this->type_enseignant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
