<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FraisInscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'regime_id' => $this->regime_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'montant' => $this->montant,
            'observation' => $this->observation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
