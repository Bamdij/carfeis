<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaiementEleveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'eleve_id' => $this->eleve_id,
            'frais_inscription_id' => $this->frais_inscription_id,
            'annee_scolaire_id' => $this->annee_scolaire_id,
            'mois_1' => $this->mois_1,
            'montant_1' => $this->montant_1,
            'mois_2' => $this->mois_2,
            'montant_2' => $this->montant_2,
            'mois_3' => $this->mois_3,
            'montant_3' => $this->montant_3,
            'mois_4' => $this->mois_4,
            'montant_5' => $this->montant_5,
            'mois_6' => $this->mois_6,
            'montant_6' => $this->montant_6,
            'mois_7' => $this->mois_7,
            'montant_7' => $this->montant_7,
            'mois_8' => $this->mois_8,
            'montant_8' => $this->montant_8,
            'mois_9' => $this->mois_9,
            'montant_9' => $this->montant_9,
            'mois_10' => $this->mois_10,
            'montant_10' => $this->montant_10,
            'mois_11' => $this->mois_11,
            'montant_11' => $this->montant_11,
            'mois_12' => $this->mois_12,
            'montant_12' => $this->montant_12,
            'obseration_1' => $this->obseration_1,
            'obseration_2' => $this->obseration_2,
            'obseration_3' => $this->obseration_3,
            'obseration_4' => $this->obseration_4,
            'obseration_5' => $this->obseration_5,
            'obseration_6' => $this->obseration_6,
            'obseration_7' => $this->obseration_7,
            'obseration_8' => $this->obseration_8,
            'obseration_9' => $this->obseration_9,
            'obseration_10' => $this->obseration_10,
            'obseration_11' => $this->obseration_11,
            'obseration_12' => $this->obseration_12,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
