<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepenseCategorieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_depense_id' => $this->category_depense_id,
            'date_depense' => $this->date_depense,
            'designation_depense' => $this->designation_depense,
            'montant_depense' => $this->montant_depense,
            'user_id' => $this->user_id,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
