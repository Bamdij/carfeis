<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AlphabetisationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_alphabetisation' => $this->name_alphabetisation,
            'daara_frequente' => $this->daara_frequente,
            'inteligence' => $this->inteligence,
            'person_inscript' => $this->person_inscript,
            'cap_finance_resp' => $this->cap_finance_resp,
            'date_inscription' => $this->date_inscription,
            'eleve_id' => $this->eleve_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
