<?php

namespace App\Http\Requests\API;

use App\Models\EnseignantClasse;
use InfyOm\Generator\Request\APIRequest;

class UpdateEnseignantClasseAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = EnseignantClasse::$rules;
        
        return $rules;
    }
}
