<div class="row">
    <div class="form-group col-sm-4" style="display:none;">
        <strong>{!! Form::label('user_id', 'User Id:') !!}</strong> 
        <input class="form-control"  name="user_id" type="text" id="user_id" value="{{Auth::user()->id}}">
        <input class="form-control"  name="eleve_id" type="text" id="eleve_id" value="{{$eleve->id}}">
        <input class="form-control"  name="annee_scolaire_id" type="text" id="annee_scolaire_id" value="{{$annee_scolaire->id}}">
    </div>
        <!-- App Frid 3 Field -->
        <div class="form-group col-sm-4">
        <strong> {!! Form::label('start_date', 'Date de Debut:') !!}</strong> 
        {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Last Lesson Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('end_date', 'Date de Fin:') !!}</strong>
        {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
    </div>
        <!-- Last Lesson Field -->
        <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_evaluation', 'Date Evaluation:') !!}</strong>  
        {!! Form::date('date_evaluation', null, ['class' => 'form-control']) !!}
    </div>
    <!-- App Mond 1 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_mond_1', 'Jour  Lundi:') !!}</strong> 
        <input class="form-control"  name="monday_app" type="text" id="monday_app" value="{{$evaluationSemestre->monday_app}}">
    </div>
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_mond_1', 'Appreciation1 Lundi:') !!}</strong>   
        <select name="app_mond_1" id="app_mond_1" class="form-control" required>
            <option value="{{$evaluationSemestre->app_mond_1}}" selected>{{$evaluationSemestre->app_mond_1}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>
    <!-- App Mond 2 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_mond_2', 'Appreciation2 Lundi:') !!}</strong> 
        <select name="app_mond_2" id="app_mond_2" class="form-control" required>
        <option value="{{$evaluationSemestre->app_mond_2}}" selected>{{$evaluationSemestre->app_mond_2}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Mon 3 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_mon_3', 'Appreciation3 Lundi:') !!}</strong>  
        <select name="app_mon_3" id="app_mond_3" class="form-control" required>
        <option value="{{$evaluationSemestre->app_mond_3}}" selected>{{$evaluationSemestre->app_mond_3}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- Thuesday App Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('thuesday_app', 'Jour Mardi:') !!}</strong>  
        <input class="form-control"  name="monday_app" type="text" id="thuesday_app" value="Mardi">
    </div>

    <!-- App Thues 3 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_thues_1', 'Appreciation 1 Mardi:') !!}</strong>  
        <select name="app_thues_1" id="app_thues_1" class="form-control" required>
        <option value="{{$evaluationSemestre->app_thues_1}}" selected>{{$evaluationSemestre->app_thues_1}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>
    <!-- App Thues 3 Field -->
    <div class="form-group col-sm-3">
        <strong> {!! Form::label('app_thues_2', 'Appreciation 2 Mardi:') !!}</strong>  
        <select name="app_thues_2" id="app_thues_2" class="form-control" required>
        <option value="{{$evaluationSemestre->app_thues_1}}" selected>{{$evaluationSemestre->app_thues_1}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>
    <!-- App Thues 3 Field -->
    <div class="form-group col-sm-3">
    <strong>{!! Form::label('app_thues_3', 'Appreciation 3 Mardi:') !!}</strong> 
    <select name="app_thues_3" id="app_thues_3" class="form-control" required>
    <option value="{{$evaluationSemestre->app_thues_3}}" selected>{{$evaluationSemestre->app_thues_3}}</option>
        <option value="Oui">Oui</option>
        <option value="Non">Non</option>
    </select>
    </div>
    <!-- Wednesday App Field -->
    <div class="form-group col-sm-3">
        <strong> {!! Form::label('wednesday_app', 'Jour  Mercredi:') !!}</strong>  
        <input class="form-control"  name="wednesday_app" type="text" id="monday_app" value="Mercredi">
    </div>

    <!-- App Wed 1 Field -->
    <div class="form-group col-sm-3">
    <strong>{!! Form::label('app_wed_1', 'Appreciation 1 Mercredi:') !!}</strong>    
        <select name="app_wed_1" id="app_wed_1" class="form-control" required>
        <option value="{{$evaluationSemestre->app_wed_1}}" selected>{{$evaluationSemestre->app_wed_1}}</option>
        <option value="Oui">Oui</option>
        <option value="Non">Non</option>
    </select>
    </div>

    <!-- App Wed 2 Field -->
    <div class="form-group col-sm-3">
    <strong> {!! Form::label('app_wed_2', 'Appreciation 2 Mercredi:') !!}</strong>   
        <select name="app_wed_2" id="app_wed_2" class="form-control" required>
        <option value="{{$evaluationSemestre->app_wed_2}}" selected>{{$evaluationSemestre->app_wed_2}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Wed 3 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_wed_3', 'Appreciation 3 Mercredi:') !!}</strong> 
        <select name="app_wed_3" id="app_wed_3" class="form-control" required>
        <option value="{{$evaluationSemestre->app_wed_3}}" selected>{{$evaluationSemestre->app_wed_3}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- Thursday App Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('thursday_app', 'Jour Jeudi:') !!}</strong>  
        <input class="form-control"  name="thursday_app" type="text" id="monday_app" value="Jeudi">   
    </div>

    <!-- App Thurd 1 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_thurd_1', 'Appreciation 1 Jeudi:') !!}</strong>  
        <select name="app_thurd_1" id="app_thurd_1" class="form-control" required>
        <option value="{{$evaluationSemestre->app_thurd_1}}" selected>{{$evaluationSemestre->app_thurd_1}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Thurd 2 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_thurd_2', 'Appreciation 2 Jeudi:') !!}</strong> 
        <select name="app_thurd_2" id="app_thurd_2" class="form-control" required>
        <option value="{{$evaluationSemestre->app_thurd_2}}" selected>{{$evaluationSemestre->app_thurd_2}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Thurd 3 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_thurd_3','Appreciation 3 Jeudi:') !!}</strong> 
        <select name="app_thurd_3" id="app_thurd_3" class="form-control" required>
        <option value="{{$evaluationSemestre->app_thurd_3}}" selected>{{$evaluationSemestre->app_thurd_3}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- Friday App Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('friday_app', 'Jour Vendredi:') !!}</strong> 
        <input class="form-control"  name="friday_app" type="text" id="friday_app" value="Vendredi">   
    </div>

    <!-- App Frid 1 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_frid_1','Appreciation 1 Vendredi:') !!}</strong> 
        <select name="app_frid_1" id="app_frid_1" class="form-control" required>
        <option value="{{$evaluationSemestre->app_frid_1}}" selected>{{$evaluationSemestre->app_frid_1}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Frid 2 Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('app_frid_2','Appreciation 2 Vendredi:') !!}</strong>  
        <select name="app_frid_2" id="app_frid_2" class="form-control" required>
        <option value="{{$evaluationSemestre->app_frid_2}}" selected>{{$evaluationSemestre->app_frid_2}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- App Frid 3 Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('app_frid_3', 'Appreciation 3 Vendredi:') !!}
        <select name="app_frid_3" id="app_frid_3" class="form-control" required>
        <option value="{{$evaluationSemestre->app_frid_3}}" selected>{{$evaluationSemestre->app_frid_3}}</option>
            <option value="Oui">Oui</option>
            <option value="Non">Non</option>
        </select>
    </div>

    <!-- Last Lesson Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('last_lesson', 'Derniere leçon:') !!}</strong> 
        @include('evaluation_semestres.last_lesson')

    </div>

    </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('evaluation_detail.eleve',$eleve->slug) }}" class="btn btn-secondary">Annuler</a>
</div>
