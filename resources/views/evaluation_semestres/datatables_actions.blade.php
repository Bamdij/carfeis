{!! Form::open(['route' => ['eleves.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('evaluation_detail.eleve', $slug) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
       Hebdomadaire
    </a>
    <a href="{{ route('evaluation_mensuel_detail.eleve', $slug) }}" class='btn btn-ghost-info' >
       <i class="fa fa-eye"></i>
       Mensuelle
    </a>
    <!-- <a data-toggle="modal" data-target=".bd-example-modal-lg" id="is_add_eleve" data-id="{{$id}}"class='btn btn-ghost-info'>
       <i class="fa fa-graduation-cap"></i>
    </a> -->
</div>
{!! Form::close() !!}

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div id="eleve_text"></div>

       </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mode Maintenance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="alert alert-warning" role="alert">
        Cette Etape est en cours de developpement nous vous prions de revenir sous peu Merci.
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
