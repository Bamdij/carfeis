<div class="table-responsive-sm table table-bordered">
    <table class="table table-striped" id="evaluationMensuels-table">
        <thead>
            <tr>
        <th>Annee Scolaire</th>
        <th>Mois Evaluation</th>
        <th>Sourate</th>
        <th>Derniere Leçon</th>
        <th>Premiere Partie</th>
        <th>Deuxieme Partie</th>
        <th>Troisieme Partie</th>
        <th>Comportement</th>
        <th>Appreciation</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($evaluationMensuels as $evaluationMensuel)
            <tr>
            <td>{{ $evaluationMensuel->annee }}</td>
            <td>{{ $evaluationMensuel->month }}</td>
            <td>{{ $evaluationMensuel->sourate }}</td>
            <td>{{ $evaluationMensuel->last_lesson }}</td>
            <td>{{ $evaluationMensuel->first_party }}/ 10<strong>({{ $evaluationMensuel->appreciation_first_party }})</strong> </td>
            <td>{{ $evaluationMensuel->second_party }} / 10<strong>({{ $evaluationMensuel->appreciation_second_party }})</strong></td>
            <td>{{ $evaluationMensuel->third_party }} / 10 <strong>({{ $evaluationMensuel->appreciation_third_party }})</strong></td>
            <td>{{ $evaluationMensuel->comportement_fr }}</td>
            <td>{{ $evaluationMensuel->appreciation }}</td>
                <td>
                    {!! Form::open(['route' => ['evaluationMensuels.destroy', $evaluationMensuel->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('evaluationMensuels.edit', [$evaluationMensuel->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>