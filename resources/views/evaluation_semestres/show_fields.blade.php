<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $evaluationSemestre->user_id }}</p>
</div>

<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $evaluationSemestre->eleve_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $evaluationSemestre->annee_scolaire_id }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $evaluationSemestre->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{{ $evaluationSemestre->end_date }}</p>
</div>

<!-- Date Evaluation Field -->
<div class="form-group">
    {!! Form::label('date_evaluation', 'Date Evaluation:') !!}
    <p>{{ $evaluationSemestre->date_evaluation }}</p>
</div>

<!-- Monday App Field -->
<div class="form-group">
    {!! Form::label('monday_app', 'Monday App:') !!}
    <p>{{ $evaluationSemestre->monday_app }}</p>
</div>

<!-- App Mond 1 Field -->
<div class="form-group">
    {!! Form::label('app_mond_1', 'App Mond 1:') !!}
    <p>{{ $evaluationSemestre->app_mond_1 }}</p>
</div>

<!-- App Mond 2 Field -->
<div class="form-group">
    {!! Form::label('app_mond_2', 'App Mond 2:') !!}
    <p>{{ $evaluationSemestre->app_mond_2 }}</p>
</div>

<!-- App Mon 3 Field -->
<div class="form-group">
    {!! Form::label('app_mon_3', 'App Mon 3:') !!}
    <p>{{ $evaluationSemestre->app_mon_3 }}</p>
</div>

<!-- Thuesday App Field -->
<div class="form-group">
    {!! Form::label('thuesday_app', 'Thuesday App:') !!}
    <p>{{ $evaluationSemestre->thuesday_app }}</p>
</div>

<!-- App Thues 1 Field -->
<div class="form-group">
    {!! Form::label('app_thues_1', 'App Thues 1:') !!}
    <p>{{ $evaluationSemestre->app_thues_1 }}</p>
</div>

<!-- App Thues 2 Field -->
<div class="form-group">
    {!! Form::label('app_thues_2', 'App Thues 2:') !!}
    <p>{{ $evaluationSemestre->app_thues_2 }}</p>
</div>

<!-- App Thues 3 Field -->
<div class="form-group">
    {!! Form::label('app_thues_3', 'App Thues 3:') !!}
    <p>{{ $evaluationSemestre->app_thues_3 }}</p>
</div>

<!-- Wednesday App Field -->
<div class="form-group">
    {!! Form::label('wednesday_app', 'Wednesday App:') !!}
    <p>{{ $evaluationSemestre->wednesday_app }}</p>
</div>

<!-- App Wed 1 Field -->
<div class="form-group">
    {!! Form::label('app_wed_1', 'App Wed 1:') !!}
    <p>{{ $evaluationSemestre->app_wed_1 }}</p>
</div>

<!-- App Wed 2 Field -->
<div class="form-group">
    {!! Form::label('app_wed_2', 'App Wed 2:') !!}
    <p>{{ $evaluationSemestre->app_wed_2 }}</p>
</div>

<!-- App Wed 3 Field -->
<div class="form-group">
    {!! Form::label('app_wed_3', 'App Wed 3:') !!}
    <p>{{ $evaluationSemestre->app_wed_3 }}</p>
</div>

<!-- Thursday App Field -->
<div class="form-group">
    {!! Form::label('thursday_app', 'Thursday App:') !!}
    <p>{{ $evaluationSemestre->thursday_app }}</p>
</div>

<!-- App Thurd 1 Field -->
<div class="form-group">
    {!! Form::label('app_thurd_1', 'App Thurd 1:') !!}
    <p>{{ $evaluationSemestre->app_thurd_1 }}</p>
</div>

<!-- App Thurd 2 Field -->
<div class="form-group">
    {!! Form::label('app_thurd_2', 'App Thurd 2:') !!}
    <p>{{ $evaluationSemestre->app_thurd_2 }}</p>
</div>

<!-- App Thurd 3 Field -->
<div class="form-group">
    {!! Form::label('app_thurd_3', 'App Thurd 3:') !!}
    <p>{{ $evaluationSemestre->app_thurd_3 }}</p>
</div>

<!-- Friday App Field -->
<div class="form-group">
    {!! Form::label('friday_app', 'Friday App:') !!}
    <p>{{ $evaluationSemestre->friday_app }}</p>
</div>

<!-- App Frid 1 Field -->
<div class="form-group">
    {!! Form::label('app_frid_1', 'App Frid 1:') !!}
    <p>{{ $evaluationSemestre->app_frid_1 }}</p>
</div>

<!-- App Frid 2 Field -->
<div class="form-group">
    {!! Form::label('app_frid_2', 'App Frid 2:') !!}
    <p>{{ $evaluationSemestre->app_frid_2 }}</p>
</div>

<!-- App Frid 3 Field -->
<div class="form-group">
    {!! Form::label('app_frid_3', 'App Frid 3:') !!}
    <p>{{ $evaluationSemestre->app_frid_3 }}</p>
</div>

<!-- Last Lesson Field -->
<div class="form-group">
    {!! Form::label('last_lesson', 'Last Lesson:') !!}
    <p>{{ $evaluationSemestre->last_lesson }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $evaluationSemestre->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $evaluationSemestre->updated_at }}</p>
</div>

