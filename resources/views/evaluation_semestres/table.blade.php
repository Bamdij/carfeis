<div class="table-responsive-sm">
    <table class="table table-striped" id="evaluationSemestres-table">
        <thead>
            <tr>
                <th>User Id</th>
        <th>Eleve Id</th>
        <th>Annee Scolaire Id</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Date Evaluation</th>
        <th>Monday App</th>
        <th>App Mond 1</th>
        <th>App Mond 2</th>
        <th>App Mon 3</th>
        <th>Thuesday App</th>
        <th>App Thues 1</th>
        <th>App Thues 2</th>
        <th>App Thues 3</th>
        <th>Wednesday App</th>
        <th>App Wed 1</th>
        <th>App Wed 2</th>
        <th>App Wed 3</th>
        <th>Thursday App</th>
        <th>App Thurd 1</th>
        <th>App Thurd 2</th>
        <th>App Thurd 3</th>
        <th>Friday App</th>
        <th>App Frid 1</th>
        <th>App Frid 2</th>
        <th>App Frid 3</th>
        <th>Last Lesson</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($evaluationSemestres as $evaluationSemestre)
            <tr>
                <td>{{ $evaluationSemestre->user_id }}</td>
            <td>{{ $evaluationSemestre->eleve_id }}</td>
            <td>{{ $evaluationSemestre->annee_scolaire_id }}</td>
            <td>{{ $evaluationSemestre->start_date }}</td>
            <td>{{ $evaluationSemestre->end_date }}</td>
            <td>{{ $evaluationSemestre->date_evaluation }}</td>
            <td>{{ $evaluationSemestre->monday_app }}</td>
            <td>{{ $evaluationSemestre->app_mond_1 }}</td>
            <td>{{ $evaluationSemestre->app_mond_2 }}</td>
            <td>{{ $evaluationSemestre->app_mon_3 }}</td>
            <td>{{ $evaluationSemestre->thuesday_app }}</td>
            <td>{{ $evaluationSemestre->app_thues_1 }}</td>
            <td>{{ $evaluationSemestre->app_thues_2 }}</td>
            <td>{{ $evaluationSemestre->app_thues_3 }}</td>
            <td>{{ $evaluationSemestre->wednesday_app }}</td>
            <td>{{ $evaluationSemestre->app_wed_1 }}</td>
            <td>{{ $evaluationSemestre->app_wed_2 }}</td>
            <td>{{ $evaluationSemestre->app_wed_3 }}</td>
            <td>{{ $evaluationSemestre->thursday_app }}</td>
            <td>{{ $evaluationSemestre->app_thurd_1 }}</td>
            <td>{{ $evaluationSemestre->app_thurd_2 }}</td>
            <td>{{ $evaluationSemestre->app_thurd_3 }}</td>
            <td>{{ $evaluationSemestre->friday_app }}</td>
            <td>{{ $evaluationSemestre->app_frid_1 }}</td>
            <td>{{ $evaluationSemestre->app_frid_2 }}</td>
            <td>{{ $evaluationSemestre->app_frid_3 }}</td>
            <td>{{ $evaluationSemestre->last_lesson }}</td>
                <td>
                    {!! Form::open(['route' => ['evaluationSemestres.destroy', $evaluationSemestre->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('evaluationSemestres.show', [$evaluationSemestre->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('evaluationSemestres.edit', [$evaluationSemestre->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>