<!-- First Name Field -->
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS ELEVE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $eleve->first_name }}</p>
    </div>

    <!-- Last Name Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $eleve->last_name }}</p>
    </div>
    <div class="form-group col-sm-4">
        <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
        <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
    </div>
    <!-- Date Naissance Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
        <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
    </div>

    <!-- Sexe Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
        <p>{{ $eleve->sexe }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('Enseignant', 'Enseignant:') !!}</strong>
        <p>{{ $eleve_class_by_year->enseignant }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('Enseignant', 'Classe:') !!}</strong>
        <p>{{ $eleve_class_by_year->classe }}</p>
    </div>
</div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Evaluation Mensuelle</h3>
    </div>
</div><!--/.row-->
<div class="row dynamic_taille_field_container">
    <div class="col-md-2 offset-md-10 niveau_added mt-3 mb-4">
        <button class="pull-right btn btn-primary" id="evaluation_mensuel" data-id="{{$eleve->id}}" data-toggle="modal" data-target=".bd-example-modal-lg" >Ajouter une Evaluation Mensuelle
        </button>
    </div><!--/.col-md-6 -->
</div>
@if(!$evaluationMensuels->isEmpty())
<div class="row">
    @include('evaluation_semestres.table_mensuel')
</div>
@else
<div class="alert alert-warning" role="alert">
  Eleve Pas encors evaluer!
</div>
@endif

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div id="evaluation_access_mensuel"></div>
    </div>
  </div>
</div>


