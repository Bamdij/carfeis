@if(Auth::user()->status == "superadmin" || Auth::user()->status == "directeur" || Auth::user()->status == "responsable_pedagogique" || Auth::user()->status == "responsable_finance" ) 
<li class="c-sidebar-nav-item p-2">
    <a class="c-sidebar-nav-link c-active active" href="{{route('home')}}">
        <i class="c-sidebar-nav-icon cil-home"></i>Home
    </a>
</li>
@endif
 @if(Auth::user()->status == "superadmin" || Auth::user()->status == "directeur" ) 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4"  href="{{route('user.index')}}">
        <i class="c-sidebar-nav-icon cil-people"></i>Administrateurs
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('Users*') ? 'active' : '' }}">
        <a class="c-sidebar-nav-link" href="{{route('user.index')}}">
                <i class="c-sidebar-nav-icon cil-user"></i>Ajouter administrateur
            </a>
        </li>
    </ul>
</li>
 @endif 
 @if(Auth::user()->status == "superadmin" || Auth::user()->status == "directeur" || Auth::user()->status == "responsable_pedagogique") 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>FICHIERS
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
        <li class="c-sidebar-nav-item p-2 {{ Request::is('anneeScolaires*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('anneeScolaires.index') }}">
                <i class="c-sidebar-nav-icon cil-sign-language"></i>Année Scolaire
            </a>
        </li>
         <li class="c-sidebar-nav-item p-2 {{ Request::is('typeProfessions*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('typeProfessions.index') }}">
                <i class="c-sidebar-nav-icon cil-book"></i>Type Professions
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2 {{ Request::is('lienParents*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('lienParents.index') }}">
                <i class="c-sidebar-nav-icon cil-user-unfollow"></i>Lien Parents
            </a>
        </li>
         <li class="c-sidebar-nav-item p-2{{ Request::is('typeEnseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('typeEnseignants.index') }}">
                <i class="c-sidebar-nav-icon cil-library-add"></i>Type Enseignants
            </a>
        </li> 
        <li class="c-sidebar-nav-item p-2{{ Request::is('enseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('enseignants.index') }}">
                <i class="c-sidebar-nav-icon cil-people"></i>Enseignants
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2{{ Request::is('typePaiements*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('type_paiements.index') }}">
                <i class="c-sidebar-nav-icon cil-bank"></i>Type Paiement
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2{{ Request::is('categoryDepenses*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('categoryDepenses.index') }}">
                <i class="c-sidebar-nav-icon cil-bar-chart"></i>Categorie Depenses
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2{{ Request::is('Niveaux*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('niveaux.index') }}">
                <i class="c-sidebar-nav-icon cil-stream"></i>Niveaux  
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2 {{ Request::is('regimes*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('regimes.index') }}">
                <i class="c-sidebar-nav-icon cil-folder-open"></i>Regimes
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2{{ Request::is('eleves*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleves.index') }}">
                <i class="c-sidebar-nav-icon cil-user"></i>Eleves
            </a>
        </li>
        <li class="c-sidebar-nav-item p-2{{ Request::is('aproposCarfeis*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('aproposCarfeis.index') }}">
                <i class="c-sidebar-nav-icon cil-user"></i>A Propos de Carfeis
            </a>
        </li>
    </ul>
</li> 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Pédagogie
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('eleveClasses*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('get_enseignant_class.eleve') }}">
                <i class="c-sidebar-nav-icon cil-briefcase"></i>Professeurs par classe
            </a> 
        </li>
        <li class="c-sidebar-nav-item p-2 {{ Request::is('typeProfessions*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('get_eleve_by_class.classe') }}">
                <i class="c-sidebar-nav-icon cil-folder-open"></i>Eleves par classe
            </a>
        </li>
    </ul>
</li> 
@endif
@if(Auth::user()->status == "superadmin" || Auth::user()->status == "directeur" || Auth::user()->status == "responsable_pedagogique" || Auth::user()->status == "responsable_finance" ) 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Finances
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('fraisInscriptions*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('fraisInscriptions.index') }}">
                <i class="c-sidebar-nav-icon cil-briefcase"></i>Frais Inscriptions
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('paiementEleves*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('paiment.eleve') }}">
                <i class="c-sidebar-nav-icon cil-briefcase"></i>Paiement Eleve
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('depenseCategories*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('depenseCategories.index') }}">
                <i class="c-sidebar-nav-icon cil-options"></i>Suivi de Depenses
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('mensuualiteEnseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('mensuualiteEnseignants.index') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Mensualite Enseignants
            </a>
        </li>
    </ul>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('mensuualiteEnseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('get_list_status.annuelle') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Voir Status Paiement
            </a>
        </li>
    </ul>
</li> 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Situation des Paiements 
    </a>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('mensuualiteEnseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('get_list_status.annuelle') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Voir Status Paiement
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('mensuualiteEnseignants*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('get_list_status_depenses.annuelle') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Voir Status Depenses
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('messageContacts*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleve.get_all_paiement') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Suivi Paiement Eleve
            </a>
        </li>
    </ul>
</li> 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Parents
    </a>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('parents*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('parents.index') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Voir Parents
            </a>
        </li>
    </ul>
</li>
@endif
@if(Auth::user()->status == "superadmin" || Auth::user()->status == "directeur" || Auth::user()->status == "responsable_professeur"  ) 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Evaluation Eleve
    </a>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('evaluationSemestres*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('getinfos.eleve') }}">
                <i class="c-sidebar-nav-icon cil-money"></i>Evaluation
            </a>
        </li>
    </ul>
</li> 
@endif

@if(Auth::user()->status == "Parent") 
<li class="c-sidebar-nav-item p-2">
    <a class="c-sidebar-nav-link c-active active" href="{{route('dashboard.parent')}}">
        <i class="c-sidebar-nav-icon cil-home"></i>Tableau de Bord
    </a>
</li>
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Suivie Eleve
    </a>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('evaluationSemestres*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleve_parent.parent') }}">
                <i class="c-sidebar-nav-icon  cil-user"></i>Mes Eleves
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('evaluationSemestres*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleve_parent_evaaluation_hebdomadaire.parent') }}">
                <i class="c-sidebar-nav-icon cil-tags"></i> Hebdomadaire
            </a>
        </li>
    </ul>
    <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('evaluationSemestres*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleve_parent_evaaluation_mensuel.parent') }}">
                <i class="c-sidebar-nav-icon cil-description"></i>Evaluation Mensuel
            </a>
        </li>
    </ul>
</li> 
<li class=" c-sidebar-nav-dropdown c-hide">
    <a class="c-sidebar-nav-dropdown-toggle p-4" href="#">
        <i class="c-sidebar-nav-icon cil-folder-open"></i>Paiement Et suivie
    </a>
     <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item p-2 {{ Request::is('evaluationSemestres*') ? 'active' : '' }}"  >
            <a class="c-sidebar-nav-link"href="{{ route('eleve_parent_paiement.parent') }}">
                <i class="c-sidebar-nav-icon  cil-user"></i>Suivie Paiement
            </a>
        </li>
    </ul>
</li> 
@endif
