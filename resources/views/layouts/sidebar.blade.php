
<div class="c-sidebar  c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-md-down-none py-3">
         @if(!empty(Auth::user()->image_path))
            <img src="{{asset(Auth::user()->image_path)}}" width="46" alt="{{Auth::user()->name}}"
             class="c-sidebar-brand-full rounded-circle" alt="core ui">
            <img src="{{asset(Auth::user()->image_path)}}" 
             class="c-sidebar-brand-minimized" alt="core ui" height="50" width="50">
        @else     
        <img src="{{asset('images/carfeis.png')}}" width="46"
             class="c-sidebar-brand-full rounded-circle" alt="core ui">
        <img src="{{asset('images/carfeis.png')}}" 
             class="c-sidebar-brand-minimized" alt="core ui" height="50" width="50">
        @endif
    </div>


    <ul class="c-sidebar-nav pt-5">
        @include('layouts.menu')
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 692px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 369px;"></div>
        </div>
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>
