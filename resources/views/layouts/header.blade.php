<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button"  data-target="#sidebar"
        data-class="c-sidebar-show" data-toggle="collapse">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button> 
<a class="c-header-brand d-lg-none c-header-brand-sm-up-center" href="#">
<!-- @if(!empty(Auth::user()->image_path))
    <img class="c-avatar-img" src="{{asset(Auth::user()->image_path)}}" alt="">
@else
<img class="c-avatar-img"  src="{{asset('images/carfeis.png')}}" alt="">
@endif -->
</a>
<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
        data-class="c-sidebar-lg-show" responsive="true" >
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>
<ul class="c-header-nav mfs-auto">
</ul>
<ul class="c-header-nav">
    <li class="c-header-nav-item dropdown ">
        <a class="c-header-nav-link " data-toggle="dropdown" href="#" 
           aria-haspopup="true" aria-expanded="false" >
            <div class="c-avatar hover_image mr-3">
            @if(!empty(Auth::user()->image_path))
                <img class="c-avatar-img " src="{{asset(Auth::user()->image_path)}}" alt="">
            @else
            <img class="c-avatar-img " src="{{asset('images/carfeis.png')}}" alt="">
            @endif
            </div>
            <div class="overlay overlayLeft"></div>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2"><strong>Compte</strong></div>
            <a class="dropdown-item" href="{{route('edit.profil',Auth::user()->slug)}}">

                <i class="c-icon mfe-2 cil-user"></i>Profile
            </a>
            <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="c-icon mfe-2 cil-account-logout"></i>Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
</ul>
<div class="">
    @yield('breadcrumb')
</div>
