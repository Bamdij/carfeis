<!-- Annee Scolaire Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    {!! Form::text('annee_scolaire_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('inscriptionEleves.index') }}" class="btn btn-secondary">Cancel</a>
</div>
