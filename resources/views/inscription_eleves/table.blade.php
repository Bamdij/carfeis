<div class="table-responsive-sm">
    <table class="table table-striped" id="inscriptionEleves-table">
        <thead>
            <tr>
                <th>Eleve Id</th>
        <th>Annee Scolaire Id</th>
        <th>Classe Id</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($inscriptionEleves as $inscriptionEleve)
            <tr>
                <td>{{ $inscriptionEleve->eleve_id }}</td>
            <td>{{ $inscriptionEleve->annee_scolaire_id }}</td>
            <td>{{ $inscriptionEleve->classe_id }}</td>
            <td>{{ $inscriptionEleve->description }}</td>
                <td>
                    {!! Form::open(['route' => ['inscriptionEleves.destroy', $inscriptionEleve->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('inscriptionEleves.show', [$inscriptionEleve->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('inscriptionEleves.edit', [$inscriptionEleve->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>