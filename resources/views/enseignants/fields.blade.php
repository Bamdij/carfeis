<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control" required>
            <option value="">choisir Année scolaire</option>
            <option value="{{$annee_scolaire->id}}">{{$annee_scolaire->name}}</option>
        <select> 
    </div>  
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'Prénom:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','required' => true]) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Nom:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','required' => true]) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Téléphone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Adresse Field -->
<div class="form-group col-sm-6">
    {!! Form::label('adresse', 'Adresse:') !!}
    {!! Form::text('adresse', null, ['class' => 'form-control']) !!}
</div>

<!-- Niveau Etude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('niveau_etude', 'Niveau Etude:') !!}
    {!! Form::text('niveau_etude', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Recrutement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_recrutement', 'Date de Recrutement:') !!}
    {!! Form::date('date_recrutement', null, ['class' => 'form-control','id'=>'date_recrutement','required' => true]) !!}
</div>

{{-- 
@if(empty($enseignant))
<div class="form-group col-sm-6">
    {!! Form::label('type_enseignant_id', 'Type Enseigement:') !!}
    <select name="type_enseignant_id" id="status" class="form-control"   onchange="changeFunctionEnseignant(this)" required>
        <option value="">--Choisir enseignement --</option>
        @foreach($type_enseig as $type)
        @if(!empty($enseignant->type_enseignant))
        @if($type->id === $enseignant->type_enseignant->id)
        <option value="{{$enseignant->type_enseignant->id}}" selected>{{$enseignant->type_enseignant->name}}</option>
        @endif
        @else
        <option value="{{$type->id}}" >{{$type->name}}</option>
        @endif
        @endforeach
    </select>
</div>
    <div class="form-group col-sm-6" id="hid_div"  >
        <div id="teacher_text"></div>
        <div id="teachniv_id"></div>
    </div>
    <div class="form-group col-sm-6" id="hide_div" >
        <div id="teacher_class"></div>
        <div id="teacher_class_id"></div>
    </div>
    @endif
</div>
--}}
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('enseignants.index') }}" class="btn btn-secondary">Annuler</a>
</div>
