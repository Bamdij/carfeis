@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('enseignants.index') !!}">Enseignant</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             @include('flash::message')
             <div class="row">
                    <div class="col-lg-6">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Enseignant</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($enseignant, ['route' => ['enseignants.update', $enseignant->id], 'method' => 'patch']) !!}

                              @include('enseignants.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="card">
                          <div class="card-header">
                          <strong>Classe Enseignant</strong>
                          <a class="pull-right" id="is_add_classe_modal" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="{{$enseignant->id}}"><i class="fa fa-plus-square fa-lg"></i></a>
                          </div>
                          <div class="card-body">
                              @include('enseignants.field_edit_enseignant')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div id="add_class"></div>

    </div>
  </div>
</div>