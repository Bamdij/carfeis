
@if(!$enseignant_class->isEmpty())
<div class="table-responsive-sm">
    <table class="table table-striped" id="eleveClasses-table">
        <thead>
            <tr>
        <th>Type Enseignant</th>
        <th>Classe </th>
        <th colspan="3">Action</th>

            </tr>
        </thead>
        <tbody>
        @foreach($enseignant_class as $eleveClasse)
            <tr>
                <td>{{ $eleveClasse->type }}</td>
            <td>{{ $eleveClasse->name }}</td>
                <td>
                    {!! Form::open(['route' => ['delete.classe', $eleveClasse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('eleveClasses.show', [$eleveClasse->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('eleveClasses.edit', [$eleveClasse->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> -->
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif