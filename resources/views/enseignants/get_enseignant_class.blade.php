@if(!$niveau_enseignant->isEmpty())
<div class="table-responsive-sm">
    <table class="table table-striped" id="infoParents-table">
        <thead>
            <tr>
                <th>Classe</th>
                <th></th>
                <th></th>
                <th colspan="3">Voir la liste de la classe</th>
            </tr>
        </thead>
        <tbody>
        @foreach($niveau_enseignant as $prof)
            <tr>
                <td>{{ $prof->name }}</td>
                <td></td>
                <td></td>
                <td>
                @if( $prof->name == "No Information")
                <div class='btn-group'>
                    <a href="#"   data-toggle="modal" data-target="#exampleModal" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                </div>
                @else
                <div class='btn-group'>
                    <a href="#"  data-toggle="modal" id="elev_class_teacher" data-id="{{$prof->sous_niveau_id}}" data-target=".bd-example-modal-lg" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                </div>
                @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-warning" role="alert">
    Aucune CLasse associée.
</div>
@endif

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="alert alert-warning" role="alert">
           Information Non disponible
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
