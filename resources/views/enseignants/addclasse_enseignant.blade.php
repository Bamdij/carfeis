@if($niveau_eleve->type_enseignant_id  == "2")
<strong><label for="recipient-name" class="col-form-label">Classe:</label></strong>
    <select name="sous_niveau_id"  class="form-control" >
    <option value="">choisir la classe</option>
    @foreach($class_eleve as $niv)
    <option value="{{$niv->id}}">{{$niv->name}}</option>
    @endforeach
<select>
<input type="hidden" id="niveau" value="{{$niveau_eleve->id}}">
@else
<strong><label for="recipient-name" class="col-form-label">Classe:</label></strong>
    <select name="sous_niveau_id"  class="form-control" onchange="changeFunctionChoiceClass(this)" >
    <option value="">choisir la classe</option>
    @foreach($class_eleve as $niv)
    <option value="{{$niv->id}}">{{$niv->name}}</option>
    @endforeach
<select>
<input type="hidden" id="niveau" value="{{$niveau_eleve->id}}">
@endif