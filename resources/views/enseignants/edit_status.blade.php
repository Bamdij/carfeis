<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ajout Classe pour {{$enseignant->first_name}}  {{$enseignant->last_name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
</div>
  <div class="modal-body">
    <form  method="POST" >
        @csrf
        <div class="row">
          <div class="form-group col-sm-6">
                <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
                <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control"  required>
                    <option value="">choisir Année Scolaire</option>
                    <option value="{{$annee_scolaire->id}}">{{$annee_scolaire->name}}</option>
                <select> 
            </div>
            <div class="form-group col-sm-6">
                <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
                <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="changeFunctionAjouterClasse(this)" required>
                    <option value="">choisir le type enseignement</option>
                    @foreach($type_enseig as $prof)
                    <option value="{{$prof->id}}">{{$prof->name}}</option>
                    @endforeach
                <select> 
            </div> 
            <input type="hidden" id="enseignant_id" value="{{$enseignant->id}}">
 
            <div class="form-group col-sm-6" >
            <div id="classe_text"></div>
            <div class="niveau_id" ></div>
            </div>
            <div class="form-group col-sm-6" id="close_div">
                <div id="professeur_class" >
                <div id="type_classe_prof" ></div>
                </div>
                <div id="teacher_class_normal" ></div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
            <div id="open_div" style="display:none">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              <button  type="submit" id="update_enseignant" class="btn btn-primary">Ajouter</button>
              </div>
              <div id="clo_div" style="display:none">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              <button  type="submit" id="enseignant_only" class="btn btn-primary">Ajouter</button>
              </div>
    </form>
  </div>