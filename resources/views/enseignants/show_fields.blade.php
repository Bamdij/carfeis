<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
    <p>{{ $enseignant->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group  col-sm-6">
    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
    <p>{{ $enseignant->last_name }}</p>
</div>

<!-- Phone Field -->
<div class="form-group  col-sm-6">
    <strong>{!! Form::label('phone', 'Téléphone:') !!}</strong>
    <p>{{ $enseignant->phone }}</p>
</div>

<!-- Adresse Field -->
<div class="form-group  col-sm-6">
    <strong>{!! Form::label('adresse', 'Adresse:') !!}</strong>
    <p>{{ $enseignant->adresse }}</p>
</div>

<!-- Niveau Etude Field -->
<div class="form-group  col-sm-6">
<strong>{!! Form::label('niveau_etude', 'Niveau Etude:') !!}</strong>
    <p>{{ $enseignant->niveau_etude }}</p>
</div>

<!-- Date Recrutement Field -->
<div class="form-group  col-sm-6">
<strong>{!! Form::label('date_recrutement', 'Date Recrutement:') !!}</strong>
    <p>{{Carbon\Carbon::parse($enseignant->date_recrutement)->format('d/m/Y')}}</p>
</div>
@if(!empty($enseignant->type_enseignant_id))
<!-- Type Enseignant Id Field -->
<div class="form-group  col-sm-6">
<strong>{!! Form::label('type_enseignant_id', 'Type Enseignant:') !!}</strong>
    <p>{{$enseignant->type_enseignant->name }}</p>
</div>
@endif
</div>
    <!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-2 offset-md-10 niveau_added mt-3 mb-4">
        <button class="add_form_field form-control" id="is_add_classe_modal_show" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="{{$enseignant->id}}" style="background-color:#3e0f05 ;">
            <span style="font-size:16px; font-weight:bold;">+</span>
        </button>
    </div><!--/.col-md-6 -->
    <div class="col-md-12">
        <h3 class="form-section-title">Classe(s) Occupée(s)</h3>
    </div>
</div><!--/.row-->

@if(!$enseignant_year->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Annee scolaire</th>
                <th>Type Enseignant</th>
                <th>Classe</th>
                <th>Date d'inscription</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($enseignant_year  as $elev_class)
            <tr>
                <td>{{ $elev_class->annee->name }}</td>
                <td>{{ $elev_class->type->name}}</td>
                <td>{{ $elev_class->name_sous}}</td>
                <td>{{ \Carbon\Carbon::parse($elev_class->created_at)}}</td>
                <td>
                    {!! Form::open(['route' => ['enseignantAnnees.destroy',  $elev_class->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Aucune classe frequentée
</div>
@endif
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div id="add_class_show"></div>

    </div>
  </div>
</div>