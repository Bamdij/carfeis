<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Liste des classes Par Enseignants</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire :</label></strong>
        <select name="annnee_scolaire_id" id="annnee_scolaire_id" class="form-control"  onchange="getAnneeScolaireEnseignant(this)" required>
            <option value="">choisir Année Scolaire</option>
            @foreach($annee_niveau as $prof)
            <option value="{{$prof->id}}">{{$prof->name}}</option>
            @endforeach
        <select> 
    </div>   -->
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
        <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="getTypeEnseignant(this)" required>
            <option value="">choisir le type enseignant</option>
            @foreach($type_enseig as $prof)
            <option value="{{$prof->id}}">{{$prof->name}}</option>
            @endforeach
        <select> 
    </div>  
    <div class="form-group col-sm-6">
        <div id="enseignant_edit_text_class"></div>
    </div>
</div>
<br><br>
<div id="closed_table" style="display:none">
<div id="enseignant_text_class_getting"></div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
      <div id="liste_eleve_par_classe_par_prof"></div>
    </div>
  </div>
</div>