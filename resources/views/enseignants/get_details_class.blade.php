@if(!empty($sous_niv))
@if(!$eleve_class_edit->isEmpty())
<br><br>
<div class="container-fluid">
        <div class="animated fadeIn">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="">
                       <div class="mt-1">
                            <div class="col-md-12">
                                <h3 class="form-section-title">Liste des Eleves de la classe {{$sous_niv->name}}</h3>
                            </div>
                        </div><!--/.row-->
                         </div>
                         <div class="card-body">
                         <div class="table-responsive-sm table-bordered">
                                <table class="table table-striped" id="eleveClasses-table">
                                    <thead>
                                        <tr>
                                        <th>Prenom</th>
                                    <th>Nom</th>
                                    <th>Date de Naissance</th>
                                    <th>Lieu de Naissance</th>
                                    <th>Photo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($eleve_class_edit as $eleveClasse)
                                        <tr>
                                            @if($eleveClasse->name=="Information non disponible")
                                            <td> No Information</td>
                                            <td> No Information</td>
                                            <td> No Information</td>
                                            <td> No Information</td>
                                            <td> No Information</td>
                                            @else
                                            <td>{{ $eleveClasse->name->first_name }}</td>
                                            <td>{{ $eleveClasse->name->last_name }}</td>
                                            <td>{{ $eleveClasse->name->date_naissance }}</td>
                                            <td>{{ $eleveClasse->name->lieu_naissance }}</td>
                                            <td><img src="{{ $eleveClasse->name->avatar_eleve }}" alt="{{ $eleveClasse->name->first_name }}" width="50" heigth="50"></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    <div class="col-md-12">
        <p class=""> <strong>Professeurs</strong> = {{$enseigant_class->first_name}} {{$enseigant_class->last_name}}</p>
    </div>
    <div class="col-md-12">
        <p class=""><strong>Effectifs</strong> = {{$eleve_class_edit->count()}} (éléves)</p>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
    </div>
@else
<div class="alert alert-warning" role="alert">
 Aucun eleve dans cette Classe
</div>
@endif
@else
<div class="alert alert-warning" role="alert">
 Information non disponible
</div>
@endif



