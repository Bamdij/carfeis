<!-- Name Field -->
<div class="row">
@if(!empty($anneeScolaire))
<div class="form-group col-sm-6">
    <label for="name">Année Scolaire:</label>
    <input class="form-control" name="name" type="text" id="name" value="{{$anneeScolaire->name}}" placeholder="année scolaire ex:2020-2021">
</div>
<div class="form-group col-sm-6" >
    <label for="is_actif">Status:</label>
    <select name="status" id="is_actif" class="form-control">
         @if($anneeScolaire->status ==true)
        <option value="{{ intval(1) }}" selected>Activé</option>
        <option value="{{ intval(0) }}">Non activé</option>
        @else 
         <option value="{{ intval(1) }}">Activé</option>
        <option value="{{ intval(0) }}" selected>Non activé</option> 
         @endif
    </select>
</div>
@else
<div class="form-group col-sm-6">
    <label for="name">Année Scolaire:</label>
    <input class="form-control" name="name" type="text" id="name" placeholder="année scolaire ex:2020-2021">
</div>
<div class="form-group col-sm-6" >
    <label for="is_actif">Status:</label>
    <select name="status" id="status" class="form-control">
        <option value="{{ intval(1) }}">Activé</option>
        <option value="{{ intval(0) }}">Non activé</option>
    </select>
</div>
@endif
</div>
<!-- Slug Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('anneeScolaires.index') }}" class="btn btn-secondary">Annuler</a>
</div>
