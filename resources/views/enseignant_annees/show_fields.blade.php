<!-- Enseignant Id Field -->
<div class="form-group">
    {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
    <p>{{ $enseignantAnnee->enseignant_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $enseignantAnnee->annee_scolaire_id }}</p>
</div>

<!-- Type Enseignant Id Field -->
<div class="form-group">
    {!! Form::label('type_enseignant_id', 'Type Enseignant Id:') !!}
    <p>{{ $enseignantAnnee->type_enseignant_id }}</p>
</div>

<!-- Niveau Id Field -->
<div class="form-group">
    {!! Form::label('niveau_id', 'Niveau Id:') !!}
    <p>{{ $enseignantAnnee->niveau_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $enseignantAnnee->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $enseignantAnnee->updated_at }}</p>
</div>

