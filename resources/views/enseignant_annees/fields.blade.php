<!-- Enseignant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
    {!! Form::text('enseignant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    {!! Form::text('annee_scolaire_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Enseignant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_enseignant_id', 'Type Enseignant Id:') !!}
    {!! Form::text('type_enseignant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Niveau Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('niveau_id', 'Niveau Id:') !!}
    {!! Form::text('niveau_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('enseignantAnnees.index') }}" class="btn btn-secondary">Cancel</a>
</div>
