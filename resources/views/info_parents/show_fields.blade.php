<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $infoParent->eleve_id }}</p>
</div>

<!-- Name Pere Field -->
<div class="form-group">
    {!! Form::label('name_pere', 'Name Pere:') !!}
    <p>{{ $infoParent->name_pere }}</p>
</div>

<!-- Phone Pere Field -->
<div class="form-group">
    {!! Form::label('phone_pere', 'Phone Pere:') !!}
    <p>{{ $infoParent->phone_pere }}</p>
</div>

<!-- Email Pere Field -->
<div class="form-group">
    {!! Form::label('email_pere', 'Email Pere:') !!}
    <p>{{ $infoParent->email_pere }}</p>
</div>

<!-- Type Profession Id Field -->
<div class="form-group">
    {!! Form::label('type_profession_id', 'Type Profession Id:') !!}
    <p>{{ $infoParent->type_profession_id }}</p>
</div>

<!-- Name Mere Field -->
<div class="form-group">
    {!! Form::label('name_mere', 'Name Mere:') !!}
    <p>{{ $infoParent->name_mere }}</p>
</div>

<!-- Phone Mere Field -->
<div class="form-group">
    {!! Form::label('phone_mere', 'Phone Mere:') !!}
    <p>{{ $infoParent->phone_mere }}</p>
</div>

<!-- Email Mere Field -->
<div class="form-group">
    {!! Form::label('email_mere', 'Email Mere:') !!}
    <p>{{ $infoParent->email_mere }}</p>
</div>

<!-- Type Profession Mere Id Field -->
<div class="form-group">
    {!! Form::label('type_profession_mere_id', 'Type Profession Mere Id:') !!}
    <p>{{ $infoParent->type_profession_mere_id }}</p>
</div>

<!-- Name Tuteur Field -->
<div class="form-group">
    {!! Form::label('name_tuteur', 'Name Tuteur:') !!}
    <p>{{ $infoParent->name_tuteur }}</p>
</div>

<!-- Lien Parent Id Field -->
<div class="form-group">
    {!! Form::label('lien_parent_id', 'Lien Parent Id:') !!}
    <p>{{ $infoParent->lien_parent_id }}</p>
</div>

<!-- Phone Tuteur Field -->
<div class="form-group">
    {!! Form::label('phone_tuteur', 'Phone Tuteur:') !!}
    <p>{{ $infoParent->phone_tuteur }}</p>
</div>

<!-- Email Tuteur Field -->
<div class="form-group">
    {!! Form::label('email_tuteur', 'Email Tuteur:') !!}
    <p>{{ $infoParent->email_tuteur }}</p>
</div>

<!-- Contact Urgence Field -->
<div class="form-group">
    {!! Form::label('contact_urgence', 'Contact Urgence:') !!}
    <p>{{ $infoParent->contact_urgence }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $infoParent->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $infoParent->updated_at }}</p>
</div>

