<div class="table-responsive-sm">
    <table class="table table-striped" id="infoParents-table">
        <thead>
            <tr>
                <th>Eleve Id</th>
        <th>Name Pere</th>
        <th>Phone Pere</th>
        <th>Email Pere</th>
        <th>Type Profession Id</th>
        <th>Name Mere</th>
        <th>Phone Mere</th>
        <th>Email Mere</th>
        <th>Type Profession Mere Id</th>
        <th>Name Tuteur</th>
        <th>Lien Parent Id</th>
        <th>Phone Tuteur</th>
        <th>Email Tuteur</th>
        <th>Contact Urgence</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($infoParents as $infoParent)
            <tr>
                <td>{{ $infoParent->eleve_id }}</td>
            <td>{{ $infoParent->name_pere }}</td>
            <td>{{ $infoParent->phone_pere }}</td>
            <td>{{ $infoParent->email_pere }}</td>
            <td>{{ $infoParent->type_profession_id }}</td>
            <td>{{ $infoParent->name_mere }}</td>
            <td>{{ $infoParent->phone_mere }}</td>
            <td>{{ $infoParent->email_mere }}</td>
            <td>{{ $infoParent->type_profession_mere_id }}</td>
            <td>{{ $infoParent->name_tuteur }}</td>
            <td>{{ $infoParent->lien_parent_id }}</td>
            <td>{{ $infoParent->phone_tuteur }}</td>
            <td>{{ $infoParent->email_tuteur }}</td>
            <td>{{ $infoParent->contact_urgence }}</td>
                <td>
                    {!! Form::open(['route' => ['infoParents.destroy', $infoParent->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('infoParents.show', [$infoParent->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('infoParents.edit', [$infoParent->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>