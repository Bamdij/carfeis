<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DU PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_pere', 'Prenom & Nom du  Pere:') !!}
    {!! Form::text('name_pere', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Phone Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_pere', 'telephone du  Pere:') !!}
    {!! Form::text('phone_pere', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_pere', 'Email Pere:') !!}
    {!! Form::text('email_pere', null, ['class' => 'form-control']) !!}
</div>
<!-- Sexe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_id', 'Profession:') !!}
    <select name="type_profession_id" id="type_profession_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $prof)
    <option value="{{$prof->id}}">{{$prof->name}}</option>
    @endforeach
</select>
</div>
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DE LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<!-- Name Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_mere', 'Prenom & Nom de la Mere:') !!}
    {!! Form::text('name_mere', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Phone Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_mere', 'Telephone de la Mere:') !!}
    {!! Form::text('phone_mere', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_mere', 'Email Mere:') !!}
    {!! Form::text('email_mere', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_mere_id', 'Profession:') !!}
    <select name="type_profession_mere_id" id="type_profession_mere_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $prof)
    <option value="{{$prof->id}}">{{$prof->name}}</option>
    @endforeach
</select>
</div>
</div>
<div class="row">
    <div class="form-group col-sm-12"  >
        <strong> {!! Form::label('is_parent_tuteur', 'Le Tuteur est-il :') !!}</strong>
        <div class="form-check form-check-inline">
            <strong><label class="form-check-label" for="inlineRadio1">Le Pére ?</label></strong>&nbsp;&nbsp;
            <input class="form-check-input" type="radio" name="is_parent_tuteur" id="inlineRadio1"value="{{ intval(1) }}">
        </div>
        <div class="form-check form-check-inline">
            <strong><label class="form-check-label" for="inlineRadio1">La Mére ?</label></strong>&nbsp;&nbsp;
                    <input class="form-check-input" type="radio" name="is_parent_tuteur" id="inlineRadio1"value="{{ intval(0) }}">
        </div>  
        <div class="form-check form-check-inline">
            <strong><label class="form-check-label" for="inlineRadio1">ou ajouter un Tuteur?</label></strong>&nbsp;&nbsp;
            <input class="form-check-input" type="radio" name="is_parent_tuteur" id="inlineRadio1"value="{{ intval(2) }}">
        </div>  
        </div> 
</div>
<div style="display:none;" id="open_tuteur">
    <div class="mt-1">
        <div class="col-md-12">
            <h3 class="form-section-title">INFORMATIONS DU TUTEUR</h3>
        </div>
    </div><!--/.row-->
<!-- Name Tuteur Field -->
    <div class="row">
        <div class="form-group col-sm-6">
            <label for="name_tuteur">Prenom  Nom du Tuteur:</label>
            <input class="form-control" name="name_tuteur" type="text" id="name_tuteur1">
        </div>

        <!-- Phone Tuteur Field -->
        <div class="form-group col-sm-6">
            <label for="phone_tuteur">Telephone Tuteur:</label>
            <input class="form-control" name="phone_tuteur" type="text" id="phone_tuteur1">
        </div>

        <!-- Email Tuteur Field -->
        <div class="form-group col-sm-6">
            <label for="email_tuteur">Email Tuteur:</label>
            <input class="form-control" name="email_tuteur" type="text" id="email_tuteur1">
        </div>

        <!-- Contact Urgence Field -->
        <div class="form-group col-sm-6">
            <label for="contact_urgence">Contact Urgence:</label>
            <input class="form-control" name="contact_urgence" type="text" id="contact_urgence1">
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('lien_parent', 'Lien Parent:') !!}
            <select name="lien_parent_id" id="lien_parent" class="form-control" >
                <option value="">choisir le Lien</option>
                @foreach($lien_parent as $prof)
                <option value="{{$prof->id}}">{{$prof->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-6" style="display:none">
            <label for="name_pere">eleve</label>
            <input class="form-control" value="{{$eleve_id->id}}" name="eleve_id" type="text" id="name_pere">
        </div>
    </div>
</div>
<div class="container">
<div class="row" >
    <div class="col-md-6 col-sm-12 offset-8" style="display: flex;">
        <input type="submit" value="Terminer" class="btn btn-primary btn-kal-grey mr-3" name="terminer">
        <input type="submit" value="Ajouter et Continuer"class="btn btn-primary btn-kal-blue" name="continuer">
    </div>   
</div>
</div>