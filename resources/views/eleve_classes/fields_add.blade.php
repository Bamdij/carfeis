<div class="row">
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
        <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="changeFunctionforNiveau(this)" required>
            <option value="">choisir le type enseignement</option>
            @foreach($type_enseig as $prof)
            <option value="{{$prof->id}}">{{$prof->name}}</option>
            @endforeach
        <select> 
    </div>  
    <div class="form-group col-sm-6">
    <div id="eleve_text_class"></div>
    </div>
    <div class="form-group col-sm-6" id="hidd_div">
        <div id="eleve_class_enseig"></div>
        <div id="student_class_ok"></div>
    </div>
</div>
<br><br>
<div id="close_table">
<div id="eleve_class_all"></div>
</div>

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document" >
        <div class="modal-content" >
            <div id="carte_text"></div>
        
        </div>
    </div>
</div>
