<!-- Niveau Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('niveau_id', 'Niveau Id:') !!}
    {!! Form::text('niveau_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Classe Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('classe_id', 'Classe Id:') !!}
    {!! Form::text('classe_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Eleve Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    {!! Form::text('eleve_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eleveClasses.index') }}" class="btn btn-secondary">Cancel</a>
</div>
