@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('eleveClasses.index') !!}">Eleve Classe</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Eleve Classe</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($eleve_class_edit, ['route' => ['eleveClasses.update', $eleve_class_edit->id], 'method' => 'patch']) !!}

                              @include('eleve_classes.fields_edit_elev')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection