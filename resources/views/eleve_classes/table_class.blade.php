@if(!$eleve_class->isEmpty())
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Liste des Eleves de la classe {{$class_eleve->name}}</h3>
    </div>
</div><!--/.row-->
<div class="table-responsive-sm">
    <table class="table table-striped" id="eleveClasses-table">
        <thead>
            <tr>
            <th>Prenom</th>
        <th>Nom</th>
        <th>Date de Naissance</th>
        <th>Lieu de Naissance</th>
        <th>Photo</th>
        <th >Carte</th> 

             <th colspan="3">Action</th> 
            </tr>
        </thead>
        <tbody>
        @foreach($eleve_class as $eleveClasse)
            @if(!empty($eleveClasse))
            <tr>
                <td>{{ $eleveClasse->name->first_name }}</td>
                <td>{{ $eleveClasse->name->last_name }}</td>
                <td>{{ $eleveClasse->name->date_naissance }}</td>
                <td>{{ $eleveClasse->name->lieu_naissance }}</td>
                <td><img src="{{ $eleveClasse->name->avatar_eleve }}" alt="{{ $eleveClasse->name->first_name }}" width="50" heigth="50"></td>
                <td><a href="{{route('carte.eleve', $eleveClasse->name->slug)}}"  target="blank"  class='btn btn-ghost-success'><i class="fa fa-print" aria-hidden="true"></i></i></a></td>
                <td>
                    {!! Form::open(['route' => ['eleveClasses.destroy', $eleveClasse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('details_eleve.classe', [$eleveClasse->name->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a  href="{{ route('edit_class.eleve', [$eleveClasse->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @else
            <td>{{ $eleveClasse->name->first_name }}</td> 
            <td>{{ $eleveClasse->name->first_name }}</td>
            <td>{{ $eleveClasse->name->first_name }}</td>
            <td>{{ $eleveClasse->name->first_name }}</td>
            <td>{{ $eleveClasse->name->first_name }}</td>
            <td>
                    {!! Form::open(['route' => ['eleveClasses.destroy', $eleveClasse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            @endif
        @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-warning" role="alert">
 Aucun eleve dans cette Classe
</div>
@endif
