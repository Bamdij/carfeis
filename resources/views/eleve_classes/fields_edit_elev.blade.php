<div>
    <form  method="POST" action="">
        @csrf
        <div class="row">
            <div class="form-group col-sm-6">
                <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
                <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="changeFunctionEditEleveClasse(this)" required>
                    <option value="">choisir le type enseignement</option>
                    @foreach($type_enseig as $prof)
                    <option value="{{$prof->id}}">{{$prof->name}}</option>
                    @endforeach
                <select> 
            </div>  
            <input type="hidden" id="student_clae_id" value="{{$eleve_class_edit->id}}">
            <div class="form-group col-sm-6">
            <div id="elev_edit_classtext"></div>
            <div class="niv_id" ></div>
            </div>
            <div class="form-group col-sm-6" id="hidd_div">
            <div id="eleve_class_editor"></div>
            <div id="student_class_normal" ></div>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div id="open_save">
                <a href="{{ route('get_eleve_by_class.classe') }}" class="btn btn-secondary">Retour</a>
                <button  type="submit" id="edit_class_student" class="btn btn-primary">Modifier</button>
            </div>
            <!-- <div id="close_save" style="display:none">
                <a href="{{ route('get_eleve_by_class.classe') }}" class="btn btn-secondary">Retour</a>
                <button  type="submit" id="student_to_arabe" class="btn btn-primary">Modifier</button>
            </div> -->
        </div>
    </form>
</div>