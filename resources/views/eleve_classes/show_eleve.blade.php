<!-- First Name Field -->
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS ELEVE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('classe', 'Classe:') !!}</strong>
        <div  class="blink_me"><span class="badge badge-pill badge-warning">{{ $s_class->name }}</span></div>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $eleve->first_name }}</p>
    </div>
    <!-- Last Name Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $eleve->last_name }}</p>
    </div>

    <!-- Date Naissance Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
        <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
    </div>

    <!-- Sexe Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
        <p>{{ $eleve->sexe }}</p>
    </div>

    <!-- Groupe Sanguin Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}</strong> 
        <p>{{ $eleve->groupe_sanguin }}</p>
    </div>

    <!-- Is Malade Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_malade', 'L\'enfant souffre t-il d\'une maladie?:') !!}</strong>
        <p>{{ $eleve->is_malade }}</p>
    </div>

    <!-- Quelle Maladie Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('quelle_maladie', 'Quelle Maladie:') !!}</strong>
        <p>{{ $eleve->quelle_maladie }}</p>
    </div>

    <!-- Is Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_traitement','L\'enfant suit t-il d\'un traitement?:') !!}</strong>
        <p>{{ $eleve->is_traitement }}</p>
    </div>

    <!-- Quel Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('quel_traitement', 'Quel Traitement:') !!}</strong>
        <p>{{ $eleve->quel_traitement }}</p>
    </div>
</div>
@if(!empty($infoParent))
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS Du PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">

    <!-- Name Pere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('name_pere', 'Prénom et Nom Pere:') !!}</strong> 
        <p>{{ $infoParent->name_pere }}</p>
    </div>

    <!-- Phone Pere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_pere', 'Telephone Pere:') !!}</strong>
        <p>{{ $infoParent->phone_pere }}</p>
    </div>

    <!-- Email Pere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('email_pere', 'Email Pere:') !!}</strong> 
        <p>{{ $infoParent->email_pere }}</p>
    </div>

    <!-- Type Profession Id Field -->
    <!-- <div class="form-group col-sm-4">
        <strong>{!! Form::label('type_profession_id', 'Type Profession Id:') !!}</strong>
        <p>{{ $infoParent->type_profession_id }}</p>
    </div> -->
</div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DE LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- Name Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('name_mere', 'Prenom et nom Mere:') !!}</strong>
        <p>{{ $infoParent->name_mere }}</p>
    </div>

    <!-- Phone Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_mere', 'Telephone Mere:') !!}</strong>
        <p>{{ $infoParent->phone_mere }}</p>
    </div>

    <!-- Email Mere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('email_mere', 'Email Mere:') !!}</strong> 
        <p>{{ $infoParent->email_mere }}</p>
    </div>

    <!-- Type Profession Mere Id Field -->
    <!-- <div class="form-group col-sm-4">
        <strong>{!! Form::label('type_profession_mere_id', 'Type Profession Mere Id:') !!}</strong>
        <p>{{ $infoParent->type_profession_mere_id }}</p>
    </div> -->
</div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS Du TUTEUR</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- Name Tuteur Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('name_tuteur', 'Prénom et Nom Tuteur:') !!}</strong>
        <p>{{ $infoParent->name_tuteur }}</p>
    </div>

    <!-- Lien Parent Id Field -->
    <!-- <div class="form-group col-sm-4">
        <strong>{!! Form::label('lien_parent_id', 'Lien Parent :') !!}</strong>
        <p>{{ $infoParent->lien_parent_id }}</p>
    </div> -->

    <!-- Phone Tuteur Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('phone_tuteur', 'Telephone Tuteur:') !!}</strong> 
        <p>{{ $infoParent->phone_tuteur }}</p>
    </div>

    <!-- Email Tuteur Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('email_tuteur', 'Email Tuteur:') !!}</strong>
        <p>{{ $infoParent->email_tuteur }}</p>
    </div>

    <!-- Contact Urgence Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('contact_urgence', 'Contact Urgence:') !!}</strong> 
        <p>{{ $infoParent->contact_urgence }}</p>
    </div>
</div>
@else
<div class="alert alert-warning" role="alert">
  Informations sur les parents non disponible!
</div>
@endif
