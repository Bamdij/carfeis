<!-- Niveau Id Field -->
<div class="form-group">
    {!! Form::label('niveau_id', 'Niveau Id:') !!}
    <p>{{ $eleveClasse->niveau_id }}</p>
</div>

<!-- Classe Id Field -->
<div class="form-group">
    {!! Form::label('classe_id', 'Classe Id:') !!}
    <p>{{ $eleveClasse->classe_id }}</p>
</div>

<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $eleveClasse->eleve_id }}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{{ $eleveClasse->observation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $eleveClasse->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $eleveClasse->updated_at }}</p>
</div>

