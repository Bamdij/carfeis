@extends('layouts.app')

@section('content')
	<ol class="breadcrumb">
            <li class="breadcrumb-item">
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                             </div>
                             <div class="card-body">
							 <div class="w3-container w3-mobile">
								<div class="row">
									@if($payment_status == 'completed')
										<div class="display-middle w3-card-4" style="width: 100%;">
											<header class="w3-container w3-dark-red confirmation_header">
											<h1 class="w3-center confirmation_title">Confirmation</h1>
											</header>
											<div class="w3-container" id="confirmation_content">
												<div class="w3-panel w3-light yellow confirmation_message">
												<h3 class="thank">Merci {{Auth::user()->name}} </h3>
												<p>Votre transaction du montant de {{ $donate_amount }} FCFA a été envoyé avec succés.</p>
												<p class="w3-right">
													<a href="{{ $invoice_pdf }}" class="w3-btn w3-blue" target="_blank">Télécharger votre facture</a>
												</p>
												<p class="w3-left">
													<a href="{{ route('infor_paiement_parent.mensual',$eleve->slug) }}" class="w3-btn w3-red">Retourner </a>
												</p>
												</div>
											</div>
										</div>
									@elseif($payment_status == 'cancelled')
										<div class="w3-display-middle w3-card-4" style="width: 100%;" id="confirmation_container">
											<header class="w3-container w3-blue confirmation_header">
											<h1 class="w3-center confirmation_title">Oupss...!</h1>
											</header>
											<div class="w3-container" id="confirmation_content">
												<div class="w3-panel w3-light confirmation_message">
												<h3 class="thank">Hala lala!</h3>
												<p>{{ $message }}</p>
												<p class="w3-left">
													<a href="{{ route('infor_paiement_parent.mensual',$eleve->slug) }}" class="w3-btn w3-red">Retourner </a>
												</p>
												</div>
											</div>
										</div>
									@endif
								</div>
							</div>
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@stop