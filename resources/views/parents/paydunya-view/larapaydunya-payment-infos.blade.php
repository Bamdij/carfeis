@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     	<div class="container-fluid">
          	<div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                <div class="row">
                     <div class="col-lg-12">
                        <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                            </div>
                            <div class="card-body">
								<div class="mt-1">
									<div class="col-md-12">
										<h3 class="form-section-title">RESUME DE LA TRANSACTION</h3>
									</div>
								</div><!--/.row-->
								<div class="">
									<div class="row">
										<div class="col-md-6">
											<div class=" ma_money">
											<img src="{{asset('images/bouton-02.png')}}"width="100%"  height="auto" alt="paydunya">	
											</div>	
										</div>
										<div class="col-md-6 detail_paiement">
										
										<h1 class="text-center mt-4 theme-add-page-title">Resume de la transaction </h1>
										<div class=" ma_money">
											<b>{{ number_format($amount, '0', ',', ' ') }} FCFA</b>
											<br><br>
											<div class="row">
												<div class="col-md-12 ma_confirmation"> 
													<a href="{{ $transaction_url }}" class="payment-btn">
														<button class="btn btn-primary mb-4  ma_button">
															<span class="add-and-continue-btn-label" >Payer Maintenant avec Orange Money Wave FreeMoney...
														</button>
													</a>
												</div>
											</div>
										</div>	
									</div>
								</div>
								</div>
							</div>
                     	</div>
                 	</div>
         		 </div>
    		</div>
		</div>
@stop