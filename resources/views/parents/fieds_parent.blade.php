@if(!empty($parents))
<div class="row">
<!-- Name Field -->
<!-- Name Field -->
<div class="form-group col-sm-6">
    <label for="name">Prenom et Nom:</label>
    <input class="form-control" required name="name" type="text" id="name" value="{{$parents->name}}">
</div>

<div class="form-group col-sm-6">
    <label for="email">Email:</label>
    <input class="form-control" required name="email"  type="email" id="email" value="{{$parents->email}}">
</div>
<div class="form-group col-sm-6">
    <label for="pseudo">Pseudo:</label>
    <input class="form-control" required name="pseudo" type="text"  id="pseudo" value="{{$parents->pseudo}}">
</div>
<div class="form-group col-sm-6" style='display:none;'>
    <label for="pseudo">Pseudo:</label>
    <input class="form-control"  name="status" type="text" value="Parent">
</div>
<!-- Is Actif Field -->
<div class="form-group col-sm-6">
    <label for="spoken_languages">Choisir Eleve :</label>
    <select name="eleve_id[]" id="spoken_languages"  class="form-control"  multiple="multiple" >
        @foreach($eleves as $eleve)
        @foreach($eleves_parent as $item)
        @if( $eleve->id == $item->eleve_id )
        <option value="{{$eleve->id}}" selected>{{$eleve->first_name}} - {{$eleve->last_name}} - {{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}} </option>
        @endif
        @endforeach
        <option value="{{$eleve->id}}">{{$eleve->first_name}} - {{$eleve->last_name}} - {{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}} </option>
       @endforeach
    </select>
</div>
@else
<div class="row">
<!-- Name Field -->
<!-- Name Field -->
<div class="form-group col-sm-6">
    <label for="name">Prenom et Nom:</label>
    <input class="form-control" required name="name" type="text" id="name"   value="{{ old('name') }}" required>
</div>

<div class="form-group col-sm-6">
    <label for="email">Email:</label>
    <input class="form-control" required name="email"  type="email" id="email"  value="{{ old('email') }}" required>
</div>
<div class="form-group col-sm-6">
    <label for="pseudo">Pseudo:</label>
    <input class="form-control" required name="pseudo" type="text"  id="pseudo"  value="{{ old('pseudo') }}" required>
</div>
<div class="form-group col-sm-6" style='display:none;'>
    <label for="pseudo">Pseudo:</label>
    <input class="form-control"  name="status" type="text" value="Parent">
</div>
<!-- Is Actif Field -->
<div class="form-group col-sm-6">
    <label for="spoken_languages">Choisir Eleve:</label>
    <select name="eleve_id[]" id="spoken_languages"  class="form-control"  multiple="multiple"  required>
        @foreach($eleves as $eleve)
        <option value="{{$eleve->id}}">{{$eleve->first_name}} - {{$eleve->last_name}} - {{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}} </option>
       @endforeach
    </select>
</div>
@endif
<div class="clearfix"></div>
<!-- Submit Field -->
<div class="form-group col-sm-12"  >
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('parents.index') }}" class="btn btn-secondary">Annuler</a>
</div>







