<!-- First Name Field -->
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS ELEVE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $eleve->first_name }}</p>
    </div>

    <!-- Last Name Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $eleve->last_name }}</p>
    </div>
    <div class="form-group col-sm-4">
        <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
        <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
    </div>
    <!-- Date Naissance Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
        <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
    </div>

    <!-- Sexe Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
        <p>{{ $eleve->sexe }}</p>
    </div>

    <!-- Groupe Sanguin Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}</strong> 
        <p>{{ $eleve->groupe_sanguin }}</p>
    </div>
    @if($eleve->is_malade == true)
    <!-- Is Malade Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_malade', 'L\'enfant souffre t-il d\'une maladie?:') !!}</strong>
        <span class="badge badge-danger"><p>OUI</p></span>
    </div>

    <!-- Quelle Maladie Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('quelle_maladie', 'Quelle Maladie:') !!}</strong>
        <p>{{ $eleve->quelle_maladie }}</p>
    </div>
    @if($eleve->is_traitement == true)
    <!-- Is Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_traitement','L\'enfant suit t-il d\'un traitement?:') !!}</strong>
        <span class="badge badge-warning"><p>OUI</p></span>
    </div>
    @endif
    <!-- Quel Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('quel_traitement', 'Quel Traitement:') !!}</strong>
        <p>{{ $eleve->quel_traitement }}</p>
    </div>
    @endif
</div>
@if(!empty($infoParent))
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DU PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">

    <!-- Name Pere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('name_pere', 'Prénom et Nom Pere:') !!}</strong> 
        <p>{{ $infoParent->name_pere }}</p>
    </div>

    <!-- Phone Pere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_pere', 'Telephone Pere:') !!}</strong>
        <p>{{ $infoParent->phone_pere }}</p>
    </div>
</div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DE LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- Name Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('name_mere', 'Prenom et nom Mere:') !!}</strong>
        <p>{{ $infoParent->name_mere }}</p>
    </div>

    <!-- Phone Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_mere', 'Telephone Mere:') !!}</strong>
        <p>{{ $infoParent->phone_mere }}</p>
    </div>
</div>
@else
<div class="alert alert-warning" role="alert">
  Informations sur les parents non disponible!
</div>
@endif
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Classe Frequentées</h3>
    </div>
</div><!--/.row-->
@if(!$eleve_class_by_year->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Annee scolaire</th>
                <th>Classe</th>
                <th>Professeur</th>
                <th>Regime</th>
                <th>Date d'inscription</th>
                <!-- <th>Situation Paiement</th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($eleve_class_by_year  as $elev_class)
            <tr>
                <td>{{ $elev_class->annee_scolaire }}</td>
                <td>{{ $elev_class->classe}}</td>
            <td>{{ $elev_class->enseignant }}</td>
            <td>{{ $elev_class->regime }}</td>
            <td>{{ \Carbon\Carbon::parse($elev_class->created_at)}}</td>
            <!-- <td><a href="{{route('carte.year',$elev_class->id)}}"  target="_blank"  class='btn btn-ghost-success'><i class="fa fa-print" aria-hidden="true"></i></i></a></td> -->
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Aucune classe frequentée
</div>
@endif

