@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Eleve</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                  <a href="{{ route('eleve_parent.parent') }}" class="btn btn-light">RETOUR</a>
                             </div>
                             <div class="card-body">
                                 @include('parents.show_fields_eleve')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
