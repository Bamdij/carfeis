@extends('layouts.app')



@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Paiement Eleve</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                        {{Session::get('fail')}}
                        </div>
                    @endif
                    @include('flash::message')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('eleve_parent_paiement.parent') }}" class="btn btn-light">RETOUR</a>
                            </div>
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <div class="card-body">
                                            <div class="">
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement Mensuel {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
                                                    <p>{{ $eleve->first_name }}</p>
                                                </div>
                                                <!-- Formulaire test -->
                                                
                                                <!-- End form -->

                                                <!-- Last Name Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
                                                    <p>{{ $eleve->last_name }}</p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
                                                    <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
                                                    <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                                <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
                                                <p>{{$eleve->lieu_naissance}}</p>
                                                </div>
                                                <!-- Sexe Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
                                                    <p>{{ $eleve->sexe }}</p>
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            @if(!empty($status_paiement))
                                            @include('parents.test_paiement')
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Resumé de la Situation de Paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <strong>{!! Form::label('regime_id', 'Regime:') !!}</strong>
                                                    <p> {{$eleve_regime->name}} </p>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <strong>{!! Form::label('mensualite_by_month', 'Mensualité:') !!}</strong>
                                                    <p> {{$eleve_regime->mensualite}} </p>
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Suivie de Paiement</h3>
                                                    <p> <strong>les Mois en rouge sont les mois non payé pour effectuer un paiement selectionner un mois</strong> </p>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                            <input type="hidden" name="annee_scolaire_id" value="{{$eleve_situat->annee_scloaire_id}}"  id="annee_scolaire_id">
                                                <input type="hidden" name="eleve_id" value="{{$eleve_situat->eleve_id}}"  id="eleve_id">
                                                <input type="hidden" name="eleve_slug" value="{{$eleve->slug}}"  id="eleve_slug">
                                                <input type="hidden" name="parent_id" value="{{$eleve_situat->parent_id}}"  id="parent_id">
                                                <input type="hidden" name="paiement" value="{{$eleve_situat->id}}"  id="paiement">
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->payed_janv == false )
                                                <input type="hidden" name="payed_janv" value="{{intval(0)}}"  id="jan">
                                                <input type="hidden"  value="JANVIER"  id="mont1">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_jan">JANVIER</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>JANVIER</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->payed_fev == false )
                                                <input type="hidden" name="payed_fev" value="{{intval(0)}}"  id="fev">
                                                <input type="hidden"  value="FEVRIER"  id="mont2">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_fev">FEVRIER</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>FEVRIER</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->payed_mars == false )
                                                <input type="hidden" name="payed_mars" value="{{intval(0)}}" id="mars">
                                                <input type="hidden"  value="MARS"  id="mont3">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_mar">MARS</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>MARS</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->payed_avril == false )
                                                <input type="hidden" name="payed_avril" value="{{intval(0)}}" id="avril">
                                                <input type="hidden"  value="AVRIL"  id="mont4">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_ap">AVRIL</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>AVRIL</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->mai_payed == false )
                                                <input type="hidden" name="mai_payed" value="{{intval(0)}}"  id="mai">
                                                <input type="hidden"  value="MAI"  id="mont5">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_mai">MAI</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>MAI</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->juin_payed == false )
                                                <input type="hidden" name="juin_payed" value="{{intval(0)}}" id="juin">
                                                <input type="hidden"  value="JUIN"  id="mont6">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_jun">JUIN</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>JUIN</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                @if($eleve_situat->payed_jul == false )
                                                <input type="hidden" name="payed_jul" value="{{intval(0)}}" id="juil">
                                                <input type="hidden"  value="JUILLET"  id="mont7">
                                                <button type="button" class="btn btn-outline-danger" id="suivie_payed_juil">JUILLET</button>
                                                @else
                                                <button type="button" class="btn btn-outline-success" disabled>JUILLET</button>
                                                @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($eleve_situat->payed_aout == false )
                                                    <input type="hidden" name="payed_aout" value="{{intval(0)}}" id="aout">
                                                    <input type="hidden"  value="AOUT"  id="mont8">
                                                    <button type="button" class="btn btn-outline-danger" id="suivie_payed_aout">AOUT</button>
                                                    @else
                                                    <button type="button" class="btn btn-outline-success" disabled>AOUT</button>
                                                    @endif       
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($eleve_situat->payed_sept == false )
                                                    <input type="hidden" name="payed_sept" value="{{intval(0)}}" id="sept">
                                                    <input type="hidden"  value="SEPTEMBRE"  id="mont9">
                                                    <button type="button" class="btn btn-outline-danger" id="suivie_payed_sep">SEPTEMBRE</button>
                                                    @else
                                                    <button type="button" class="btn btn-outline-success" disabled>SEPTEMBRE</button>
                                                    @endif        
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($eleve_situat->payed_octobre == false )
                                                    <input type="hidden" name="payed_octobre" value="{{intval(0)}}" id="octob">
                                                    <input type="hidden"  value="OCTOBRE"  id="mont10">
                                                    <button type="button" class="btn btn-outline-danger" id="suivie_payed_oct">OCTOBRE</button>
                                                    @else
                                                    <button type="button" class="btn btn-outline-success" disabled>OCTOBRE</button>
                                                    @endif  
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($eleve_situat->payed_nov == false )
                                                    <input type="hidden" name="payed_nov" value="{{intval(0)}}" id="nov">
                                                    <input type="hidden"  value="NOVEMBRE"  id="mont11">
                                                    <button type="button" class="btn btn-outline-danger" id="suivie_payed_nov">NOVEMBRE</button>
                                                    @else
                                                    <button type="button" class="btn btn-outline-success" disabled>NOVEMBRE</button>
                                                    @endif 
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($eleve_situat->payed_dec == false )
                                                    <input type="hidden" name="payed_dec" value="{{intval(0)}}" id="dec">
                                                    <input type="hidden"  value="DECEMBRE"  id="mont12">
                                                    <button type="button" class="btn btn-outline-danger" id="suivie_payed_dec">DECEMBRE</button>
                                                    @else
                                                    <button type="button" class="btn btn-outline-success" disabled>DECEMBRE</button>
                                                    @endif 
                                                </div>
                                            </div>
                                            <!-- <form  method="POST" action="">
                                                @csrf
                                                <div class="container top" style="display:none;">
                                                    <div class="row month-container" style="display:flex;">
                                                        <div class="col-md-2 mt-3">
                                                             <strong>{!! Form::label('mensualite_by_month', 'Choisir:') !!}</strong>
                                                            <select name="s_number_month1" id="number_month" class="form-control" onchange="changeFunctionChangeMonthChoice(this)" required>
                                                                 <option value="" >Choisir le nombre de Mois</option>
                                                                <option value="1" selected >1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            <select>
                                                        </div>
                                                        <div class="col-md-5 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Montant Attendu:') !!}</strong>
                                                            <input class="form-control" id="close" type="text" value="{{$eleve_regime->mensualite}}" >
                                                            <div id="montant_getting"></div>
                                                        </div>
                                                        <div class="col-md-5 mt-3">
                                                        <strong>{!! Form::label('periode_debut', 'Periode De:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_debut" placeholder="Noter les mois ex:janvier-fevrier" name="periode_debut" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3">
                                                        <strong>{!! Form::label('periode_end', 'A date de fin:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_end" placeholder="Noter les mois ex:janvier-fevrier" name="periode_end" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" style="display:none;">
                                                        <strong>{!! Form::label('mensualite_by_month', 'Mois:') !!}</strong>

                                                            <input class="form-control" type="text" id="s_month" placeholder="Noter les mois ex:janvier-fevrier" name="s_month1" value="{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('montant_attendu', 'Montant Réçu:') !!}</strong>
                                                            <input class="form-control" type="number" placeholder="Montant recu" id="mensualite" name="s_mensualite1" value="{{$eleve_regime->mensualite}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Observation:') !!}</strong>

                                                            <input class="form-control" type="text" id="observation" placeholder="Observation mois " name="s_observation1"  value=" Paiement mois de {{\Carbon\Carbon::now()->locale('fr_FR')->monthName}}">
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="montant" value="{{$eleve_regime->mensualite}}">
                                                    <input type="hidden" id="paiement_eleve_id" name="paiement_eleve_id" value="{{$status_paiement->id}}" >
                                                    <input  type="hidden" id="eleve" name="eleve_id" value="{{$status_paiement->eleve_id}}" >
                                                    <input  type="hidden" id="eleve_slug" name="eleve_slug" value="{{$eleve->slug}}" >
                                                    <input  type="hidden" id="annee_scolaire" name="annee_scolaire_id" value="{{$status_paiement->annee_scolaire_id}}" >
                                                    <input type="hidden" name="user_id" id="user" value="{{Auth::user()->id}}">
                                                    
                                                </div>                                                
                                                    <div class="form-group pull-rigth col-sm-12 p-4" id="eleve_arabe_div" >
                                                        <input class="btn btn-primary" type="submit" id="paydunya" value="Payer Maintenant" >
                                                        <a href="{{ route('eleve_parent_paiement.parent') }}" class="btn btn-secondary">Annuler</a>
                                                    </div>
                                            </form> -->
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection