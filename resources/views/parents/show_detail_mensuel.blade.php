<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Suivie Evaluation Mensuelle <strong>{{$eleve->first_name}} - {{$eleve->last_name}} </strong></h3>
    </div>
</div><!--/.row-->
@if(!$evaluationMensuels->isEmpty())
<div class="table-responsive-sm table table-bordered">
    <table class="table table-striped" id="evaluationMensuels-table">
        <thead>
            <tr>
        <th>Annee Scolaire</th>
        <th>Mois Evaluation</th>
        <th>Sourate</th>
        <th>Derniere Leçon</th>
        <th>Premiere Partie</th>
        <th>Deuxieme Partie</th>
        <th>Troisieme Partie</th>
        <th>Comportement</th>
        <th>Appreciation</th>
            </tr>
        </thead>
        <tbody>
        @foreach($evaluationMensuels as $evaluationMensuel)
            <tr>
            <td>{{ $evaluationMensuel->annee }}</td>
            <td>{{ $evaluationMensuel->month }}</td>
            <td>{{ $evaluationMensuel->sourate }}</td>
            <td>{{ $evaluationMensuel->last_lesson }}</td>
            <td>{{ $evaluationMensuel->first_party }} / 10<strong>({{ $evaluationMensuel->appreciation_first_party }})</strong> </td>
            <td>{{ $evaluationMensuel->second_party }} / 10 <strong>({{ $evaluationMensuel->appreciation_second_party }})</strong></td>
            <td>{{ $evaluationMensuel->third_party }} / 10 <strong>({{ $evaluationMensuel->appreciation_third_party }})</strong></td>
            <td>{{ $evaluationMensuel->comportement_fr }}</td>
            <td>{{ $evaluationMensuel->appreciation }}</td>
               
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-warning" role="alert">
  Eleve Pas encors evaluer!
</div>
@endif



