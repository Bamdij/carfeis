<!-- First Name Field -->
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS Parents</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom et Nom:') !!}</strong>
        <p>{{$parents->name }}</p>
    </div>

    <!-- Last Name Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('last_name', 'Email:') !!}</strong>
        <p>{{  $parents->email }}</p>
    </div>
    <!-- Sexe Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('sexe', 'Pseudo:') !!}</strong>
        <p>{{ $parents->pseudo }}</p>
    </div>
    </div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Eleves</h3>
    </div>
</div><!--/.row-->
@if(!$parent_eleve->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Matricule</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Date de Naissance</th>
                <th>Lieu de Naissance</th>
            </tr>
        </thead>
        <tbody>
        @foreach($parent_eleve  as $elev_class)
            <tr>
                <td>{{ $elev_class->eleve->numero }}</td>
                <td>{{$elev_class->eleve->last_name}}</td>
            <td>{{ $elev_class->eleve->first_name}}</td>
            <td>{{ \Carbon\Carbon::parse($elev_class->eleve->date_naissance)}}</td>
            <td>{{ $elev_class->eleve->lieu_naissance}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Parent d'aucun eleve
</div>
@endif

