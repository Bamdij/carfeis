@extends('layouts.app')

@section('content')
<style>
    td li {
        display: flex;
        align-items: center;
        justify-content: center;
}


</style>
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Evaluation Hebdomadaire</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 @include('flash::message')

                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong></strong>
                                  <a href="{{ route('eleve_parent_evaaluation_hebdomadaire.parent') }}" class="btn btn-light">Retour</a>
                             </div>
                             <div class="card-body">
                                 @include('parents.show_details_evaluation')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
