@extends('layouts.app')



@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('paiment.eleve') }}">Paiment Eleve</a>
      </li>
      <li class="breadcrumb-item active">Paiement Inscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                        {{Session::get('fail')}}
                        </div>
                    @endif
                    @include('flash::message')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <div class="card-body">
                                            <div class="">
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement Mensuel {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
                                                    <p>{{ $eleve->first_name }}</p>
                                                </div>
                                                <!-- Formulaire test -->
                                                
                                                <!-- End form -->

                                                <!-- Last Name Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
                                                    <p>{{ $eleve->last_name }}</p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
                                                    <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
                                                    <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                                <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
                                                <p>{{$eleve->lieu_naissance}}</p>
                                                </div>
                                                <!-- Sexe Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
                                                    <p>{{ $eleve->sexe }}</p>
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('regime_id', 'Regime:') !!}</strong>
                                                    <p> {{$eleve_regime->name}} </p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('mensualite_by_month', 'Mensualité:') !!}</strong>
                                                    <p> {{$eleve_regime->mensualite}} </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <strong>{!! Form::label('mensualite_by_month', 'Mois Choisi:') !!}</strong>
                                                    <p  class="badge badge-warning"> {{$month_paiement}} </p>
                                                </div>
                                            </div>
                                            <form  method="POST" action="">
                                                @csrf
                                                <div class="container top" style="display:none;">
                                                    <div class="row month-container" style="display:flex;">
                                                        <div class="col-md-2 mt-3">
                                                             <strong>{!! Form::label('mensualite_by_month', 'Choisir:') !!}</strong>
                                                            <select name="s_number_month1" id="number_month" class="form-control" onchange="changeFunctionChangeMonthChoice(this)" required>
                                                                 <option value="" >Choisir le nombre de Mois</option>
                                                                <option value="1" selected >1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            <select>
                                                        </div>
                                                        <div class="col-md-5 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Montant Attendu:') !!}</strong>
                                                            <input class="form-control" id="montant_month" name="s_montant_attendu1" type="text" value="{{$eleve_regime->mensualite}}" >
                                                        </div>
                                                        <div class="col-md-5 mt-3">
                                                        <strong>{!! Form::label('periode_debut', 'Periode De:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_debut" placeholder="Noter les mois ex:janvier-fevrier" name="periode_debut" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3">
                                                        <strong>{!! Form::label('periode_end', 'A date de fin:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_end" placeholder="Noter les mois ex:janvier-fevrier" name="periode_end" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" style="display:none;">
                                                        <strong>{!! Form::label('mensualite_by_month', 'Mois:') !!}</strong>

                                                            <input class="form-control" type="text" id="s_month" placeholder="Noter les mois ex:janvier-fevrier" name="s_month1" value="{{ $month_paiement }}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('montant_attendu', 'Montant Réçu:') !!}</strong>
                                                            <input class="form-control" type="number" placeholder="Montant recu" id="mensualite" name="s_mensualite1" value="{{$eleve_regime->mensualite}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Observation:') !!}</strong>

                                                            <input class="form-control" type="text" id="observation" placeholder="Observation mois " name="s_observation1"  value=" Paiement mois de {{$month_paiement}}">
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="montant" value="{{$eleve_regime->mensualite}}">
                                                    <input type="hidden" id="paiement_eleve_id" name="paiement_eleve_id" value="{{$status_paiement->id}}" >
                                                    <input  type="hidden" id="eleve" name="eleve_id" value="{{$status_paiement->eleve_id}}" >
                                                    <input  type="hidden" id="eleve_slug" name="eleve_slug" value="{{$eleve->slug}}" >
                                                    <input  type="hidden" id="annee_scolaire" name="annee_scolaire_id" value="{{$status_paiement->annee_scolaire_id}}" >
                                                    <input type="hidden" name="user_id" id="user" value="{{Auth::user()->id}}">
                                                    <input type="hidden" name="month_paiement" id="status_payed" value="{{$month_paiement}}">
                                                    <input type="hidden" name="status_name" id="pay_name" value="{{$status_name}}">
                                                    <input type="hidden" name="month_payed" id="pay_mont" value="{{$eleve_situat->id}}">
                                                    
                                                </div> 
                                                <div class="row">
                                                    <div class="form-group  col-sm-6 p-4" id="eleve_arabe_div" >
                                                        <input class="btn btn-primary" type="submit" id="parent_paydunya" value="Continuer Maintenant" >
                                                        <a href="{{ route('infor_paiement_parent.mensual',$eleve->slug) }}" class="btn btn-secondary">Retourner</a>
                                                    </div>	
                                                </div>
                                                </div>                                                       
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection