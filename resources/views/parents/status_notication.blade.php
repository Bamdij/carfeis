<div class="modal-header">
          <h5 class="modal-title sinistre-item-title" id="exampleModalLabel23"> Modifier l'accès de <span id="admin-fullname"></span> babb</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="post" id="update_form" nam="update_freelance" >
            <div class="modal-body" id="update_admin_access_status_form_body">
            <div class="row assign-expert-row">
                <div class="col-md-12 user-admin-status-bloc">
                <input type="text" id="admin_id" >
                </div>
                <div class="col-md-12 update_admin_access_status_container">
                <div class="form-group">
                    <strong><label for="description">Dites-nous pour quelle raison voulez-vous inscrire votre enfant dans notre etablissement ?</label></strong>
                    <textarea class="form-control" required name="descripttion" cols="50" rows="10"></textarea>
                </div>                
            </div>
            </div>
            </div>
            <div class="modal-footer">
            <div class="row">
                <div class="col-md-6 col-sm-6  ">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                </div>
                <div class="col-md-6 col-sm-6 submit-btn-container">
                <button type="submit" id="update_s_admin" class="btn btn-primary ">Modifier</button>
                </div>
            </div>
            </div>
        </form>
</div>
