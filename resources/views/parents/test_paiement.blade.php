<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Suivi de Paiement</h3>
    </div>
</div><!--/.row-->
<div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                
                <th>Annee Scolaire</th>
                <th>Frais inscription</th>
                <th>Montant Récu</th>
                <th>Date de Paiement</th>
                <th>Imprimer</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $status_paiement->annee_scolaire->name }}</td>
                <td>{{ $status_paiement->frais_inscription->montant }}</td>
                <td><span class="badge badge-info">{{ $status_paiement->montant_inscription }}</td>
                <td><span class="badge badge-warning">{{ $status_paiement->created_at }}</span></td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('get_recu.annee_scolaire', [$status_paiement->eleve->slug]) }}"  target="blank" class='btn btn-ghost-success'><i class="fa fa-print"></i></a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    </div>
@if(!$mensualite->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Mensualite</th>
                <th>Mois</th>
                <th>Date de Paiement</th>
                <th>Imprimer</th>
            </tr>
        </thead>
        <tbody>
        @foreach($mensualite  as $elev_class)
            <tr>
                <td>{{ $elev_class->numero }}</td>
                <td><span class="badge badge-danger">{{ $elev_class->mensualite}}</span></td>
                <td><span class="badge badge-warning">{{ $elev_class->month}}</span></td>
                <td>{{ $elev_class->created_at}}</td>
                <td>
                    <div class='btn-group'>
                        @if(empty($elev_class->invoice_url))
                        <a href="{{ route('get_recu_mensual.annee_scolaire', [$elev_class->slug]) }}"  target="blank" class='btn btn-ghost-success'><i class="fa fa-print"></i></a>
                        @else
                        <a href="{{$elev_class->invoice_url }}"  target="blank" class='btn btn-ghost-success'><i class="fa fa-print"></i></a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Aucun Paiement Mensuel disponible
</div>
@endif