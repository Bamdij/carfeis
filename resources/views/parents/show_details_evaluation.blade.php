<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Suivie Evaluation Hebdomadaire de <strong>{{$eleve->first_name}} - {{$eleve->last_name}}</strong></h3>
    </div>
</div><!--/.row-->
@if(!$evaluation->isEmpty())
<div class="table-responsive-sm table-bordered">
    <table class="table table-striped" id="evaluationSemestres-table">
        <thead>
            <tr>
               
        <th>Annee Scolaire</th>
        <th>Periode</th>
        <th>Lundi</th>
        <th>Mardi</th>
        <th>Mercredi</th>
        <th>Jeudi</th>
        <th>Vendredi</th>
        <th>Derniere leçon</th>
            </tr>
        </thead>
        <tbody>
            
        @foreach($evaluation as $evaluationSemestre)
            <tr>
            <td>{{ $evaluationSemestre->annee }}</td>
            <td>de {{Carbon\Carbon::parse( $evaluationSemestre->start_date)->format('d/m/Y')}} à {{Carbon\Carbon::parse( $evaluationSemestre->end_date)->format('d/m/Y')}} </td>
            <td>                  
              @if($evaluationSemestre->app_mond_1== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_mond_1 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_mond_1 }}
                </li>
                @endif
                @if($evaluationSemestre->app_mond_2== "Oui")
                <li style="color:#2eb85c !important">
                     {{ $evaluationSemestre->app_mond_2 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_mond_2 }}
                </li>
                @endif
                @if($evaluationSemestre->app_mon_3== "Oui")
                <li style="color:#2eb85c !important">
                   {{ $evaluationSemestre->app_mon_3 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_mon_3 }}
                </li>
                @endif                
            </td>
            <td>
                @if($evaluationSemestre->app_thues_1== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thues_1 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thues_1 }}
                </li>
                @endif  
                @if($evaluationSemestre->app_thues_2== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thues_2 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thues_2 }}
                </li>
                @endif  
                @if($evaluationSemestre->app_thues_3== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thues_3 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thues_3 }}
                </li>
                @endif  
            </td>
            <td>
                @if($evaluationSemestre->app_wed_1== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_wed_1 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_wed_1 }}
                </li>
                @endif  
                @if($evaluationSemestre->app_wed_2== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_wed_2 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_wed_2 }}
                </li>
                @endif  
                @if($evaluationSemestre->app_wed_3== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_wed_3 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_wed_3 }}
                </li>
                @endif  
            </td>
            <td>
                @if($evaluationSemestre->app_thurd_1== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thurd_1 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thurd_1 }}
                </li>
                @endif  
                @if($evaluationSemestre->app_thurd_2== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thurd_2 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thurd_2 }}
                </li>
                @endif 
                @if($evaluationSemestre->app_thurd_3== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_thurd_3 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_thurd_3 }}
                </li>
                @endif 
            </td>
            <td>
                @if($evaluationSemestre->app_frid_1== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_frid_1 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_frid_1 }}
                </li>
                @endif 
                @if($evaluationSemestre->app_frid_2== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_frid_2 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_frid_2 }}
                </li>
                @endif 
                @if($evaluationSemestre->app_frid_3== "Oui")
                <li style="color:#2eb85c !important">
                    {{ $evaluationSemestre->app_frid_3 }}
                </li>
                    @else
                    <li style="color:#FF0000">
                    {{ $evaluationSemestre->app_frid_3 }}
                </li>
                @endif 
            </td>
            <td>{{ $evaluationSemestre->last_lesson }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-warning" role="alert">
  Eleve Pas encors evalué!
</div>
@endif