@if(!$parent_eleve->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Matricule</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Date de Naissance</th>
                <th>Lieu de Naissance</th>
                <th>Photo</th>

                <th colspan="3">Paiement</th>

            </tr>
        </thead>
        <tbody>
        @foreach($parent_eleve  as $elev_class)
            <tr>
                <td>{{ $elev_class->eleve->numero }}</td>
                <td>{{$elev_class->eleve->last_name}}</td>
            <td>{{ $elev_class->eleve->first_name}}</td>
            <td>{{ \Carbon\Carbon::parse($elev_class->eleve->date_naissance)->format('d/m/Y')}}</td>
            <td>{{ $elev_class->eleve->lieu_naissance}}</td>
            <td><img src="{{$elev_class->eleve->avatar_eleve}}" alt="" height="50" width="50"   class="img-rounded" style="align:center;border:0" /></td>
            <td>
                    <div class='btn-group'>
                        <a href="{{ route('infor_paiement_parent.mensual', [$elev_class->eleve->slug]) }}" class='btn btn-ghost-danger'><i class="fa fa-cc-mastercard"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Parent d'aucun eleve
</div>
@endif