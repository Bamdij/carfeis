<!-- Obje Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obje', 'Obje:') !!}
    {!! Form::text('obje', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('messageContacts.index') }}" class="btn btn-secondary">Cancel</a>
</div>
