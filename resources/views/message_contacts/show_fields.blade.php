<!-- Obje Field -->
<div class="form-group">
    {!! Form::label('obje', 'Obje:') !!}
    <p>{{ $messageContact->obje }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $messageContact->description }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $messageContact->user_id }}</p>
</div>

<!-- Is Admin Field -->
<div class="form-group">
    {!! Form::label('is_admin', 'Is Admin:') !!}
    <p>{{ $messageContact->is_admin }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $messageContact->slug }}</p>
</div>

<!-- Is Parent Sending Field -->
<div class="form-group">
    {!! Form::label('is_parent_sending', 'Is Parent Sending:') !!}
    <p>{{ $messageContact->is_parent_sending }}</p>
</div>

<!-- Admin Id Field -->
<div class="form-group">
    {!! Form::label('admin_id', 'Admin Id:') !!}
    <p>{{ $messageContact->admin_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $messageContact->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $messageContact->updated_at }}</p>
</div>

