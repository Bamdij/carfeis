<div class="table-responsive-sm">
    <table class="table table-striped" id="messageContacts-table">
        <thead>
            <tr>
                <th>Obje</th>
        <th>Description</th>
        <th>User Id</th>
        <th>Is Admin</th>
        <th>Slug</th>
        <th>Is Parent Sending</th>
        <th>Admin Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messageContacts as $messageContact)
            <tr>
                <td>{{ $messageContact->obje }}</td>
            <td>{{ $messageContact->description }}</td>
            <td>{{ $messageContact->user_id }}</td>
            <td>{{ $messageContact->is_admin }}</td>
            <td>{{ $messageContact->slug }}</td>
            <td>{{ $messageContact->is_parent_sending }}</td>
            <td>{{ $messageContact->admin_id }}</td>
                <td>
                    {!! Form::open(['route' => ['messageContacts.destroy', $messageContact->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('messageContacts.show', [$messageContact->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('messageContacts.edit', [$messageContact->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>