<!-- Enseignant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
    {!! Form::text('enseignant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sous Niveau Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sous_niveau_id', 'Sous Niveau Id:') !!}
    {!! Form::text('sous_niveau_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('enseignantClasses.index') }}" class="btn btn-secondary">Cancel</a>
</div>
