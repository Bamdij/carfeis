<div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Enseignant Id</th>
        <th>Sous Niveau Id</th>
        <th>Is Teacher Arab</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($enseignantClasses as $enseignantClasse)
            <tr>
                <td>{{ $enseignantClasse->enseignant_id }}</td>
            <td>{{ $enseignantClasse->sous_niveau_id }}</td>
            <td>{{ $enseignantClasse->is_teacher_arab }}</td>
                <td>
                    {!! Form::open(['route' => ['enseignantClasses.destroy', $enseignantClasse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('enseignantClasses.show', [$enseignantClasse->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('enseignantClasses.edit', [$enseignantClasse->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>