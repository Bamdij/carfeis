<!-- Enseignant Id Field -->
<div class="form-group">
    {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
    <p>{{ $enseignantClasse->enseignant_id }}</p>
</div>

<!-- Sous Niveau Id Field -->
<div class="form-group">
    {!! Form::label('sous_niveau_id', 'Sous Niveau Id:') !!}
    <p>{{ $enseignantClasse->sous_niveau_id }}</p>
</div>

<!-- Is Teacher Arab Field -->
<div class="form-group">
    {!! Form::label('is_teacher_arab', 'Is Teacher Arab:') !!}
    <p>{{ $enseignantClasse->is_teacher_arab }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $enseignantClasse->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $enseignantClasse->updated_at }}</p>
</div>

