<!-- Name Alphabetisation Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_alphabetisation', 'Name Alphabetisation:') !!}
    {!! Form::text('name_alphabetisation', null, ['class' => 'form-control']) !!}
</div>

<!-- Daara Frequente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('daara_frequente', 'Daara Frequente:') !!}
    {!! Form::text('daara_frequente', null, ['class' => 'form-control']) !!}
</div>

<!-- Inteligence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inteligence', 'Inteligence:') !!}
    {!! Form::text('inteligence', null, ['class' => 'form-control']) !!}
</div>

<!-- Person Inscript Field -->
<div class="form-group col-sm-6">
    {!! Form::label('person_inscript', 'Person Inscript:') !!}
    {!! Form::text('person_inscript', null, ['class' => 'form-control']) !!}
</div>

<!-- Cap Finance Resp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cap_finance_resp', 'Cap Finance Resp:') !!}
    {!! Form::text('cap_finance_resp', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Inscription Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_inscription', 'Date Inscription:') !!}
    {!! Form::text('date_inscription', null, ['class' => 'form-control','id'=>'date_inscription']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#date_inscription').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('alphabetisations.index') }}" class="btn btn-secondary">Cancel</a>
</div>
