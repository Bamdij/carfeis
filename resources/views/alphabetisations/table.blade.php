<div class="table-responsive-sm">
    <table class="table table-striped" id="alphabetisations-table">
        <thead>
            <tr>
                <th>Name Alphabetisation</th>
        <th>Daara Frequente</th>
        <th>Inteligence</th>
        <th>Person Inscript</th>
        <th>Cap Finance Resp</th>
        <th>Date Inscription</th>
        <th>Eleve Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($alphabetisations as $alphabetisation)
            <tr>
                <td>{{ $alphabetisation->name_alphabetisation }}</td>
            <td>{{ $alphabetisation->daara_frequente }}</td>
            <td>{{ $alphabetisation->inteligence }}</td>
            <td>{{ $alphabetisation->person_inscript }}</td>
            <td>{{ $alphabetisation->cap_finance_resp }}</td>
            <td>{{ $alphabetisation->date_inscription }}</td>
            <td>{{ $alphabetisation->eleve_id }}</td>
                <td>
                    {!! Form::open(['route' => ['alphabetisations.destroy', $alphabetisation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('alphabetisations.show', [$alphabetisation->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('alphabetisations.edit', [$alphabetisation->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>