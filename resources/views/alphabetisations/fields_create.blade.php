<!-- Name Alphabetisation Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_alphabetisation', 'Nom Alphabetisation:') !!}
    {!! Form::text('name_alphabetisation', null, ['class' => 'form-control','required'=>true]) !!}
</div>

<!-- Daara Frequente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('daara_frequente', 'Daara Frequenté:') !!}
    {!! Form::text('daara_frequente', null, ['class' => 'form-control','required'=>true]) !!}
</div>

<!-- Inteligence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inteligence', 'Intelligence:') !!}
    {!! Form::text('inteligence', null, ['class' => 'form-control']) !!}
</div>

<!-- Person Inscript Field -->
<div class="form-group col-sm-6">
    {!! Form::label('person_inscript', 'Personne Inscripteur:') !!}
    {!! Form::text('person_inscript', null, ['class' => 'form-control']) !!}
</div>

<!-- Cap Finance Resp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cap_finance_resp', 'Capacité Financiare du Responsable:') !!}
    {!! Form::text('cap_finance_resp', null, ['class' => 'form-control','required'=>true]) !!}
</div>

<!-- Date Inscription Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_inscription', 'Date Inscription:') !!}
    {!! Form::text('date_inscription', null, ['class' => 'form-control','id'=>'date_inscription','required'=>true]) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#date_inscription').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
<div class="form-group col-sm-6" style="display:none">
    <label for="name_pere">eleve</label>
    <input class="form-control" value="{{$eleve_id->id}}" name="eleve_id" type="text" id="name_pere">
</div>
</div>
<div class="container">
<div class="row" >
<div class="col-md-6 col-sm-12 offset-8" style="display: flex;">
    <input type="submit" value="Terminer" class="btn btn-primary btn-kal-grey mr-3" name="terminer"> 
    <input type="submit" value="Ajouter et Continuer" class="btn btn-primary btn-kal-grey mr-3" name="continuer">

    <!-- <a href="{{ route('eleves.index') }}" class="btn btn-secondary btn-kal-grey mr-3">Annuler</a> 
    <input type="submit" value="Terminer"class="btn btn-primary btn-kal-blue" name="continuer"> -->
</div>   
</div>
