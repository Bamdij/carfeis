<!-- Name Alphabetisation Field -->
<div class="form-group">
    {!! Form::label('name_alphabetisation', 'Name Alphabetisation:') !!}
    <p>{{ $alphabetisation->name_alphabetisation }}</p>
</div>

<!-- Daara Frequente Field -->
<div class="form-group">
    {!! Form::label('daara_frequente', 'Daara Frequente:') !!}
    <p>{{ $alphabetisation->daara_frequente }}</p>
</div>

<!-- Inteligence Field -->
<div class="form-group">
    {!! Form::label('inteligence', 'Inteligence:') !!}
    <p>{{ $alphabetisation->inteligence }}</p>
</div>

<!-- Person Inscript Field -->
<div class="form-group">
    {!! Form::label('person_inscript', 'Person Inscript:') !!}
    <p>{{ $alphabetisation->person_inscript }}</p>
</div>

<!-- Cap Finance Resp Field -->
<div class="form-group">
    {!! Form::label('cap_finance_resp', 'Cap Finance Resp:') !!}
    <p>{{ $alphabetisation->cap_finance_resp }}</p>
</div>

<!-- Date Inscription Field -->
<div class="form-group">
    {!! Form::label('date_inscription', 'Date Inscription:') !!}
    <p>{{ $alphabetisation->date_inscription }}</p>
</div>

<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $alphabetisation->eleve_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $alphabetisation->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $alphabetisation->updated_at }}</p>
</div>

