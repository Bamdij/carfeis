<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://laravel.com/img/notification-logo.png" class="logo" alt="Laravel Logo">
@else
{{ $slot }}
<img src="{{asset('images/logocarfeis.png')}}" class="logo" width="50" heigth="50" alt="Laravel Logo" style="position: absolute;left: 37px;top:16px;">
@endif
</a>
</td>
</tr>
