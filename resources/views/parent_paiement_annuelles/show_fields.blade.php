<!-- Annee Scloaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scloaire_id', 'Annee Scloaire Id:') !!}
    <p>{{ $parentPaiementAnnuelle->annee_scloaire_id }}</p>
</div>

<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    <p>{{ $parentPaiementAnnuelle->parent_id }}</p>
</div>

<!-- Janvier Field -->
<div class="form-group">
    {!! Form::label('janvier', 'Janvier:') !!}
    <p>{{ $parentPaiementAnnuelle->janvier }}</p>
</div>

<!-- Payed Janv Field -->
<div class="form-group">
    {!! Form::label('payed_janv', 'Payed Janv:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_janv }}</p>
</div>

<!-- Fevrier Field -->
<div class="form-group">
    {!! Form::label('fevrier', 'Fevrier:') !!}
    <p>{{ $parentPaiementAnnuelle->fevrier }}</p>
</div>

<!-- Payed Fev Field -->
<div class="form-group">
    {!! Form::label('payed_fev', 'Payed Fev:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_fev }}</p>
</div>

<!-- Mars Field -->
<div class="form-group">
    {!! Form::label('mars', 'Mars:') !!}
    <p>{{ $parentPaiementAnnuelle->mars }}</p>
</div>

<!-- Payed Mars Field -->
<div class="form-group">
    {!! Form::label('payed_mars', 'Payed Mars:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_mars }}</p>
</div>

<!-- Avril Field -->
<div class="form-group">
    {!! Form::label('avril', 'Avril:') !!}
    <p>{{ $parentPaiementAnnuelle->avril }}</p>
</div>

<!-- Payed Avril Field -->
<div class="form-group">
    {!! Form::label('payed_avril', 'Payed Avril:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_avril }}</p>
</div>

<!-- Mai Field -->
<div class="form-group">
    {!! Form::label('mai', 'Mai:') !!}
    <p>{{ $parentPaiementAnnuelle->mai }}</p>
</div>

<!-- Mai Payed Field -->
<div class="form-group">
    {!! Form::label('mai_payed', 'Mai Payed:') !!}
    <p>{{ $parentPaiementAnnuelle->mai_payed }}</p>
</div>

<!-- Juin Field -->
<div class="form-group">
    {!! Form::label('juin', 'Juin:') !!}
    <p>{{ $parentPaiementAnnuelle->juin }}</p>
</div>

<!-- Juin Payed Field -->
<div class="form-group">
    {!! Form::label('juin_payed', 'Juin Payed:') !!}
    <p>{{ $parentPaiementAnnuelle->juin_payed }}</p>
</div>

<!-- Juillett Field -->
<div class="form-group">
    {!! Form::label('juillett', 'Juillett:') !!}
    <p>{{ $parentPaiementAnnuelle->juillett }}</p>
</div>

<!-- Payed Jul Field -->
<div class="form-group">
    {!! Form::label('payed_jul', 'Payed Jul:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_jul }}</p>
</div>

<!-- Aout Field -->
<div class="form-group">
    {!! Form::label('aout', 'Aout:') !!}
    <p>{{ $parentPaiementAnnuelle->aout }}</p>
</div>

<!-- Payed Aout Field -->
<div class="form-group">
    {!! Form::label('payed_aout', 'Payed Aout:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_aout }}</p>
</div>

<!-- Septembre Field -->
<div class="form-group">
    {!! Form::label('septembre', 'Septembre:') !!}
    <p>{{ $parentPaiementAnnuelle->septembre }}</p>
</div>

<!-- Payed Sept Field -->
<div class="form-group">
    {!! Form::label('payed_sept', 'Payed Sept:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_sept }}</p>
</div>

<!-- Octobre Field -->
<div class="form-group">
    {!! Form::label('octobre', 'Octobre:') !!}
    <p>{{ $parentPaiementAnnuelle->octobre }}</p>
</div>

<!-- Payed Octobre Field -->
<div class="form-group">
    {!! Form::label('payed_octobre', 'Payed Octobre:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_octobre }}</p>
</div>

<!-- Novembre Field -->
<div class="form-group">
    {!! Form::label('novembre', 'Novembre:') !!}
    <p>{{ $parentPaiementAnnuelle->novembre }}</p>
</div>

<!-- Payed Nov Field -->
<div class="form-group">
    {!! Form::label('payed_nov', 'Payed Nov:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_nov }}</p>
</div>

<!-- Decembre Field -->
<div class="form-group">
    {!! Form::label('decembre', 'Decembre:') !!}
    <p>{{ $parentPaiementAnnuelle->decembre }}</p>
</div>

<!-- Payed Dec Field -->
<div class="form-group">
    {!! Form::label('payed_dec', 'Payed Dec:') !!}
    <p>{{ $parentPaiementAnnuelle->payed_dec }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $parentPaiementAnnuelle->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $parentPaiementAnnuelle->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $parentPaiementAnnuelle->updated_at }}</p>
</div>

