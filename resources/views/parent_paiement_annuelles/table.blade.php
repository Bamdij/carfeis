<div class="table-responsive-sm">
    <table class="table table-striped" id="parentPaiementAnnuelles-table">
        <thead>
            <tr>
                <th>Annee Scloaire Id</th>
        <th>Parent Id</th>
        <th>Janvier</th>
        <th>Payed Janv</th>
        <th>Fevrier</th>
        <th>Payed Fev</th>
        <th>Mars</th>
        <th>Payed Mars</th>
        <th>Avril</th>
        <th>Payed Avril</th>
        <th>Mai</th>
        <th>Mai Payed</th>
        <th>Juin</th>
        <th>Juin Payed</th>
        <th>Juillett</th>
        <th>Payed Jul</th>
        <th>Aout</th>
        <th>Payed Aout</th>
        <th>Septembre</th>
        <th>Payed Sept</th>
        <th>Octobre</th>
        <th>Payed Octobre</th>
        <th>Novembre</th>
        <th>Payed Nov</th>
        <th>Decembre</th>
        <th>Payed Dec</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($parentPaiementAnnuelles as $parentPaiementAnnuelle)
            <tr>
                <td>{{ $parentPaiementAnnuelle->annee_scloaire_id }}</td>
            <td>{{ $parentPaiementAnnuelle->parent_id }}</td>
            <td>{{ $parentPaiementAnnuelle->janvier }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_janv }}</td>
            <td>{{ $parentPaiementAnnuelle->fevrier }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_fev }}</td>
            <td>{{ $parentPaiementAnnuelle->mars }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_mars }}</td>
            <td>{{ $parentPaiementAnnuelle->avril }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_avril }}</td>
            <td>{{ $parentPaiementAnnuelle->mai }}</td>
            <td>{{ $parentPaiementAnnuelle->mai_payed }}</td>
            <td>{{ $parentPaiementAnnuelle->juin }}</td>
            <td>{{ $parentPaiementAnnuelle->juin_payed }}</td>
            <td>{{ $parentPaiementAnnuelle->juillett }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_jul }}</td>
            <td>{{ $parentPaiementAnnuelle->aout }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_aout }}</td>
            <td>{{ $parentPaiementAnnuelle->septembre }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_sept }}</td>
            <td>{{ $parentPaiementAnnuelle->octobre }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_octobre }}</td>
            <td>{{ $parentPaiementAnnuelle->novembre }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_nov }}</td>
            <td>{{ $parentPaiementAnnuelle->decembre }}</td>
            <td>{{ $parentPaiementAnnuelle->payed_dec }}</td>
            <td>{{ $parentPaiementAnnuelle->status }}</td>
                <td>
                    {!! Form::open(['route' => ['parentPaiementAnnuelles.destroy', $parentPaiementAnnuelle->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('parentPaiementAnnuelles.show', [$parentPaiementAnnuelle->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('parentPaiementAnnuelles.edit', [$parentPaiementAnnuelle->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>