@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('parentPaiementAnnuelles.index') !!}">Parent Paiement Annuelle</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Parent Paiement Annuelle</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($parentPaiementAnnuelle, ['route' => ['parentPaiementAnnuelles.update', $parentPaiementAnnuelle->id], 'method' => 'patch']) !!}

                              @include('parent_paiement_annuelles.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection