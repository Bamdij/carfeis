<!-- Janvier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('janvier', 'Janvier:') !!}
    {!! Form::text('janvier', null, ['class' => 'form-control']) !!}
</div>

<!-- Fevrier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fevrier', 'Fevrier:') !!}
    {!! Form::text('fevrier', null, ['class' => 'form-control']) !!}
</div>

<!-- Mars Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mars', 'Mars:') !!}
    {!! Form::text('mars', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Mars Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_mars', 'Payed Mars:') !!}
    {!! Form::text('payed_mars', null, ['class' => 'form-control']) !!}
</div>

<!-- Avril Field -->
<div class="form-group col-sm-6">
    {!! Form::label('avril', 'Avril:') !!}
    {!! Form::text('avril', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Avril Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_avril', 'Payed Avril:') !!}
    {!! Form::text('payed_avril', null, ['class' => 'form-control']) !!}
</div>

<!-- Mai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mai', 'Mai:') !!}
    {!! Form::text('mai', null, ['class' => 'form-control']) !!}
</div>

<!-- Juin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('juin', 'Juin:') !!}
    {!! Form::text('juin', null, ['class' => 'form-control']) !!}
</div>

<!-- Juillett Field -->
<div class="form-group col-sm-6">
    {!! Form::label('juillett', 'Juillett:') !!}
    {!! Form::text('juillett', null, ['class' => 'form-control']) !!}
</div>

<!-- Aout Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aout', 'Aout:') !!}
    {!! Form::text('aout', null, ['class' => 'form-control']) !!}
</div>

<!-- Septembre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('septembre', 'Septembre:') !!}
    {!! Form::text('septembre', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Sept Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_sept', 'Payed Sept:') !!}
    {!! Form::text('payed_sept', null, ['class' => 'form-control']) !!}
</div>

<!-- Octobre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('octobre', 'Octobre:') !!}
    {!! Form::text('octobre', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Octobre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_octobre', 'Payed Octobre:') !!}
    {!! Form::text('payed_octobre', null, ['class' => 'form-control']) !!}
</div>

<!-- Novembre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('novembre', 'Novembre:') !!}
    {!! Form::text('novembre', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Nov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_nov', 'Payed Nov:') !!}
    {!! Form::text('payed_nov', null, ['class' => 'form-control']) !!}
</div>

<!-- Decembre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decembre', 'Decembre:') !!}
    {!! Form::text('decembre', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed Dec Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_dec', 'Payed Dec:') !!}
    {!! Form::text('payed_dec', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('parentPaiementAnnuelles.index') }}" class="btn btn-secondary">Cancel</a>
</div>
