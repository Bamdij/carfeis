@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('eleves.index') }}">Paiement Eleve</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 @include('flash::message')

                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                  <a href="{{ route('eleves.index') }}" class="btn btn-light">Retour</a>
                             </div>
                             <div class="card-body">
                                 @include('paiement_eleves.show_fields_info')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
