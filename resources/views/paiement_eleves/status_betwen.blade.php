
<div class="container">
    <div class="row">
        <div class="well col-xs-12 col-sm-12 col-md-12 ">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>Carfeis</strong>
                        <br>
                        Grand Yoff
                        <br>
                        DAKAR, SENEGAL
                        <br>
                        <abbr title="Phone">P:</abbr> (221) 77 070 88 43
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <em><strong>Date de Debut</strong>: {{$date_debut}}</em>
                    </p>
                    <p>
                    <strong> <em>Date de Fin </strong> {{$end_debut}}</em>
                    </p>
                    <p>
                    <strong> <em>Annee scolaire </strong> {{$annee_scolaire->name}}</em>
                    </p>
                    
                </div>
            </div>
            <div class="">
                <div class="text-center">
                    <h5> Situation Paiement <span ></span> Periode de {{$date_debut}} à  {{$end_debut}} </h5>
                </div>
                <div class="table-responsive-sm">
                    <table class="table table-striped" id="paiementEleves-table">
                        <thead>
                            <tr>
                                <th>Matricule</th>
                                <th>Prenom et Nom</th>
                                <th>Date et Lieu de Naissance</th>
                                <th>Classe</th>
                                <th>Mensualite</th>
                                <th>Mois de Paiement</th>
                                <th>Date de Paiement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $item)
                                <tr>
                                    <td>{{$item->eleve->numero}}</td>
                                    <td>{{$item->eleve->first_name}} {{$item->eleve->last_name}}</td>
                                    <td>{{Carbon\Carbon::parse($item->eleve->date_naissance)->format('d/m/Y')}} {{$item->eleve->lieu_naissance}}</td>
                                    <td>{{$item->classe}}</td>
                                    <td>{{$item->mensualite}}</td>
                                    <td>{{$item->month}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                            @endforeach
                        <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
                            <td class="text-center text-danger"><h4><strong>{{$sum}}</strong></h4></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 col-sm-6  ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


