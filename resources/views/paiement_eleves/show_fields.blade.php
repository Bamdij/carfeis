<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $paiementEleve->eleve_id }}</p>
</div>

<!-- Frais Inscription Id Field -->
<div class="form-group">
    {!! Form::label('frais_inscription_id', 'Frais Inscription Id:') !!}
    <p>{{ $paiementEleve->frais_inscription_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $paiementEleve->annee_scolaire_id }}</p>
</div>

<!-- Mois 1 Field -->
<div class="form-group">
    {!! Form::label('mois_1', 'Mois 1:') !!}
    <p>{{ $paiementEleve->mois_1 }}</p>
</div>

<!-- Montant 1 Field -->
<div class="form-group">
    {!! Form::label('montant_1', 'Montant 1:') !!}
    <p>{{ $paiementEleve->montant_1 }}</p>
</div>

<!-- Mois 2 Field -->
<div class="form-group">
    {!! Form::label('mois_2', 'Mois 2:') !!}
    <p>{{ $paiementEleve->mois_2 }}</p>
</div>

<!-- Montant 2 Field -->
<div class="form-group">
    {!! Form::label('montant_2', 'Montant 2:') !!}
    <p>{{ $paiementEleve->montant_2 }}</p>
</div>

<!-- Mois 3 Field -->
<div class="form-group">
    {!! Form::label('mois_3', 'Mois 3:') !!}
    <p>{{ $paiementEleve->mois_3 }}</p>
</div>

<!-- Montant 3 Field -->
<div class="form-group">
    {!! Form::label('montant_3', 'Montant 3:') !!}
    <p>{{ $paiementEleve->montant_3 }}</p>
</div>

<!-- Mois 4 Field -->
<div class="form-group">
    {!! Form::label('mois_4', 'Mois 4:') !!}
    <p>{{ $paiementEleve->mois_4 }}</p>
</div>

<!-- Montant 5 Field -->
<div class="form-group">
    {!! Form::label('montant_5', 'Montant 5:') !!}
    <p>{{ $paiementEleve->montant_5 }}</p>
</div>

<!-- Mois 6 Field -->
<div class="form-group">
    {!! Form::label('mois_6', 'Mois 6:') !!}
    <p>{{ $paiementEleve->mois_6 }}</p>
</div>

<!-- Montant 6 Field -->
<div class="form-group">
    {!! Form::label('montant_6', 'Montant 6:') !!}
    <p>{{ $paiementEleve->montant_6 }}</p>
</div>

<!-- Mois 7 Field -->
<div class="form-group">
    {!! Form::label('mois_7', 'Mois 7:') !!}
    <p>{{ $paiementEleve->mois_7 }}</p>
</div>

<!-- Montant 7 Field -->
<div class="form-group">
    {!! Form::label('montant_7', 'Montant 7:') !!}
    <p>{{ $paiementEleve->montant_7 }}</p>
</div>

<!-- Mois 8 Field -->
<div class="form-group">
    {!! Form::label('mois_8', 'Mois 8:') !!}
    <p>{{ $paiementEleve->mois_8 }}</p>
</div>

<!-- Montant 8 Field -->
<div class="form-group">
    {!! Form::label('montant_8', 'Montant 8:') !!}
    <p>{{ $paiementEleve->montant_8 }}</p>
</div>

<!-- Mois 9 Field -->
<div class="form-group">
    {!! Form::label('mois_9', 'Mois 9:') !!}
    <p>{{ $paiementEleve->mois_9 }}</p>
</div>

<!-- Montant 9 Field -->
<div class="form-group">
    {!! Form::label('montant_9', 'Montant 9:') !!}
    <p>{{ $paiementEleve->montant_9 }}</p>
</div>

<!-- Mois 10 Field -->
<div class="form-group">
    {!! Form::label('mois_10', 'Mois 10:') !!}
    <p>{{ $paiementEleve->mois_10 }}</p>
</div>

<!-- Montant 10 Field -->
<div class="form-group">
    {!! Form::label('montant_10', 'Montant 10:') !!}
    <p>{{ $paiementEleve->montant_10 }}</p>
</div>

<!-- Mois 11 Field -->
<div class="form-group">
    {!! Form::label('mois_11', 'Mois 11:') !!}
    <p>{{ $paiementEleve->mois_11 }}</p>
</div>

<!-- Montant 11 Field -->
<div class="form-group">
    {!! Form::label('montant_11', 'Montant 11:') !!}
    <p>{{ $paiementEleve->montant_11 }}</p>
</div>

<!-- Mois 12 Field -->
<div class="form-group">
    {!! Form::label('mois_12', 'Mois 12:') !!}
    <p>{{ $paiementEleve->mois_12 }}</p>
</div>

<!-- Montant 12 Field -->
<div class="form-group">
    {!! Form::label('montant_12', 'Montant 12:') !!}
    <p>{{ $paiementEleve->montant_12 }}</p>
</div>

<!-- Obseration 1 Field -->
<div class="form-group">
    {!! Form::label('obseration_1', 'Obseration 1:') !!}
    <p>{{ $paiementEleve->obseration_1 }}</p>
</div>

<!-- Obseration 2 Field -->
<div class="form-group">
    {!! Form::label('obseration_2', 'Obseration 2:') !!}
    <p>{{ $paiementEleve->obseration_2 }}</p>
</div>

<!-- Obseration 3 Field -->
<div class="form-group">
    {!! Form::label('obseration_3', 'Obseration 3:') !!}
    <p>{{ $paiementEleve->obseration_3 }}</p>
</div>

<!-- Obseration 4 Field -->
<div class="form-group">
    {!! Form::label('obseration_4', 'Obseration 4:') !!}
    <p>{{ $paiementEleve->obseration_4 }}</p>
</div>

<!-- Obseration 5 Field -->
<div class="form-group">
    {!! Form::label('obseration_5', 'Obseration 5:') !!}
    <p>{{ $paiementEleve->obseration_5 }}</p>
</div>

<!-- Obseration 6 Field -->
<div class="form-group">
    {!! Form::label('obseration_6', 'Obseration 6:') !!}
    <p>{{ $paiementEleve->obseration_6 }}</p>
</div>

<!-- Obseration 7 Field -->
<div class="form-group">
    {!! Form::label('obseration_7', 'Obseration 7:') !!}
    <p>{{ $paiementEleve->obseration_7 }}</p>
</div>

<!-- Obseration 8 Field -->
<div class="form-group">
    {!! Form::label('obseration_8', 'Obseration 8:') !!}
    <p>{{ $paiementEleve->obseration_8 }}</p>
</div>

<!-- Obseration 9 Field -->
<div class="form-group">
    {!! Form::label('obseration_9', 'Obseration 9:') !!}
    <p>{{ $paiementEleve->obseration_9 }}</p>
</div>

<!-- Obseration 10 Field -->
<div class="form-group">
    {!! Form::label('obseration_10', 'Obseration 10:') !!}
    <p>{{ $paiementEleve->obseration_10 }}</p>
</div>

<!-- Obseration 11 Field -->
<div class="form-group">
    {!! Form::label('obseration_11', 'Obseration 11:') !!}
    <p>{{ $paiementEleve->obseration_11 }}</p>
</div>

<!-- Obseration 12 Field -->
<div class="form-group">
    {!! Form::label('obseration_12', 'Obseration 12:') !!}
    <p>{{ $paiementEleve->obseration_12 }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $paiementEleve->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $paiementEleve->updated_at }}</p>
</div>

