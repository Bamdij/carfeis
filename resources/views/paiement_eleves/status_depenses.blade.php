@if(!$result->isEmpty())
<div class="container">
    <div class="row">
        <div class="well col-xs-12 col-sm-12 col-md-12 ">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>Carfeis</strong>
                        <br>
                        Grand Yoff
                        <br>
                        DAKAR, SENEGAL
                        <br>
                        <abbr title="Phone">P:</abbr> (221) 77 070 88 43
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <em><strong>Date de Debut</strong>: {{$date_debut}}</em>
                    </p>
                    <p>
                    <strong> <em>Date de Fin </strong> {{$end_debut}}</em>
                    </p>
                    <p>
                    <strong> <em>Annee scolaire </strong> {{$annee_scolaire->name}}</em>
                    </p>
                    
                </div>
            </div>
            <div class="">
                <div class="text-center">
                    <h5> Depenses {{$category->name}} <span ></span> Periode de {{$date_debut}} à  {{$end_debut}} </h5>
                </div>
                <div class="table-responsive-sm">
                    <table class="table table-striped" id="paiementEleves-table">
                        <thead>
                            <tr>
                            <th>Date Depense</th>
                            <th>Designation Depense</th>
                            <th>Montant Depense</th>
                            <th>Personne Donateur</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($result as $item)
                                <tr>
                                    <td>{{Carbon\Carbon::parse($item->date_depense)->format('d/m/Y')}}</td>
                                    <td>{{$item->designation_depense}}</td>
                                    <td>{{$item->montant_depense}}</td>
                                    <td class="badge badge-warning">{{$item->user->name }}</td>
                                </tr>
                            @endforeach
                        <tr>
                        <td></td>
                        <td></td>
                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
                            <td class="text-center text-danger"><h4><strong>{{$sum}}</strong> fca</h4></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 col-sm-6  ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="alert alert-warning" role="alert">
  Informations sur les paiements non disponibles pour les periodes {{$date_debut}} et {{$end_debut}}!
</div>
@endif
