
<!DOCTYPE html>

<html lang="fr" class="light">
<!-- BEGIN: Head -->

<head>
    <meta charset="utf-8">
    <link rel="icon" href="{!! asset('images/logoCarfeisEd.png') !!}" type = "image/carfeis">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Icewall admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Icewall Admin Template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="LEFT4CODE">
    <title>Carfeis Carte</title>
    <!-- BEGIN: CSS Assets-->
    <!-- END: CSS Assets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
          .img-bg{  width: 1031px;
            height: 484px;
            /* margin-left: -200px ; */
            background-image: url('/images/FondRecu.jpg');
            background-repeat: no-repeat;
            background-size: contain;
            background-position: bottom;
            /* background-color: #93ea93; */
            opacity: 0.9;
        }
        .well{ min-height: 20px;
                padding: 127px !important;
                margin-bottom: 20px;
                background-color: #f5f5f505;
                /* border: 1px solid #e3e3e3; */
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
                box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
            }
        
    </style>
 </head>
<body onload="window.print()">
<br><br>
<div class="container" >
    <div class="row">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-4 img-bg">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>{{$mensualite->numero}}</strong>
                        <br>
                       <strong>{{$eleve->first_name}}</strong>
                        <br>
                       <strong>{{$eleve->last_name}}</strong> 
                        <br>
                        <abbr title="Phone"></abbr>{{ \Carbon\Carbon::parse($eleve->date_naissance)->format('Y-m-d')}}                  </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                    <strong><p>Date Paiement :{{ \Carbon\Carbon::parse($eleve->created_at)}}</p></strong>
                    </p>
                </div>
            </div>
            <div class="">
                <div class="text-center">
                    <h3>Reçu Paiement</h3>
                </div>
                </span>
                <table class="">
                    <thead>
                        <tr>
                            <th>nombre de mois</th>
                            <th></th>
                            <th class="text-center">Mois</th>
                            <th class="text-center">Mensualite </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-9"><em>{{$mensualite->number_month}}</em></h4></td>
                            <td class="col-md-1" style="text-align: center"></td>
                            <td class="col-md-1 text-center">{{$mensualite->month}}</td>
                            <td class="col-md-1 text-center">{{$regime->mensualite}}</td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right p-3">
                            <p>
                                <strong>Total: </strong>
                            </p>
                           </td><br><br>
                            <td class="text-center">
                            <p>
                                <strong>{{intval($mensualite->mensualite)}} FCFA</strong>
                            </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

<!------ Include the above in your HEAD tag ---------->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
</body>
</html>

