<!-- First Name Field -->
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS ELEVE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('first_name', 'Matricule:') !!}</strong>
        <p>{{ $eleve->numero }}</p>
    </div>
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $eleve->first_name }}</p>
    </div>
    <!-- Last Name Field -->
    <div class="form-group col-sm-3">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $eleve->last_name }}</p>
    </div>
    <div class="form-group col-sm-3">
        <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
        <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
    </div>
    <!-- Date Naissance Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
        <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
    </div>

    <!-- Sexe Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
        <p>{{ $eleve->sexe }}</p>
    </div>

    <!-- Groupe Sanguin Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}</strong> 
        <p>{{ $eleve->groupe_sanguin }}</p>
    </div>
    @if($eleve->is_malade == true)
    <!-- Is Malade Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_malade', 'L\'enfant souffre t-il d\'une maladie?:') !!}</strong>
        <span class="badge badge-danger"><p>OUI</p></span>
    </div>

    <!-- Quelle Maladie Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('quelle_maladie', 'Quelle Maladie:') !!}</strong>
        <p>{{ $eleve->quelle_maladie }}</p>
    </div>
    @if($eleve->is_traitement == true)
    <!-- Is Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_traitement','L\'enfant suit t-il d\'un traitement?:') !!}</strong>
        <span class="badge badge-warning"><p>OUI</p></span>
    </div>
    @endif
    <!-- Quel Traitement Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('quel_traitement', 'Quel Traitement:') !!}</strong>
        <p>{{ $eleve->quel_traitement }}</p>
    </div>
    @endif
</div>
@if(!empty($infoParent))
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DU PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">

    <!-- Name Pere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('name_pere', 'Prénom et Nom Pere:') !!}</strong> 
        <p>{{ $infoParent->name_pere }}</p>
    </div>

    <!-- Phone Pere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_pere', 'Telephone Pere:') !!}</strong>
        <p>{{ $infoParent->phone_pere }}</p>
    </div>

    <!-- Email Pere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('email_pere', 'Email Pere:') !!}</strong> 
        <p>{{ $infoParent->email_pere }}</p>
    </div>
</div>
<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DE LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- Name Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('name_mere', 'Prenom et nom Mere:') !!}</strong>
        <p>{{ $infoParent->name_mere }}</p>
    </div>

    <!-- Phone Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('phone_mere', 'Telephone Mere:') !!}</strong>
        <p>{{ $infoParent->phone_mere }}</p>
    </div>

    <!-- Email Mere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('email_mere', 'Email Mere:') !!}</strong> 
        <p>{{ $infoParent->email_mere }}</p>
    </div>
</div>
@endif
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Resume du Paiement</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <!-- Name Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('is_confimr', 'Status Paiement:') !!}</strong>
        @if($paie->is_confirme == false)
           <p><span class="badge badge-danger">Non Payer</span></p> 
        @else
            <p><span class="badge badge-success">Payer</span></p>
         @endif
    </div>
    <!-- Name Mere Field -->
    <div class="form-group col-sm-4">
            <strong>{!! Form::label('annee', 'Annee Scolaire:') !!}</strong>
            <p>{{ $annee->name }}</p>
        </div>
    <!-- Phone Mere Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('regime', 'Regime:') !!}</strong>
        <p>{{ $frais->regime->name }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('regime', 'Mensualité:') !!}</strong>
        <p>{{ $frais->regime->mensualite }}</p>
    </div>
    <!-- Email Mere Field -->
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('frais_inscription', 'Frais Inscription:') !!}</strong> 
        <p>{{ $frais->montant }}</p>
    </div>
    <div class="form-group col-sm-4">
    <strong>{!! Form::label('frais_inscription', 'Montant Inscription:') !!}</strong> 
        <p>{{ $paie->montant_inscription }}</p>
    </div>
</div>
<div class="container">
    <div class="row" >
        <div class="col-md-6 col-sm-12" style="display: flex;">
        <a href="{{ route('eleves.index') }}" class="btn btn-secondary btn-kal-grey mr-3">Retour</a>
        <a href="{{ route('get_recu.annee_scolaire', [$eleve->slug]) }}"  target="blank" class="btn btn-primary btn-kal-grey mr-3">Imprimer le Reçu</a>
        <a href="{{ route('carte.eleve', [$eleve->slug]) }}" target="blank" class="btn btn-primary btn-kal-blue">Imprimer la Carte </a>
        </div>   
    </div>
</div>