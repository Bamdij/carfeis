<!-- Eleve Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    {!! Form::text('eleve_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Frais Inscription Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('frais_inscription_id', 'Frais Inscription Id:') !!}
    {!! Form::text('frais_inscription_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    {!! Form::text('annee_scolaire_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_1', 'Mois 1:') !!}
    {!! Form::text('mois_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_1', 'Montant 1:') !!}
    {!! Form::text('montant_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_2', 'Mois 2:') !!}
    {!! Form::text('mois_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_2', 'Montant 2:') !!}
    {!! Form::text('montant_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_3', 'Mois 3:') !!}
    {!! Form::text('mois_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_3', 'Montant 3:') !!}
    {!! Form::text('montant_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_4', 'Mois 4:') !!}
    {!! Form::text('mois_4', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 5 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_5', 'Montant 5:') !!}
    {!! Form::text('montant_5', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 6 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_6', 'Mois 6:') !!}
    {!! Form::text('mois_6', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 6 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_6', 'Montant 6:') !!}
    {!! Form::text('montant_6', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 7 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_7', 'Mois 7:') !!}
    {!! Form::text('mois_7', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 7 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_7', 'Montant 7:') !!}
    {!! Form::text('montant_7', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 8 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_8', 'Mois 8:') !!}
    {!! Form::text('mois_8', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 8 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_8', 'Montant 8:') !!}
    {!! Form::text('montant_8', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 9 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_9', 'Mois 9:') !!}
    {!! Form::text('mois_9', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 9 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_9', 'Montant 9:') !!}
    {!! Form::text('montant_9', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 10 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_10', 'Mois 10:') !!}
    {!! Form::text('mois_10', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 10 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_10', 'Montant 10:') !!}
    {!! Form::text('montant_10', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 11 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_11', 'Mois 11:') !!}
    {!! Form::text('mois_11', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 11 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_11', 'Montant 11:') !!}
    {!! Form::text('montant_11', null, ['class' => 'form-control']) !!}
</div>

<!-- Mois 12 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mois_12', 'Mois 12:') !!}
    {!! Form::text('mois_12', null, ['class' => 'form-control']) !!}
</div>

<!-- Montant 12 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant_12', 'Montant 12:') !!}
    {!! Form::text('montant_12', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_1', 'Obseration 1:') !!}
    {!! Form::text('obseration_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_2', 'Obseration 2:') !!}
    {!! Form::text('obseration_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_3', 'Obseration 3:') !!}
    {!! Form::text('obseration_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_4', 'Obseration 4:') !!}
    {!! Form::text('obseration_4', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 5 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_5', 'Obseration 5:') !!}
    {!! Form::text('obseration_5', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 6 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_6', 'Obseration 6:') !!}
    {!! Form::text('obseration_6', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 7 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_7', 'Obseration 7:') !!}
    {!! Form::text('obseration_7', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 8 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_8', 'Obseration 8:') !!}
    {!! Form::text('obseration_8', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 9 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_9', 'Obseration 9:') !!}
    {!! Form::text('obseration_9', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 10 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_10', 'Obseration 10:') !!}
    {!! Form::text('obseration_10', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 11 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_11', 'Obseration 11:') !!}
    {!! Form::text('obseration_11', null, ['class' => 'form-control']) !!}
</div>

<!-- Obseration 12 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obseration_12', 'Obseration 12:') !!}
    {!! Form::text('obseration_12', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('paiementEleves.index') }}" class="btn btn-secondary">Cancel</a>
</div>
