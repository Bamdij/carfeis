<div class="table-responsive-sm">
    <table class="table table-striped" id="paiementEleves-table">
        <thead>
            <tr>
                <th>Eleve Id</th>
        <th>Frais Inscription Id</th>
        <th>Annee Scolaire Id</th>
        <th>Mois 1</th>
        <th>Montant 1</th>
        <th>Mois 2</th>
        <th>Montant 2</th>
        <th>Mois 3</th>
        <th>Montant 3</th>
        <th>Mois 4</th>
        <th>Montant 5</th>
        <th>Mois 6</th>
        <th>Montant 6</th>
        <th>Mois 7</th>
        <th>Montant 7</th>
        <th>Mois 8</th>
        <th>Montant 8</th>
        <th>Mois 9</th>
        <th>Montant 9</th>
        <th>Mois 10</th>
        <th>Montant 10</th>
        <th>Mois 11</th>
        <th>Montant 11</th>
        <th>Mois 12</th>
        <th>Montant 12</th>
        <th>Obseration 1</th>
        <th>Obseration 2</th>
        <th>Obseration 3</th>
        <th>Obseration 4</th>
        <th>Obseration 5</th>
        <th>Obseration 6</th>
        <th>Obseration 7</th>
        <th>Obseration 8</th>
        <th>Obseration 9</th>
        <th>Obseration 10</th>
        <th>Obseration 11</th>
        <th>Obseration 12</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paiementEleves as $paiementEleve)
            <tr>
                <td>{{ $paiementEleve->eleve_id }}</td>
            <td>{{ $paiementEleve->frais_inscription_id }}</td>
            <td>{{ $paiementEleve->annee_scolaire_id }}</td>
            <td>{{ $paiementEleve->mois_1 }}</td>
            <td>{{ $paiementEleve->montant_1 }}</td>
            <td>{{ $paiementEleve->mois_2 }}</td>
            <td>{{ $paiementEleve->montant_2 }}</td>
            <td>{{ $paiementEleve->mois_3 }}</td>
            <td>{{ $paiementEleve->montant_3 }}</td>
            <td>{{ $paiementEleve->mois_4 }}</td>
            <td>{{ $paiementEleve->montant_5 }}</td>
            <td>{{ $paiementEleve->mois_6 }}</td>
            <td>{{ $paiementEleve->montant_6 }}</td>
            <td>{{ $paiementEleve->mois_7 }}</td>
            <td>{{ $paiementEleve->montant_7 }}</td>
            <td>{{ $paiementEleve->mois_8 }}</td>
            <td>{{ $paiementEleve->montant_8 }}</td>
            <td>{{ $paiementEleve->mois_9 }}</td>
            <td>{{ $paiementEleve->montant_9 }}</td>
            <td>{{ $paiementEleve->mois_10 }}</td>
            <td>{{ $paiementEleve->montant_10 }}</td>
            <td>{{ $paiementEleve->mois_11 }}</td>
            <td>{{ $paiementEleve->montant_11 }}</td>
            <td>{{ $paiementEleve->mois_12 }}</td>
            <td>{{ $paiementEleve->montant_12 }}</td>
            <td>{{ $paiementEleve->obseration_1 }}</td>
            <td>{{ $paiementEleve->obseration_2 }}</td>
            <td>{{ $paiementEleve->obseration_3 }}</td>
            <td>{{ $paiementEleve->obseration_4 }}</td>
            <td>{{ $paiementEleve->obseration_5 }}</td>
            <td>{{ $paiementEleve->obseration_6 }}</td>
            <td>{{ $paiementEleve->obseration_7 }}</td>
            <td>{{ $paiementEleve->obseration_8 }}</td>
            <td>{{ $paiementEleve->obseration_9 }}</td>
            <td>{{ $paiementEleve->obseration_10 }}</td>
            <td>{{ $paiementEleve->obseration_11 }}</td>
            <td>{{ $paiementEleve->obseration_12 }}</td>
                <td>
                    {!! Form::open(['route' => ['paiementEleves.destroy', $paiementEleve->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('paiementEleves.show', [$paiementEleve->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('paiementEleves.edit', [$paiementEleve->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>