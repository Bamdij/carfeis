<form  method="POST" action="#">
    @csrf
    <div class="row"> 
        <div class="form-group col-sm-6">
            <strong><label for="recipient-name" class="col-form-label">Annee scolaire:</label></strong>
            <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control"  required>
                <option value="">choisir Annee scolaire</option>
                <option value="{{$annee_niveau->id}}">{{$annee_niveau->name}}</option>
            <select> 
        </div>
        <div class="form-group col-sm-6">
            <strong><label for="recipient-name" class="col-form-label">Type de Depenses:</label></strong>
            <select name="category_id" id="category_id" class="form-control"  required>
                <option value="">choisir le Type de Depenses</option>
                @foreach($categories as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            <select> 
        </div>
        <div class="form-group col-sm-6">
            <strong><label for="recipient-name" class="col-form-label">Periode de :</label></strong>
           <input type="date"  class="form-control"  name="date_debut" id="date_debut" required>
        </div>
        <div class="form-group col-sm-6">
            <strong><label for="recipient-name" class="col-form-label">Au :</label></strong>
           <input type="date"  class="form-control" name="end_date" id="end_date">
        </div>
    </div>
    <div class="row" id="close_search">
        <div class="form-group col-sm-12">
            <a href=""  data-toggle="modal" data-target=".bd-example-modal-lg"><input class="form-control" type="submit" id="paiement_liste_depense" name="numero"  value="Rechercher"></a> 
        </div>
        <!-- <div class="form-group col-sm-6">
        <a href="/"> <input class="form-control"  value="Annuler" id="Retour_list"></a>
        </div> -->
    </div>
</form>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div id="respon_depenses_paiement"></div>
    </div>
  </div>
</div>