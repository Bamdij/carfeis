@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="{!! route('home') !!}">Acceuil</a>
      </li>
      <li class="breadcrumb-item active"></li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Suivie de Paiement des eleves</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'paiementEleves.store']) !!}

                                   @include('paiement_eleves.list_paiement')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
