<!-- Question Field -->
<div class="row">
<div class="form-group col-sm-6">
    <strong>{!! Form::label('name', 'Prenom et nom:') !!}</strong>
    <p>{{ $user->name }}</p>
</div>
<div class="form-group col-sm-6">
    <strong>{!! Form::label('email', 'Email:') !!}</strong>
    <p>{{ $user->email }}</p>
</div>
<div class="form-group col-sm-6">
    <strong>{!! Form::label('pseudo', 'Pseudo:') !!}</strong>
    <p>{{ $user->pseudo }}</p>
</div>
<div class="form-group col-sm-6">
    <strong>{!! Form::label('status', 'Profil Utilisateur:') !!}</strong>
    <p>{{ $user->status }}</p>
</div>
</div>

