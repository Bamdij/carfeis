@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@push('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endpush
<div class="modal fade update_admin_access_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel23" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div id="access_admin_text"></div>
       
    </div>
</div>