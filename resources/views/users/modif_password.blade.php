<div class="row">
<div class="form-group col-sm-6">
    <label for="new_password">Nouveau mot de passe:</label>
    <input class="form-control" required name="new_password" type="password" id="password">
</div>
<div class="form-group col-sm-6">
    <label for="confirmation">confirmation mot de passe:</label>
    <input class="form-control" required name="confirm_password" type="password" id="confirmation">
</div>
</div>
<div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    @if(!empty($user))
    @if($user->status == "Parent")
    <a href="{{ route('dashboard.parent') }}" class="btn btn-secondary">Annuler</a>
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
</div>
