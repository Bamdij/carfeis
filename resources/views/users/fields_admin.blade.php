<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    <label for="name">Prenom et Nom:</label>
    <input class="form-control" required name="name"  value="{{Auth::user()->name}}" type="text" id="name">
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    <label for="email">Email:</label>
    <input class="form-control" required name="email" value="{{Auth::user()->email}}"  type="email" id="email">
</div>
<div class="form-group col-sm-6">
    <label for="pseudo">Pseudo:</label>
    <input class="form-control" required name="pseudo"  value="{{Auth::user()->pseudo}}"  type="text" id="pseudo">
</div>
<!--   <div class="form-group col-sm-6">
    <label for="image_path">Image Avatar:</label>
    <input name="image_path" type="file" id="image_path" class="form-control" >
</div>  -->  
 
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    @if(!empty($user))
    @if($user->status == "Parent")
    <a href="{{ route('dashboard.parent') }}" class="btn btn-secondary">Annuler</a>
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
</div>
