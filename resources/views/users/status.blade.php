<div class="modal-header">
          <h5 class="modal-title sinistre-item-title" id="exampleModalLabel23"> Modifier l'accès de <span id="admin-fullname"></span> {{$user->name}} </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="post" id="update_form" nam="update_freelance" >
            <div class="modal-body" id="update_admin_access_status_form_body">
            <div class="row assign-expert-row">
                <div class="col-md-12 user-admin-status-bloc">
                    <div class="alert labeltv-alert" id="admin-actual-status"></div>
                </div>
                <div class="col-md-12 update_admin_access_status_container">
                    @csrf
                    <select name="is_active_access" id="access_admin" class="form-control" required>
                        @if($user->is_active_access ==true)
                        <option value="{{ intval(1) }}" selected>Activé</option>
                        <option value="{{ intval(0) }}">Désactivé</option>
                        @else
                        <option value="{{ intval(1) }}">Activé</option>
                        <option value="{{ intval(0) }}" selected>Désactivé</option>
                        @endif
                    </select>
                </div>
                <input type="hidden" id="admin_id" value="{{$user->id}}">
            </div>
            </div>
            <div class="modal-footer">
            <div class="row">
                <div class="col-md-6 col-sm-6  ">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                </div>
                <div class="col-md-6 col-sm-6 submit-btn-container">
                <button type="submit" id="update_s_admin" class="btn btn-primary ">Modifier</button>
                </div>
            </div>
            </div>
        </form>
</div>
