@extends('layouts.app')

@section('content')
    <div class="container-fluid py-5">
        <div class="animated fadeIn">
             @include('flash::message')
            <div class="col-lg-12">
                <div class="row statistics px-md-2 mx-0">
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-user-follow add-text text-primary "></i> &nbsp; 
                          <div class="ml-auto">
                            <p class="admin-dsb">Nombre Eleves</p>
                            <h4 class="chiffre">{{$parent}}</h4>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-envelope-open add-text text-primary "></i> &nbsp;
                          <div class="ml-auto">
                            <p class="title admin-dsb">Message Reçu</p>
                            <h4 class="chiffre">5</h4>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-envelope-closed add-text text-primary "></i> &nbsp;
                          <div class="ml-auto">
                            <p class="title admin-dsb">Nouveau Message</p>
                            <h4 class="chiffre">0</h4>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                </div>
                <!-- @if(!empty($apropos))
                <div class="container-fluid mx-auto pt-3">
                     <div class="mt-1">
                        <div class="col-md-12">
                            <h3 class="form-section-title">{{$apropos->title}}</h3>
                        </div>
                    </div>
                    <div class="chart-width">
                        <div class="panel-body shadow p-3 mb-5 bg-white rounded labeltv-card">
                         {!! $apropos->description !!}
                        </div>
                    </div>
                </div>
                @else
                <div class="container-fluid mx-auto pt-3">
                     <div class="mt-1">
                        <div class="col-md-12">
                            <h3 class="form-section-title">A Propos de CARFEIS</h3>
                        </div>
                    </div>
                    <div class="chart-width">
                        <div class="panel-body shadow p-3 mb-5 bg-white rounded labeltv-card">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.
                            Ut velit mauris, egestas sed, gravida nec, ornare ut, mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin. Fusce varius, ligula non tempus aliquam, nunc turpis ullamcorper nibh, in tempus sapien eros vitae ligula. Pellentesque rhoncus nunc et augue. Integer id felis. Curabitur aliquet pellentesque diam. Integer quis metus vitae elit lobortis egestas. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi vel erat non mauris convallis vehicula. Nulla et sapien. Integer tortor tellus, aliquam faucibus, convallis id, congue eu, quam. Mauris ullamcorper felis vitae erat. Proin feugiat, augue non elementum posuere, metus purus iaculis lectus, et tristique ligula justo vitae magna.
                            Aliquam convallis sollicitudin purus. Praesent aliquam, enim at fermentum mollis, ligula massa adipiscing nisl, ac euismod nibh nisl eu lectus. Fusce vulputate sem at sapien. Vivamus leo. Aliquam euismod libero eu enim. Nulla nec felis sed leo placerat imperdiet. Aenean suscipit nulla in justo. Suspendisse cursus rutrum augue. Nulla tincidunt tincidunt mi. Curabitur iaculis, lorem vel rhoncus faucibus, felis magna fermentum augue, et ultricies lacus lorem varius purus. Curabitur eu amet.
                        </div>
                    </div>
                </div>
                @endif -->
                <div class="container-fluid mx-auto pt-3">
                    <div class="chart-width">
                        <div class="panel-body shadow p-3 mb-5 bg-white rounded labeltv-card">
                            <canvas id="myChart" height="280"  width="800"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
var ctx1 = document.getElementById('myChart').getContext('2d');
var progress = document.getElementById('animationProgress');

var myData = [
 '3','1','4','2',
]

var myWeek = [
    '2018','2019','2020','2021',
]

var chart = new Chart(ctx1, {
    // The type of chart we want to create
    type: 'line',
    
    // The data for our dataset
    data: {
        labels: myWeek,
        datasets: [{
            label: 'Nombre de vos éléves dans Afeis par Année ',
            backgroundColor: 'rgb(246, 227, 184)',
            borderColor: '#F9CA24',
            colors: ["#F9CA24"],
            data: myData,
           
        }],
        
    },

    // qsccgg  
    plotOptions: {
                bar: {
                    barHeight: '100%',
                    distributed: true,
                    dataLabels: {
                        // position: 'bottom'
                    },
                    
                }
            },
            
            colors: ["#F9CA24"],
            fill: {
                colors: ['rgba(248, 153, 10, 0.02) 96.74%)'],
            },
            zoom: {
                enabled: true
            },
            grid: {
      show: true,
      
      xaxis: {
        lines: {
          show: true
        }
      },
      yaxis: {
        lines: {
          show: true
        }
      }
    },
    

    // Configuration options go here
    options: {
        zoom: {
                enabled: true
            },
            marker: {
                show: true
            },
    },
});

</script>
@endpush


