@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('user.index') !!}">Utilisateur</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             @if (Session::has('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
             @endif
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Coach</strong>
                        </div>
                        <div class="card-body">
                             {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch']) !!} 

                              @include('users.fields')

                              {!! Form::close() !!} 
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Modification Mot de Passe</strong>
                        </div>
                        <div class="card-body">
                             {!! Form::model($user, ['route' => ['password.changing', $user->id], 'method' => 'post']) !!} 

                              @include('users.modif_password')

                              {!! Form::close() !!} 
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
