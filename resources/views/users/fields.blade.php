<div class="row">
<!-- Name Field -->
@if(!empty($user))
<!-- Name Field -->
<div class="form-group col-sm-6">
    <label for="name">Prenom et Nom:</label>
    <input class="form-control" required name="name" value="{{$user->name}}" type="text" id="name">
</div>

<div class="form-group col-sm-6">
    <label for="email">Email:</label>
    <input class="form-control" required name="email" value="{{$user->email}}" type="email" id="email">
</div>
<div class="form-group col-sm-6">
    <label for="pseudo">Pseudo:</label>
    <input class="form-control" required name="pseudo" type="text" value="{{$user->pseudo}}" id="pseudo">
</div>
<!-- Is Actif Field -->
<div class="form-group col-sm-6">
    <label for="status">Profil Utilisateur:</label>
    <select name="status" id="status" class="form-control">
        <option value="">choisir le profil</option>
        <option value="superadmin">Administrateur</option>
        <option value="directeur">Directeur Institut</option>
        <option value="responsable_pedagogique">Responsable Pédagogique</option>
        <option value="responsable_finance">Responsable Financier</option>
        <option value="responsable_professeur">Responsable Evaluation</option>
    </select>
</div>
<div class="form-group col-sm-6" >
    <label for="is_actif">Activer:</label>
    <select name="is_active_access" id="is_actif" class="form-control">
         @if($user->is_active_access ==true)
        <option value="{{ intval(1) }}" selected>Activé</option>
        <option value="{{ intval(0) }}">Non activé</option>
        @else 
         <option value="{{ intval(1) }}">Activé</option>
        <option value="{{ intval(0) }}" selected>Non activé</option> 
         @endif
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Prenom et Nom:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => true]) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','required' => true]) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('pseudo', 'Pseudo:') !!}
    {!! Form::text('pseudo', null, ['class' => 'form-control','required' => true]) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Profil Utilisateur:') !!}
    <select name="status" id="status" class="form-control">
        <option value="superadmin">Administrateur</option>
        <option value="directeur" >Directeur Institut</option>
        <option value="responsable_pedagogique">Responsable Pédagogique</option>
        <option value="responsable_finance">Responsable Financier</option>
        <option value="responsable_professeur">Responsable Evaluation</option>

        <!-- <option value="1">Enseignant</option> -->
    </select>
</div>
</div> 
 @endif
<div class="clearfix"></div>
<!-- Submit Field -->
<div class="form-group col-sm-12"  >
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    @if(!empty($user))
    @if($user->status == "Parent")
    <a href="{{ route('dashboard.parent') }}" class="btn btn-secondary">Annuler</a>
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
    @else
    <a href="{{ route('user.index') }}" class="btn btn-secondary">Annuler</a>
    @endif
</div>
