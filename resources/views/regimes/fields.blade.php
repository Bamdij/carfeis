<div class="row">
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nom du regime:') !!}
    <select name="name"  class="form-control" required>
    <option value="">choisir le nom du regime</option>
    @if(!empty($regime))
    <option value="{{$regime->name}}" selected >{{$regime->name}}</option>
    @endif
    <option value="Internat" >Internat</option>
    <option value="Externat" >Externat</option>
    <!-- <option value="Demi-pensionnat" >Demi-Pensionnat</option> -->
    <!-- <option value="social" >Cas Social</option> -->
<select>
</div>
<div class="form-group col-sm-6">   
    {!! Form::label('name', 'Status du regime:') !!}
    <select name="status"  class="form-control" required>
    <option value="">choisir le status du regime</option>
    @if(!empty($regime))
    <option value="{{$regime->status}}" selected >{{$regime->status}}</option>
    @endif
    <option value="Paiement-integral" >Paiement Integral</option>
    <option value="Paiement-partiel" >Paiement Partiel</option>
    <option value="Prise-en-charge" >Prise en Charge</option>
    <option value="social" >Cas Social</option>
<select>
</div>

<div class="form-group col-sm-6">
    <strong>{!! Form::label('mensualite', 'Mensualité:') !!}</strong>
    {!! Form::number('mensualite', null, ['class' => 'form-control']) !!}
</div>
<!-- Description Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('regimes.index') }}" class="btn btn-secondary">Annuler</a>
</div>
