<!-- Enseignant Id Field -->
@if(!empty($mensuualiteEnseignant))
<div class="row">
    <div class="form-group col-sm-6" style="display:none;">
        {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
        {!! Form::text('enseignant_id', null, ['class' => 'form-control']) !!}
        <input class="form-control" name="enseignant_id" type="hidden" value="{{$mensuualiteEnseignant->enseignant_id }}" id="month">
        <input class="form-control" name="annee_scolaire_id" type="hidden" value="{{$mensuualiteEnseignant->annee_scolaire_id }}" id="month">
        <input class="form-control" name="type_paiement_id" type="hidden" value="{{$mensuualiteEnseignant->type_paiement_id }}" id="month">
        <input class="form-control" name="user_id" type="hidden" value="{{$mensuualiteEnseignant->user_id }}" id="month">
        <input class="form-control" name="identity_card" type="hidden" value="{{$mensuualiteEnseignant->identity_card }}" id="month">
        <input class="form-control" name="month_end" type="hidden" value="{{$mensuualiteEnseignant->month_end }}" id="month">
    </div>
    <!-- Month Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('month', 'Mois:') !!}
        {!! Form::text('month', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Montant Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('montant', 'Montant:') !!}
        {!! Form::text('montant', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Observation Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('observation', 'Observation:') !!}
        {!! Form::text('observation', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('enseignant.paiement',$enseignant->slug) }}" class="btn btn-secondary">Annuler</a>
    </div>
</div>
@endif
