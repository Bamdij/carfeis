<!-- Enseignant Id Field -->
<div class="form-group">
    {!! Form::label('enseignant_id', 'Enseignant Id:') !!}
    <p>{{ $mensuualiteEnseignant->enseignant_id }}</p>
</div>

<!-- Identity Card Field -->
<div class="form-group">
    {!! Form::label('identity_card', 'Identity Card:') !!}
    <p>{{ $mensuualiteEnseignant->identity_card }}</p>
</div>

<!-- Month Field -->
<div class="form-group">
    {!! Form::label('month', 'Month:') !!}
    <p>{{ $mensuualiteEnseignant->month }}</p>
</div>

<!-- Montant Field -->
<div class="form-group">
    {!! Form::label('montant', 'Montant:') !!}
    <p>{{ $mensuualiteEnseignant->montant }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $mensuualiteEnseignant->annee_scolaire_id }}</p>
</div>

<!-- Type Paiement Id Field -->
<div class="form-group">
    {!! Form::label('type_paiement_id', 'Type Paiement Id:') !!}
    <p>{{ $mensuualiteEnseignant->type_paiement_id }}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{{ $mensuualiteEnseignant->observation }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $mensuualiteEnseignant->user_id }}</p>
</div>

<!-- Month End Field -->
<div class="form-group">
    {!! Form::label('month_end', 'Month End:') !!}
    <p>{{ $mensuualiteEnseignant->month_end }}</p>
</div>

<!-- Is Payed Field -->
<div class="form-group">
    {!! Form::label('is_payed', 'Is Payed:') !!}
    <p>{{ $mensuualiteEnseignant->is_payed }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $mensuualiteEnseignant->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $mensuualiteEnseignant->updated_at }}</p>
</div>

