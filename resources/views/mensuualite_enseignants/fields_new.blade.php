<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS ENSEIGNANT</h3>
    </div>
</div><!--/.row-->
<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $enseignant->first_name }}</p>
    </div>

    <!-- Last Name Field -->
    <div class="form-group  col-sm-4">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $enseignant->last_name }}</p>
    </div>

    <!-- Phone Field -->
    <div class="form-group  col-sm-4">
        <strong>{!! Form::label('phone', 'Téléphone:') !!}</strong>
        <p>{{ $enseignant->phone }}</p>
    </div>

    <!-- Adresse Field -->
    <div class="form-group  col-sm-4">
        <strong>{!! Form::label('adresse', 'Adresse:') !!}</strong>
        <p>{{ $enseignant->adresse }}</p>
    </div>

    <!-- Niveau Etude Field -->
    <div class="form-group  col-sm-4">
    <strong>{!! Form::label('niveau_etude', 'Niveau Etude:') !!}</strong>
        <p>{{ $enseignant->niveau_etude }}</p>
    </div>

    <!-- Date Recrutement Field -->
    <div class="form-group  col-sm-4">
    <strong>{!! Form::label('date_recrutement', 'Date Recrutement:') !!}</strong>
        <p>{{Carbon\Carbon::parse($enseignant->date_recrutement)->format('d/m/Y')}}</p>
    </div>
@if(!empty($enseignant->type_enseignant))
    <!-- Type Enseignant Id Field -->
    <div class="form-group  col-sm-4">
    <strong>{!! Form::label('type_enseignant_id', 'Type Enseignant:') !!}</strong>
        <p>{{$enseignant->type_enseignant->name }}</p>
    </div>
@endif
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">SUIVI PAIEMENT ENSEIGNANT</h3>
    </div>
</div><!--/.row-->
@if(!$paiement_month->isEmpty())
@include('mensuualite_enseignants.table_paiement')
@else
<div class="alert alert-warning" role="alert">
  Aucun Paiement disponible
</div>
@endif
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Paiement Mensuel</h3>
    </div>
</div><!--/.row-->
{!! Form::open(['route' => 'mensuualiteEnseignants.store']) !!}
<div class="row">
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire :</label></strong>
        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control" required>
            <option value="">choisir l'annee scolaire</option>
            <option value="{{$annee_scolaire->id}}">{{$annee_scolaire->name}}</option>
        <select> 
    </div>  
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Type de Paiement:</label></strong>
        <select name="type_paiement_id" id="type_paiement_id" class="form-control" required>
            <option value="">choisir le type de paiement</option>
            @foreach($type_paiment as $paiement)
            <option value="{{$paiement->id}}">{{$paiement->name}}</option>
            @endforeach
        <select> 
    </div> 
    <!-- Month Field -->
    <div class="form-group col-sm-6">
       <strong>{!! Form::label('month', 'Mois de Paiement:') !!}</strong> 
        <input class="form-control" name="month" type="text" value="{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }}" id="month" required>
    </div>

    <!-- Montant Field -->
    <div class="form-group col-sm-6">
      <strong>{!! Form::label('montant', 'Montant:') !!}</strong> 
        {!! Form::number('montant', null, ['class' => 'form-control','required'=>true]) !!}
    </div>
    <!-- Observation Field -->
    <div class="form-group col-sm-6">
      <strong>{!! Form::label('observation', 'Observation:') !!}</strong> 
        {!! Form::text('observation', null, ['class' => 'form-control']) !!}
    </div>

    <!-- User Id Field -->
    <div class="form-group col-sm-6" style="display:none;">
        <label for="user_id">User Id:</label>
        <input class="form-control" name="user_id" type="text" value="{{Auth::user()->id}}" id="user_id">
        <input class="form-control" name="identity_card" type="text" value="12334455677" id="user_id">
    </div>
    <!-- Month End Field -->
    <div class="form-group col-sm-6" style="display:none;">
       <strong></strong> {!! Form::label('month_end', 'Month End:') !!}
        {!! Form::text('month_end', null, ['class' => 'form-control']) !!}
        <input class="form-control" name="month_end" type="text" value="{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }}" id="month">
        <input class="form-control" name="enseignant_id" type="text" value="{{$enseignant->id }}" id="month">

    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('mensuualiteEnseignants.index') }}" class="btn btn-secondary">Annuler</a>
    </div>
</div>

{!! Form::close() !!}