<div class="table-responsive-sm">
    <table class="table table-striped" id="mensuualiteEnseignants-table">
        <thead>
            <tr>
        <th>Annee Scolaire</th>
        <th>Type Paiement</th>
        <th>Montant</th>
        <th>Mois</th>
        <th>Payeur</th>
        <th>Bulletin</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paiement_month as $mensuualiteEnseignant)
            <tr>
            <td>{{ $mensuualiteEnseignant->annee_scolaire->name }}</td>
            <td>{{ $mensuualiteEnseignant->type_paiement->name }}</td>
            <td><span  class="badge badge-danger">{{$mensuualiteEnseignant->montant }}</span></td>
            <td><span class="badge badge-warning">{{ $mensuualiteEnseignant->month }}</span></td>
            <td><span class="badge badge-info">{{ $mensuualiteEnseignant->user->name }}</span></td>
            <td><a href="{{ route('enseignant.recu_paiement', [$mensuualiteEnseignant->slug]) }}"  target="blank" class='btn btn-ghost-success'><i class="fa fa-print"></i></a><td>
                    {!! Form::open(['route' => ['mensuualiteEnseignants.destroy', $mensuualiteEnseignant->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('mensuualiteEnseignants.show', [$mensuualiteEnseignant->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a> -->
                        <a href="{{ route('mensuualiteEnseignants.edit', [$mensuualiteEnseignant->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>