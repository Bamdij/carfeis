<!-- Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    <strong>{!! Form::label('name', 'Categorie Dépense:') !!}</strong>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categoryDepenses.index') }}" class="btn btn-secondary">Annuler</a>
</div>
