<!-- Num Sourate Field -->
<div class="form-group">
    {!! Form::label('num_sourate', 'Num Sourate:') !!}
    <p>{{ $sourate->num_sourate }}</p>
</div>

<!-- Libelle Sourate Fr Field -->
<div class="form-group">
    {!! Form::label('libelle_sourate_fr', 'Libelle Sourate Fr:') !!}
    <p>{{ $sourate->libelle_sourate_fr }}</p>
</div>

<!-- Libelle Sourate Ar Field -->
<div class="form-group">
    {!! Form::label('libelle_sourate_ar', 'Libelle Sourate Ar:') !!}
    <p>{{ $sourate->libelle_sourate_ar }}</p>
</div>

<!-- Nbre Verset Field -->
<div class="form-group">
    {!! Form::label('nbre_verset', 'Nbre Verset:') !!}
    <p>{{ $sourate->nbre_verset }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $sourate->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $sourate->updated_at }}</p>
</div>

