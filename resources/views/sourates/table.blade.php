<div class="table-responsive-sm">
    <table class="table table-striped" id="sourates-table">
        <thead>
            <tr>
                <th>Num Sourate</th>
        <th>Libelle Sourate Fr</th>
        <th>Libelle Sourate Ar</th>
        <th>Nbre Verset</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sourates as $sourate)
            <tr>
                <td>{{ $sourate->num_sourate }}</td>
            <td>{{ $sourate->libelle_sourate_fr }}</td>
            <td>{{ $sourate->libelle_sourate_ar }}</td>
            <td>{{ $sourate->nbre_verset }}</td>
                <td>
                    {!! Form::open(['route' => ['sourates.destroy', $sourate->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('sourates.show', [$sourate->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('sourates.edit', [$sourate->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>