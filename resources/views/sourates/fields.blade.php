<!-- Num Sourate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_sourate', 'Num Sourate:') !!}
    {!! Form::text('num_sourate', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Sourate Fr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_sourate_fr', 'Libelle Sourate Fr:') !!}
    {!! Form::text('libelle_sourate_fr', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Sourate Ar Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_sourate_ar', 'Libelle Sourate Ar:') !!}
    {!! Form::text('libelle_sourate_ar', null, ['class' => 'form-control']) !!}
</div>

<!-- Nbre Verset Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nbre_verset', 'Nbre Verset:') !!}
    {!! Form::text('nbre_verset', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sourates.index') }}" class="btn btn-secondary">Cancel</a>
</div>
