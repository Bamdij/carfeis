@extends('layouts.app')

@section('content')
    <div class="container-fluid py-5">
        <div class="animated fadeIn">
             @include('flash::message')
            <div class="col-lg-12">
                <div class="row statistics px-md-2 mx-0">
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-user-follow add-text text-primary "></i> &nbsp; 
                          <div class="ml-auto">
                            <p class="admin-dsb">Eleves</p>
                            <h4 class="chiffre">{{$eleve}}</h4>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-people add-text text-primary "></i> &nbsp;
                          <div class="ml-auto">
                            <p class="title admin-dsb">Enseignants</p>
                            <h4 class="chiffre">{{$enseignant}}</h4>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-md-4 px-sm-2 mb-3">
                      <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-bank add-text text-primary "></i> &nbsp;
                          <div class="ml-auto">
                            <p class="title admin-dsb">Administrateurs</p>
                            <h4 class="chiffre">{{$user}}</h4>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                </div>
                <div class="container-fluid mx-auto pt-3">
                    <div class="chart-width">
                        <div class="panel-body shadow p-3 mb-5 bg-white rounded labeltv-card">
                            <canvas id="myChart" height="280"  width="800"></canvas>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
var ctx1 = document.getElementById('myChart').getContext('2d');
var progress = document.getElementById('animationProgress');

var myData = [
   @foreach($annee_scolaire as $analis)
     '{{ $analis['count']}}', //remeber to put comma (,) at end for array
   @endforeach
]

var myWeek = [
  @foreach($annee_scolaire as $analis)
     '{{ $analis['name']}}', //remeber to put comma (,) at end for array
   @endforeach
   
]

var chart = new Chart(ctx1, {
    // The type of chart we want to create
    type: 'bar',
    
    // The data for our dataset
    data: {
        labels: myWeek,
        datasets: [{
            label: 'Repartition des éléves GRH par Année ',
            backgroundColor: 'rgb(246, 227, 184)',
            borderColor: '#F9CA24',
            colors: ["#F9CA24"],
            data: myData,
           
        }],
        
    },

    // qsccgg  
    plotOptions: {
                bar: {
                    barHeight: '100%',
                    distributed: true,
                    dataLabels: {
                        // position: 'bottom'
                    },
                    
                }
            },
            
            colors: ["#F9CA24"],
            fill: {
                colors: ['rgba(248, 153, 10, 0.02) 96.74%)'],
            },
            zoom: {
                enabled: true
            },
            grid: {
      show: true,
      
      xaxis: {
        lines: {
          show: true
        }
      },
      yaxis: {
        lines: {
          show: true
        }
      }
    },
    

    // Configuration options go here
    options: {
        zoom: {
                enabled: true
            },
            marker: {
                show: true
            },
    },
});

</script>
@endpush


