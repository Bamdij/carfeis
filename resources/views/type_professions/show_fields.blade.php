<!-- Name Field -->
<div class="row">
<div class="form-group  col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $typeProfession->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $typeProfession->slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $typeProfession->description }}</p>
</div>

<!-- Created At Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $typeProfession->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $typeProfession->updated_at }}</p>
</div>
</div>

