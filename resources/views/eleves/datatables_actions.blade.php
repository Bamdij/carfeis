{!! Form::open(['route' => ['eleves.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('eleves.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('eleves.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
    <!-- <a data-toggle="modal" data-target=".bd-example-modal-lg" id="is_add_eleve" data-id="{{$id}}"class='btn btn-ghost-info'>
       <i class="fa fa-graduation-cap"></i>
    </a> -->
    <hr>
    <a href="{{route('infor_paiement.mensual',$slug)}}" class='btn btn-ghost-warning'>
    <i class="fa fa-cc-discover" style="color:orange;"></i>
    </a>
    @if($is_added == false )
    <a href="{{ route('getclasse.classe', $id) }}"  class='btn btn-ghost-info'>
       <i class="fa fa-graduation-cap"></i>
    </a>
    @endif
   
</div>
{!! Form::close() !!}

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div id="eleve_text"></div>

       </div>
    </div>
  </div>
</div>