<!DOCTYPE html>

<html lang="fr" class="light">
<!-- BEGIN: Head -->

<head>
    <meta charset="utf-8">
    <link rel="icon" href="{!! asset('images/logoCarfeisEd.png') !!}" type = "image/carfeis">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Icewall admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Icewall Admin Template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="LEFT4CODE">
    <title>Carfeis Carte</title>
    <!-- BEGIN: CSS Assets-->
    <!-- END: CSS Assets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
          .img-bg{  width: 1031px;
            height: 484px;
            /* margin-left: -200px ; */
            background-image: url('/images/fondCarte.jpg');
            background-repeat: no-repeat;
            background-size: contain;
            background-position: bottom;
            /* background-color: #93ea93; */
            opacity: 0.9;
        }
        .well{ min-height: 20px;
                padding: 127px !important;
                margin-bottom: 20px;
                background-color: #f5f5f505;
                /* border: 1px solid #e3e3e3; */
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
                box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
            }
        
    </style>
 </head>
<body onload="window.print()">
<br><br>
<div class="container" >
    <div class="row ">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-4  img-bg">
        <br><br>
        <div class="row">
                <div class="col-xs-6 col-sm-3 col-md-6 p-3">
                  <br><br>
                   <p>
                  <strong>Prenom:</strong>
                  <em>{{$eleve->first_name}}</em> 
                   </p>
                   <p>
                  <strong>Nom:</strong>
                  <em>{{$eleve->last_name}}</em> 
                   </p>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                    <strong><em></em></strong>
                    </p>
                    <p class="text-danger">
                        <img src="{{$eleve->avatar_eleve}}" alt="" width="90" heigth="90">
                    </p>
                    <p class="">
                      <strong>Numero:</strong><em>{{$eleve->numero}}</em> 
                   </p>
                   <p>
                  <strong>Année:</strong><br>
                  <em>{{$annee->name}}</em> 
                   </p>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<!------ Include the above in your HEAD tag ---------->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
</body>
</html>


