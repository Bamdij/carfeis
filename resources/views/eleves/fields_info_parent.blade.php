<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS SUR LE PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_pere', 'Prenom & Nom du  Pere:') !!}
    <input class="form-control" name="name_pere" required value="{{$infoparent->name_pere}}" type="text" id="name_pere">
</div>

<!-- Phone Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_pere', 'telephone du  Pere:') !!}
    <input class="form-control" name="phone_pere" value="{{$infoparent->phone_pere}}"required  type="text" id="phone_pere">
</div>

<!-- Email Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_pere', 'Email Pere:') !!}
    <input class="form-control" name="name_mere" required value="{{$infoparent->email_pere}}" type="text" id="email_pere">
</div>
<!-- Sexe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_id', 'Profession:') !!}
    <select name="type_profession_id" id="type_profession_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $type)
        @if(!empty($infoparent->type_profession_id))
        @if($type->id === $infoparent->type_profession_id)
        <option value="{{$type->id}}" selected>{{$type->name}}</option>
        @endif
        @else
        <option value="{{$type->id}}" >{{$type->name}}</option>
        @endif
    @endforeach
</select>
</div>
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS DE LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<!-- Name Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_mere', 'Prenom & Nom de la Mere:') !!}
    <input class="form-control" name="name_mere" value="{{$infoparent->name_mere}}" type="text" id="name_mere">
</div>

<!-- Phone Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_mere', 'Telephone de la Mere:') !!}
    <input class="form-control" name="name_tuteur" value="{{$infoparent->phone_mere}}" type="text" id="phone_mere">
</div>

<!-- Email Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_mere', 'Email Mere:') !!}
    <input class="form-control" name="name_tuteur" value="{{$infoparent->email_mere}}" type="text" id="email_mere">
</div>
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_mere_id', 'Profession:') !!}
    <select name="type_profession_mere_id" id="type_profession_mere_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $type)
        @if(!empty($infoparent->type_profession_mere_id))
        @if($type->id === $infoparent->type_profession_mere_id)
        <option value="{{$type->id}}" selected>{{$type->name}}</option>
        @endif
        @else
        <option value="{{$type->id}}" >{{$type->name}}</option>
        @endif
    @endforeach
</select>
</div>
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS SUR LE TUTEUR</h3>
    </div>
</div><!--/.row-->
<!-- Name Tuteur Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_tuteur', 'Prenom & Nom du Tuteur:') !!}
    <input class="form-control" name="name_tuteur" value="{{$infoparent->name_tuteur}}" type="text" id="name_tuteur">
</div>

<!-- Phone Tuteur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_tuteur', 'Telephone Tuteur:') !!}
    <input class="form-control" name="phone_tuteur" value="{{$infoparent->phone_tuteur}}" type="text" id="phone_tuteur">
</div>

<!-- Email Tuteur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_tuteur', 'Email Tuteur:') !!}
    <input class="form-control" name="email_tuteur" value="{{$infoparent->email_tuteur}}" type="text" id="email_tuteur">
</div>

<!-- Contact Urgence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_urgence', 'Contact Urgence:') !!}
    <input class="form-control" name="contact_urgence" value="{{$infoparent->contact_urgence}}" type="text" id="contact_urgence">

</div>
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_mere_id', 'Profession:') !!}
    <select name="type_profession_tuteur_id" id="type_profession_tuteur_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $type)
        <option value="{{$type->id}}" selected>{{$type->name}}</option>
    @endforeach
</select>
<div class="form-group col-sm-6" style="display:none">
    <label for="name_pere">eleve</label>
    <input class="form-control" value="{{$eleve_id->id}}" name="eleve_id" type="text" id="name_pere">
</div>
</div>
<!-- Submit Field -->
 <div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eleves.index') }}" class="btn btn-secondary">Annuler</a>
</div>