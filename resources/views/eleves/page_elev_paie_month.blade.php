<div class="table-responsive-sm">
    <table class="table table-striped" id="paiementEleves-table">
        <thead>
            <tr>
                <th>Eleve Id</th>
                <th>Frais Inscription Id</th>
                <th>Annee Scolaire Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paiementEleves as $paiementEleve)
            <tr>
                <td>{{ $paiementEleve->eleve_id }}</td>
                <td>{{ $paiementEleve->frais_inscription_id }}</td>
                <td>{{ $paiementEleve->annee_scolaire_id }}</td>
            <td>
                    {!! Form::open(['route' => ['paiementEleves.destroy', $paiementEleve->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('paiementEleves.show', [$paiementEleve->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('paiementEleves.edit', [$paiementEleve->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>