<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'Prénom Eleve') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Nom eleve:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Date Naissance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_naissance', 'Date Naissance:') !!}
    <input class="form-control"  name="date_naissance" type="text" value="{{$eleve->date_naissance}}" id="date_naissance">

</div>
@push('scripts')
   <script type="text/javascript">
           $('#date_naissance').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
<!-- Date Naissance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}
    {!! Form::text('lieu_naissance', null, ['class' => 'form-control','required'=> true]) !!}
</div>
<!-- Sexe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sexe', 'Sexe:') !!}
    <select name="sexe" id="sexe" class="form-control">
    @if($eleve->sexe =="Masculin")
    <option value="Masculin" selected>Masculin</option>
    <option value="Feminin">Non activé</option>
    @else 
    <option value="Masculin">Masculin</option>
    <option value="Feminin" selected>Feminin</option> 
    @endif
</select>
</div>

<!-- Groupe Sanguin Field -->
<div class="form-group col-sm-6">
    <strong>{!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}</strong>
    <select name="groupe_sanguin" id="groupe_sanguin" class="form-control"  >
    <option value="">choisir le groupe sanguin</option>
    <option value="O-">O-</option>
    <option value="O+" >O+</option>
    <option value="B-" >B-</option>
    <option value="B+" >B+</option>
    <option value="A-" >A-</option>
    <option value="A+" >A+</option>
    <option value="AB-" >AB-</option>
    <option value="AB+" >AB+</option>
    </select>
</div>

<!-- Is Malade Field -->
<div class="form-group col-sm-12">
    {!! Form::label('is_malade', 'L\'enfant souffre t-il d\'une maladie?:') !!}
    <div class="form-check form-check-inline">
    @if($eleve->is_malade ==true)
    <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio1" checked value="{{ intval(1) }}">
    @else   
     <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio1" value="{{ intval(1) }}">
    @endif
    <label class="form-check-label" for="inlineRadio1">OUI</label>
    </div>
    <div class="form-check form-check-inline">
    @if($eleve->is_malade ==false)
    <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio1" checked value="{{ intval(0) }}">
    @else   
     <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio1" value="{{ intval(0) }}">
    @endif
    <label class="form-check-label" for="inlineRadio2">Non</label>
    </div>
</div>

<!-- Quelle Maladie Field -->
<div class="form-group col-sm-12">
    {!! Form::label('quelle_maladie', 'Si oui laquelle:') !!}
    {!! Form::text('quelle_maladie', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Traitement Field -->
<div class="form-group col-sm-12">
    {!! Form::label('is_traitement', 'L\'enfant suit t-il un taitement?:') !!}
    <div class="form-check form-check-inline">
    @if($eleve->is_traitement ==true)
    <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio1" checked value="{{ intval(1) }}">
    @else   
     <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio1" value="{{ intval(1) }}">
    @endif
    <label class="form-check-label" for="inlineRadio1">OUI</label>
    </div>
    <div class="form-check form-check-inline">
    @if($eleve->is_traitement ==false)
    <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio1" checked value="{{ intval(0) }}">
    @else   
     <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio1" value="{{ intval(0) }}">
    @endif
    <label class="form-check-label" for="inlineRadio2">Non</label>
    </div>
</div>
<!-- Quel Traitement Field -->
<div class="form-group col-sm-12">
    {!! Form::label('quel_traitement', 'Si oui lequel:') !!}
    {!! Form::text('quel_traitement', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
    <label for="avatar_eleve">Photo:</label>
    <input class="form-control" name="avatar_eleve" type="file" id="avatar_eleve" >
</div>
</div>
<!-- Submit Field -->
 <div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eleves.index') }}" class="btn btn-secondary">Annuler</a>
</div> 
