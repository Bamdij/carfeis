<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INSCRIPTION ELEVE</h3>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control" required>
            <option value="">choisir Année scolaire</option>
            <option value="{{$annee_niveau->id}}">{{$annee_niveau->name}}</option>
        <select> 
    </div>  
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Choisir le Regime:</label></strong>
        <select name="regime_id" id="regime_id" class="form-control" required>
            <option value="">choisir le Regime</option>
            @foreach($regime as $prof_regime)
            <option value="{{$prof_regime->regime_id}}"><strong>{{$prof_regime->name}}- {{ $prof_regime->status }} - {{$prof_regime->mensualite}}</strong> FCFA</option>
            @endforeach
        <select> 
    </div> 
<div class="form-group col-sm-6">
    <strong>{!! Form::label('first_name', 'Prénom Eleve') !!}</strong>
    {!! Form::text('first_name', null, ['class' => 'form-control','required'=> true]) !!}
</div>
<!-- Last Name Field -->
<div class="form-group col-sm-6">
   <strong>{!! Form::label('last_name', 'Nom eleve:') !!}</strong> 
    {!! Form::text('last_name', null, ['class' => 'form-control','required'=> true]) !!}
</div>


<div class="form-group col-sm-6">
   <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong> 
    {!! Form::date('date_naissance', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<div class="form-group col-sm-6">
    <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
    {!! Form::text('lieu_naissance', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<div class="form-group col-sm-6">
    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
    <select name="sexe" id="access_admin" class="form-control" required>
    <option value="">choisir le sexe</option>
    <option value="Masculin">Masculin</option>
    <option value="Feminin" >Feminin</option>
</select>
</div>

<!-- Groupe Sanguin Field -->
<div class="form-group col-sm-6">
    <strong>{!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}</strong>
    <select name="groupe_sanguin" id="groupe_sanguin" class="form-control" >
    <option value="">choisir le groupe sanguin</option>
    <option value="O-">O-</option>
    <option value="O+" >O+</option>
    <option value="B-" >B-</option>
    <option value="B+" >B+</option>
    <option value="A-" >A-</option>
    <option value="A+" >A+</option>
    <option value="AB-" >AB-</option>
    <option value="AB+" >AB+</option>
    </select>
</div>

<!-- Is Malade Field -->
<div class="form-group col-sm-6">
    <strong>{!! Form::label('is_malade', 'L\'enfant souffre t-il d\'une maladie?:') !!}</strong><br>
    <div class="form-check form-check-inline" required>
    <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio1" value="{{ intval(1) }}">
    <label class="form-check-label" for="inlineRadio1">OUI</label>
    </div>
    <div class="form-check form-check-inline">
    <input class="form-check-input" type="radio" name="is_malade" id="inlineRadio2"value="{{ intval(0) }}">
    <label class="form-check-label" for="inlineRadio2">Non</label>
    </div>
</div>

<!-- Quelle Maladie Field -->
<div class="form-group col-sm-12">
    <strong>{!! Form::label('quelle_maladie', 'Si oui laquelle:') !!}</strong>
    {!! Form::text('quelle_maladie', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Traitement Field -->
<div class="form-group col-sm-12">
   <strong> {!! Form::label('is_traitement', 'L\'enfant suit t-il un taitement?:') !!}</strong>
    <div class="form-check form-check-inline">
    <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio1"value="{{ intval(1) }}">
    <label class="form-check-label" for="inlineRadio1">OUI</label>
    </div>
    <div class="form-check form-check-inline">
    <input class="form-check-input" type="radio" name="is_traitement" id="inlineRadio2" value="{{ intval(0) }}">
    <label class="form-check-label" for="inlineRadio2">Non</label>
    </div>
</div>

<!-- Quel Traitement Field -->
<div class="form-group col-sm-12">
   <strong>{!! Form::label('quel_traitement', 'Si oui lequel:') !!}</strong> 
    {!! Form::text('quel_traitement', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    <strong>{!! Form::label('description', 'Dites-nous pour quelle raison voulez-vous inscrire votre enfant dans notre etablissement ?') !!}</strong>
    {!! Form::textarea('descripttion', null, ['class' => 'form-control','required'=> true]) !!}
</div>
<div class="form-group col-sm-12">
   <strong><label for="avatar_eleve">Photo:</label></strong> 
    <input class="form-control" name="avatar_eleve" type="file" id="avatar_eleve" >
</div>
<input type="hidden" name="is_added" value="{{intval(0)}}">
</div>
<div class="container">
<div class="row" >
<div class="col-md-6 col-sm-12 offset-8" style="display: flex;">
    <!-- <input type="submit" value="Terminer" class="btn btn-primary btn-kal-grey mr-3" name="terminer"> -->
    <a href="{{ route('eleves.create') }}" class="btn btn-secondary btn-kal-grey mr-3">Annuler</a> 
    <input type="submit" value="Ajouter et Continuer"class="btn btn-primary btn-kal-blue" name="continuer">
</div>   
</div>
