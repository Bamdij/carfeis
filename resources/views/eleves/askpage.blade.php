@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('eleves.index') !!}">Eleve Classe</a>
      </li>
      <li class="breadcrumb-item active">Inscription Réinscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Eleve Classe</strong>
                            </div>
                            <div class="card-body">
                            <div class="mt-1">
                                <div class="col-md-12">
                                    <h3 class="form-section-title">INSCRIPTION ET REINSCRIPTION ELEVE</h3>
                                </div>
                            </div><!--/.row-->
                                <div class="row" id="close_search">
                                        <div class="form-group col-sm-6">
                                           <a href="{{route('eleve.new_inscription')}}"><input class="form-control" type="submit" id="new_inscription" name="numero"  value="INSCRIPTION" placeholder="Numero de la carte" required></a> 
                                        </div>
                                        <div class="form-group col-sm-6">
                                        <a href="{{route('eleve.new_reinscription')}}"> <input class="form-control" type="submit" value="REINSCRIPTION" id="reinscription"></a>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
