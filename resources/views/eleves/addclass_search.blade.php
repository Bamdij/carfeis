@extends('layouts.app')



@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('eleves.index') !!}">Reinscription Eleve</a>
      </li>
      <li class="breadcrumb-item active">Reinscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                        {{Session::get('fail')}}
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('eleves.create') }}" class="btn btn-light">RETOUR</a>
                            </div>
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <div class="card-body">
                                            <div class="">
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">REINSCRIRE {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
                                                    <p>{{ $eleve->first_name }}</p>
                                                </div>

                                                <!-- Last Name Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
                                                    <p>{{ $eleve->last_name }}</p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
                                                    <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
                                                    <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                                <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
                                                <p>{{$eleve->lieu_naissance}}</p>
                                                </div>
                                                <!-- Sexe Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
                                                    <p>{{ $eleve->sexe }}</p>
                                                </div>
                                            </div>
                                            @if(!$eleve_class_by_year->isEmpty())
                                                @include('eleves.table_classe')
                                                @endif 
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Ajouter l'eleve dans une classe</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <form  method="POST" action="#">
                                                @csrf
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
                                                        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control"  onchange="changeFunctionAnnee(this)" required>
                                                            <option value="">choisir Année scolaire</option>
                                                            <option value="{{$annee_niveau->id}}">{{$annee_niveau->name}}</option>
                                                        <select> 
                                                        <input type="hidden" id="eleve_id" value="{{$eleve->id}}">
                                                        <div id="annee_scolaire_choice_student"></div>
                                                    </div>  
                                                    <div class="form-group col-sm-6">
                                                        <strong><label for="recipient-name" class="col-form-label">Choisir le Regime:</label></strong>
                                                        <select name="regime_id" id="regime_id" class="form-control"  onchange="changeFunctionRegime(this)" required>
                                                            <option value="">choisir Regime</option>
                                                            @foreach($regime as $prof)
                                                            <option value="{{$prof->regime_id}}">{{$prof->name}}-{{$prof->status}}-{{$prof->mensualite}}</option>
                                                            @endforeach
                                                        <select> 
                                                        <div id="regime_eleve"></div>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
                                                        <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="changeFunction(this)" required>
                                                            <option value="">choisir le type enseignement</option>
                                                            @foreach($type_enseig as $prof)
                                                            <option value="{{$prof->id}}">{{$prof->name}}</option>
                                                            @endforeach
                                                        <select> 
                                                        <input type="hidden" id="eleve_id" value="{{$eleve->id}}">
                                                        <input type="hidden" id="eleve_slug" value="{{$eleve->slug}}">
                                                        <div id="niv_for_elev"></div>
                                                        <div id="teach_niv_for_elev"></div>

                                                    </div>  
                                                    <div class="form-group col-sm-6">
                                                    <div id="eleve_text"></div>
                                                    <div class="niv_id" ></div>
                                                    </div>
                                                    <div class="form-group col-sm-6" id="hidden_div">
                                                        <div id="eleve_class"></div>
                                                        <div id="class_id" ></div>
                                                    </div>
                                                    <div class="form-group col-sm-6" id="hi_div">
                                                        <div id="teacher_level"></div>
                                                        <div id="teacher_class_choice_student"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-12" id="elev_franco_div" style="display:none">
                                                    <input class="btn btn-primary" type="submit" value="Réinscrire" id="eleve_french_added_div"  >
                                                    <a href="{{ route('eleves.create') }}" class="btn btn-secondary">Annuler</a>
                                                </div>
                                                <div class="form-group col-sm-12" id="eleve_arabe_div" style="display:none" >
                                                    <input class="btn btn-primary" type="submit" value="Réinscrire" id="eleve_arab_added_div">
                                                    <a href="{{ route('eleves.create') }}" class="btn btn-secondary">Annuler</a>
                                                </div>
                                            </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection