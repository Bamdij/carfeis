@extends('layouts.app')



@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('eleves.index') }}">Paiment Eleve</a>
      </li>
      <li class="breadcrumb-item active">Paiement Inscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                @include('flash::message')

                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                        {{Session::get('fail')}}
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('eleves.index') }}" class="btn btn-light">RETOUR</a>
                            </div>
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <div class="card-body">
                                            <div class="">
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement Mensuel {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
                                                    <p>{{ $eleve->first_name }}</p>
                                                </div>
                                                <!-- Formulaire test -->
                                                
                                                <!-- End form -->

                                                <!-- Last Name Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
                                                    <p>{{ $eleve->last_name }}</p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
                                                    <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
                                                    <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                                <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
                                                <p>{{$eleve->lieu_naissance}}</p>
                                                </div>
                                                <!-- Sexe Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
                                                    <p>{{ $eleve->sexe }}</p>
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            @include('eleves.test_no_year')
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Information</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="alert alert-warning" role="alert">
                                            Eleve no inscrit pour cette Année Scolaire.
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection