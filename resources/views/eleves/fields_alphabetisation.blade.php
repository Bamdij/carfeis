<!-- Name Alphabetisation Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_alphabetisation', 'Alphabetisation:') !!}
    <input class="form-control" value="{{$alphbetisation->name_alphabetisation}}" name="name_alphabetisation"  type="text" id="name_alphabetisation">
</div>

<!-- Daara Frequente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('daara_frequente', 'Daara Frequente:') !!}
    <input class="form-control" name="daara_frequente"  value="{{$alphbetisation->daara_frequente}}" type="text" id="daara_frequente">

</div>

<!-- Inteligence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inteligence', 'Intelligence:') !!}
    <input class="form-control" name="inteligence"  value="{{$alphbetisation->inteligence}}"  type="text" id="inteligence">
</div>

<!-- Person Inscript Field -->
<div class="form-group col-sm-6">
    {!! Form::label('person_inscript', 'Personne Inscripteur:') !!}
    <input class="form-control" name="person_inscript"  value="{{$alphbetisation->person_inscript}}" type="text" id="person_inscript">
</div>

<!-- Cap Finance Resp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cap_finance_resp', 'Capacité Financier Resp:') !!}
    <input class="form-control" name="cap_finance_resp"  value="{{$alphbetisation->cap_finance_resp}}" type="text" id="cap_finance_resp">

</div>

<!-- Date Inscription Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_inscription', 'Date Inscription:') !!}
    <input class="form-control" name="date_inscription" value="{{$alphbetisation->date_inscription}}" type="text" id="date_inscription">

</div>
<div class="form-group col-sm-6" style="display:none">
    <label for="name_pere">eleve</label>
    <input class="form-control" value="{{$eleve_id->id}}" name="eleve_id" type="text" id="name_pere">
</div>
@push('scripts')
   <script type="text/javascript">
           $('#date_inscription').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eleves.index') }}" class="btn btn-secondary">Annuler</a>
</div>
