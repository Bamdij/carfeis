@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('eleves.index') !!}">Eleve</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             @include('flash::message')
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-edit fa-lg"></i>
                            <strong>Edit Eleve</strong>
                        </div>
                        <div class="card-body">
                            {!! Form::model($eleve, ['route' => ['eleves.update', $eleve->id], 'method' => 'patch','files'=>'true']) !!}

                            @include('eleves.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-edit fa-lg"></i>
                            <strong>Edit Eleve Parent & Tuteur</strong>
                        </div>
                        @if(!empty($infoparent))
                        <div class="card-body">
                            {!! Form::model($eleve, ['route' => ['infoParents.update', $infoparent->id], 'method' => 'patch']) !!}
                            @include('eleves.fields_info_parent')
                            {!! Form::close() !!}
                        </div>
                        @else
                        <div class="card-body">
                            {!! Form::open(['route' => 'infoParents.store','files'=>'true']) !!}
                            @include('eleves.fields1')
                            {!! Form::close() !!}
                        </div>
                        @endif
                    </div>
                </div>
                </div>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-edit fa-lg"></i>
                            <strong>Edit Eleve Alphabetisation</strong>
                        </div>
                        @if(!empty($alphbetisation))
                        <div class="card-body">
                            {!! Form::model($eleve, ['route' => ['alphabetisations.update',$alphbetisation->id], 'method' => 'patch']) !!}

                            @include('eleves.fields_alphabetisation')

                            {!! Form::close() !!}
                        </div>
                        @else
                        <div class="card-body">
                        {!! Form::open(['route' => 'alphabetisations.store','files'=>'true']) !!}

                            @include('eleves.fields2')

                            {!! Form::close() !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection