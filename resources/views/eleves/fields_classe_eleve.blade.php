<form  method="POST" action="#">
    @csrf
    <div class="row"> 
        <div class="form-group col-sm-6">
            <strong><label for="recipient-name" class="col-form-label">Type Enseignement:</label></strong>
            <select name="type_enseignant_id" id="type_enseignant_id" class="form-control"  onchange="changeFunction(this)" required>
                <option value="">choisir le type enseignement</option>
                @foreach($type_enseig as $prof)
                <option value="{{$prof->id}}">{{$prof->name}}</option>
                @endforeach
            <select> 
            <input type="hidden" id="eleve_id" value="{{$eleve->id}}">
            <input type="hidden" id="eleve_slug" value="{{$eleve->slug}}">
            <div id="niv_for_elev"></div>
            <div id="teach_niv_for_elev"></div>

        </div>  
        <div class="form-group col-sm-6">
        <div id="eleve_text"></div>
        <div class="niv_id" ></div>
        </div>
        <div class="form-group col-sm-6" id="hidden_div">
            <div id="eleve_class"></div>
            <div id="class_id" ></div>
        </div>
        <div class="form-group col-sm-6" id="hi_div">
            <div id="teacher_level"></div>
            <div id="teacher_class_choice_student"></div>
        </div>
    </div>
    <div class="form-group col-sm-12" id="elev_franco_div" style="display:none">
        <input class="btn btn-primary" type="submit" value="Terminer" id="add_eleve_french_added_div"  >
        <a href="{{ route('eleves.create') }}" class="btn btn-secondary">Annuler</a>
    </div>
    <div class="form-group col-sm-12" id="eleve_arabe_div" style="display:none" >
        <input class="btn btn-primary" type="submit" value="Terminer" id="add_eleve_arab_added_div">
        <a href="{{ route('eleves.create') }}" class="btn btn-secondary">Annuler</a>
    </div>
</form>