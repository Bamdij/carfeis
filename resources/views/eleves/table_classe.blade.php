<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">Classe Frequentées</h3>
    </div>
</div><!--/.row-->
@if(!$eleve_class_by_year->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="enseignantClasses-table">
        <thead>
            <tr>
                <th>Annee scolaire</th>
                <th>Classe</th>
                <th>Professeur</th>
            </tr>
        </thead>
        <tbody>
        @foreach($eleve_class_by_year  as $elev_class)
            <tr>
                <td>{{ $elev_class->annee_scolaire }}</td>
                <td>{{ $elev_class->classe}}</td>
            <td>{{ $elev_class->enseignant }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@else
<div class="alert alert-warning" role="alert">
  Aucune classe frequentée
</div>
@endif