
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Eleve</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                  <a href="{{route('eleve.get_all_paiement')}}" class="btn btn-light">RETOUR</a>
                             </div>
                             <div class="card-body">
                               <!-- Name Pere Field -->
                            <div class="mt-1">
                                <div class="col-md-12">
                                    <h3 class="form-section-title">Listes des Eleves qui ont Payé {{ $annee_niveau->name}}</h3>
                                </div>
                            </div><!--/.row-->
                            <div class="row">

                        <div class="table-responsive-sm">

                            <table class="table table-striped" id="evaluationMensuels-table">
                                <thead>
                                    <tr>
                                        <th>Prenom & Nom</th>
                                <th>Date et Lieu De Naissance</th>
                                <th>Regime</th>
                                <th>Mois De Paiement</th>
                                <th>Montant attandu</th>
                                <th>Montant Reçu</th>
                                <th>Observation</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($eleves as $eleve)
                                @if($eleve->status == "OUI")
                                    <tr>
                                        <td>  <a href="{{ route('eleves.show', $eleve->info->id) }}" style="color:#1c1e1e !important;">{{ $eleve->info->first_name }} {{ $eleve->info->last_name }}</a></td>
                                    <td>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}} - {{ $eleve->info->lieu_naissance }} </td>
                                    @if(!empty($eleve->eleve_regime))
                                    <td>
                                        @if($eleve->eleve_regime->status == "social")
                                        <span class="btn btn-danger">{{ $eleve->eleve_regime->name}} - {{ $eleve->eleve_regime->status}}</span> 
                                        @else
                                        <span class="btn btn-warning">{{ $eleve->eleve_regime->name}} - {{ $eleve->eleve_regime->status}}</span> 
                                        @endif
                                    </td>
                                    @else
                                    <td>No Information</td>
                                    @endif
                                    <td> 
                                    @foreach($eleve->elev_mont as $mensuel)
                                    {{ $mensuel->month }} -
                                    @endforeach
                                    </td>
                                    <td>
                                    <span class="btn btn-danger"> {{ $eleve->eleve_regime->mensualite}}  </span>

                                    </td>
                                    <td>
                                    @foreach($eleve->elev_mont as $mensuel)
                                        {{ $mensuel->mensualite }} -
                                        @endforeach
                                    </td>
                                    <td>
                                    @foreach($eleve->elev_mont as $mensuel)
                                    {{ $mensuel->observation }} -
                                    @endforeach
                                    </td>   
                                            
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                                </div>
                                </div>
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
