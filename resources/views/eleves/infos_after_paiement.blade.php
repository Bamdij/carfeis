@extends('layouts.app')



@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ route('eleves.index') }}">Paiment Eleve</a>
      </li>
      <li class="breadcrumb-item active">Paiement Inscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                @include('flash::message')

                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                        {{Session::get('fail')}}
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('eleves.index') }}" class="btn btn-light">RETOUR</a>
                            </div>
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <div class="card-body">
                                            <div class="">
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement Mensuel {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
                                                </div>
                                            </div><!--/.row-->
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
                                                    <p>{{ $eleve->first_name }}</p>
                                                </div>
                                                <!-- Formulaire test -->
                                                
                                                <!-- End form -->

                                                <!-- Last Name Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
                                                    <p>{{ $eleve->last_name }}</p>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
                                                    <img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
                                                    <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
                                                </div>
                                                <!-- Date Naissance Field -->
                                                <div class="form-group col-sm-4">
                                                                <strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
                                                <p>{{$eleve->lieu_naissance}}</p>
                                                </div>
                                                <!-- Sexe Field -->
                                                <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
                                                    <p>{{ $eleve->sexe }}</p>
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            @if(!empty($status_paiement))
                                            @include('eleves.test_paiement')
                                            <div class="mt-1">
                                                <div class="col-md-12">
                                                    <h3 class="form-section-title">Ajouter un paiement</h3>
                                                </div>
                                            </div><!--/.row-->
                                            @if($status_paiement->is_confirme == false)
                                            <form method="post">
                                                <div class="row">
                                                    <div class="form-group col-sm-2">
                                                        <strong>{!! Form::label('Frais','Inscription:') !!}</strong><br>
                                                    <span class="badge badge-danger">Non VALIDER</span> 
                                                    </div>
                                                    <div class="form-group col-sm-6" >
                                                    <strong>{!! Form::label('Frais','Valider Ici:') !!}</strong><br>
                                                        <select name="is_confirme" id="is_confirme" class="form-control">
                                                            <option value="" >Changer le status</option>
                                                            <option value="{{ intval(0) }}" >Non Valider</option>
                                                            <option value="{{ intval(1) }}" selected>Valider</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                    <strong>{!! Form::label('Frais','Confirmer:') !!}</strong><br>
                                                        <input class="btn btn-primary" type="submit" id="change_status_paiement" value="VALIDER" >
                                                    </div>
                                                    <input type="hidden" id="paiement_eleve" value="{{$status_paiement->id}}">
                                                </div>
                                            </form>
                                            @else
                                            <div class="row">
                                            <div class="form-group col-sm-4">
                                                <strong>{!! Form::label('regime_id', 'Regime:') !!}</strong>
                                                <p> {{$eleve_regime->name}} </p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <strong>{!! Form::label('mensualite_by_month', 'Mensualité:') !!}</strong>
                                                <p> {{$eleve_regime->mensualite}} </p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <strong>{!! Form::label('mensualite_by_month', 'Mois:') !!}</strong>
                                                <p  class="badge badge-warning">{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }} </p>
                                            </div>
                                            </div>
                                            <form  method="POST" action="">
                                                @csrf
                                                    <!-- <div class="row dynamic_taille_field_container">
                                                    <div class="col-md-2 offset-md-10 mensualite_added mt-3 mb-4">
                                                        <button class="add_month_field form-control" name="month[]" style="background-color:#3e0f05 ;">
                                                            <span style="font-size:16px; font-weight:bold;">+</span>
                                                        </button>
                                                    </div> -->
                                                </div>
                                                <div class="container top">
                                                    <div class="row month-container" style="display:flex;">
                                                        <div class="col-md-2 mt-3">
                                                             <strong>{!! Form::label('mensualite_by_month', 'Choisir:') !!}</strong>
                                                            <select name="s_number_month1" id="number_month" class="form-control" onchange="changeFunctionChangeMonthChoice(this)" required>
                                                                 <option value="" >Choisir le nombre de Mois</option>
                                                                <option value="1" >1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            <select>
                                                        </div>
                                                        <div class="col-md-4 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Montant Attendu:') !!}</strong>
                                                            <input class="form-control" id="close" type="text" value="{{$eleve_regime->mensualite}}" readonly="readonly">
                                                            <div id="montant_getting"></div>
                                                        </div>
                                                        <div class="col-md-5 mt-3" style="display:none">
                                                        <strong>{!! Form::label('periode_debut', 'Periode De:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_debut" placeholder="Noter les mois ex:janvier-fevrier" name="periode_debut" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" style="display:none;">
                                                        <strong>{!! Form::label('periode_end', 'A date de fin:') !!}</strong>

                                                            <input class="form-control" type="date" id="periode_end" placeholder="Noter les mois ex:janvier-fevrier" name="periode_end" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" style="display:none;">
                                                        <strong>{!! Form::label('mensualite_by_month', 'Mois:') !!}</strong>

                                                            <input class="form-control" type="text" id="s_month" placeholder="Noter les mois ex:janvier-fevrier" name="s_month1" value="{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }}" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('montant_attendu', 'Montant Réçu:') !!}</strong>
                                                            <input class="form-control" type="number" placeholder="Montant recu" id="mensualite" name="s_mensualite1" required>
                                                        </div>
                                                        <div class="col-md-6 mt-3" >
                                                        <strong>{!! Form::label('mensualite_by_month', 'Observation:') !!}</strong>

                                                            <input class="form-control" type="text" id="observation" placeholder="Observation mois " name="s_observation1" required>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="montant" value="{{$eleve_regime->mensualite}}">
                                                    <input type="hidden" id="paiement_eleve_id" name="paiement_eleve_id" value="{{$status_paiement->id}}" >
                                                    <input  type="hidden" id="eleve" name="eleve_id" value="{{$status_paiement->eleve_id}}" >
                                                    <input  type="hidden" id="eleve_slug" name="eleve_slug" value="{{$eleve->slug}}" >
                                                    <input  type="hidden" id="annee_scolaire" name="annee_scolaire_id" value="{{$status_paiement->annee_scolaire_id}}" >
                                                    <input type="hidden" name="user_id" id="user" value="{{Auth::user()->id}}">
                                                    
                                                </div>
                                                <br><br>
                                                <div class="row">
                                                <strong> {!! Form::label('Mode De Paiement', 'CHOISIR LE MODE DE PAIEMENT :') !!}</strong>
                                                    <div class="form-group col-sm-6">
                                                        <div class="icon-container">
                                                            <i class="fa fa-cc-visa" style="color:navy;"></i>
                                                            <i class="fa fa-cc-amex" style="color:blue;"></i>
                                                            <i class="fa fa-cc-mastercard" style="color:red;"></i>
                                                            <i class="fa fa-cc-discover" style="color:orange;"></i>
                                                            <img src="{{asset('images/bouton-02.png')}}"width="30%"  height="auto" alt="paydunya">	    
                                                        </div>    
                                                    </div>
                                                    <div class="" style="display:none;" >
                                                        <label class="radio-inline"><span class="btn btn-info" >Paiement En Espece  <input type="radio" name="optradio" id="opOne" value='first' checked></span></label>
                                                        <label class="radio-inline"><span class="btn btn-warning" >Paiement avec Orange Money Wave ...<input type="radio" name="optradio" id="opTwo" value='second'></span></label>
                                                    </div> 
                                                </div>
                                                <div id="optionOneDiv">
                                                    <div class="form-group col-sm-12 p-4" id="eleve_arabe_div" >
                                                        <input class="btn btn-primary" type="submit" id="post_paiement" value="Enregistrer" >
                                                        <a href="{{ route('paiment.eleve') }}" class="btn btn-secondary">Annuler</a>
                                                    </div>
                                                </div>
                                                <!-- <div id="optiontwoDiv">
                                                    <div class="form-group col-sm-12 p-4" id="eleve_arabe_div" >
                                                        <input class="btn btn-primary" type="submit" id="paydunya" value="Payer Maintenant" >
                                                        <a href="{{ route('paiment.eleve') }}" class="btn btn-secondary">Annuler</a>
                                                    </div>
                                                </div> -->
                                            </form>
                                            @endif
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                            Aucun paiement valider vous devez prendre le numéro de l'eleve et aller dans finance paiement puis effectuer un paiement d'abord pour pouvoir utiliser le paiement ici.
                                            </div>
                                             @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection