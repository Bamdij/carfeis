<div class="row">
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('annee', 'Année Scolaire:') !!}</strong>
        <p>{{ $frais_inscript->annee_scolaire->name }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('first_name', 'Prénom:') !!}</strong>
        <p>{{ $eleve->first_name }}</p>
    </div>
    <!-- Last Name Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
        <p>{{ $eleve->last_name }}</p>
    </div>
    <!-- Date Naissance Field -->
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
        <p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
    </div>
     <!-- Last Name Field -->
     <div class="form-group col-sm-4">
        <strong>{!! Form::label('regime_id', 'Regime:') !!}</strong>
        <p>{{ $regime->name }} {{ $regime->status }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('mensualite_by_month', 'Mensualité:') !!}</strong>
        <p>{{ $regime->mensualite }}</p>
    </div>
    <div class="form-group col-sm-4">
        <strong>{!! Form::label('Frais', 'Frais Inscription:') !!}</strong><br>
       <span class="badge badge-warning">{{ $frais_inscript->montant }}</span> 
    </div>
<!-- Frais Inscription Id Field -->
</div>
<div class="row">
<div class="form-group col-sm-6">
    <strong><label for="frais_inscription_id">Montant inscription :</label></strong>
    <input class="form-control" name="montant_inscription"  type="number" id="montant_inscription">
</div>
<div class="form-group col-sm-6">
    <strong><label for="frais_inscription_id">Observation :</label></strong>
    <input class="form-control" name="observation_inscription" placeholder="Ajouter une description si vous changez le montant" type="text" id="observation_inscription">
</div>
<input type="hidden" name="regime_id" value="{{$regime->id}}">
<input type="hidden" name="eleve_id" value="{{$eleve->id}}">
<input type="hidden" name="annee_scolaire_id" value="{{$frais_inscript->annee_scolaire_id}}">
<input type="hidden" name="frais_inscription_id" value="{{$frais_inscript->id}}">
<input type="hidden" id="user" name="user_id" value="{{Auth::user()->id}}">
</div>
<!-- <div class="form-group col-sm-12">
    {!! Form::submit('Terminer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('fraisInscriptions.index') }}" class="btn btn-secondary">Annuler</a>
</div> -->
<div class="container">
    <div class="row" >
        <div class="col-md-6 col-sm-12 offset-8" style="display: flex;">
            <!-- <input type="submit" value="Terminer" class="btn btn-primary btn-kal-grey mr-3" name="terminer"> -->
            <a href="{{ route('eleves.create') }}" class="btn btn-secondary btn-kal-grey mr-3">Annuler</a> 
            <input type="submit" value="Terminer"class="btn btn-primary btn-kal-blue" name="continuer">
        </div>   
    </div>
</div>