<div class="table-responsive-sm">
    <table class="table table-striped" id="evaluationMensuels-table">
        <thead>
            <tr>
                <th>Prenom & Nom</th>
        <th>Date et Lieu De Naissance</th>
        <th>Regime</th>
        <th>Mois De Paiement</th>
        <th>Montant attandu</th>
        <th>Montant Reçu</th>
        <th>Observation</th>

            </tr>
        </thead>
        <tbody>
        @foreach($eleves as $eleve)
            <tr>
                <td><a href="{{ route('eleves.show', $eleve->info->id) }}" style="color:#1c1e1e !important;" >{{ $eleve->info->first_name }} {{ $eleve->info->last_name }}</a></td>
            <td>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}} - {{ $eleve->info->lieu_naissance }} </td>
            @if(!empty($eleve->eleve_regime))
            <td>
                @if($eleve->eleve_regime->status == "social")
                <span class="btn btn-danger">{{ $eleve->eleve_regime->name}} - {{ $eleve->eleve_regime->status}}</span> 
                @else
                <span class="btn btn-warning">{{ $eleve->eleve_regime->name}} - {{ $eleve->eleve_regime->status}}</span> 
                @endif
            </td>
            @else
            <td>No Information</td>
            @endif
            @if($eleve->elev_mont == "Aucun Paiement")
            <td> <span class="badge badge-pill badge-danger">Non Payer</span></td>
            @if(!empty($eleve->eleve_regime))
            <td> <span class="btn btn-danger"> {{ $eleve->eleve_regime->mensualite}}  </span></td>
            @else
            <td>No Information</td>
            @endif
            <td><span class="badge badge-pill badge-danger">Non Payer </span></td>
            <td><span class="badge badge-pill badge-danger">Rien Reçu </span></td>
            @else
           
             <td> 
             @foreach($eleve->elev_mont as $mensuel)
             {{ $mensuel->month }} -
             @endforeach
            </td>
             <td>
             <span class="btn btn-warning"> {{ $eleve->eleve_regime->mensualite}}  </span>
            </td>
            <td>
             @foreach($eleve->elev_mont as $mensuel)
                 {{ $mensuel->mensualite }} -
                @endforeach
            </td>
            <td>
            @foreach($eleve->elev_mont as $mensuel)
            {{ $mensuel->observation }} -
              @endforeach
              </td>           
           @endif
        @endforeach
        </tbody>
    </table>
</div>




