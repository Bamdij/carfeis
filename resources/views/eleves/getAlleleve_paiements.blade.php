@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Eleve</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                  <a href="{{route('eleve.get_all_paiement')}}" class="btn btn-light">RETOUR</a>
                             </div>
                             <div class="card-body">
                                <div class="col-md-12 pull-right" style="padding:26px;">
                                    <strong><label for="recipient-name" class="col-form-label">Rechercher:</label></strong><br>
                                    <button type="button" class="btn btn-danger"><a href="{{route('eleve.get_paiement_non_paied','eleve_nonpayed')}}"  target="_blank" style="color:white;" >Liste des Eleves qui n'ont pas Payé</a></button>  
                                    <button type="button" class="btn btn-warning"> <a href="{{route('eleve.get_paiement_non_paied','one_month')}}"  target="_blank" style="color:white;">Liste des Eleves qui ont Payé 1 seul mois</a> </button>  
                                    <button type="button" class="btn btn-success"> <a href="{{route('eleve.get_paiement_non_paied','eleve_payed')}}"  target="_blank" style="color:white;">Liste des Eleves qui ont Payé</a> </button>  
                                </div>
                                <br><br>
                            @include('eleves.all_paiement')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection



