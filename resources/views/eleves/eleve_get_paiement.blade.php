@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Paiement Eleves</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             @if(Session::has('fail'))
                <div class="alert alert-danger">
                {{Session::get('fail')}}
                </div>
            @endif
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             PaiementEleves
                             <!-- <a class="pull-right" href=""><i class="fa fa-plus-square fa-lg"></i></a> -->
                         </div>
                         <div class="card-body">
                             @include('eleves.table_paiement')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

