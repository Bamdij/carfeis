<!-- Name Pere Field -->
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS SUR LE PERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_pere', 'Prenom & Nom du  Pere:') !!}
    {!! Form::text('name_pere', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Phone Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_pere', 'telephone du  Pere:') !!}
    {!! Form::text('phone_pere', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Pere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_pere', 'Email Pere:') !!}
    {!! Form::text('email_pere', null, ['class' => 'form-control']) !!}
</div>
<!-- Sexe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_id', 'Profession:') !!}
    <select name="type_profession_id" id="type_profession_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $prof)
    <option value="{{$prof->id}}">{{$prof->name}}</option>
    @endforeach
</select>
</div>
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS SUR LA MERE</h3>
    </div>
</div><!--/.row-->
<div class="row">
<!-- Name Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_mere', 'Prenom & Nom de la Mere:') !!}
    {!! Form::text('name_mere', null, ['class' => 'form-control','required'=> true]) !!}
</div>

<!-- Phone Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_mere', 'Telephone de la Mere:') !!}
    {!! Form::text('phone_mere', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Mere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_mere', 'Email Mere:') !!}
    {!! Form::text('email_mere', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('type_profession_mere_id', 'Profession:') !!}
    <select name="type_profession_mere_id" id="type_profession_mere_id" class="form-control" required>
    <option value="">choisir la Profession</option>
    @foreach($type_prof as $prof)
    <option value="{{$prof->id}}">{{$prof->name}}</option>
    @endforeach
</select>
</div>
</div>
<div class="mt-1">
    <div class="col-md-12">
        <h3 class="form-section-title">INFORMATIONS SUR LE TUTEUR</h3>
    </div>
</div><!--/.row-->
<!-- Name Tuteur Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name_tuteur', 'Prenom & Nom du Tuteur:') !!}
    {!! Form::text('name_tuteur', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Tuteur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_tuteur', 'Telephone Tuteur:') !!}
    {!! Form::text('phone_tuteur', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Tuteur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_tuteur', 'Email Tuteur:') !!}
    {!! Form::text('email_tuteur', null, ['class' => 'form-control']) !!}
</div>

<!-- Contact Urgence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_urgence', 'Contact Urgence:') !!}
    {!! Form::text('contact_urgence', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('lien_parent', 'Lien Parent:') !!}
    <select name="lien_parent_id" id="lien_parent" class="form-control" required>
    <option value="">choisir le Lien</option>
    @foreach($lien_parent as $prof)
    <option value="{{$prof->id}}">{{$prof->name}}</option>
    @endforeach
</select>
</div>
<div class="form-group col-sm-6" style="display:none">
    <label for="name_pere">eleve</label>
    <input class="form-control" value="{{$eleve_id->id}}" name="eleve_id" type="text" id="name_pere">
</div>
</div>
<!-- Submit Field -->
 <div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eleves.index') }}" class="btn btn-secondary">Annuler</a>
</div>