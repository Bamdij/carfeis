@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="#">Frais Inscriptio</a>
      </li>
      <li class="breadcrumb-item active">Inscription Réinscription</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Frais Inscription</strong>
                            </div>
                            <div class="card-body">
                            <div class="mt-1">
                                <div class="col-md-12">
                                    <h3 class="form-section-title">Frais Inscription </h3>
                                </div>
                            </div><!--/.row-->
                            {!! Form::open(['route' => 'paiementEleves.store']) !!}
                               @include('eleves.field_frais')
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection