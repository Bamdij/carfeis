<!-- Name Field -->
<div class="row">
<div class="form-group  col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $lienParent->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $lienParent->slug }}</p>
</div>

<!-- Created At Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $lienParent->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $lienParent->updated_at }}</p>
</div>
</div>
