<!-- Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('lienParents.index') }}" class="btn btn-secondary">Annuler</a>
</div>
</div>
