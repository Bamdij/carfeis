@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Eleve</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">                             </div>
                             <div class="card-body">
							 <div class="mt-1">
								<div class="col-md-12">
									<h3 class="form-section-title">Paiement Mensuel {{ $eleve->first_name }} {{ $eleve->last_name }}</h3>
								</div>
							</div><!--/.row-->
							<div class="row">
								<div class="form-group col-sm-4">
									<strong>{!! Form::label('first_name', 'Prenom:') !!}</strong>
									<p>{{ $eleve->first_name }}</p>
								</div>

								<!-- Last Name Field -->
								<div class="form-group col-sm-4">
									<strong>{!! Form::label('last_name', 'Nom:') !!}</strong>
									<p>{{ $eleve->last_name }}</p>
								</div>
								<div class="form-group col-sm-4">
									<!-- <strong>{!! Form::label('date_naissance', 'Photo:') !!}</strong> -->
									<img src="{{asset($eleve->avatar_eleve)}}" alt="{{ $eleve->first_name }}" width="50" heigth="50">    
								</div>
								<!-- Date Naissance Field -->
								<div class="form-group col-sm-4">
									<strong>{!! Form::label('date_naissance', 'Date Naissance:') !!}</strong>
									<p>{{Carbon\Carbon::parse( $eleve->date_naissance)->format('d/m/Y')}}</p>
								</div>
								<!-- Date Naissance Field -->
								<div class="form-group col-sm-4">
												<strong>{!! Form::label('lieu_naissance', 'Lieu de Naissance:') !!}</strong>
								<p>{{$eleve->lieu_naissance}}</p>
								</div>
								<!-- Sexe Field -->
								<div class="form-group col-sm-4">
									<strong>{!! Form::label('sexe', 'Sexe:') !!}</strong>
									<p>{{ $eleve->sexe }}</p>
								</div>
							</div>
							<div class="mt-1">
								<div class="col-md-12">
									<h3 class="form-section-title">Valider le Paiement Maintenant</h3>
								</div>
							</div><!--/.row-->
							 <div class="">
									@if(count($errors) > 0)
										<div class="" style="width:50%;margin:auto;" id="error_alert_container">
											<ul>
												@foreach($errors->all() as $error)
												<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
									<div class="row">
										<div class="form-group col-sm-8">
										<form method="POST" id="payment-form"  action="{{ route('sending.payment.infos') }}">
											{{ csrf_field() }}
											<div class="row">
											<div class="form-group col-sm-6">
											<input class="form-control" name="amount" type="text" value="{{$mensualite->mensualite}}" readonly="readonly"> 
												<input class="form-control" name="mensualite_id" type="hidden" value="{{$mensualite->id}}" readonly="readonly"> 
												<div id="paiemt"></div>
											</div> 
											<div class="form-group col-sm-3">
													<button class="btn btn-primary">Valider Le Paiement</button>
											</div>    					
											</div> 				
										</form>
										</div>
										<div class="form-group col-sm-4">
											{!! Form::open(['route' => ['delete.status_admin', $mensualite->id], 'method' => 'delete']) !!}
											{!! Form::button('Annuler et Retourner', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Voulez-vous annuler le paiement?')"]) !!}
											{!! Form::close() !!}
										</div>
									</div>
								</div>
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
	
@stop