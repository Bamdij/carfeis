@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('niveaux.index') !!}">Niveau</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             {!! Form::model($niveau, ['route' => ['niveaux.update', $niveau->id], 'method' => 'patch']) !!}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Niveau</strong>
                          </div>
                          <div class="card-body">
                              @include('niveaux.fields')

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                SousNiveaus
                                <a class="add_form_field1 pull-right"  name="s_niveau[]" href=""><i class="fa fa-plus-square fa-lg"></i></a>
                            </div>
                            <div class="card-body">
                                 @include('niveaux.s_table')
                                <div class="pull-right mr-3">
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('niveaux.index') }}" class="btn btn-secondary">Annuler</a>
                </div>
                </div>
                {!! Form::close() !!}

        </div>
    </div>
@endsection
