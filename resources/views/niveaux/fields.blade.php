@if(!empty($niveau))
<div class="row">
    <div class="form-group col-sm-6">
        <strong>{!! Form::label('type_enseignant_id', 'Type Enseignement:') !!}</strong> 
        <select name="type_enseignant_id" id="status" class="form-control" required>
            <option value="">--Choisir le type enseignement --</option>
            @foreach($type_enseig as $type)
                @if(!empty($niveau->type_enseignant))
                    @if($type->id === $niveau->type_enseignant->id)
                    <option value="{{$type->id}}" selected>{{$type->name}}</option>
                    @endif
                    @if($type->id != $niveau->type_enseignant->id)
                    <option value="{{$type->id}}" >{{$type->name}}</option>
                    @endif
                @else
                <option value="{{$type->id}}" >{{$type->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-6">
       <strong>{!! Form::label('name', 'Niveaux:') !!}</strong>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
@else
<section class="tailles p-4">
    <div class="row">
        <div class="form-group col-sm-6">
         <strong> {!! Form::label('type_enseignant_id', 'Type Enseignement:') !!}</strong>  
            <select name="type_enseignant_id" id="type_enseignant_id" class="form-control" required>
                <option value="">--Choisir le type Enseignement --</option>
                @foreach($type_enseig as $type)
                <option value="{{$type->id}}" >{{$type->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-6">
           <strong> {!! Form::label('name', 'Niveaux:') !!}</strong>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
        <div class="mt-1">
            <div class="col-md-12">
                <h3 class="form-section-title">Associer le niveau avec  des Classe</h3>
            </div>
        </div><!--/.row-->
        <div class="row dynamic_taille_field_container">
            <div class="col-md-2 offset-md-10 niveau_added mt-3 mb-4">
                <button class="add_form_field form-control" name="s_niveau[]" style="background-color:#3e0f05 ;">
                    <span style="font-size:16px; font-weight:bold;">+</span>
                </button>
            </div><!--/.col-md-6 -->
        </div>
        <div class="container top">
            <div class="row taille-container" style="display:flex;">
                <div class="col-md-6 mt-3">
                    <input class="form-control" type="text" placeholder="classe 1" name="s_niveau1" required>
                </div>
            </div>
        </div>
</section>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('niveaux.index') }}" class="btn btn-secondary">Annuler</a>
</div>
@endif
