<div class="table-responsive-sm">
    <table class="table table-striped" id="sousNiveaus-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($s_niveau as $sousNiveau)
            <tr>
                <td>{{ $sousNiveau->name }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('sousNiveaus.edit', [$sousNiveau->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row taille-container" style="display:flex;">

    </div>
</div>