@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('aproposCarfeis.index') !!}">Apropos Carfeis</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Apropos Carfeis</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($aproposCarfeis, ['route' => ['aproposCarfeis.update', $aproposCarfeis->id], 'method' => 'patch']) !!}

                              @include('apropos_carfeis.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor1').ckeditor();
    });
</script>
@endpush