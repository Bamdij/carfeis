@if(!empty($aproposCarfeis))
<div class="row">
    <div class="form-group col-sm-6">
    <label><strong>Titre :</strong></label>
        <input class="form-control" name="title" type="text" id="title" value="{!!$aproposCarfeis->title !!}" placeholder="Ajouter un titer" required>
    </div>
    <div class="form-group col-sm-12">
    <label><strong>Description :</strong></label>
    <textarea class="ckeditor form-control" name="description" value="{!!$aproposCarfeis->description !!}"></textarea>
    </div>
</div>
@else
<div class="row">
    <div class="form-group col-sm-6">
    <label><strong>Titre :</strong></label>
        <input class="form-control" name="title" type="text" id="title" placeholder="Ajouter un titer" required>
    </div>
    <div class="form-group col-sm-12">
    <label><strong>Description :</strong></label>
    <textarea class="ckeditor form-control" name="description"></textarea>
</div>
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('aproposCarfeis.index') }}" class="btn btn-secondary">Annuler</a>
</div>

