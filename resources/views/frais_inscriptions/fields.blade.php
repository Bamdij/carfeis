<!-- Regime Id Field -->
<div class="row">
@if(!empty($fraisInscription))
<div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control" required>
            <option value="">choisir Année scolaire</option>
            @foreach($annee_niveau as $prof)
                @if($prof->id == $fraisInscription->annee_scolaire_id)
                <option value="{{ $fraisInscription->annee_scolaire_id}}" selected>{{$prof->name}}</option>
                @else
                <option value="{{$prof->id}}">{{$prof->name}}</option>
                @endif
            @endforeach
        <select> 
    </div>  
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Choisir le Regime:</label></strong>
        <select name="regime_id" id="regime_id" class="form-control" required>
            <option value="">choisir le Regime</option>
            @foreach($regime as $prof_regime)
            @if($prof_regime->id == $fraisInscription->regime_id)
                <option value="{{$prof_regime->id}}" selected><strong>{{$prof_regime->name}} - {{$prof_regime->status}}- {{$prof_regime->mensualite}}</strong> FCFA</option>
                @else
                <option value="{{$prof_regime->id}}"><strong>{{$prof_regime->name}} -{{$prof_regime->status}}- {{$prof_regime->mensualite}}</strong> FCFA</option>
                @endif
            @endforeach
        <select> 
    </div> 
@else
<div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
        <select name="annee_scolaire_id" id="annee_scolaire_id" class="form-control" required>
            <option value="">choisir Année scolaire</option>
            @foreach($annee_niveau as $prof)
            <option value="{{$prof->id}}">{{$prof->name}}</option>
            @endforeach
        <select> 
    </div>  
    <div class="form-group col-sm-6">
        <strong><label for="recipient-name" class="col-form-label">Choisir le Regime:</label></strong>
        <select name="regime_id" id="regime_id" class="form-control" required>
            <option value="">choisir le Regime</option>
            @foreach($regime as $prof_regime)
            <option value="{{$prof_regime->id}}"><strong>{{$prof_regime->name}} - {{$prof_regime->status}} - {{$prof_regime->mensualite}}</strong> FCFA</option>
            @endforeach
        <select> 
    </div> 
@endif
<!-- Montant Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montant', 'Montant:') !!}
    {!! Form::number('montant', null, ['class' => 'form-control']) !!}
</div>

<!-- Observation Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('observation', 'Observation:') !!}
    {!! Form::text('observation', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('fraisInscriptions.index') }}" class="btn btn-secondary">Annuler</a>
</div>
