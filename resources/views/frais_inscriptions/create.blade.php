@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('fraisInscriptions.index') !!}">Frais Inscription</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                @if(Session::has('fail'))
                    <div class="alert alert-danger">
                    {{Session::get('fail')}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Frais Inscription</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'fraisInscriptions.store']) !!}

                                   @include('frais_inscriptions.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
