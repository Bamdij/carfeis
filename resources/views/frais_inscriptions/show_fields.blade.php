<!-- Regime Id Field -->
<div class="form-group">
    {!! Form::label('regime_id', 'Regime Id:') !!}
    <p>{{ $fraisInscription->regime_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $fraisInscription->annee_scolaire_id }}</p>
</div>

<!-- Montant Field -->
<div class="form-group">
    {!! Form::label('montant', 'Montant:') !!}
    <p>{{ $fraisInscription->montant }}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{{ $fraisInscription->observation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $fraisInscription->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $fraisInscription->updated_at }}</p>
</div>

