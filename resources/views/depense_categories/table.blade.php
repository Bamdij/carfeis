@if(!$categories->isEmpty())
<style>
    .admin-dsb{
        color:#320c08!important;
        font-size: 18px;
        font-weight: 700;
    }
    .admin-dsb:hover{
        color:#fff!important;
        font-size: 18px;
        font-weight: 700;
    }
    .bg-white:hover{
        background-color:#87fb79!important
    }

</style>
<div class="container-fluid py-5">
    <div class="animated fadeIn">
            @include('flash::message')
        <div class="col-lg-12">
            <div class="row statistics px-md-2 mx-0">
                @foreach($categories as $cat)
                <div class="col-md-4 px-sm-2 mb-3">
                <a href="{{route('depense.category_depense',$cat->slug)}}" style="text-decoration : none !important" >

                    <div class="front-card pt-4 pb-2 px-sm-4 px-3 bg-white">
                        <div class="mb-4 mt-2 d-flex">
                            <i class="cil-window-maximize add-text text-primary "></i> &nbsp; 
                            <div class="ml-auto">
                                <p class="admin-dsb">{{$cat->name}}</p>
                                <h4 class="chiffre" style="color: #320c08;">{{$cat->count}}</h4>
                            </div>  
                        </div>
                    </div>
                    </a>

                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@else
<div class="alert alert-warning" role="alert">
  Aucune Depense Ajoutée
</div>
@endif