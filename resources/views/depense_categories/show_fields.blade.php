<!-- Category Depense Id Field -->
<div class="form-group">
    {!! Form::label('category_depense_id', 'Category Depense Id:') !!}
    <p>{{ $depenseCategorie->category_depense_id }}</p>
</div>

<!-- Date Depense Field -->
<div class="form-group">
    {!! Form::label('date_depense', 'Date Depense:') !!}
    <p>{{ $depenseCategorie->date_depense }}</p>
</div>

<!-- Designation Depense Field -->
<div class="form-group">
    {!! Form::label('designation_depense', 'Designation Depense:') !!}
    <p>{{ $depenseCategorie->designation_depense }}</p>
</div>

<!-- Montant Depense Field -->
<div class="form-group">
    {!! Form::label('montant_depense', 'Montant Depense:') !!}
    <p>{{ $depenseCategorie->montant_depense }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $depenseCategorie->user_id }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $depenseCategorie->description }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $depenseCategorie->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $depenseCategorie->updated_at }}</p>
</div>

