
@if(!$depenses->isEmpty())
    <div class="table-responsive-sm">
    <table class="table table-striped" id="depenseCategories-table">
        <thead>
            <tr>
        <th>Type de Depense</th>
        <th>Date Depense</th>
        <th>Designation Depense</th>
        <th>Montant Depense</th>
        <th>Personne Donateur</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($depenses as $depenseCategorie)
            <tr> 
            <td>{{$category->name}}</td>
            <td>{{Carbon\Carbon::parse( $depenseCategorie->date_depense)->format('d/m/Y')}}</td>
            <td>{{ $depenseCategorie->designation_depense }}</td>
            <td>{{ $depenseCategorie->montant_depense }} </td>
            <td class="badge badge-warning">{{$depenseCategorie->user->name }}</td>
                <td>
                    {!! Form::open(['route' => ['depenseCategories.destroy',$depenseCategorie->id], 'method' => 'delete']) !!}
                    <div class='bn-group'>
                       {{--  <a href="{{ route('depenseCategories.show', [$depenseCategorie->id]) }}" class='bn btn-ghost-success'><i class="fa fa-eye"></i></a> --}}
                        <a href="{{ route('depenseCategories.edit', [$depenseCategorie->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> 
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-warning" role="alert">
  Aucune Depense faite avec la category {{$category->name }}
</div>
@endif
