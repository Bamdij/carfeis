<!-- Date Depense Field -->
<div class="row">
<div class="form-group col-sm-6">
    <strong><label for="recipient-name" class="col-form-label">Année Scolaire:</label></strong>
    <select name="annee_scolaire_id" id="type_enseignant_id" class="form-control"  required>
        <option value="">choisir Année Scolaire</option>
        <option value="{{$annee_scolaire->id}}">{{$annee_scolaire->name}}</option>
    <select> 
</div>  
<div class="form-group col-sm-6">
    <strong><label for="recipient-name" class="col-form-label">Type Dépenses:</label></strong>
    <select name="category_depense_id" id="type_enseignant_id" class="form-control"  required>
        <option value="">choisir le type de Dépense</option>
        @foreach($categories as $prof)
        <option value="{{$prof->id}}">{{$prof->name}}</option>
        @endforeach
    <select> 
</div>  
<div class="form-group col-sm-6">
<strong> {!! Form::label('date_depense', 'Date Depense:') !!}</strong>
    {!! Form::date('date_depense', null, ['class' => 'form-control','required' => true ]) !!}
</div>

<!-- Designation Depense Field -->
<div class="form-group col-sm-6">
<strong>{!! Form::label('designation_depense', 'Designation Depense:') !!}</strong>
    {!! Form::text('designation_depense', null, ['class' => 'form-control','required' => true ]) !!}
</div>

<!-- Montant Depense Field -->
<div class="form-group col-sm-6">
<strong> {!! Form::label('montant_depense', 'Montant Depense:') !!}</strong>
    {!! Form::text('montant_depense', null, ['class' => 'form-control','required' => true ]) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('user_id', 'User Id:') !!}
    <input class="form-control" name="user_id" type="text"  value="{{Auth::user()->id}}" id="date_depense">
</div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Ajouter', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('depenseCategories.index') }}" class="btn btn-secondary">Annuler</a>
</div>
