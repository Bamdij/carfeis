@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Depense Categories</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                            
                                  <a href="{{ route('depenseCategories.index') }}" class="btn btn-light">Retour</a>
                             <a class="pull-right btn btn-light" href="{{ route('depenseCategories.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                            @include('depense_categories.table_new')
                              <div class="pull-right mr-3">   
                              <a href="" class="btn btn-light">Voir tout</a>

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

