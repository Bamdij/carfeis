<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $evaluationMensuel->eleve_id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $evaluationMensuel->user_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $evaluationMensuel->annee_scolaire_id }}</p>
</div>

<!-- Month Field -->
<div class="form-group">
    {!! Form::label('month', 'Month:') !!}
    <p>{{ $evaluationMensuel->month }}</p>
</div>

<!-- Date Evaluation Field -->
<div class="form-group">
    {!! Form::label('date_evaluation', 'Date Evaluation:') !!}
    <p>{{ $evaluationMensuel->date_evaluation }}</p>
</div>

<!-- Sourate Field -->
<div class="form-group">
    {!! Form::label('sourate', 'Sourate:') !!}
    <p>{{ $evaluationMensuel->sourate }}</p>
</div>

<!-- Last Lesson Field -->
<div class="form-group">
    {!! Form::label('last_lesson', 'Last Lesson:') !!}
    <p>{{ $evaluationMensuel->last_lesson }}</p>
</div>

<!-- First Party Field -->
<div class="form-group">
    {!! Form::label('first_party', 'First Party:') !!}
    <p>{{ $evaluationMensuel->first_party }}</p>
</div>

<!-- Appreciation First Party Field -->
<div class="form-group">
    {!! Form::label('appreciation_first_party', 'Appreciation First Party:') !!}
    <p>{{ $evaluationMensuel->appreciation_first_party }}</p>
</div>

<!-- Second Party Field -->
<div class="form-group">
    {!! Form::label('second_party', 'Second Party:') !!}
    <p>{{ $evaluationMensuel->second_party }}</p>
</div>

<!-- Appreciation Second Party Field -->
<div class="form-group">
    {!! Form::label('appreciation_second_party', 'Appreciation Second Party:') !!}
    <p>{{ $evaluationMensuel->appreciation_second_party }}</p>
</div>

<!-- Third Party Field -->
<div class="form-group">
    {!! Form::label('third_party', 'Third Party:') !!}
    <p>{{ $evaluationMensuel->third_party }}</p>
</div>

<!-- Appreciation Third Party Field -->
<div class="form-group">
    {!! Form::label('appreciation_third_party', 'Appreciation Third Party:') !!}
    <p>{{ $evaluationMensuel->appreciation_third_party }}</p>
</div>

<!-- Comportement Fr Field -->
<div class="form-group">
    {!! Form::label('comportement_fr', 'Comportement Fr:') !!}
    <p>{{ $evaluationMensuel->comportement_fr }}</p>
</div>

<!-- Comportement Ar Field -->
<div class="form-group">
    {!! Form::label('comportement_ar', 'Comportement Ar:') !!}
    <p>{{ $evaluationMensuel->comportement_ar }}</p>
</div>

<!-- Appreciation Field -->
<div class="form-group">
    {!! Form::label('appreciation', 'Appreciation:') !!}
    <p>{{ $evaluationMensuel->appreciation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $evaluationMensuel->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $evaluationMensuel->updated_at }}</p>
</div>

