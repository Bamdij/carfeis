<div class="table-responsive-sm">
    <table class="table table-striped" id="evaluationMensuels-table">
        <thead>
            <tr>
                <th>Eleve Id</th>
        <th>User Id</th>
        <th>Annee Scolaire Id</th>
        <th>Month</th>
        <th>Date Evaluation</th>
        <th>Sourate</th>
        <th>Last Lesson</th>
        <th>First Party</th>
        <th>Appreciation First Party</th>
        <th>Second Party</th>
        <th>Appreciation Second Party</th>
        <th>Third Party</th>
        <th>Appreciation Third Party</th>
        <th>Comportement Fr</th>
        <th>Comportement Ar</th>
        <th>Appreciation</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($evaluationMensuels as $evaluationMensuel)
            <tr>
                <td>{{ $evaluationMensuel->eleve_id }}</td>
            <td>{{ $evaluationMensuel->user_id }}</td>
            <td>{{ $evaluationMensuel->annee_scolaire_id }}</td>
            <td>{{ $evaluationMensuel->month }}</td>
            <td>{{ $evaluationMensuel->date_evaluation }}</td>
            <td>{{ $evaluationMensuel->sourate }}</td>
            <td>{{ $evaluationMensuel->last_lesson }}</td>
            <td>{{ $evaluationMensuel->first_party }}</td>
            <td>{{ $evaluationMensuel->appreciation_first_party }}</td>
            <td>{{ $evaluationMensuel->second_party }}</td>
            <td>{{ $evaluationMensuel->appreciation_second_party }}</td>
            <td>{{ $evaluationMensuel->third_party }}</td>
            <td>{{ $evaluationMensuel->appreciation_third_party }}</td>
            <td>{{ $evaluationMensuel->comportement_fr }}</td>
            <td>{{ $evaluationMensuel->comportement_ar }}</td>
            <td>{{ $evaluationMensuel->appreciation }}</td>
                <td>
                    {!! Form::open(['route' => ['evaluationMensuels.destroy', $evaluationMensuel->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('evaluationMensuels.show', [$evaluationMensuel->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('evaluationMensuels.edit', [$evaluationMensuel->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>