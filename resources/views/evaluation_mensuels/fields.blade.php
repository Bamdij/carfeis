<div class="row">
                <div class="form-group col-sm-4" style="display:none;">
                  <strong>{!! Form::label('user_id', 'User Id:') !!}</strong> 
                  <input class="form-control"  name="user_id" type="text" id="user_id" value="{{Auth::user()->id}}">
                  <input class="form-control"  name="eleve_id" type="text" id="eleve_id" value="{{$eleve->id}}">
                    <input class="form-control"  name="annee_scolaire_id" type="text" id="annee_scolaire_id" value="{{$annee_scolaire->id}}">
                </div>
                <!-- App Mond 1 Field -->
                <div class="form-group col-sm-4">
                   <strong>{!! Form::label('month', 'Mois Evaluation:') !!}</strong> 
                    <input class="form-control"  name="month" type="text" id="month"  value="{{ \Carbon\Carbon::now()->locale('fr_FR')->monthName }} ">
                </div>
                 <!-- Last Lesson Field -->
                 <div class="form-group col-sm-4">
                 <strong>{!! Form::label('date_evaluation', 'Date Evaluation:') !!}</strong>  
                    <input class="form-control"  name="date_evaluation" type="date" required id="date_evaluation" value="{{$evaluationMensuel->date_evaluation}}" >

                </div>
                <div class="form-group col-sm-4">
                 <strong>{!! Form::label('sourate', 'Sourate:') !!}</strong>   
                 @include('evaluation_semestres.sourate')
                </div>
                <!-- App Mond 2 Field -->
                <!-- Thuesday App Field -->
                <div class="form-group col-sm-6">
                  <strong>{!! Form::label('first_party', 'Note Premiere Partie:') !!}</strong>  
                    <input class="form-control"  name="first_party" type="number" id="first_party" value="{{$evaluationMensuel->first_party}}" >
                </div>
                <!-- App Mon 3 Field -->
                <div class="form-group col-sm-6">
                    <strong>{!! Form::label('appreciation_first_party', 'Appreciation Premiere Partie:') !!}</strong>  
                    <select name="appreciation_first_party" id="appreciation_first_party" class="form-control" required>
                        <option value="{{$evaluationMensuel->appreciation_first_party}}" selected>{{$evaluationMensuel->appreciation_first_party}}</option>
                        <option value="Tres Bien">Trés Bien</option>
                        <option value="Bien">Bien</option>
                        <option value="Assez Bien">Assez Bien</option>
                        <option value="Passable">Passable</option>
                        <option value="Mediocre">Médiocre</option>
                        <option value="Insuffisant">Insuffisant</option>
                    </select>
                </div>
                 <!-- Thuesday App Field -->
                 <div class="form-group col-sm-6">
                  <strong>{!! Form::label('second_party', 'Note Deuxieme Partie:') !!}</strong>  
                    <input class="form-control"  name="second_party" type="number" id="second_party" value="{{$evaluationMensuel->second_party}}" >
                </div>
                <!-- App Mon 3 Field -->
                <div class="form-group col-sm-6">
                    <strong>{!! Form::label('appreciation_second_party', 'Appreciation Deuxieme Partie:') !!}</strong>  
                    <select name="appreciation_second_party" id="appreciation_second_party" class="form-control" required>
                    <option value="{{$evaluationMensuel->appreciation_second_party}}" selected>{{$evaluationMensuel->appreciation_second_party}}</option>    
                        <option value="Tres Bien">Trés Bien</option>
                        <option value="Bien">Bien</option>
                        <option value="Assez Bien">Assez Bien</option>
                        <option value="Passable">Passable</option>
                        <option value="Mediocre">Médiocre</option>
                        <option value="insuffisant">Insuffisant</option>
                    </select>
                </div>
                <!-- Thuesday App Field -->
                <div class="form-group col-sm-6">
                  <strong>{!! Form::label('third_party', 'Note Troisieme Partie:') !!}</strong>  
                    <input class="form-control"  name="third_party" type="number" id="third_party"  value="{{$evaluationMensuel->third_party}}" >
                </div>
                <!-- App Mon 3 Field -->
                <div class="form-group col-sm-6">
                    <strong>{!! Form::label('appreciation_third_party', 'Appreciation Troisieme Partie:') !!}</strong>  
                    <select name="appreciation_third_party" id="appreciation_third_party" class="form-control" required>
                    <option value="{{$evaluationMensuel->appreciation_third_party}}" selected>{{$evaluationMensuel->appreciation_third_party}}</option>    
                        <option value="Tres Bien">Trés Bien</option>
                        <option value="Bien">Bien</option>
                        <option value="Assez Bien">Assez Bien</option>
                        <option value="Passable">Passable</option>
                        <option value="Mediocre">Médiocre</option>
                        <option value="Insuffisant">Insuffisant</option>
                    </select>
                </div>
                 <!-- Last Lesson Field -->
                 <div class="form-group col-sm-6">
                   <strong>{!! Form::label('last_lesson', 'Derniere leçon:') !!}</strong> 
                   @include('evaluation_semestres.sourate')
                </div>
                 <!-- Thuesday App Field -->
                 <div class="form-group col-sm-6">
                  <strong>{!! Form::label('comportement_fr', 'Comportement:') !!}</strong>  
                    <input class="form-control"  name="comportement_fr" type="text" id="comportement_fr" value="{{$evaluationMensuel->comportement_fr}}" >
                </div>
                <div class="form-group col-sm-6">
                  <strong>{!! Form::label('appreciation', 'Appreciation Du Professeur:') !!}</strong>  
                  <select name="appreciation" id="appreciation" class="form-control" required>
                  <option value="{{$evaluationMensuel->appreciation}}" selected>{{$evaluationMensuel->appreciation}}</option>    
                        <option value="Bon">Trés Bon</option>
                        <option value="Absence">Absence</option>
                        <option value="Mauvaise Memoire">Mauvaise Memoire</option>
                        <option value="Refus apprendre">Refus d'apprendre</option>
                        <option value="malade">Malade</option>
                        <option value="Rendez-vous">Rendez-vous</option>
                    </select>
                </div>
               

                </div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('evaluation_mensuel_detail.eleve',$eleve->slug) }}" class="btn btn-secondary">Annuler</a>
</div>
