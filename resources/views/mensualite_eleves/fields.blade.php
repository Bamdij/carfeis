<!-- Paiement Eleve Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paiement_eleve_id', 'Paiement Eleve Id:') !!}
    {!! Form::text('paiement_eleve_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Eleve Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    {!! Form::text('eleve_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    {!! Form::text('annee_scolaire_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('month', 'Month:') !!}
    {!! Form::text('month', null, ['class' => 'form-control']) !!}
</div>

<!-- Mensualite Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mensualite', 'Mensualite:') !!}
    {!! Form::text('mensualite', null, ['class' => 'form-control']) !!}
</div>

<!-- Observation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observation', 'Observation:') !!}
    {!! Form::text('observation', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('mensualiteEleves.index') }}" class="btn btn-secondary">Cancel</a>
</div>
