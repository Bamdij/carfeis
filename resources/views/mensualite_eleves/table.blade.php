<div class="table-responsive-sm">
    <table class="table table-striped" id="mensualiteEleves-table">
        <thead>
            <tr>
                <th>Paiement Eleve Id</th>
        <th>Eleve Id</th>
        <th>Annee Scolaire Id</th>
        <th>Month</th>
        <th>Mensualite</th>
        <th>Observation</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($mensualiteEleves as $mensualiteEleve)
            <tr>
                <td>{{ $mensualiteEleve->paiement_eleve_id }}</td>
            <td>{{ $mensualiteEleve->eleve_id }}</td>
            <td>{{ $mensualiteEleve->annee_scolaire_id }}</td>
            <td>{{ $mensualiteEleve->month }}</td>
            <td>{{ $mensualiteEleve->mensualite }}</td>
            <td>{{ $mensualiteEleve->observation }}</td>
                <td>
                    {!! Form::open(['route' => ['mensualiteEleves.destroy', $mensualiteEleve->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('mensualiteEleves.show', [$mensualiteEleve->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('mensualiteEleves.edit', [$mensualiteEleve->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>