<!-- Paiement Eleve Id Field -->
<div class="form-group">
    {!! Form::label('paiement_eleve_id', 'Paiement Eleve Id:') !!}
    <p>{{ $mensualiteEleve->paiement_eleve_id }}</p>
</div>

<!-- Eleve Id Field -->
<div class="form-group">
    {!! Form::label('eleve_id', 'Eleve Id:') !!}
    <p>{{ $mensualiteEleve->eleve_id }}</p>
</div>

<!-- Annee Scolaire Id Field -->
<div class="form-group">
    {!! Form::label('annee_scolaire_id', 'Annee Scolaire Id:') !!}
    <p>{{ $mensualiteEleve->annee_scolaire_id }}</p>
</div>

<!-- Month Field -->
<div class="form-group">
    {!! Form::label('month', 'Month:') !!}
    <p>{{ $mensualiteEleve->month }}</p>
</div>

<!-- Mensualite Field -->
<div class="form-group">
    {!! Form::label('mensualite', 'Mensualite:') !!}
    <p>{{ $mensualiteEleve->mensualite }}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{{ $mensualiteEleve->observation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $mensualiteEleve->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $mensualiteEleve->updated_at }}</p>
</div>

