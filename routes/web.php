<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EleveController;
use App\Http\Controllers\PaydunyaCarfeisController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/connexion', [App\Http\Controllers\UserController::class, 'loginAdmin'])->name('user.connexion');
Route::resource('/user', UserController::class);


Route::resource('typeProfessions', App\Http\Controllers\TypeProfessionController::class);


Route::resource('lienParents', App\Http\Controllers\LienParentController::class);


Route::resource('typeEnseignants', App\Http\Controllers\TypeEnseignantController::class);


Route::resource('type_paiements', App\Http\Controllers\TypePaiementController::class);
Route::post('/add/enseignant/class/{id}',[App\Http\Controllers\EnseignantController::class,'AddEnseignantOnly']);
Route::post('update/{id}',[App\Http\Controllers\UserController::class,'UpdateStatusUser']);
Route::resource('categoryDepenses', App\Http\Controllers\CategoryDepenseController::class);
Route::resource('niveaux', App\Http\Controllers\NiveauController::class);
Route::resource('sousNiveaus', App\Http\Controllers\SousNiveauController::class);
Route::get('status/{id}',[App\Http\Controllers\UserController::class,'getmodalstatus']);
Route::resource('eleves', App\Http\Controllers\EleveController::class);
Route::resource('infoParents', App\Http\Controllers\InfoParentController::class);
Route::resource('alphabetisations', App\Http\Controllers\AlphabetisationController::class);
Route::resource('enseignants', App\Http\Controllers\EnseignantController::class);
Route::get('eleve_classe/{id}',[App\Http\Controllers\EleveController::class,'getEleveAddClass'])->name('getclasse.classe');
Route::get('getclasse/{id}',[App\Http\Controllers\EleveController::class,'getClasseByNiveaau']);
Route::get('add_classe/{id}',[App\Http\Controllers\EleveController::class,'getClasseByNiveaaAdd']);
Route::post('add_eleveclass/{id}',[App\Http\Controllers\EleveController::class,'PostClasseEleve'])->name('eleve.classe');
Route::post('add_eleveclass_withprof/{id}',[App\Http\Controllers\EleveController::class,'PostClasseEleveWithProf']);
Route::resource('eleveClasses', App\Http\Controllers\EleveClasseController::class);
Route::get('getclasseteacher/{id}',[App\Http\Controllers\EnseignantController::class,'getNiveauEnseignantByTypeEnseignant']);
Route::get('getclasseteacher_eleve/{id}',[App\Http\Controllers\EnseignantController::class,'getNiveauEnseignantByClasseEleve']);
Route::get('Addclasseteacher/{id}',[App\Http\Controllers\EnseignantController::class,'AddClassToEnseignant']);
Route::get('edit/enseig/{id}',[App\Http\Controllers\EnseignantController::class,'editClassePro']);
Route::get('edit/enseignant_add_class/{id}',[App\Http\Controllers\EnseignantController::class,'editClasseProfesseurClasse']);
Route::get('getniveau/enseignant/{id}',[App\Http\Controllers\EnseignantController::class,'EditClasseProfesseur']);
Route::get('enseignant_prof_classe/{id}',[App\Http\Controllers\EnseignantController::class,'EditClasseProf']);
Route::post('/update/enseignant/class/{id}',[App\Http\Controllers\EnseignantController::class,'updateClasseEnseignant']);
Route::resource('enseignantClasses', App\Http\Controllers\EnseignantClasseController::class);
Route::delete('/delete/enseignant/class/{id}',[App\Http\Controllers\EnseignantController::class,'deleteSousniveau'])->name('delete.classe');
Route::get('get_eleve_by_class',[App\Http\Controllers\EnseignantController::class,'getClassByEleve'])->name('get_eleve_by_class.classe');
Route::get('getniveau_class/{id}',[App\Http\Controllers\EnseignantController::class,'getClasseByEleve']);
Route::get('get_class_enseig/{id}',[App\Http\Controllers\EnseignantController::class,'getClasseByNiveauEleve']);
Route::get('get_class_all_eleve/{id}',[App\Http\Controllers\EnseignantController::class,'getEleveByClasse']);
Route::get('get_details_eleve/{id}',[App\Http\Controllers\EnseignantController::class,'showdetailseleve'])->name('details_eleve.classe');
Route::get('editclass_elev/{id}',[App\Http\Controllers\EnseignantController::class,'changeEleveClasse'])->name('edit_class.eleve');
Route::get('edit/classe_el/{id}',[App\Http\Controllers\EnseignantController::class,'EditClasseEl']);
Route::get('choice_classe/{id}',[App\Http\Controllers\EnseignantController::class,'getNiveaEleveAdd']);
Route::post('/update/student/class/{id}',[App\Http\Controllers\EnseignantController::class,'updateClasseStudent']);
Route::resource('anneeScolaires', App\Http\Controllers\AnneeScolaireController::class);
Route::get('get_enseignant_on_class', [App\Http\Controllers\EnseignantController::class,'getClasseByTeacher'])->name('get_enseignant_class.eleve');
Route::get('get_enseignantafter_class/{id}', [App\Http\Controllers\EnseignantController::class,'getEnseignantByTypeEnseignement']);
Route::get('get_classe_enseignantafter_class/{id}', [App\Http\Controllers\EnseignantController::class,'getEnseignantByClasseEnseign']);
Route::get('get_classe_eneseignant_eleve/{id}', [App\Http\Controllers\EnseignantController::class,'getEleveByClasseAndEnseignant']);
Route::get('carte_student/{slug}',[App\Http\Controllers\EleveController::class,'getStudentCard'])->name('carte.eleve');
Route::get('carte_all_years/{id}',[App\Http\Controllers\EleveController::class,'getCarteStudentPrinting'])->name('carte.year');

Route::get('get_eleve_to_paiement/{slug}',[App\Http\Controllers\EleveController::class,'getEleveToAddInscription']);
Route::get('get_eleve_on_paiment_by_year',[App\Http\Controllers\EleveController::class,'getPaiementEleveByYear'])->name('paiment.eleve');
Route::post('eleve/new_classes',[App\Http\Controllers\EleveController::class,'getEleveWithNumero'])->name('elevegetting.classe');
Route::post('add_eleve_classe_continuons/{id}',[App\Http\Controllers\EleveController::class,'AddClasseEleveContinuons']);
Route::post('add_eleveclass_with_prof_arab/{id}',[App\Http\Controllers\EleveController::class,'AddClasseEleveContinuonsArabe']);
Route::get('eleve/inscription_new',[App\Http\Controllers\EleveController::class,'getFormtoInscrire'])->name('eleve.new_inscription');
Route::get('eleve/reinscription_new',[App\Http\Controllers\EleveController::class,'getFormtoReInscrire'])->name('eleve.new_reinscription');
Route::resource('inscriptionEleves', App\Http\Controllers\InscriptionEleveController::class);
Route::resource('inscriptionEleves', App\Http\Controllers\InscriptionEleveController::class);
Route::resource('regimes', App\Http\Controllers\RegimeController::class);
Route::resource('fraisInscriptions', App\Http\Controllers\FraisInscriptionController::class);
Route::resource('paiementEleves', App\Http\Controllers\PaiementEleveController::class);
Route::get('get_elevt_to_paie_by_momth/{id}',[App\Http\Controllers\EleveController::class,'getEleveToAddPaiement']);
Route::post('get_elev_paiement',[App\Http\Controllers\EleveController::class,'SearchingElevePaiement'])->name('get_eleve_to_paiement.annee_scolaire');
Route::get('recu/paiement/{slug}',[App\Http\Controllers\PaiementEleveController::class,'getPrinRecu'])->name('get_recu.annee_scolaire');
Route::get('recu/paiement_mensual/{slug}',[App\Http\Controllers\PaiementEleveController::class,'getPaiementMonthRecu'])->name('get_recu_mensual.annee_scolaire');
Route::post('change_status_confime/{id}',[App\Http\Controllers\EleveController::class,'ChangeStatusPaiement']);
Route::resource('enseignantAnnees', App\Http\Controllers\EnseignantAnneeController::class);
Route::post('paiement_mensual/{id}',[App\Http\Controllers\EleveController::class,'PaiementMonthPosted'])->name('paiement.mensual');
Route::resource('mensualiteEleves', App\Http\Controllers\MensualiteEleveController::class);
Route::get('infos_after_paiement/{slug}',[App\Http\Controllers\EleveController::class,'getInfoPaiementMonth'])->name('infor_paiement.mensual');
Route::post('eleve/getting_years',[App\Http\Controllers\EleveController::class,'getEleveNumeroPost'])->name('getting_eleves.classe');
Route::get('/depense_by_category/{slug}',[App\Http\Controllers\DepenseCategorieController::class,'getDeCategory'])->name('depense.category_depense');
Route::get('paiment_enseignant/{slug}',[App\Http\Controllers\MensuualiteEnseignantController::class,'getInfoPaiementEnseignant'])->name('enseignant.paiement');
Route::get('recu_paiment_enseignant/{slug}',[App\Http\Controllers\MensuualiteEnseignantController::class,'getRecuPaiement'])->name('enseignant.recu_paiement');
Route::resource('depenseCategories', App\Http\Controllers\DepenseCategorieController::class);
Route::get('edit_profil/{slug}',[App\Http\Controllers\UserController::class,'ChangeInfoProfile'])->name('edit.profil');
Route::post('edit_profil_user',[App\Http\Controllers\UserController::class,'UpdateProfileAdmin'])->name('edit.profil_user');
Route::post('edit_password_user',[App\Http\Controllers\UserController::class,'ChangePassword'])->name('password.profil_user');
Route::post('changing_password_user/{id}',[App\Http\Controllers\UserController::class,'ChangePasswordUser'])->name('password.changing');
Route::get('get-list-paiement-status',[App\Http\Controllers\PaiementEleveController::class,'getListePaiementStudent'])->name('get_list_status.annuelle');
Route::post('get_list_par_moi',[App\Http\Controllers\PaiementEleveController::class,'getStudentPaiementDayByyear'])->name('get_list_par_moi.annuelle');
Route::get('get-list-depenses-status',[App\Http\Controllers\PaiementEleveController::class,'getListeDepensesStatus'])->name('get_list_status_depenses.annuelle');
Route::post('get_list_depenses_moi',[App\Http\Controllers\PaiementEleveController::class,'getlistofdepensebydate'])->name('get_list_depense_moi.annuelle');
// Route::get('tasks',[App\Http\Controllers\PaiementEleveController::class,'exportCsv']);

Route::resource('mensuualiteEnseignants', App\Http\Controllers\MensuualiteEnseignantController::class);
Route::resource('evaluationSemestres', App\Http\Controllers\EvaluationSemestreController::class);
Route::get('index_eleves',[App\Http\Controllers\EvaluationSemestreController::class,'indexinfo'])->name('getinfos.eleve');
Route::get('get_eleve_evaluation/{slug}',[App\Http\Controllers\EvaluationSemestreController::class,'GetEvaluationEleve'])->name('evaluation_detail.eleve');
Route::get('get_evaluation/{id}',[App\Http\Controllers\EvaluationSemestreController::class,'getEleveToEvaluated'])->name('evaluation.eleve');

Route::resource('evaluationMensuels', App\Http\Controllers\EvaluationMensuelController::class);
Route::get('get_mensuel_evalution/{slug}',[App\Http\Controllers\EvaluationSemestreController::class,'getEleveToEvaluatedMensuel'])->name('evaluation_mensuel_detail.eleve');
Route::get('get_evaluer_mont/{id}',[App\Http\Controllers\EvaluationSemestreController::class,'getEleveToEvaluerModalMensuel']);
Route::get('create/parent',[App\Http\Controllers\UserController::class,'GetTocreateParent'])->name('create_eleve.parent');
Route::post('parent/user',[App\Http\Controllers\UserController::class,'CreateParent'])->name('create_parent.post');
Route::patch('parent/user/{id}',[App\Http\Controllers\UserController::class,'updateParent'])->name('create_parent.update');
Route::resource('sourates', App\Http\Controllers\SourateController::class);
Route::get('dashboard/parent',[App\Http\Controllers\UserController::class,'dashboardParent'])->name('dashboard.parent');
Route::get('get_Eleve_by_parent',[App\Http\Controllers\ParentsController::class,'GetParentEleve'])->name('eleve_parent.parent');
Route::get('get_Eleve_by_parent/{slug}',[App\Http\Controllers\ParentsController::class,'GetInfoEleveByParent'])->name('eleve_parent_slug.parent');
Route::get('get_Eleve_by_parent_evalutation_mensuel',[App\Http\Controllers\ParentsController::class,'GetParentEleveMensuel'])->name('eleve_parent_evaaluation_mensuel.parent');
Route::get('get_Eleve_by_parent_evalutation_hebdomadaire',[App\Http\Controllers\ParentsController::class,'GetParentEleveHebdomadaire'])->name('eleve_parent_evaaluation_hebdomadaire.parent');
Route::get('get_Eleve_by_parent_paiement',[App\Http\Controllers\ParentsController::class,'GetParentElevePaiement'])->name('eleve_parent_paiement.parent');


Route::get('suivie_evalution_Eleve_by_parent/{slug}',[App\Http\Controllers\ParentsController::class,'GetEValuationSemestreByParent'])->name('suivie_evaluation.parent');
Route::get('suivie_evalution_mensuel_Eleve_by_parent/{slug}',[App\Http\Controllers\ParentsController::class,'GetEvaluationMensuelEleveByParent'])->name('suivie_evaluation_mensuel.parent');


Route::resource('parents', App\Http\Controllers\ParentsController::class);

Route::get('eleve/paiement/all',[App\Http\Controllers\EleveController::class,'getEleveAllByPaiement'])->name('eleve.get_all_paiement');
Route::get('eleve/paiement/{query}/',[App\Http\Controllers\EleveController::class,'getListOfEleveNonPaiement'])->name('eleve.get_paiement_non_paied');

Route::resource('messageContacts', App\Http\Controllers\MessageContactController::class);


Route::resource('aproposCarfeis', App\Http\Controllers\AproposCarfeisController::class);

//Route pour Paydunya

Route::get('paydunya-payment-form/{slug}',[App\Http\Controllers\PaydunyaCarfeisController::class,'getPaydunyaPaymentView'])->name('paydunya.payment.form');
Route::post('larapaydunya-sending-payment-infos',[App\Http\Controllers\PaydunyaCarfeisController::class,'postPayDunyaPaymentInfos'])->name('sending.payment.infos');
Route::post('payment-form',[App\Http\Controllers\PaydunyaCarfeisController::class,'getPaydunyaPaymentView'])->name('payment.form');
Route::post('payment-process',[App\Http\Controllers\PaydunyaCarfeisController::class,'postPaydunyaPaymentProcess'])->name('paydunya.payment.process');
Route::get('return/paydunya-payment-status',[App\Http\Controllers\PaydunyaCarfeisController::class,'paydunyaPaymentStatus'])->name('paydunya.payment.status');

Route::post('paiement_mensual_paydunya/{id}',[App\Http\Controllers\EleveController::class,'PaiementMonthPostedPaydunua'])->name('paiement.mensual_paydunya');

Route::get('infos_after_paiement_parent/{slug}',[App\Http\Controllers\ParentsController::class,'getInfoPaiementMonthByparent'])->name('infor_paiement_parent.mensual');
Route::post('update_paiement_parent/{id}',[App\Http\Controllers\ParentsController::class,'UpdateMonthPaiementByParent'])->name('update_paiement_parent.mensual');
Route::get('get_form_paiement_parent/{query}/{slug}/{name_status}',[App\Http\Controllers\ParentsController::class,'GetEleveMontPaiement']);
Route::get('get_change_paiement_parent/{name_status}/{id}',[App\Http\Controllers\ParentsController::class,'changeStatusMonth']);
Route::post('paiement_paydunya_parent/{id}',[App\Http\Controllers\ParentsController::class,'PaiementMonthPostedPaydunuaParent']);
Route::get('get_paiement_paydunya_parent/{payed_month}/{status_payed}/{paiemet_id}/{mensualite_id}',[App\Http\Controllers\PaydunyaCarfeisController::class,'getPaydunyaParent']);
Route::post('paiement-sending-payment-infos',[App\Http\Controllers\PaydunyaCarfeisController::class,'postPayDunyaPaymentInfosParents'])->name('sending.payment.infos.parent');
Route::post('payment-process_parent',[App\Http\Controllers\PaydunyaCarfeisController::class,'postPaydunyaPaymentProcess'])->name('paydunya.payment.process_parent');
Route::get('return/paydunya-payment-status_parent',[App\Http\Controllers\PaydunyaCarfeisController::class,'paydunyaPaymentStatusParent'])->name('paydunya.payment.status_parent');
Route::delete('delete/parent/mensualite/{id}',[App\Http\Controllers\PaydunyaCarfeisController::class,'deleteMensualiteaftercancel'])->name('delete.status_parent');
Route::delete('delete/mensualitebyadmin/{id}',[App\Http\Controllers\PaydunyaCarfeisController::class,'deleteMensualitebYAdmin'])->name('delete.status_admin');

// Route::resource('parentPaiementAnnuelles', App\Http\Controllers\ParentPaiementAnnuelleController::class);
