<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('typeProfessions', 'TypeProfessionAPIController');


Route::resource('lien_parents', 'LienParentAPIController');


Route::resource('type_enseignants', 'TypeEnseignantAPIController');


Route::resource('type_paiements', 'TypePaiementAPIController');


Route::resource('category_depenses', 'CategoryDepenseAPIController');


Route::resource('niveaux', 'NiveauAPIController');


Route::resource('sous_niveaus','SousNiveauAPIController');


Route::resource('eleves','EleveAPIController');


Route::resource('info_parents','InfoParentAPIController');


Route::resource('alphabetisations','AlphabetisationAPIController');




Route::resource('enseignants', 'EnseignantAPIController');


Route::resource('eleveClasses', 'EleveClasseAPIController');


Route::resource('enseignant_classes', 'EnseignantClasseAPIController');


Route::resource('anneeScolaires', 'AnneeScolaireAPIController'::class);


Route::resource('inscriptionEleves', 'InscriptionEleveAPIController');




Route::resource('regimes', 'RegimeAPIController');


Route::resource('fraisInscriptions', 'FraisInscriptionAPIController');


Route::resource('paiementEleves', 'PaiementEleveAPIController');


Route::resource('enseignant_annees', 'EnseignantAnneeAPIController');


Route::resource('mensualite_eleves', 'MensualiteEleveAPIController');


Route::resource('depense_categories', 'DepenseCategorieAPIController');


Route::resource('mensuualite_enseignants','MensuualiteEnseignantAPIController');




Route::resource('evaluation_semestres','EvaluationSemestreAPIController');


Route::resource('evaluation_mensuels', 'EvaluationMensuelAPIController');


Route::resource('sourates','SourateAPIController');


Route::resource('parents', 'ParentsAPIController');




Route::resource('message_contacts', App\Http\Controllers\API\MessageContactAPIController::class);


Route::resource('apropos_carfeis', App\Http\Controllers\API\AproposCarfeisAPIController::class);
